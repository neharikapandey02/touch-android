package com.example.touch.RetrofitApiCalling;

import com.example.touch.adapters.PLaceIdModel;
import com.example.touch.models.AnotherUserMain;
import com.example.touch.models.AnotherUserProfileIdByKeySuccess;
import com.example.touch.models.BlockListData;
import com.example.touch.models.BlockUserResponse;
import com.example.touch.models.CategoryMain;
import com.example.touch.models.ChangePasswordMain;
import com.example.touch.models.ChatFromUserMain;
import com.example.touch.models.ChatListMain;
import com.example.touch.models.ChatMain;
import com.example.touch.models.CommentedMain;
import com.example.touch.models.CommentsMain;
import com.example.touch.models.CountListFollowIngPost;
import com.example.touch.models.DeleteAudioResponse;
import com.example.touch.models.DeleteMain;
import com.example.touch.models.DisplayPhoneSuccess;
import com.example.touch.models.FollowUnfollowCheckMain;
import com.example.touch.models.FollowUnfollowMain;
import com.example.touch.models.FollowersListMain;
import com.example.touch.models.FollowingMain;
import com.example.touch.models.ForgotPasswordMain;
import com.example.touch.models.GetAnotherUserProfileData;
import com.example.touch.models.GetTagIdSuccess;
import com.example.touch.models.HastagMain;
import com.example.touch.models.HomeMain;
import com.example.touch.models.ImageListTagAndName;
import com.example.touch.models.LatestPostResponse;
import com.example.touch.models.LikeCommentFollowMain;
import com.example.touch.models.LikesMain;
import com.example.touch.models.LocationSearchMain;
import com.example.touch.models.LoginMain;
import com.example.touch.models.MultipleImageGetMain;
import com.example.touch.models.MultipleImageMain;
import com.example.touch.models.PackageMainData;
import com.example.touch.models.PaymentDoneMainData;
import com.example.touch.models.PostUploadCountResponse;
import com.example.touch.models.ProfileLikeSuccess;
import com.example.touch.models.ProfileVisibleResponse;
import com.example.touch.models.ReadChatCheckMain;
import com.example.touch.models.ReadChatMain;
import com.example.touch.models.ReadNotificationMain;
import com.example.touch.models.SearchDetailsMain;
import com.example.touch.models.SearchMain;
import com.example.touch.models.SearchUserListData;
import com.example.touch.models.SessionResponse;
import com.example.touch.models.SignUpMain;
import com.example.touch.models.StatusMain;
import com.example.touch.models.UpdatePhoneSuccess;
import com.example.touch.models.UpdatePriceMain;
import com.example.touch.models.UpdateProfileMain;
import com.example.touch.models.UserLikeMain;
import com.example.touch.models.UserProfileStatus;
import com.example.touch.models.ValidateEmailMain;
import com.google.gson.annotations.Expose;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitApi {
    /*@POST("/webservice.php")
    Call<ForgotPasswordMain> doForgotPassword(@Body KeysModel loginRequest);*/



    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ForgotPasswordMain> doForgotPassword(@Field("forgotpassword") String forgotPassword,
                                              @Field("email") String email);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<UpdatePriceMain> doUpdatePrice(@Field("update_price") String update_price,
                                        @Field("user_id") String user_id,
                                        @Field("price") String price);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<FollowersListMain> doFollowersList(@Field("following_list_home") String following_list,
                                            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<FollowingMain> doFollowersList1(@Field("followers_list") String followers_list,
                                        @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<FollowingMain> doFollowingList1(@Field("following_list") String followers_list,
                                        @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<HomeMain> doProfileList(@Field("latest_post_new") String latest_post,
                                 @Field("user_id") String user_id,
                                 @Field("limit") String limit,
                                 @Field("page") int page);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<HomeMain> getAllSearch(@Field("all_search") String latest_post,
                                 @Field("limit") String limit,
                                 @Field("page") int page);
    @FormUrlEncoded
    @POST("/webservice.php")
    Call<HomeMain> getNearMe(@Field("search_filter") String search_filter,
                             @Field("lat") String lat,
                             @Field("lang") String lng);
    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ReadChatCheckMain> doStatusOfChat(@Field("check_chat_status") String check_chat_status,
                                           @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("/webservice.php")

    Call<HomeMain> doProfilePostList(@Field("profile_post") String latest_post,
                                 @Field("user_id") String user_id,
                                 @Field("sender") String sender);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ChatMain> doChatList(@Field("personal_chat_list") String personal_chat_list,
                              @Field("sender_id") String sender_id,
                              @Field("reciver_id") String reciver_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ReadChatMain> doReadChat(@Field("read_chat") String read_chat,
                                  @Field("user_id") String user_id,
                                  @Field("sender_id") String senderId);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ChatListMain> doChatListTotal(@Field("chat_list") String chat_list,
                                       @Field("user_id") String user_id);


    @POST("/webservice.php")
    Call<ChatFromUserMain> doChatFromUser(@Body RequestBody requestBody);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<UserLikeMain> doLikeList(@Field("user_like_list") String chat,
                                  @Field("post_id") String sender_id);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<SearchDetailsMain> doDetailSearchData(@Field("fetch_all_user_image") String fetch_all_user_image,
                                               @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<LikesMain> doLikeProfileImage(@Field("like_list") String like_list,
                                       @Field("user_id") String user_id,
                                       @Field("post_id") String post_id,
                                       @Field("status") String status);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<CommentsMain> doCommentList(@Field("fetch_all_comment") String like_list,
                                     @Field("post_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<LoginMain> doLogin(@Field("login") String login,
                            @Field("email") String email,
                            @Field("password") String password,
                            @Field("device_id") String device_id,
                            @Field("device_type") String device_type,
                            @Field("user_lat") String user_lat,
                            @Field("user_lng") String user_lang);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<CommentedMain> doCommentOnPost(@Field("multiple_comment") String multiple_comment,
                                        @Field("user_id") String user_id,
                                        @Field("comment_id") String comment_id,
                                        @Field("comment") String comment,
                                        @Field("post_id") String post_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<MultipleImageGetMain> doMultipleImageGet(@Field("multiple_image_list") String login,
                                                  @Field("user_id") String email,
                                                  @Field("limit") String limit,
                                                  @Field("page") String page);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<DeleteMain> doDeleteImages(@Field("delete_profile_image") String login,
                                    @Field("key") String email);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<SignUpMain> doSignUp(@Field("registration") String login,
                              @Field("device_type") String device_type,
                              @Field("first_name") String first_name,
                              @Field("last_name") String email,
                              @Field("email") String password,
                              @Field("password") String device_id,
                              @Field("phone_number") String phone_number,
                              @Field("dob") String dob,
                              @Field("gender") String gender,
                              @Field("user_type") String user_type,
                              @Field("category") String category,
                              @Field("about_us") String about_us,
                              @Field("hair_color") String hair_color,
                              @Field("weight") String weight,
                              @Field("height") String height,
                              @Field("brestsize") String brestsize,
                              @Field("eye_color") String eye_color,
                              @Field("hobby") String hobby,
                              @Field("available_from") String available_from,
                              @Field("ethnicity") String ethnicity,
                              @Field("origin") String origin,
                              @Field("country") String country,
                              @Field("city") String city,
                              @Field("language") String language,
                              @Field("service_type") String incall,
                              @Field("website") String website);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<SearchMain> doSearchData(@Field("search_filter") String search_filter,
                                  @Field("key") String key,
                                  @Field("lat") String lat,
                                  @Field("lang") String lang,
                                  @Field("category_name") String category_name);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<SearchMain> doSearchDataByKey(
            @Field("tab_search") String tab_search,
            @Field("key") String key,
            @Field("tab_search_key") String tab_search_key,
            @Field("user_id") String userId);

@FormUrlEncoded
    @POST("/webservice.php")
    Call<SearchMain> doSearchDataByKey1(@Field("tab_search") String tab_search,
                                  @Field("key") String key,
                                  @Field("tab_search_key") String tab_search_key,
                                        @Field("latitude") String lat,
                                        @Field("longitude") String lng,
                                        @Field("current_user") String userId);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ImageListTagAndName> doListImageTagWithAddress(@Field("tag_user_list") String tab_search,
                                                        @Field("tag_id") String tag_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<CategoryMain> doCategoryList(@Field("category_list") String category_list);




    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ChangePasswordMain> doChangePassword(@Field("user_change_password") String user_change_password,
                                              @Field("id") String first_name,
                                              @Field("password") String email,
                                              @Field("npass") String password);


    /*@FormUrlEncoded
    @POST("/webservice.php")
    Call<LocationSearchMain> doLocationSearch(@Field("location_user_list") String user_change_password,
                                              @Field("key") String first_name);*/

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<LocationSearchMain> doLocationSearch(@Field("location_user_list") String location_user_list,
                                              @Field("latitude") String latitude,
                                              @Field("longitude") String longitude,
                                              @Field("key") String key,
                                              @Field("place_id") String placeId,
                                              @Field("country_code") String countryCode,
                                              @Field("types") String countryTypes);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ValidateEmailMain> doEmailvalidate(@Field("email_validation") String email_validation,
                                            @Field("email") String email,
                                            @Field("user_name") String user_name);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<CountListFollowIngPost> doFollowingFollowCount(@Field("count_api") String user_change_password,
                                                        @Field("user_id") String first_name);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<FollowUnfollowCheckMain> doFollowUnfollowCheck(@Field("follow_unfollow_status") String follow_unfollow_status,
                                                        @Field("followed_by") String followed_by,
                                                        @Field("following_to") String following_to);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<AnotherUserMain> doAnotherUserProfile(@Field("get_another_user_profile") String get_another_user_profile,
                                               @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<GetAnotherUserProfileData> getAnotherUserProfile(@Field("get_another_user_profile") String get_another_user_profile,
                                                          @Field("user_id") String user_id,
                                                          @Field("page") String page,
                                                          @Field("limit") String limit,
                                                          @Field("another_user_id") String another_user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<PackageMainData> doPackageInfo(@Field("check_package") String check_package,
                                        @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<FollowingMain> doFollowingList(@Field("near_by") String near_by,
                                        @Field("lat") String lat,
                                        @Field("lng") String lng,
                                        @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<PaymentDoneMainData> doPaymentDone(@Field("stripe_payment_gateway") String stripe_payment_gateway,
                                            @Field("user_id") String user_id,
                                            @Field("stripetoken") String stripetoken,
                                            @Field("amount") String amount,
                                            @Field("booking_id") String booking_id,
                                            @Field("package_id") String package_id,
                                            @Field("discription") String discription);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<HastagMain> doSearchTag(@Field("tag_list") String tag_list,
                                 @Field("key") String key);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<SearchUserListData> doSearchUser(@Field("search_user_list") String tag_list,
                                          @Field("key") String key);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ReadNotificationMain> doStatusTrue(@Field("read_activity") String near_by,
                                               @Field("user_id") String user_id);
     @FormUrlEncoded
    @POST("/webservice.php")
    Call<LikeCommentFollowMain> doFollowingLikeList(@Field("activity") String activity,
                                                    @Field("user_id") String user_id,
                                                    @Field("limit") String limit,
                                                    @Field("page") int page);


     @FormUrlEncoded
    @POST("/webservice.php")
    Call<StatusMain> doStatus(@Field("check_activity_status") String activity,
                              @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<FollowUnfollowMain> doFollowUnfollow(@Field("follow_unfollow") String follow_unfollow,
                                              @Field("followed_by") String followed_by,
                                              @Field("following_to") String following_to,
                                              @Field("status") String status);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<LatestPostResponse> getLatestPost(@Field("latest_post") String latestPost,
                                           @Field("user_id") String userId);

    @Multipart
    @POST("/webservice.php")
    Call<UpdateProfileMain> doUserUpdate(@Part MultipartBody.Part audio1,
                                         @Part MultipartBody.Part image1,
                                         @Part("edit_profile") RequestBody edit_profile,
                                         @Part("user_id") RequestBody id,
                                         @Part("first_name") RequestBody first_name,
                                         @Part("last_name") RequestBody last_name,
                                         @Part("email") RequestBody email,
                                         @Part("phone_number") RequestBody phone_number,
                                         @Part("dob") RequestBody dob,
                                         @Part("gender") RequestBody gender,
                                         @Part("about_us") RequestBody about_us,
                                         @Part("hair_color") RequestBody hair_color,
                                         @Part("weight") RequestBody weight,
                                         @Part("height") RequestBody height,
                                         @Part("brestsize") RequestBody brestsize,
                                         @Part("eye_color") RequestBody eye_color,
                                         @Part("hobby") RequestBody hobby,
                                         @Part("available_from") RequestBody available_from,
                                         @Part("ethnicity") RequestBody ethnicity,
                                         @Part("origin") RequestBody origin,
                                         @Part("country") RequestBody country,
                                         @Part("city") RequestBody city,
                                         @Part("language") RequestBody language,
                                         @Part("service_type") RequestBody incall,
                                         @Part("website") RequestBody website);



    @POST("/webservice.php")
    Call<MultipleImageMain> doUserMultipleImages(@Body RequestBody file);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ProfileLikeSuccess> doLikeOtherProfile(
            @Field("like_profile") String like_profile,
            @Field("user_id") String userId,
            @Field("status") String status,
            @Field("profile_id") String userIdAnotherUser);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<AnotherUserProfileIdByKeySuccess> getIdByKey(
            @Field("get_user_id") String status,
            @Field("username") String key);

    @POST("/webservice.php")
    Call<UpdateProfileMain> doUserUpdate1(
            @Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<GetTagIdSuccess> getHastagIdByKey(
            @Field("get_tag_id") String status,
            @Field("tag_name") String key);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<BlockUserResponse> doBlockUser(
            @Field("block_user") String blockUser,
            @Field("user_id") String userId,
            @Field("block_id") String userIdAnotherUser);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<BlockListData> getBlockList(
            @Field("block_userlist") String list,
            @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<BlockUserResponse> doUnblock(@Field("unblock_user") String status,
                                      @Field("user_id") String userId,
                                      @Field("block_id")String blockId);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<DisplayPhoneSuccess> displayPrice(@Field("display_price") String status,
                                           @Field("user_id") String userId,
                                           @Field("display_price_toall")String display);

 @FormUrlEncoded
    @POST("/webservice.php")
    Call<DisplayPhoneSuccess> displayPhone(@Field("display_number_to_all") String status,
                                           @Field("user_id") String userId,
                                           @Field("display_no")String display);

 @FormUrlEncoded
    @POST("/webservice.php")
    Call<UpdatePhoneSuccess> updatePhone(@Field("edit_user_number") String status,
                                         @Field("user_id") String userId,
                                         @Field("phone_number")String phone);


 @FormUrlEncoded
    Call<PLaceIdModel> getPLaceId(
            @Field("latlng") String latLng,
            @Field("key") String key);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<DeleteAudioResponse> deleteAudio(
            @Field("remove_audo_file") String s,
            @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<SessionResponse> getSession(
            @Field("getToken") String s,
            @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<UpdatePhoneSuccess> deleteAccount(
            @Field("delete_user_profile") String s,
            @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ProfileVisibleResponse> profileVisible(
            @Field("profile_on_off") String s,
            @Field("user_id") String userId,
            @Field("profile_status") String profileStatus);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<DeleteMain> updateUserLatLng(
            @Field("update_user_lat_long") String s,
            @Field("user_id") String userId,
            @Field("latitude") String searchLat,
            @Field("longitude") String searchLang);


    @FormUrlEncoded
    @POST("/webservice.php")
    Call<ProfileVisibleResponse> emailOnOff(
            @Field("turn_on_of_email") String s,
            @Field("user_id") String userId,
            @Field("email_on") String s1);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<DeleteMain> doLogout(
            @Field("user_logout") String status,
            @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<PostUploadCountResponse> getPostUploadCount(
            @Field("today_upload_count") String status,
            @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<DeleteMain> addHastag(
            @Field("user_search_hastag") String status,
            @Field("user_id") String userId,
            @Field("hastag") String tagName);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<DeleteMain> deleteHastag(
            @Field("user_search_hastag_remove") String status,
            @Field("user_id") String userId,
            @Field("hastag") String tag);

    @FormUrlEncoded
    @POST("/webservice.php")
    Call<UserProfileStatus> getUserProfileStatus(
            @Field("user_profile_status") String s,
            @Field("user_id") String userId);
}
