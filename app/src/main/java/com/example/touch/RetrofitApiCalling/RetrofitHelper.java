package com.example.touch.RetrofitApiCalling;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.touch.activities.UploadImageActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.interfaces.URLs;
import com.example.touch.models.AnotherUserMain;
import com.example.touch.models.AnotherUserProfileIdByKeySuccess;
import com.example.touch.models.BlockListData;
import com.example.touch.models.BlockUserResponse;
import com.example.touch.models.CategoryMain;
import com.example.touch.models.ChangePasswordMain;
import com.example.touch.models.ChatFromUserMain;
import com.example.touch.models.ChatListMain;
import com.example.touch.models.ChatMain;
import com.example.touch.models.CommentedMain;
import com.example.touch.models.CommentsMain;
import com.example.touch.models.CountListFollowIngPost;
import com.example.touch.models.DeleteAudioResponse;
import com.example.touch.models.DeleteMain;
import com.example.touch.models.DisplayPhoneSuccess;
import com.example.touch.models.FollowUnfollowCheckMain;
import com.example.touch.models.FollowUnfollowMain;
import com.example.touch.models.FollowersListMain;
import com.example.touch.models.FollowingMain;
import com.example.touch.models.ForgotPasswordMain;
import com.example.touch.models.GetAnotherUserProfileData;
import com.example.touch.models.GetTagIdSuccess;
import com.example.touch.models.HastagMain;
import com.example.touch.models.HomeMain;
import com.example.touch.models.ImageListTagAndName;
import com.example.touch.models.LatestPostResponse;
import com.example.touch.models.LikeCommentFollowMain;
import com.example.touch.models.LikesMain;
import com.example.touch.models.LocationSearchMain;
import com.example.touch.models.LoginMain;
import com.example.touch.models.MultipleImageGetMain;
import com.example.touch.models.MultipleImageMain;
import com.example.touch.models.PackageMainData;
import com.example.touch.models.PaymentDoneMainData;
import com.example.touch.models.PostUploadCountResponse;
import com.example.touch.models.ProfileLikeSuccess;
import com.example.touch.models.ProfileVisibleResponse;
import com.example.touch.models.ReadChatCheckMain;
import com.example.touch.models.ReadChatMain;
import com.example.touch.models.ReadNotificationMain;
import com.example.touch.models.SearchDetailsMain;
import com.example.touch.models.SearchMain;
import com.example.touch.models.SearchUserListData;
import com.example.touch.models.SessionResponse;
import com.example.touch.models.SignUpMain;
import com.example.touch.models.StatusMain;
import com.example.touch.models.UpdatePhoneSuccess;
import com.example.touch.models.UpdatePriceMain;
import com.example.touch.models.UpdateProfileMain;
import com.example.touch.models.UserLikeMain;
import com.example.touch.models.UserProfileStatus;
import com.example.touch.models.ValidateEmailMain;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitHelper {

    private static RetrofitHelper ourInstance = new RetrofitHelper();
    private SharedPreferencesData sharedPreferencesData;

    public static RetrofitHelper getInstance() {
        return ourInstance;
    }

    private RetrofitHelper() {
    }

    RetrofitApi retrofitapi;

    public void init(final Context context) {
        sharedPreferencesData = new SharedPreferencesData(context);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).
                connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request.Builder ongoing = chain.request().newBuilder();
                        ongoing.addHeader("Content-Type", "application/json");
                        if(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.TOKEN_ID) != null){
                            ongoing.addHeader("token",sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.TOKEN_ID));

                            return chain.proceed(ongoing.build());
                        }
                        else {
                            return chain.proceed(ongoing.build());
                        }
                    }
                })
                .build();
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URLs.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        retrofitapi = retrofit.create(RetrofitApi.class);
    }
    public void getForgotPassword(Callback<ForgotPasswordMain> callback, String email) {
        Call<ForgotPasswordMain> response;
        response = retrofitapi.doForgotPassword(Constants.FORGOT_PASSWORD,email);
        response.enqueue(callback);
    }

    public void getUpdatePrice(Callback<UpdatePriceMain> callback, String userId, String price) {
        Call<UpdatePriceMain> response;
        response = retrofitapi.doUpdatePrice(Constants.UPDATE_PRICE,userId,price);
        response.enqueue(callback);
    }

    public void getFollowersUser(Callback<FollowersListMain> callback, String userId) {
        Call<FollowersListMain> response;
        response = retrofitapi.doFollowersList(Constants.FOLLOWING_LIST,userId);
        response.enqueue(callback);
    }

    public void getProfileList(Callback<HomeMain> callback, String userId, String limit, int page) {
        Call<HomeMain> response;
        response = retrofitapi.doProfileList(Constants.LATEST_POST,userId, limit, page);
        response.enqueue(callback);
    }


    public void getNearMe(Callback<HomeMain> nearMeCallback, String lat, String lng) {
        Call<HomeMain> response;
        response = retrofitapi.getNearMe("1", lat, lng);
        response.enqueue(nearMeCallback);
    }

    public void getAllSearch(Callback<HomeMain> latestPostCallback, String userId, String limit, int page) {
        Call<HomeMain> response;
        response = retrofitapi.getAllSearch("1", limit, page);
        response.enqueue(latestPostCallback);
    }



    public void getStatusOfChat(Callback<ReadChatCheckMain> callback, String userId) {
        Call<ReadChatCheckMain> response;
        response = retrofitapi.doStatusOfChat(Constants.LATEST_POST,userId);
        response.enqueue(callback);
    }

    public void getProfilePostOnly(Callback<HomeMain> callback, String userId, String sender) {
        Call<HomeMain> response;
        response = retrofitapi.doProfilePostList(Constants.PROFILE_POST,userId,sender);
        response.enqueue(callback);
    }

    public void getChatList(Callback<ChatMain> callback, String sender_id, String reciver_id) {
        Call<ChatMain> response;
        response = retrofitapi.doChatList(Constants.PERSONAL_CHAT_LIST,sender_id,reciver_id);
        response.enqueue(callback);
    }
      public void getReadChatStatus(Callback<ReadChatMain> callback, String userId, String senderId) {
        Call<ReadChatMain> response;
        response = retrofitapi.doReadChat(Constants.READ_CHAT, userId, senderId);
        response.enqueue(callback);
    }

    public void getChatListTotal(Callback<ChatListMain> callback, String userId) {
        Call<ChatListMain> response;
        response = retrofitapi.doChatListTotal(Constants.CHAT_LIST,userId);
        response.enqueue(callback);
    }



    public void getLikeTotal(Callback<UserLikeMain> callback, String post_id) {
        Call<UserLikeMain> response;
        response = retrofitapi.doLikeList(Constants.USER_LIKE_LIST,post_id);
        response.enqueue(callback);
    }

    public void getDetailsSearchData(Callback<SearchDetailsMain> callback, String userId) {
        Call<SearchDetailsMain> response;
        response = retrofitapi.doDetailSearchData(Constants.FETCH_ALL_USER_IMAGE,userId);
        response.enqueue(callback);
    }

    public void getLikeProfileImage(Callback<LikesMain> callback, String user_id, String post_id, String status) {
        Call<LikesMain> response;
        response = retrofitapi.doLikeProfileImage(Constants.LIKE_LIST,user_id,post_id,status);
        response.enqueue(callback);
    }


    public void getCommentsList(Callback<CommentsMain> callback, String post_id) {
        Call<CommentsMain> response;
        response = retrofitapi.doCommentList(Constants.FETCH_ALL_COMENT,post_id);
        response.enqueue(callback);
    }


    public void getCommentOnPost(Callback<CommentedMain> callback,String user_id, String comment, String post_id,
                                 String postUserId) {
        Call<CommentedMain> response;
        response = retrofitapi.doCommentOnPost(Constants.MULTIPLE_COMMENTS,user_id, postUserId,comment,post_id);
        response.enqueue(callback);
    }

    public void getFollowersList(Callback<FollowingMain> callback, String userId) {
        Call<FollowingMain> response;
        response = retrofitapi.doFollowersList1(Constants.FOLLOWERS_LIST,userId);
        response.enqueue(callback);
    }

    public void getFollowingListProfile(Callback<FollowingMain> callback, String userId) {
        Call<FollowingMain> response;
        response = retrofitapi.doFollowingList1(Constants.FOLLOWING_LIST,userId);
        response.enqueue(callback);
    }


    public void getLogin(Callback<LoginMain> callback, String email,String password,String lat,String lang, String deviceId) {
        Call<LoginMain> response;
        response = retrofitapi.doLogin(Constants.LOGIN, email, password, deviceId, "1", lat, lang);
        response.enqueue(callback);
    }

    public void getMultipleImage(Callback<MultipleImageGetMain> callback, String userId, String limit, String page) {
        Call<MultipleImageGetMain> response;
        response = retrofitapi.doMultipleImageGet(Constants.MULTIPLE_IMAGES_LIST, userId, limit, page);
        response.enqueue(callback);
    }

    public void deletedImageList(Callback<DeleteMain> callback, String key) {
        Call<DeleteMain> response;
        response = retrofitapi.doDeleteImages(Constants.DELETE_PROFILE_IMAGE,key);
        response.enqueue(callback);
    }

    public void getSignUp(Callback<SignUpMain> callback,
                          String firstName,
                          String lastName,
                          String email,
                          String password,
                          String number,
                          String dob,
                          String gender,
                          String selectedCategoryId,
                          String userType,
                          String aboutus,
                          String hairColor,
                          String weight,
                          String height,
                          String breastSize,
                          String eyeColor,
                          String hobby,
                          String availability,
                          String ethnicity,
                          String origin,
                          String country,
                          String city,
                          String language,
                          String incall,
                          String website) {
        Call<SignUpMain> response;
       response = retrofitapi.doSignUp(Constants.REGISTERATION,Constants.DEVICE_TYPE,firstName, lastName,email,password,number,dob,gender,userType,selectedCategoryId,aboutus,hairColor,weight,height,breastSize,eyeColor,hobby,availability,ethnicity,origin,country,city,language,incall,website);
        response.enqueue(callback);
    }


    public void getSearchResult(Callback<SearchMain> callback, String key, String lat, String lang, String category_name) {
        Call<SearchMain> response;
       response = retrofitapi.doSearchData(Constants.SEARCH_FILTER,key,lat,lang,category_name);
        response.enqueue(callback);
    }


    public void getSearchByTagLocationNameSuggession1(Callback<SearchMain> callback, String key, String tabSearchKey, String lat, String lng,
                                                      String userId) {
        Call<SearchMain> response;
        response = retrofitapi.doSearchDataByKey1(Constants.TAB_SEARCH,key,tabSearchKey, lat, lng, userId);
        response.enqueue(callback);
    }

    public void getSearchByTagLocationNameSuggession(Callback<SearchMain> callback, String key, String tabSearchKey,
                                                     String userId) {
        Call<SearchMain> response;
       response = retrofitapi.doSearchDataByKey(Constants.TAB_SEARCH,key,tabSearchKey, userId);
        response.enqueue(callback);
    }

    public void getListImageTagWithAddress(Callback<ImageListTagAndName> callback, String tagId) {
        Call<ImageListTagAndName> response;
       response = retrofitapi.doListImageTagWithAddress(Constants.TAG_USER_LIST,tagId);
        response.enqueue(callback);
    }

    public void getCategoryList(Callback<CategoryMain> callback) {
        Call<CategoryMain> response;
        response = retrofitapi.doCategoryList(Constants.CATEGORY_LIST);
        response.enqueue(callback);
    }

    public void getChangePassword(Callback<ChangePasswordMain> callback, String userId, String oldPassword, String npass) {
        Call<ChangePasswordMain> response;
       response = retrofitapi.doChangePassword(Constants.CHANGE_PASSWORD,userId, oldPassword,npass);
        response.enqueue(callback);
    }

    public void getLocationImages(Callback<LocationSearchMain> callback, String latitude, String longitude, String key, String place_id, String countryCode, String countryType) {
        Call<LocationSearchMain> response;
        response = retrofitapi.doLocationSearch(Constants.LOCATION_USER_LIST, latitude, longitude, key, place_id, countryCode, countryType);
        response.enqueue(callback);
    }

    public void getEmailValidate(Callback<ValidateEmailMain> callback, String email, String user_name) {
        Call<ValidateEmailMain> response;
       response = retrofitapi.doEmailvalidate(Constants.EMAIL_VALIDATATION,email,user_name);
        response.enqueue(callback);
    }

    public void getFollowerIngCount(Callback<CountListFollowIngPost> callback, String userId) {
        Call<CountListFollowIngPost> response;
       response = retrofitapi.doFollowingFollowCount(Constants.COUNT_API,userId);
        response.enqueue(callback);
    }

    public void getCheckFollowList(Callback<FollowUnfollowCheckMain> callback, String userId, String anotherUserId) {
        Call<FollowUnfollowCheckMain> response;
       response = retrofitapi.doFollowUnfollowCheck(Constants.FOLLOW_UNFOLLOW_STATUS,userId,anotherUserId);
        response.enqueue(callback);
    }

    public void getAnotherUserImage(Callback<AnotherUserMain> callback, String userIdAnother) {
        Call<AnotherUserMain> response;
        response = retrofitapi.doAnotherUserProfile(Constants.GET_ANOTHER_USER_PROFILE, userIdAnother);
        response.enqueue(callback);
    }

 public void getAnotherUserProfile(Callback<GetAnotherUserProfileData> callback, String userId, String page,
                                   String limit, String userIdAnother) {
        Call<GetAnotherUserProfileData> response;
        response = retrofitapi.getAnotherUserProfile(Constants.GET_ANOTHER_USER_PROFILE, userId, page, limit, userIdAnother);
        response.enqueue(callback);
    }


    public void getPackageUpdateInfo(Callback<PackageMainData> callback, String userId) {
        Call<PackageMainData> response;
        response = retrofitapi.doPackageInfo(Constants.CHECK_PACKAGE,userId);
        response.enqueue(callback);
    }


    public void doStripePayment(Callback<PaymentDoneMainData> callback, String user_id, String stripetoken,
                                String amount, String booking_id, String package_id, String discription) {
        Call<PaymentDoneMainData> response;
        response = retrofitapi.doPaymentDone(Constants.STRIPE_PAYMENT_GATEWAY, user_id, stripetoken, amount,
                booking_id, package_id, discription);
        response.enqueue(callback);
    }


    public void getLikeFollowComment(Callback<LikeCommentFollowMain> callback, String user_id) {
        Call<LikeCommentFollowMain> response;
        response = retrofitapi.doFollowingLikeList(Constants.ACTIVITY,user_id, "10", 1);
        response.enqueue(callback);
    }

    public void getstatus(Callback<StatusMain> callback, String user_id) {
        Call<StatusMain> response;
        response = retrofitapi.doStatus(Constants.CHECK_ACTIVITY_STATUS,user_id);
        response.enqueue(callback);
    }

    public void getFollowingList(Callback<FollowingMain> callback, String lat, String lng, String user_id) {
        Call<FollowingMain> response;
        response = retrofitapi.doFollowingList(Constants.NEARBY,lat, lng,user_id);
        response.enqueue(callback);
    }

    public void getSearchByHastag(Callback<HastagMain> callback, String tagSearch) {
        Call<HastagMain> response;
        response = retrofitapi.doSearchTag(Constants.TAG_LIST,tagSearch);
        response.enqueue(callback);
    }

    public void getSearchByUsername(Callback<SearchUserListData> callback, String tagSearch) {
        Call<SearchUserListData> response;
        response = retrofitapi.doSearchUser("1", tagSearch);
        response.enqueue(callback);
    }

    public void getIdByKey(Callback<AnotherUserProfileIdByKeySuccess> callback, String key) {
        Call<AnotherUserProfileIdByKeySuccess> call;
        call = retrofitapi.getIdByKey("1", key);
        call.enqueue(callback);
    }


    public void getStatusTrue(Callback<ReadNotificationMain> callback, String user_id) {
        Call<ReadNotificationMain> response;
        response = retrofitapi.doStatusTrue(Constants.READ_ACTIVITY, user_id);
        response.enqueue(callback);
    }

    public void getHastagIdByKey(Callback<GetTagIdSuccess> hastagCallback, String key) {
        Call<GetTagIdSuccess> response;
        response = retrofitapi.getHastagIdByKey("1", key);
        response.enqueue(hastagCallback);
    }


    public void doLikeOtherProfile(Callback<ProfileLikeSuccess> likeCallback, String userId, String status,
                                   String userIdAnotherUser) {
        Call<ProfileLikeSuccess> response;
        response = retrofitapi.doLikeOtherProfile("1", userId, status, userIdAnotherUser);
        response.enqueue(likeCallback);
    }


    public void getFollowUnfollow(Callback<FollowUnfollowMain> callback, String userId, String unfollowerId, String status) {
        Call<FollowUnfollowMain> response;
        response = retrofitapi.doFollowUnfollow(Constants.FOLLOW_UNFOLLOW, userId, unfollowerId, status);
        response.enqueue(callback);
    }

    public void doBlockUser(Callback<BlockUserResponse> blockUserCallback, String userId, String userIdAnotherUser) {
        Call<BlockUserResponse> responseCall;
        responseCall = retrofitapi.doBlockUser(Constants.BLOCK_USER, userId, userIdAnotherUser);
        responseCall.enqueue(blockUserCallback);
    }

    public void getBlockList(Callback<BlockListData> blockListCallback, String userId) {
        Call<BlockListData> responseCall;
        responseCall = retrofitapi.getBlockList("1", userId);
        responseCall.enqueue(blockListCallback);
    }

    public void doUnblock(Callback<BlockUserResponse> unblockCallback, String userId, String blockId) {
        Call<BlockUserResponse> responseCall;
        responseCall = retrofitapi.doUnblock("1", userId, blockId);
        responseCall.enqueue(unblockCallback);
    }

    public void updatePhone(Callback<UpdatePhoneSuccess> updatePhoneCallback, String userId, String phone) {
        Call<UpdatePhoneSuccess> call;
        call = retrofitapi.updatePhone("1", userId, phone);
        call.enqueue(updatePhoneCallback);
    }


    public void displayPrice(Callback<DisplayPhoneSuccess> displayPhoneCallback, String userId, String s) {
        Call<DisplayPhoneSuccess> call;
        call = retrofitapi.displayPrice("1", userId, s);
        call.enqueue(displayPhoneCallback);
    }

    public void displayPhone(Callback<DisplayPhoneSuccess> displayPhoneCallback, String userId, String s) {
        Call<DisplayPhoneSuccess> call;
        call = retrofitapi.displayPhone("1", userId, s);
        call.enqueue(displayPhoneCallback);
    }
    public void getLatestPost(Callback<LatestPostResponse> callback, String userId){
        Call<LatestPostResponse> responseCall;
        responseCall = retrofitapi.getLatestPost(Constants.LATEST_POST, userId);
        responseCall.enqueue(callback);
    }

    public void getChatStartFromAnotherUser(Callback<ChatFromUserMain> callback, String sender_id, String reciver_id, String msg, String audio, String type) {
        Call<ChatFromUserMain> response;
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        if (audio == null || audio.equalsIgnoreCase("")){
            builder.addFormDataPart("audio", "");
        }else {
            File file = new File(audio);
            builder.addFormDataPart("audio", file.getName(), RequestBody.create(MediaType.parse("audio"), file));
        }
        builder.addFormDataPart("chat", Constants.CHAT);
        builder.addFormDataPart("sender_id", sender_id);
        builder.addFormDataPart("reciver_id", reciver_id);
        builder.addFormDataPart("msg", msg);
        builder.addFormDataPart("type", type);
        MultipartBody requestBody = builder.build();
        response = retrofitapi.doChatFromUser(requestBody);
        response.enqueue(callback);
    }

    public void setUpdateProfile(Callback<UpdateProfileMain> callback,String userImage,String userId,String firstName,
                                 String lastName,String email,String phone_number,String dob,String gender,String about,
                                 String hairColor,String weight,String height, String breast,String eyeColor,String hobby,
                                 String available,String ethnicity,String origin,String countryName,String cityName,
                                 String language,String incall,String website, String audio) {
        Call<UpdateProfileMain> response;
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        if (audio == null || audio.equalsIgnoreCase("")) {
            builder.addFormDataPart("audio[]", "");
        }else {
            File file1 = new File(audio);
            builder.addFormDataPart("audio[]", file1.getName(), RequestBody.create(MediaType.parse("audio"), file1));
        }
        if (userImage == null || userImage.equalsIgnoreCase("")){
                builder.addFormDataPart("user_img", "");
            }else {
                File file = new File(userImage);
                builder.addFormDataPart("user_img", file.getName(), RequestBody.create(MediaType.parse("user_img/*"), file));
            }
        builder.addFormDataPart("user_id", userId);
        builder.addFormDataPart("first_name", firstName);
        builder.addFormDataPart("last_name", lastName);
        builder.addFormDataPart("email", email);
        builder.addFormDataPart("phone_number", phone_number);
        builder.addFormDataPart("dob", dob);
        builder.addFormDataPart("gender", gender);
        builder.addFormDataPart("edit_profile", Constants.EDIT_PROFILE);
        builder.addFormDataPart("about_us", about);
        builder.addFormDataPart("hair_color", hairColor);
        builder.addFormDataPart("weight", weight);
        builder.addFormDataPart("height", height);
        builder.addFormDataPart("brestsize", breast);
        builder.addFormDataPart("eye_color", eyeColor);
        builder.addFormDataPart("hobby", hobby);
        builder.addFormDataPart("available_from", available);
        builder.addFormDataPart("ethnicity", ethnicity);
        builder.addFormDataPart("origin", origin);
        builder.addFormDataPart("country", countryName);
        builder.addFormDataPart("city", cityName);
        builder.addFormDataPart("language", language);
        builder.addFormDataPart("service_type", incall);
        builder.addFormDataPart("website", website);
        MultipartBody requestBody = builder.build();
        response = retrofitapi.doUserUpdate1(requestBody);
        response.enqueue(callback);
    }




    public void updatePost(Callback<MultipleImageMain> callback, List userImage, String userId,
                           String keyword, String location,String latitude,String longitude,String country, String countryCode,String state, String placeId,
                           String hastag1, String hastag2, String hastag3, String hastag4, String hastag5, String postId) {
        Call<MultipleImageMain> response;
        ArrayList<String> filePaths = new ArrayList<>();
        for (int i = 0; i < userImage.size(); i++) {
            filePaths.add(userImage.get(i).toString());
        }
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);


        if (userImage == null) {
            // image1 = MultipartBody.Part.createFormData("image", "");
        } else {
            builder.addFormDataPart("multiple_image_update", "1");
            builder.addFormDataPart("user_id", userId);
            builder.addFormDataPart("keyword", keyword);
            builder.addFormDataPart("location", location);
            builder.addFormDataPart("latitude", latitude);
            builder.addFormDataPart("longitude", longitude);
            builder.addFormDataPart("country", country);
            builder.addFormDataPart("country_code", countryCode);
            builder.addFormDataPart("state", state);
            builder.addFormDataPart("place_id", placeId);
            builder.addFormDataPart("hashtag_one", hastag1);
            builder.addFormDataPart("hashtag_two", hastag2);
            builder.addFormDataPart("hashtag_three", hastag3);
            builder.addFormDataPart("hashtag_four", hastag4);
            builder.addFormDataPart("hashtag_five", hastag5);
            builder.addFormDataPart("post_ids[]", postId);
            for (int i = 0; i < filePaths.size(); i++) {
                File file = new File(filePaths.get(i));
                builder.addFormDataPart("images[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
            }
            MultipartBody requestBody = builder.build();

            response = retrofitapi.doUserMultipleImages(requestBody);
            response.enqueue(callback);
        }
    }

    public void setMultipleImages(Callback<MultipleImageMain> callback, List userImage, String userId,
                                  String keyword, String location,String latitude,String longitude,String country, String countryCode,String state, String placeId,
                                  String hastag1, String hastag2, String hastag3, String hastag4, String hastag5) {
        Call<MultipleImageMain> response;
/*
        MultipartBody.Part[] surveyImagesParts = new MultipartBody.Part[3];
        MultipartBody.Part image1 = null;
        List<MultipartBody.Part> list=new ArrayList<>();*/

        ArrayList<String> filePaths = new ArrayList<>();
        for(int i=0;i<userImage.size();i++){
            filePaths.add(userImage.get(i).toString());
        }

        //filePaths.add("/storage/emulated/0/Pictures/1569310034924.jpg");
        //filePaths.add("/storage/emulated/0/Pictures/1569310034924.jpg");


        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);


        if (userImage==null) {
           // image1 = MultipartBody.Part.createFormData("image", "");
        } else {
           /* File file = new File(userImage);

            RequestBody reqFile = RequestBody.create(MediaType.parse("images/*"), file);

            //here we passed the key for sending the images userFiles[]
            image1 = MultipartBody.Part.createFormData("image", file.getName(), reqFile);*/



            builder.addFormDataPart("multiple_image", "1");
            builder.addFormDataPart("user_id", userId);
            builder.addFormDataPart("keyword", keyword);
            builder.addFormDataPart("location", location);
            builder.addFormDataPart("latitude", latitude);
            builder.addFormDataPart("longitude", longitude);
            builder.addFormDataPart("country", country);
            builder.addFormDataPart("country_code", countryCode);
            builder.addFormDataPart("state", state);
            builder.addFormDataPart("place_id", placeId);
            builder.addFormDataPart("hashtag_one", hastag1);
            builder.addFormDataPart("hashtag_two", hastag2);
            builder.addFormDataPart("hashtag_three", hastag3);
            builder.addFormDataPart("hashtag_four", hastag4);
            builder.addFormDataPart("hashtag_five", hastag5);
            for (int i = 0; i < filePaths.size(); i++) {
                File file = new File(filePaths.get(i));
                builder.addFormDataPart("images[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
            }
            MultipartBody requestBody = builder.build();

           /* for (int index = 0; index < 1; index++) {
                list=new ArrayList<>();
                Log.e("TAG", "requestUploadSurvey: survey image " + index + "  " + userImage);
                File file1 = new File(userImage);
                RequestBody surveyBody = RequestBody.create(MediaType.parse("images/*"), file1);
                surveyImagesParts[index] = MultipartBody.Part.createFormData("images", file1.getName(), surveyBody);
                list.add(surveyImagesParts[index]);
            }*/

            response = retrofitapi.doUserMultipleImages(requestBody);
            response.enqueue(callback);
        }


        //RequestBody request_multiple_images = RequestBody.create(MediaType.parse("text/plain"), Constants.MULTIPLE_IMAGES);
        //RequestBody request_userId = RequestBody.create(MediaType.parse("text/plain"), userId);

    }


    public void deleteAudio(Callback<DeleteAudioResponse> deleteAudioCallback, String userId) {
        Call<DeleteAudioResponse> call;
        call = retrofitapi.deleteAudio("1", userId);
        call.enqueue(deleteAudioCallback);
    }


    public void getSession(Callback<SessionResponse> sessionCallback, String device_Id) {
        Call<SessionResponse> call;
        call = retrofitapi.getSession("1", device_Id);
        call.enqueue(sessionCallback);
    }

    public void deleteAccount(Callback<UpdatePhoneSuccess> deleteAccountCallback, String userId) {
        Call<UpdatePhoneSuccess> call;
        call = retrofitapi.deleteAccount("1", userId);
        call.enqueue(deleteAccountCallback);
    }

    public void profileVisible(Callback<ProfileVisibleResponse> profileVisible, String userId, String profileStatus) {
        Call<ProfileVisibleResponse> call;
        call = retrofitapi.profileVisible("1", userId, profileStatus);
        call.enqueue(profileVisible);
    }

    public void updateUserLatLng(Callback<DeleteMain> updateLatLngCAllback, String userId, String searchLat, String searchLang) {
        Call<DeleteMain> call;
        call = retrofitapi.updateUserLatLng("1", userId, searchLat, searchLang);
        call.enqueue(updateLatLngCAllback);
    }

    public void emailOnOff(Callback<ProfileVisibleResponse> profileVisible, String userId, String s) {
        Call<ProfileVisibleResponse> call;
        call = retrofitapi.emailOnOff("1", userId, s);
        call.enqueue(profileVisible);
    }

    public void doLogout(Callback<DeleteMain> logoutCallback, String userId) {
        Call<DeleteMain> call;
        call = retrofitapi.doLogout("1", userId);
        call.enqueue(logoutCallback);
    }

    public void getPostUploadCount(Callback<PostUploadCountResponse> postUploadCountCallback, String userId) {
        Call<PostUploadCountResponse> call;
        call = retrofitapi.getPostUploadCount("1", userId);
        call.enqueue(postUploadCountCallback);
    }

    public void addHastag(Callback<DeleteMain> addHastagCallback, String userId, String tagName) {
        Call<DeleteMain> call;
        call = retrofitapi.addHastag("1", userId, tagName);
        call.enqueue(addHastagCallback);
    }

    public void deleteHastag(Callback<DeleteMain> deleteHastagCallback, String userId, String tag) {
        Call<DeleteMain> call;
        call = retrofitapi.deleteHastag("1", userId, tag);
        call.enqueue(deleteHastagCallback);
    }

    public void getUserProfileStatus(Callback<UserProfileStatus> profileStatusCallback, String userId) {
        Call<UserProfileStatus> call;
        call = retrofitapi.getUserProfileStatus("1", userId);
        call.enqueue(profileStatusCallback);

    }
}

