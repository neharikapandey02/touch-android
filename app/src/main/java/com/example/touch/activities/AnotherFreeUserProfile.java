package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.AddAnotherImageListAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.AnotherUserMain;
import com.example.touch.models.MultipleImageGetMainData;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnotherFreeUserProfile extends AppCompatActivity {
    private SharedPreferencesData sharedPreferencesData;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    List<MultipleImageGetMainData> multipleImageGetMainData;
    private String userIdAnotherUser = "";

    @BindView(R.id.tv_firstName)
    TextView tv_firstName;


    @BindView(R.id.imgView_back)
    ImageView imgView_back;

    private String firstName, blockStatus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_another_free_user_profile);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        multipleImageGetMainData=new ArrayList<>();
        userIdAnotherUser = getIntent().getStringExtra(Constants.ANOTHER_USER_ID);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getAnotherUserImage(getMultipleImageCallback, userIdAnotherUser);

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.imgView_back)
    public void backClick(View view){
        onBackPressed();

    }

    @OnClick(R.id.btn_price)
    public void priceButtonClick(View view) {

        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {

            if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase(userIdAnotherUser)) {
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra(Constants.FIRST_NAME, firstName);
                intent.putExtra(Constants.RECEIVER_ID, userIdAnotherUser);
                intent.putExtra(Constants.PROFILE_IMAGE, "");
//                intent.putExtra("block_status", blo);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.same_user), Toast.LENGTH_LONG).show();
            }

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.please_login_to_chat), Snackbar.LENGTH_LONG).show();
        }
    }
    Callback<AnotherUserMain> getMultipleImageCallback = new Callback<AnotherUserMain>() {
        @Override
        public void onResponse(Call<AnotherUserMain> call, Response<AnotherUserMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                multipleImageGetMainData.clear();


                if (response.body().getUserDetail() != null) {
                    tv_firstName.setText(response.body().getUserDetail().getFirstName());
                    firstName=response.body().getUserDetail().getFirstName();


                }




            }

        }

        @Override
        public void onFailure(Call<AnotherUserMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
