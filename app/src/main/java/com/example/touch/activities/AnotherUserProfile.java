package com.example.touch.activities;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.emoji.widget.EmojiTextView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.AddAnotherImageListAdapter;
import com.example.touch.adapters.AddAnotherImageListAdapter1;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.AnotherUserMain;
import com.example.touch.models.AnotherUserProfileResult;
import com.example.touch.models.BlockUserResponse;
import com.example.touch.models.CountListFollowIngPost;
import com.example.touch.models.FollowUnfollowCheckMain;
import com.example.touch.models.FollowUnfollowMain;
import com.example.touch.models.GetAnotherUserProfileData;
import com.example.touch.models.MultipleImageGetMainData;
import com.example.touch.models.ProfileLikeSuccess;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AnotherUserProfile extends AppCompatActivity {
    @BindView(R.id.btn_price)
    ImageView btn_price;

    @BindView(R.id.btn_changePassword)
    ImageView btn_changePassword;

    @BindView(R.id.tv_firstName)
    TextView tv_firstName;


    @BindView(R.id.imgv_top)
    ImageView imgv_top;


    @BindView(R.id.circularImageView_UserImage)
    ImageView circularImageView_UserImage;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.tv_totalFollowing)
    TextView tv_totalFollowing;

     @BindView(R.id.audioTV)
    TextView audioTV;

    @BindView(R.id.tv_post)
    TextView tv_post;

    @BindView(R.id.tv_totalFollowers)
    TextView tv_totalFollowers;


    @BindView(R.id.tv_categoryType)
    TextView tv_categoryType;


    @BindView(R.id.tv_aboutUs)
    EmojiTextView tv_aboutUs;


    @BindView(R.id.tv_country)
    TextView tv_country;


    @BindView(R.id.btn_call)
    ImageView btn_call;

    @BindView(R.id.tv_Tag1)
    TextView tv_Tag1;

    @BindView(R.id.tv_Tag2)
    TextView tv_Tag2;

    @BindView(R.id.tv_Tag3)
    TextView tv_Tag3;

    @BindView(R.id.tv_Tag4)
    TextView tv_Tag4;


    @BindView(R.id.tv_Tag5)
    TextView tv_Tag5;
    @BindView(R.id.btn_updateProfile)
    ImageView btn_updateProfile;
    @BindView(R.id.btn_loginProfile)
    Button btn_loginProfile;
    @BindView(R.id.recyclerView_UploadedImages)
    RecyclerView recyclerView_UploadedImages;
    @BindView(R.id.linearLayout_Following)
    LinearLayout linearLayout_Following;
    @BindView(R.id.linearLayout_Followers)
    LinearLayout linearLayout_Followers;
    @BindView(R.id.imgView_back)
    ImageView imgView_back;
    @BindView(R.id.imgv_location)
    ImageView imgv_location;
    @BindView(R.id.profileLikeImg)
    ImageView profileLikeImg;
    @BindView(R.id.tv_followUnfollowSet)
    TextView tv_followUnfollowSet;
    @BindView(R.id.linearLayout_FollowUnfollowMe)
    LinearLayout linearLayout_FollowUnfollowMe;

    @BindView(R.id.audioImg)
    ImageView audioImg;

    @BindView(R.id.settingImg)
    ImageView settingImg;


    List<MultipleImageGetMainData> multipleImageGetMainData;
    List<AnotherUserProfileResult> anotherUserProfileResults;
    HashMap<String, String> userDetailsHashMap;
    View view;
    MediaController mc;
    String likeStatus = "";
    MediaPlayer mediaPlayer;
    String playStatus = "0";
    String audioFile = "", blockStatus = "";
    private SharedPreferencesData sharedPreferencesData;
    private String userId = "";
    private String userIdAnotherUser = "";
    private String firstName = "";
    private String lastName = "";
    private String phoneNumber = "";
    private String profileImage = "";
    private String hairColor = "";
    private String weight = "";
    private String height = "";
    private String available = "";
    private String statusCheckFlag = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        Objects.requireNonNull(getSupportActionBar()).hide(); //hide the title bar
        setContentView(R.layout.activity_another_user_profile);
        ButterKnife.bind(this);
        viewFinds();



        audioImg.setOnClickListener(view -> {
            if (audioFile != null) {
                if (playStatus.equalsIgnoreCase("0")) {
                    mediaPlayer = new MediaPlayer();
                    try {
                        mediaPlayer.setDataSource(audioFile);
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        playStatus = "1";
                        Toast.makeText(getApplicationContext(), "Playing Audio", Toast.LENGTH_LONG).show();

                        mediaPlayer.setOnCompletionListener(mediaPlayer -> {
                            mediaPlayer.stop();
                            playStatus = "0";
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    mediaPlayer.stop();
                    playStatus = "0";
                    Toast.makeText(getApplicationContext(), "Stop Audio", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), "Audio not found.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void viewFinds() {
        mc = new MediaController(getApplicationContext());
        userDetailsHashMap = new HashMap<>();
        multipleImageGetMainData = new ArrayList<>();
        anotherUserProfileResults = new ArrayList<>();
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userIdAnotherUser = getIntent().getStringExtra(Constants.ANOTHER_USER_ID);

        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        recyclerView_UploadedImages.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        recyclerView_UploadedImages.setNestedScrollingEnabled(false);
        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(AnotherUserProfile.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(AnotherUserProfile.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(AnotherUserProfile.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(AnotherUserProfile.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(AnotherUserProfile.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userIdAnotherUser = getIntent().getStringExtra(Constants.ANOTHER_USER_ID);
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        if (userId.equalsIgnoreCase(userIdAnotherUser)) {
            linearLayout_FollowUnfollowMe.setVisibility(View.GONE);
        }
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                RetrofitHelper.getInstance().getCheckFollowList(checkFollowUnfollowCallabck, userId, userIdAnotherUser);
            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getFollowerIngCount(followersListCallback, userIdAnotherUser);

            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }

            profileLikeImg.setOnClickListener(view -> {
                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    if (likeStatus.equalsIgnoreCase("0")) {
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().doLikeOtherProfile(likeCallback, userId, "1", userIdAnotherUser);
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().doLikeOtherProfile(likeCallback, userId, "0", userIdAnotherUser);
                    }

                } else {
                    Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            });

            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getAnotherUserProfile(callback, userId, "1", "5", userIdAnotherUser);

            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }

        } else {
            linearLayout_FollowUnfollowMe.setVisibility(View.GONE);
            imgv_location.setVisibility(View.GONE);
            Snackbar.make(progressBar, getResources().getString(R.string.please_login_first), Snackbar.LENGTH_LONG).show();
        }

        if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase(userIdAnotherUser)) {
            settingImg.setVisibility(View.VISIBLE);
        } else {
            settingImg.setVisibility(View.GONE);
        }

        settingImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(AnotherUserProfile.this)
                        .setTitle("Block user")
                        .setMessage("Do you want to block this user?")
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                                    progressBar.setVisibility(View.VISIBLE);
                                    RetrofitHelper.getInstance().doBlockUser(blockUserCallback, userId, userIdAnotherUser);

                                } else {
                                    Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .show();
            }
        });
    }

    Callback<BlockUserResponse> blockUserCallback = new Callback<BlockUserResponse>() {
        @Override
        public void onResponse(Call<BlockUserResponse> call, Response<BlockUserResponse> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess() == 1){
                    Toast.makeText(AnotherUserProfile.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void onFailure(Call<BlockUserResponse> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    private static String getDuration(String file) {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(file, new HashMap<String, String>());
            String durationStr = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            int seconds = (int) ((Long.parseLong(durationStr) % (1000 * 60 * 60)) % (1000 * 60) / 1000);
            return String.valueOf(seconds);
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }



    Callback<GetAnotherUserProfileData> callback = new Callback<GetAnotherUserProfileData>() {
        @Override
        public void onResponse(@NotNull Call<GetAnotherUserProfileData> call, Response<GetAnotherUserProfileData> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                blockStatus = response.body().getBlock_status().toString();
                multipleImageGetMainData.clear();
                assert response.body() != null;
                if (response.body().getUserDetail() != null && response.body().getUserDetail().getProfileImage() != null) {
                    Picasso.with(getApplicationContext())
                            .load(response.body().getUserDetail().getProfileImage())
                            .placeholder(R.drawable.profile_back)
                            .error(R.drawable.profile_back)
                            .into(imgv_top);
                    Picasso.with(getApplicationContext())
                            .load(response.body().getUserDetail().getProfileImage())
                            .placeholder(R.drawable.profile_placeholder)
                            .error(R.drawable.profile_placeholder)
                            .into(circularImageView_UserImage);
                }

                if (response.body().getUserDetail() != null) {
                    audioFile = response.body().getUserDetail().getAudio();
                    if (audioFile != null){
                        String dur = getDuration(audioFile);
                        audioTV.setText(dur+" sec");
                    }else {
                        audioImg.setBackgroundColor(R.drawable.button_corner_gray);
                        audioTV.setVisibility(View.INVISIBLE);
                    }
                    userDetailsHashMap.put(Constants.FIRST_NAME, response.body().getUserDetail().getFirstName());
                    userDetailsHashMap.put(Constants.LAST_NAME, response.body().getUserDetail().getLastName());
                    userDetailsHashMap.put(Constants.EMAIL, response.body().getUserDetail().getEmail());
                    userDetailsHashMap.put(Constants.PHONE_NUMBER, response.body().getUserDetail().getPhoneNumber());
                    userDetailsHashMap.put(Constants.DOB, response.body().getUserDetail().getDob());
                    userDetailsHashMap.put(Constants.GENDER, response.body().getUserDetail().getGender());
                    userDetailsHashMap.put(Constants.USER_TYPE, response.body().getUserDetail().getUserType());
                    userDetailsHashMap.put(Constants.ABOUT_US, response.body().getUserDetail().getAboutUs());
                    userDetailsHashMap.put(Constants.HAIR_COLOR, response.body().getUserDetail().getHairColor());
                    userDetailsHashMap.put(Constants.WEIGHT, response.body().getUserDetail().getWeight());
                    userDetailsHashMap.put(Constants.HEIGHT, response.body().getUserDetail().getHeight());
                    userDetailsHashMap.put(Constants.BREAST_SIZE, response.body().getUserDetail().getBrestsize());
                    userDetailsHashMap.put(Constants.EYE_COLOR, response.body().getUserDetail().getEyeColor());
                    userDetailsHashMap.put(Constants.HOBBY, response.body().getUserDetail().getHobby());
                    userDetailsHashMap.put(Constants.AVAILABLE, response.body().getUserDetail().getAvailableFrom());
                    userDetailsHashMap.put(Constants.ETHNICITY, response.body().getUserDetail().getEthnicity());
                    userDetailsHashMap.put(Constants.ORIGIN, response.body().getUserDetail().getOrigin());
                    userDetailsHashMap.put(Constants.COUNTRY_NAME, response.body().getUserDetail().getCountry());
                    userDetailsHashMap.put(Constants.CITY_NAME, response.body().getUserDetail().getCity());
                    userDetailsHashMap.put(Constants.LANGUAGE, response.body().getUserDetail().getLanguage());
                    userDetailsHashMap.put(Constants.SERVICE_TYPE, response.body().getUserDetail().getServiceType());
                    userDetailsHashMap.put(Constants.WEBSITE, response.body().getUserDetail().getWebsite());
                    userDetailsHashMap.put(Constants.PROFILE_IMAGE, response.body().getUserDetail().getProfileImage());
                    tv_firstName.setText(response.body().getUserDetail().getFirstName());
                    tv_country.setText(response.body().getUserDetail().getCountry());
                    tv_aboutUs.setTextSize(13);
                    tv_aboutUs.setText(response.body().getUserDetail().getAboutUs());
                    firstName = response.body().getUserDetail().getFirstName();
                    profileImage = response.body().getUserDetail().getProfileImage();
                    likeStatus = String.valueOf(response.body().getUserDetail().getProfileLikeStatus());
                    if (likeStatus.equalsIgnoreCase("0")) {
                        profileLikeImg.setImageResource(R.drawable.like_black_user);
                    } else {
                        profileLikeImg.setImageResource(R.drawable.like_red_user);
                    }

                    try {
                        if (response.body().getUserDetail().getDisplay_no().equalsIgnoreCase("0")){
                            btn_call.setEnabled(false);
                            btn_call.setImageResource(R.drawable.phone_disable);
                        }
                        else {
                            btn_call.setEnabled(true);
                            btn_call.setImageResource(R.drawable.phone_other_user);
                        }
                        if (response.body().getUserDetail().getDisplay_price().equalsIgnoreCase("0")){

                            btn_changePassword.setEnabled(false);
                            btn_changePassword.setImageResource(R.drawable.dollar_disable);
                        }else {
                            btn_changePassword.setImageResource(R.drawable.dollar_other_user);
                            btn_changePassword.setEnabled(true);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
                if (response.body().getResult() != null) {
                    anotherUserProfileResults.clear();
                    anotherUserProfileResults.addAll(response.body().getResult());
                    AddAnotherImageListAdapter1 addImagesListAdapter = new AddAnotherImageListAdapter1(AnotherUserProfile.this, anotherUserProfileResults, mc);
                    recyclerView_UploadedImages.setAdapter(addImagesListAdapter);
                } else {
                    AddAnotherImageListAdapter1 addImagesListAdapter = new AddAnotherImageListAdapter1(AnotherUserProfile.this, anotherUserProfileResults, mc);
                    recyclerView_UploadedImages.setAdapter(addImagesListAdapter);
                }


            }
        }

        @Override
        public void onFailure(@NotNull Call<GetAnotherUserProfileData> call, @NotNull Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    Callback<AnotherUserMain> getMultipleImageCallback = new Callback<AnotherUserMain>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onResponse(@NotNull Call<AnotherUserMain> call, Response<AnotherUserMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                multipleImageGetMainData.clear();
                assert response.body() != null;
                if (response.body().getUserDetail() != null && response.body().getUserDetail().getProfileImage() != null) {
                    Picasso.with(getApplicationContext())
                            .load(response.body().getUserDetail().getProfileImage())
                            .placeholder(R.drawable.profile_back)
                            .error(R.drawable.profile_back)
                            .into(imgv_top);
                    Picasso.with(getApplicationContext())
                            .load(response.body().getUserDetail().getProfileImage())
                            .placeholder(R.drawable.profile_placeholder)
                            .error(R.drawable.profile_placeholder)
                            .into(circularImageView_UserImage);
                }
                if (response.body().getUserDetail() != null) {
                    userDetailsHashMap.put(Constants.FIRST_NAME, response.body().getUserDetail().getFirstName());
                    userDetailsHashMap.put(Constants.LAST_NAME, response.body().getUserDetail().getLastName());
                    userDetailsHashMap.put(Constants.EMAIL, response.body().getUserDetail().getEmail());
                    userDetailsHashMap.put(Constants.PHONE_NUMBER, response.body().getUserDetail().getPhoneNumber());
                    userDetailsHashMap.put(Constants.DOB, response.body().getUserDetail().getDob());
                    userDetailsHashMap.put(Constants.GENDER, response.body().getUserDetail().getGender());
                    userDetailsHashMap.put(Constants.USER_TYPE, response.body().getUserDetail().getUserType());
                    userDetailsHashMap.put(Constants.ABOUT_US, response.body().getUserDetail().getAboutUs());
                    userDetailsHashMap.put(Constants.HAIR_COLOR, response.body().getUserDetail().getHairColor());
                    userDetailsHashMap.put(Constants.WEIGHT, response.body().getUserDetail().getWeight());
                    userDetailsHashMap.put(Constants.HEIGHT, response.body().getUserDetail().getHeight());
                    userDetailsHashMap.put(Constants.BREAST_SIZE, response.body().getUserDetail().getBrestsize());
                    userDetailsHashMap.put(Constants.EYE_COLOR, response.body().getUserDetail().getEyeColor());
                    userDetailsHashMap.put(Constants.HOBBY, response.body().getUserDetail().getHobby());
                    userDetailsHashMap.put(Constants.AVAILABLE, response.body().getUserDetail().getAvailableFrom());
                    userDetailsHashMap.put(Constants.ETHNICITY, response.body().getUserDetail().getEthnicity());
                    userDetailsHashMap.put(Constants.ORIGIN, response.body().getUserDetail().getOrigin());
                    userDetailsHashMap.put(Constants.COUNTRY_NAME, response.body().getUserDetail().getCountry());
                    userDetailsHashMap.put(Constants.CITY_NAME, response.body().getUserDetail().getCity());
                    userDetailsHashMap.put(Constants.LANGUAGE, response.body().getUserDetail().getLanguage());
                    userDetailsHashMap.put(Constants.SERVICE_TYPE, response.body().getUserDetail().getServiceType());
                    userDetailsHashMap.put(Constants.WEBSITE, response.body().getUserDetail().getWebsite());
                    userDetailsHashMap.put(Constants.PROFILE_IMAGE, response.body().getUserDetail().getProfileImage());
                    tv_firstName.setText(response.body().getUserDetail().getFirstName());
                    tv_country.setText(response.body().getUserDetail().getCountry());
                    tv_aboutUs.setTextSize(13);
                    tv_aboutUs.setText(response.body().getUserDetail().getAboutUs());

                    firstName = response.body().getUserDetail().getFirstName();
                    profileImage = response.body().getUserDetail().getProfileImage();

                    if (response.body().getTags() != null) {

                        if (response.body().getTags().size() > 0 && response.body().getTags().get(0).getTags() != null) {
                            tv_Tag1.setText("#" + response.body().getTags().get(0).getTags());
                        }
                        if (response.body().getTags().size() > 1 && response.body().getTags().get(1).getTags() != null) {
                            tv_Tag2.setText("#" + response.body().getTags().get(1).getTags());
                        }
                        if (response.body().getTags().size() > 2 && response.body().getTags().get(2).getTags() != null) {
                            tv_Tag3.setText("#" + response.body().getTags().get(2).getTags());
                        }
                        if (response.body().getTags().size() > 3 && response.body().getTags().get(3).getTags() != null) {
                            tv_Tag4.setText("#" + response.body().getTags().get(3).getTags());
                        }
                        if (response.body().getTags().size() > 4 && response.body().getTags().get(4).getTags() != null) {
                            tv_Tag5.setText("#" + response.body().getTags().get(4).getTags());
                        }
                    }
                }
                if (response.body().getResult() != null) {
                    multipleImageGetMainData.addAll(response.body().getResult());
                    AddAnotherImageListAdapter addImagesListAdapter = new AddAnotherImageListAdapter(getApplicationContext(), multipleImageGetMainData, mc);
                    recyclerView_UploadedImages.setAdapter(addImagesListAdapter);
                } else {
                    AddAnotherImageListAdapter addImagesListAdapter = new AddAnotherImageListAdapter(getApplicationContext(), multipleImageGetMainData, mc);
                    recyclerView_UploadedImages.setAdapter(addImagesListAdapter);
                }
            }
        }
        @Override
        public void onFailure(@NotNull Call<AnotherUserMain> call, @NotNull Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.linearLayout_FollowUnfollowMe)
    public void followUnfollowClicked(View view) {
        if (tv_followUnfollowSet.getText().toString().equalsIgnoreCase("Unfollow Me")) {
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getFollowUnfollow(followUnfollowCallback, userId, userIdAnotherUser, "2");
            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
        } else {
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getFollowUnfollow(followUnfollowCallback, userId, userIdAnotherUser, "1");
            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    Callback<FollowUnfollowMain> followUnfollowCallback = new Callback<FollowUnfollowMain>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onResponse(@NotNull Call<FollowUnfollowMain> call, Response<FollowUnfollowMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                assert response.body() != null;
                Snackbar.make(progressBar, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                if (response.body().getSuccess() == 1) {
                    tv_followUnfollowSet.setText("Unfollow Me");
                } else {
                    tv_followUnfollowSet.setText("Follow Me");
                }
                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().getFollowerIngCount(followersListCallback, userIdAnotherUser);
                } else {
                    Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(@NotNull Call<FollowUnfollowMain> call, @NotNull Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<ProfileLikeSuccess> likeCallback = new Callback<ProfileLikeSuccess>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onResponse(@NotNull Call<ProfileLikeSuccess> call, Response<ProfileLikeSuccess> response) {
            progressBar.setVisibility(View.GONE);
            if (response.body() != null) {
                if (response.body().getMessage().contains("Liked")) {
                    likeStatus = "1";
                    profileLikeImg.setImageResource(R.drawable.like_red_user);
                    String likeCount = tv_totalFollowing.getText().toString();
                    int like = Integer.parseInt(likeCount);
                    like = like + 1;
                    tv_totalFollowing.setText("" + like);

                } else {
                    likeStatus = "0";
                    profileLikeImg.setImageResource(R.drawable.like_black_user);
                    String likeCount = tv_totalFollowing.getText().toString();
                    int like = Integer.parseInt(likeCount);
                    like = like - 1;
                    tv_totalFollowing.setText("" + like);
                }
            }
        }

        @Override
        public void onFailure(@NotNull Call<ProfileLikeSuccess> call, @NotNull Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<CountListFollowIngPost> followersListCallback = new Callback<CountListFollowIngPost>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onResponse(@NotNull Call<CountListFollowIngPost> call, Response<CountListFollowIngPost> response) {
            progressBar.setVisibility(View.GONE);
            if (response.body() != null) {
                if (response.body().getFollowingCount() != null) {
                    tv_totalFollowing.setText(response.body().getLikeCount().toString());
                }
                if (response.body().getFollowersCount() != null) {
                    tv_totalFollowers.setText(response.body().getFollowersCount().toString());
                }
                if (response.body().getPostCount() != null) {
                    tv_post.setText(response.body().getPostCount().toString());
                }
            }
        }
        @Override
        public void onFailure(@NotNull Call<CountListFollowIngPost> call, @NotNull Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<FollowUnfollowCheckMain> checkFollowUnfollowCallabck = new Callback<FollowUnfollowCheckMain>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onResponse(@NotNull Call<FollowUnfollowCheckMain> call, Response<FollowUnfollowCheckMain> response) {
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    if (response.body().getFollowStatus() == 1) {
                        tv_followUnfollowSet.setText("Unfollow Me");
                    } else {
                        tv_followUnfollowSet.setText("Follow Me");
                    }
                }
            }
        }
        @Override
        public void onFailure(@NotNull Call<FollowUnfollowCheckMain> call, @NotNull Throwable t) {

        }
    };

    @OnClick(R.id.btn_updateProfile)
    public void updateButtonClick(View view) {
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
            intent.putExtra(Constants.USER_DETAILS, userDetailsHashMap);
            startActivity(intent);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.login_first_see_profile), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btn_loginProfile)
    public void loginButtonClick(View view) {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_price)
    public void priceButtonClick(View view) {

        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {

            if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase(userIdAnotherUser)) {
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra(Constants.FIRST_NAME, firstName);
                intent.putExtra(Constants.RECEIVER_ID, userIdAnotherUser);
                intent.putExtra(Constants.PROFILE_IMAGE, profileImage);
                intent.putExtra("block_status", blockStatus);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.same_user), Toast.LENGTH_LONG).show();
            }

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.please_login_to_chat), Snackbar.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.btn_changePassword)
    public void changePasswordClick(View view) {
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            Intent intent = new Intent(getApplicationContext(), PriceShowActivity.class);
            intent.putExtra(Constants.ANOTHER_USER_ID, userIdAnotherUser);
            startActivity(intent);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.login_first_change), Snackbar.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.linearLayout_Followers)
    public void followersClick(View view) {
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            Intent intent = new Intent(getApplicationContext(), FollowersActivity.class);
            intent.putExtra(Constants.ANOTHER_USER_ID, userIdAnotherUser);
            startActivity(intent);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.please_login_to_see_followers), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.imgView_back)
    public void backClick(View view) {
        onBackPressed();
    }

    @OnClick(R.id.btn_call)
    public void callClick(View view) {
        String phoneNumber = userDetailsHashMap.get(Constants.PHONE_NUMBER);
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
        startActivity(intent);
    }
}
