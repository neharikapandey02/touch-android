package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ButtonBarLayout;

import android.bluetooth.BluetoothHidDevice;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.ChangePasswordMain;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePassword extends AppCompatActivity {
    @BindView(R.id.et_oldPassword)
    EditText et_oldPassword;

    @BindView(R.id.et_newPassword)
    EditText et_newPassword;

    @BindView(R.id.et_confirmPassword)
    EditText et_confirmPassword;

    @BindView(R.id.btn_change)
    Button btn_change;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private SharedPreferencesData sharedPreferencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());


    }

    @OnClick(R.id.btn_change)
    public void changeButtonClick(View view) {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            String oldPassword = et_oldPassword.getText().toString();
            String newPassword = et_newPassword.getText().toString();
            String confirmNewPassword = et_confirmPassword.getText().toString();
            String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);

            if (validatePassword(oldPassword, newPassword, confirmNewPassword)) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getChangePassword(changePasswordCallback, userId, oldPassword, newPassword);
            }
        }else {
                Snackbar.make(btn_change, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
        }





    Callback<ChangePasswordMain> changePasswordCallback = new Callback<ChangePasswordMain>() {

        @Override
        public void onResponse(Call<ChangePasswordMain> call, Response<ChangePasswordMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                Snackbar.make(btn_change, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                if (response.body().getSuccess() == 1) {
                    et_oldPassword.setText("");
                    et_newPassword.setText("");
                    et_confirmPassword.setText("");
                    onBackPressed();
                }

            }
        }

        @Override
        public void onFailure(Call<ChangePasswordMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
    private boolean validatePassword(String oldPassword, String newPassword, String confirmPassword) {
        if (oldPassword.equalsIgnoreCase("")) {
            Snackbar.make(btn_change, getResources().getString(R.string.enter_old_password), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (newPassword.equalsIgnoreCase("")) {
            Snackbar.make(btn_change, getResources().getString(R.string.enter_new_password), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (confirmPassword.equalsIgnoreCase("")) {
            Snackbar.make(btn_change, getResources().getString(R.string.enter_confirm_password), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (!newPassword.equalsIgnoreCase(confirmPassword)) {
            Snackbar.make(btn_change, getResources().getString(R.string.match), Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}