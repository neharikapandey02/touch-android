package com.example.touch.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.provider.FontRequest;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.emoji.widget.EmojiEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.ChatAdapter;
import com.example.touch.models.ChatFromUserMain;
import com.example.touch.models.ChatMain;
import com.example.touch.models.ChatMainData;
import com.example.touch.models.ReadChatMain;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.rygelouv.audiosensei.player.AudioSenseiListObserver;
import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;
import com.squareup.picasso.Picasso;
import com.vanniktech.emoji.EmojiManager;
import com.vanniktech.emoji.ios.IosEmojiProvider;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView_Chat)
    RecyclerView recyclerView_Chat;

    private SharedPreferencesData sharedPreferencesData;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.circularImageView_receiver)
    CircularImageView circularImageView_receiver;


    @BindView(R.id.circularImageView_UserImage)
    CircularImageView circularImageView_UserImage;

    @BindView(R.id.recordTv)
    TextView recordTv;


    @BindView(R.id.tv_receiverName)
    TextView tv_receiverName;


    @BindView(R.id.imgView_back)
    ImageView imgView_back;


    @BindView(R.id.imgView_forEmoji)
    ImageView imgView_forEmoji;

    @BindView(R.id.et_sendMessage)
    EmojiEditText et_sendMessage;

    @BindView(R.id.tv_send)
    TextView tv_send;

    @BindView(R.id.countDownTimerTv)
    TextView countDownTimerTv;

    private Handler m_Handler;
    private Runnable mRunnable;

    String receiver_id;
    String profileImage;
    String firstName;
    String userProfileImage;
    ChatAdapter chatAdapter;
    List<ChatMainData> chatMainDataList;
    private boolean flag = true;
    int size = 0;
    String userId;
    ImageView imgView_ChatList;
    View rootView;
    EmojIconActions emojIcon;
    MediaPlayer mediaPlayer;
    private String outputFile;
    MediaRecorder recorder;
    private CounterClass counterClass;
    private int REQUEST_AUDIO_PERMISSION_RESULT = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontRequest fontRequest = null;
        EmojiManager.install(new IosEmojiProvider());
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        recorder = new MediaRecorder();
        viewFinds();
        recordTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (recordTv.getText().toString().equalsIgnoreCase(getString(R.string.tap_to_record_audio))) {
                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.RECORD_AUDIO) ==
                                    PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ChatActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                // put your code for Version>=Marshmallow
                                startRecording(v);

                            } else {
                                if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                                    Toast.makeText(ChatActivity.this,
                                            "App required access to audio", Toast.LENGTH_SHORT).show();
                                }

                                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE
                                }, REQUEST_AUDIO_PERMISSION_RESULT);
                            }

                        } else {
                            startRecording(v);
                            // put your code for Version < Marshmallow
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    stopRecording(v);
                }
            }
        });

    }

    public void startRecording(View view) throws IOException {
        recorder = new MediaRecorder();
        recordTv.setText(getString(R.string.stop_and_send));
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/chatrecording.mp3";
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setOutputFile(outputFile);
        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException ise) {
            // make something ...
        } catch (IOException ioe) {
            // make something
        }
        countDownTimerTv.setVisibility(View.VISIBLE);
        counterClass = new CounterClass(10000, 1000);
        counterClass.start();
        Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
    }

    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval){
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            countDownTimerTv.setText(""+l / 1000);
        }

        @Override
        public void onFinish() {
            stopRecording(recordTv);
        }
    }

    public void stopRecording(View view) {

        recordTv.setEnabled(true);

        try {
            recorder.stop();
        } catch(RuntimeException stopException) {
            // handle cleanup here
        }
        recorder.release();
        recorder = null;
        countDownTimerTv.setText("");
        countDownTimerTv.setVisibility(View.INVISIBLE);
        counterClass.cancel();
        recordTv.setText(getString(R.string.tap_to_record_audio));
        Toast.makeText(getApplicationContext(), "Audio Recorder successfully", Toast.LENGTH_LONG).show();
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            RetrofitHelper.getInstance().getChatStartFromAnotherUser(chatForUserCallback, userId, receiver_id, "", outputFile, "2");
        }else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }


    public static String encodeEmoji (String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }


    public static String decodeEmoji(String message) {
        String myString = null;
        try {
            return URLDecoder.decode(
                    message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.e("KeyDown=",keyCode+"  "+event.getDeviceId()+"");
        switch (keyCode) {
            case KeyEvent.KEYCODE_J:
                if (event.isCanceled()) {

                }
                return true;
            case KeyEvent.KEYCODE_K:
                if (event.isShiftPressed()) {

                }
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }

    private void viewFinds() {
        AudioSenseiListObserver.getInstance().registerLifecycle(getLifecycle());

        chatMainDataList = new ArrayList<>();
        chatAdapter = new ChatAdapter(ChatActivity.this, chatMainDataList);

        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userProfileImage = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);

        profileImage = getIntent().getStringExtra(Constants.PROFILE_IMAGE);
        firstName = getIntent().getStringExtra(Constants.FIRST_NAME);
        receiver_id = getIntent().getStringExtra(Constants.RECEIVER_ID);
        if (profileImage != null&&!profileImage.equalsIgnoreCase("")) {
            profileImage = getIntent().getStringExtra(Constants.PROFILE_IMAGE);
            Picasso.with(getApplicationContext())
                    .load(profileImage)
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(circularImageView_receiver);
        }

        if (!userProfileImage.equalsIgnoreCase("")) {
            Picasso.with(getApplicationContext())
                    .load(userProfileImage)
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(circularImageView_UserImage);
        }

        tv_receiverName.setText(firstName);
        recyclerView_Chat.setHasFixedSize(true);
        recyclerView_Chat.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getChatList(fetchAllChatCallback, userId, receiver_id);

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }


        callEvery4SecondForLatestChat();
       /* et_sendMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Log.e("Editable",editable.toString()+editable.toString().equalsIgnoreCase(""));
                if (editable.toString().equalsIgnoreCase("")){
                    tv_send.setVisibility(View.GONE);
                }else{
                    tv_send.setVisibility(View.VISIBLE);
                }

            }
        });*/



       /* if (Build.VERSION.SDK_INT >= 11) {
            recyclerView_Chat.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v,
                                           int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom) {
                    if (bottom < oldBottom) {
                        recyclerView_Chat.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView_Chat.smoothScrollToPosition(
                                       recyclerView_Chat.getAdapter().getItemCount());
                            }
                        }, 100);
                    }
                }
            });
        }*/
        /*recyclerView_Chat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    Toast.makeText(ChatActivity.this, "Last", Toast.LENGTH_LONG).show();

                }
            }
        });*/
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {


            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getReadChatStatus(readChatCallback, userId, receiver_id);

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }

        try {
            if (getIntent().getStringExtra("block_status").equalsIgnoreCase("1")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChatActivity.this);
                alertDialog.setCancelable(false);
                alertDialog.setTitle("Alert");
                alertDialog.setMessage("Unblock user then send a new message");
                alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        onBackPressed();
                    }
                });
                alertDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    Callback<ReadChatMain> readChatCallback = new Callback<ReadChatMain>() {
        @Override
        public void onResponse(Call<ReadChatMain> call, Response<ReadChatMain> response) {
            //progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                //imgView_ChatList.setImageDrawable(getResources().getDrawable(R.drawable.chat_unread));
            }
        }

        @Override
        public void onFailure(Call<ReadChatMain> call, Throwable t) {
            //progressBar.setVisibility(View.GONE);
        }
    };

    public static int getLastVisiblePosition(RecyclerView rv) {


        if (rv != null) {
            final RecyclerView.LayoutManager layoutManager = rv
                    .getLayoutManager();
            if (layoutManager instanceof LinearLayoutManager) {
                return ((LinearLayoutManager) layoutManager)
                        .findLastVisibleItemPosition();
            }
        }

        return 0;
    }
    Callback<ChatMain> fetchAllChatCallback = new Callback<ChatMain>() {

        @Override
        public void onResponse(Call<ChatMain> call, Response<ChatMain> response) {
            progressBar.setVisibility(View.GONE);
            Log.e("inside=", "insideCallback");

            chatMainDataList.clear();
            if (response.isSuccessful()) {
                if (response.body().getResult() != null) {
//                    for (int i=0;i<response.body().getResult().size();i++){
//                        if (response.body().getResult().get(i).getType().equalsIgnoreCase("2")){
//                            ChatMainData data = response.body().getResult().get(i);
//                            try {
//                                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
//                                mmr.setDataSource(data.getAudio(), new HashMap<String, String>());
//                                String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
//                                int millSecond = Integer.parseInt(durationStr)/1000;
//                                data.setDuration(String.valueOf(millSecond));
//                            }catch (Exception e){
//                                e.printStackTrace();
//                            }
//                            chatMainDataList.add(data);
//
//                        }else {
//                            chatMainDataList.add(response.body().getResult().get(i));
//                        }
//                    }
                    chatMainDataList.addAll(response.body().getResult());

                    if (flag) {
                        recyclerView_Chat.setAdapter(chatAdapter);
                        recyclerView_Chat.scrollToPosition(chatMainDataList.size() - 1);
                        flag = false;
                        Log.e("Inside=", "InsideTrue");
                    } else {
                        if (size != chatMainDataList.size()) {
                            size = chatMainDataList.size();

                            //recyclerView_Chat.scrollToPosition(chatMainDataList.size() - 1);
                            Log.e("Inside=", "InsideFalse");
                            recyclerView_Chat.scrollToPosition(chatMainDataList.size() - 1);
                            chatAdapter.notifyDataSetChanged();
                        }

                    }
                }

            }

        }

        @Override
        public void onFailure(Call<ChatMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    private void callEvery4SecondForLatestChat() {
        m_Handler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                // Log.e("handlerCheck=","handler");
                m_Handler.postDelayed(mRunnable, 3000);// move this inside the run method
                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    RetrofitHelper.getInstance().getChatList(fetchAllChatCallback, userId, receiver_id);
                    Log.e("Inside=", "Inside Handler");

                } else {
                    //Snackbar.make(imgView_back, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        };
        mRunnable.run(); // missing
    }


    @OnClick(R.id.imgView_back)
    public void backImageClick(View view) {
        onBackPressed();
    }

    public String getCurrentTimeFromDevice(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String currentDateandTime = sdf.format(new Date());
        Log.e("DateAndTime=",currentDateandTime);
        return currentDateandTime;
    }


    @OnClick(R.id.tv_send)
    public void backSendClick(View view) {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            Log.e("ISNETWORK",NetworkUtil.isInternetAvailable()+"");
            String message = et_sendMessage.getText().toString();
            String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
            if (!message.equalsIgnoreCase("")) {
                //progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getChatStartFromAnotherUser(chatForUserCallback, userId, receiver_id, message, outputFile, "1");
               /* if (chatAdapter != null) {
                    ChatMainData chatFromUserMainData = new ChatMainData();
                    chatFromUserMainData.setId("");
                    chatFromUserMainData.setSenderId("1");
                    chatFromUserMainData.setReciverId("2");
                    chatFromUserMainData.setMsg(message);
                    chatFromUserMainData.setImage("");
                    chatFromUserMainData.setStatus("");
                    chatFromUserMainData.setDate(getCurrentTimeFromDevice());
                    chatFromUserMainData.setCDate("");
                    chatFromUserMainData.setMe(1);
                    chatFromUserMainData.setFirstName("");
                    chatFromUserMainData.setLastName("");
                    chatFromUserMainData.setProfileImage("");
                    et_sendMessage.setText("");
                    chatAdapter.addChat(chatFromUserMainData);
                    recyclerView_Chat.scrollToPosition(chatMainDataList.size() - 1);
                    //recycler_comments.smoothScrollToPosition(commentMainDataList.size() - 1);

                } else {
                    //chatAdapter.add(commentMainData);
                    recyclerView_Chat.setAdapter(chatAdapter);
                }*/
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.enter_message), Toast.LENGTH_LONG).show();

            }


        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<ChatFromUserMain> chatForUserCallback = new Callback<ChatFromUserMain>() {
        @Override
        public void onResponse(Call<ChatFromUserMain> call, Response<ChatFromUserMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {

                if (response.body().getMessage() != null) {
                    if (chatAdapter != null) {
                        //chatAdapter.removeLastIndexData();
                        ChatMainData chatFromUserMainData = new ChatMainData();
                        chatFromUserMainData.setId(response.body().getResult().getId());
                        chatFromUserMainData.setSenderId(response.body().getResult().getSenderId());
                        chatFromUserMainData.setReciverId(response.body().getResult().getReciverId());
                        chatFromUserMainData.setMsg(response.body().getResult().getMsg());
                        chatFromUserMainData.setImage(response.body().getResult().getImage());
                        chatFromUserMainData.setStatus(response.body().getResult().getStatus());
                        chatFromUserMainData.setDate(response.body().getResult().getDate());
                        chatFromUserMainData.setCDate(response.body().getResult().getCDate());
                        chatFromUserMainData.setMe(response.body().getResult().getMe());
                        chatFromUserMainData.setFirstName(response.body().getResult().getFirstName());
                        chatFromUserMainData.setLastName(response.body().getResult().getLastName());
                        chatFromUserMainData.setProfileImage(response.body().getResult().getProfileImage());
                        chatFromUserMainData.setAudio(response.body().getResult().getAudio());
                        chatFromUserMainData.setType(response.body().getResult().getType());
                        et_sendMessage.setText("");
                        chatAdapter.addChat(chatFromUserMainData);
                        recyclerView_Chat.scrollToPosition(chatMainDataList.size() - 1);
                        //recycler_comments.smoothScrollToPosition(commentMainDataList.size() - 1);

                    } else {
                        //chatAdapter.add(commentMainData);
                        recyclerView_Chat.setAdapter(chatAdapter);
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<ChatFromUserMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        m_Handler.removeCallbacksAndMessages(null);
    }



}
