package com.example.touch.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.emoji.text.EmojiCompat;

import androidx.emoji.text.FontRequestEmojiCompatConfig;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.provider.FontRequest;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.ChatListAdapter;
import com.example.touch.constants.BundledEmojiCompatConfig;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.ChatListMain;
import com.example.touch.models.ChatListMainData;
import com.example.touch.models.ReadChatMain;
import com.google.android.material.snackbar.Snackbar;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.icu.lang.UProperty.EMOJI;

public class ChatListActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView_ChatLists)
    RecyclerView recyclerView_ChatLists;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.imgView_back)
    ImageView imgView_back;


    List<ChatListMainData> chatListMainDataList;
    ChatListAdapter chatListAdapter;
    private SharedPreferencesData sharedPreferencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_chat_list);
        ButterKnife.bind(this);
        viewFinds();
    }

    @Override
    protected void onResume(){
        super.onResume();
        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getChatListTotal(getChatListCallback, userId);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }




    private static final boolean USE_BUNDLED_EMOJI = true;
   /* private void initEmojiCompat() {
        final EmojiCompat.Config config;
        if (USE_BUNDLED_EMOJI) {
            // Use the bundled font for EmojiCompat
            config = new BundledEmojiCompatConfig(getApplicationContext());
        } else {
            // Use a downloadable font for EmojiCompat
            final FontRequest fontRequest;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                fontRequest = new FontRequest(
                        "com",
                        "com.google.android.gms",
                        "Noto Color Emoji Compat",
                        R.array.com_google_android_gms_fonts_certs);
            }
            config = new FontRequestEmojiCompatConfig(getApplicationContext(), fontRequest);
        }

        config.setReplaceAll(true)
                .registerInitCallback(new EmojiCompat.InitCallback() {
                    @Override
                    public void onInitialized() {
                        //Log.i(TAG, "EmojiCompat initialized");
                    }

                    @Override
                    public void onFailed(@Nullable Throwable throwable) {
                        //Log.e(TAG, "EmojiCompat initialization failed", throwable);
                    }
                });

        EmojiCompat.init(config);
    }

    private static class InitCallback extends EmojiCompat.InitCallback {

        private final WeakReference<TextView> mRegularTextViewRef;

        InitCallback(TextView regularTextView) {
            mRegularTextViewRef = new WeakReference<>(regularTextView);
        }

        @Override
        public void onInitialized() {
            final TextView regularTextView = mRegularTextViewRef.get();
            if (regularTextView != null) {
                final EmojiCompat compat = EmojiCompat.get();
                final Context context = regularTextView.getContext();
                regularTextView.setText(
                        compat.process(context.getString(R.string.regular_text_view, EMOJI)));
            }
        }

    }*/

    private void viewFinds() {
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        chatListMainDataList = new ArrayList<>();
        recyclerView_ChatLists.setHasFixedSize(true);
        recyclerView_ChatLists.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));





    }


    Callback<ChatListMain> getChatListCallback=new Callback<ChatListMain>() {
        @Override
        public void onResponse(Call<ChatListMain> call, Response<ChatListMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getResult()!=null){
                    chatListMainDataList.clear();
                    for(int i=0;i<response.body().getResult().size();i++){
                        if (response.body().getResult().get(i).getProfile_on().equalsIgnoreCase("1")){
                            chatListMainDataList.add(response.body().getResult().get(i));
                        }
                    }
//                    chatListMainDataList.addAll(response.body().getResult());
                    chatListAdapter = new ChatListAdapter(getApplicationContext(), chatListMainDataList);
                    recyclerView_ChatLists.setAdapter(chatListAdapter);
                }
            }
        }

        @Override
        public void onFailure(Call<ChatListMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.imgView_back)
    public void backClick(View view){
        onBackPressed();
    }
}
