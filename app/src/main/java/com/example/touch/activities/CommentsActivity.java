package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.emoji.widget.EmojiEditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;

import android.os.Bundle;
import android.os.Handler;
import android.text.InputType;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.CommentAdapter;
import com.example.touch.models.CommentMainData;
import com.example.touch.models.CommentedMain;
import com.example.touch.models.CommentsMain;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsActivity extends AppCompatActivity {
    @BindView(R.id.circularImageView_UserImage)
    CircularImageView circularImageView_UserImage;


    @BindView(R.id.imgView_commentBack)
    ImageView imgView_commentBack;

    private SharedPreferencesData sharedPreferencesData;

    @BindView(R.id.recycler_comments)
    RecyclerView recycler_comments;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.et_comments)
    EmojiEditText et_comments;

    @BindView(R.id.tv_post)
    TextView tv_post;

    private String postId;
    CommentAdapter commentAdapter;

    private Handler m_Handler;
    private Runnable mRunnable;
    int size = 0;
    private boolean flag = true;
    List<CommentMainData> commentMainDataList;
    EmojIconActions emojIcon;
    View rootView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_comments);
        ButterKnife.bind(this);
        viewFinds();

    }

    private void viewFinds() {
        /*rootView=findViewById(R.id.layout_rootView);
        et_comments.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD|InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        emojIcon = new EmojIconActions(this, rootView, et_comments, circularImageView_UserImage,"#F44336","#e8e8e8","#f4f4f4");
        emojIcon.ShowEmojIcon();*/



        recycler_comments.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        ((SimpleItemAnimator) recycler_comments.getItemAnimator()).setSupportsChangeAnimations(false);
        commentMainDataList = new ArrayList<>();
        commentAdapter = new CommentAdapter(getApplicationContext(), commentMainDataList);
        //et_comments.setUseSystemDefault(true);

        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        String userImage = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);
        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        postId = getIntent().getStringExtra(Constants.POSTID);
        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
            Picasso.with(getApplicationContext())
                    .load(userImage)
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(circularImageView_UserImage);
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getCommentsList(commentsCallback, postId);

            } else {
                Snackbar.make(imgView_commentBack, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(imgView_commentBack, getResources().getString(R.string.please_login_to_comment), Snackbar.LENGTH_LONG).show();
        }

        callEvery4SecondForLatestComments();
    }

    @OnClick(R.id.imgView_commentBack)
    public void backClick(View view) {
        onBackPressed();
    }


    Callback<CommentsMain> commentsCallback = new Callback<CommentsMain>(){

        @Override
        public void onResponse(Call<CommentsMain> call, Response<CommentsMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getResult() != null) {


                    commentMainDataList.clear();
                    commentMainDataList.addAll(response.body().getResult());
                    if (flag) {
                        recycler_comments.setAdapter(commentAdapter);
                        size = commentMainDataList.size();
                        flag = false;
                    } else {
                        if (size != commentMainDataList.size()) {
                            size = commentMainDataList.size();
                            //commentAdapter = new CommentAdapter(getApplicationContext(), commentMainDataList);
                            //recycler_comments.setAdapter(commentAdapter);
                            recycler_comments.smoothScrollToPosition(commentMainDataList.size() - 1);
                            commentAdapter.notifyDataSetChanged();

                        }
                    }


                }
            }

        }

        @Override
        public void onFailure(Call<CommentsMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    @OnClick(R.id.tv_post)
    public void postClicked(View view) {

        //String userImage=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.PROFILE_IMAGE);
        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        postId = getIntent().getStringExtra(Constants.POSTID);
        String postUserId = getIntent().getStringExtra("post_user_id");
        String comments = et_comments.getText().toString();
        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                if (!comments.equalsIgnoreCase("")) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().getCommentOnPost(commentsPostCallback, userId, comments, postId, postUserId);

                } else {
                    Snackbar.make(imgView_commentBack, getResources().getString(R.string.please_enter_comment), Snackbar.LENGTH_LONG).show();
                }

            } else {
                Snackbar.make(imgView_commentBack, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
        }
    }

    Callback<CommentedMain> commentsPostCallback = new Callback<CommentedMain>() {
        @Override
        public void onResponse(Call<CommentedMain> call, Response<CommentedMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess() == 1) {
                    if (response.body().getResult() != null) {
                        CommentMainData commentMainData = new CommentMainData();
                        commentMainData.setId(response.body().getResult().get(0).getId());
                        commentMainData.setParentId(response.body().getResult().get(0).getParentId());
                        commentMainData.setUserId(response.body().getResult().get(0).getUserId());
                        commentMainData.setPostId(response.body().getResult().get(0).getPostId());
                        commentMainData.setComment(response.body().getResult().get(0).getComment());
                        commentMainData.setStatus(response.body().getResult().get(0).getStatus());
                        commentMainData.setUserImage(response.body().getResult().get(0).getUserImage());
                        commentMainData.setFirstName(response.body().getResult().get(0).getFirstName());
                        commentMainData.setLastName(response.body().getResult().get(0).getLastName());
                        commentMainData.setCommentedDate(response.body().getResult().get(0).getCommentedDate());
                        commentMainData.setCommented_before(response.body().getResult().get(0).getCommented_before());

                        if (commentAdapter != null) {
                            commentAdapter.addComments(commentMainData);
                            et_comments.setText("");

                            recycler_comments.scrollToPosition(commentMainDataList.size() - 1);
                            //recycler_comments.smoothScrollToPosition(commentMainDataList.size() - 1);
                            //commentMainDataList.add(commentMainData);
                        } else {
                            commentMainDataList.add(commentMainData);
                            recycler_comments.setAdapter(commentAdapter);
                        }

                    }
                }
                else {
                    Snackbar.make(progressBar, response.body().getMessage(), Snackbar.LENGTH_LONG).show();

                }
            }
        }

        @Override
        public void onFailure(Call<CommentedMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    private void callEvery4SecondForLatestComments() {
        m_Handler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                // Log.e("handlerCheck=","handler");
                m_Handler.postDelayed(mRunnable, 4000);// move this inside the run method
                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    RetrofitHelper.getInstance().getCommentsList(commentsCallback, postId);


                } else {
                    Snackbar.make(imgView_commentBack, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        };
        mRunnable.run(); // missing
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        m_Handler.removeCallbacksAndMessages(null);

    }
}
