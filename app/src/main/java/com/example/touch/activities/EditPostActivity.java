package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.deep.videotrimmer.utils.FileUtils;
import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.HastagAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.AnotherUserDetails;
import com.example.touch.models.AnotherUserProfileResult;
import com.example.touch.models.HastagMain;
import com.example.touch.models.HastagMainData;
import com.example.touch.models.MultipleImageMain;
import com.example.touch.models.SearchUserListData;
import com.example.touch.utility.SpaceTokenizer;
import com.example.touch.utility.VideoTrimmerActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.model.AspectRatio;

import net.vrgsoft.videcrop.VideoCropActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPostActivity extends AppCompatActivity {


    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1001;
    @BindView(R.id.imageView_selected)
    ImageView imageView_selected;

    String outVideoUri = "";
    private static final int CROP_REQUEST = 200;
    @BindView(R.id.imageView_close)
    ImageView imageView_close;
    @BindView(R.id.circularImageView_UserImage)
    ImageView circularImageView_UserImage;
    @BindView(R.id.add_location)
    RelativeLayout add_location;
    @BindView(R.id.tv_addLocation)
    TextView tv_addLocation;
    @BindView(R.id.tv_share)
    TextView tv_share;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.layout_close)
    LinearLayout layout_close;
    @BindView(R.id.recylerview_hastag)
    RecyclerView recylerview_hastag;
    @BindView(R.id.et_getCaption)
    MultiAutoCompleteTextView et_getCaption;
    @BindView(R.id.hastagEt1)
    AutoCompleteTextView hastagEt1;
    @BindView(R.id.hastagEt2)
    AutoCompleteTextView hastagEt2;
    @BindView(R.id.hastagEt3)
    AutoCompleteTextView hastagEt3;
    @BindView(R.id.hastagEt4)
    AutoCompleteTextView hastagEt4;
    @BindView(R.id.hastagEt5)
    AutoCompleteTextView hastagEt5;
    @BindView(R.id.hastag1CloseImg)
    ImageView hastag1CloseImg;
    @BindView(R.id.hastag2CloseImg)
    ImageView hastag2CloseImg;
    @BindView(R.id.hastag3CloseImg)
    ImageView hastag3CloseImg;
    @BindView(R.id.hastag4CloseImg)
    ImageView hastag4CloseImg;
    public static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    private final int REQUEST_VIDEO_TRIMMER_RESULT = 342;

    @BindView(R.id.hastag5CloseImg)
    ImageView hastag5CloseImg;
    @BindView(R.id.scrollview_smiley)
    ScrollView scrollview_smiley;
    ArrayList<String> path;
    String trimData;
    String searchData;
    private List<HastagMainData> hastagMainData;
    private String userId;
    private SharedPreferencesData sharedPreferencesData;
    private String latitude = "";
    private String longitude = "";
    private String state = "", country = "", place_id = "", country_code = "", postId = "";
    private StringBuffer captionSB;
    ArrayList<String> captionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_edit_post);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        captionSB = new StringBuffer();
        et_getCaption.setThreshold(2);
        et_getCaption.setTokenizer(new SpaceTokenizer());
        path = new ArrayList<>();

        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        recylerview_hastag.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        try {
            Bundle bundle = getIntent().getBundleExtra("data");
            AnotherUserProfileResult data = (AnotherUserProfileResult) bundle.getSerializable("obj");
            et_getCaption.setText(data.getKeyword());
            tv_addLocation.setText(data.getLocation());
            hastagEt1.setText(data.getHashtagOne());
            hastagEt2.setText(data.getHashtagTwo());
            hastagEt3.setText(data.getHashtagThree());
            hastagEt4.setText(data.getHashtagFour());
            hastagEt5.setText(data.getHashtagFive());


            postId = data.id;
            if (data.getType().equalsIgnoreCase("image")) {
                Picasso.with(EditPostActivity.this)
                        .load(data.getBigImage())
                        .placeholder(R.drawable.profile_placeholder)
                        .error(R.drawable.profile_placeholder)
                        .into(imageView_selected);

                Bitmap bitmap = getBitmapFromURL(data.getBigImage());
//            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
//            String pathIMage = MediaStore.Images.Media.insertImage(EditPostActivity.this.getContentResolver(), bitmap, "Title", null);
                try {
                    path.clear();
                    progressBar.setVisibility(View.VISIBLE);
                    Uri uri1 = getImageUri1(EditPostActivity.this, bitmap);
                    String a = getRealPathFromURI(uri1);

                    File file = new File(a);
                    path.add(file.toString());
                    progressBar.setVisibility(View.GONE);
                    imageView_selected.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cropImage(Uri.parse(uri1.toString()));
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                Glide.with(EditPostActivity.this)
                        .load(R.drawable.loading_placeholder)
                        .fitCenter()
                        .centerCrop()
                        .into(imageView_selected);
                Downback DB = new Downback(data.getBigImage());
                DB.execute();


            }





            latitude = data.getLatitude();
            longitude = data.getLongitude();
            country = data.getCountry();
            country_code = data.getCountry_code();
            state = data.getState();
            place_id = data.getPlace_id();
        } catch (Exception e) {
            e.printStackTrace();
        }


//        String cropCheck = "";
//        try {
//            cropCheck = getIntent().getStringExtra("crop");
//            if (cropCheck.equalsIgnoreCase("1")) {
//                Log.e("Urrri1", getIntent().getStringExtra(Constants.IMAGEPATH));
//                Glide.with(EditPostActivity.this).load(getIntent().getStringExtra(Constants.IMAGEPATH)).into(imageView_selected);
//                path = new ArrayList<>();
//                Uri uri = Uri.parse(getIntent().getStringExtra(Constants.IMAGEPATH));
//                File file = new File(uri.getPath());
//                path.add(file.toString());
//                Log.e("Urrri1", file.toString());
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        hastag1CloseImg.setOnClickListener(view -> hastagEt1.setText(""));
        hastag2CloseImg.setOnClickListener(view -> hastagEt2.setText(""));
        hastag3CloseImg.setOnClickListener(view -> hastagEt3.setText(""));
        hastag4CloseImg.setOnClickListener(view -> hastagEt4.setText(""));
        hastag5CloseImg.setOnClickListener(view -> hastagEt5.setText(""));
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            captionList = new ArrayList<>();
            RetrofitHelper.getInstance().getSearchByHastag(getSearchDataCallback1, "");
            RetrofitHelper.getInstance().getSearchByUsername(getSearchUserDataCallback1, "");
        }

        hastagEt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")) {
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")) {
                                    hastagEt1.setText("");
                                } else {
                                    hastagEt1.setText(editable.toString().trim());
                                }
                                hastagEt2.requestFocus();
                                return;
                            }
                        }
                    } else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt1.setText("");
                    }
                }
            }
        });

        hastagEt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")) {
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")) {
                                    hastagEt2.setText("");
                                } else {
                                    hastagEt2.setText(editable.toString().trim());
                                }
                                hastagEt3.requestFocus();
                            }
                        }
                    } else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt2.setText("");
                    }
                }
            }
        });

        hastagEt3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")) {
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")) {
                                    hastagEt3.setText("");
                                } else {
                                    hastagEt3.setText(editable.toString().trim());
                                }
                                hastagEt4.requestFocus();
                            }
                        }
                    } else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt3.setText("");
                    }
                }
            }
        });

        hastagEt4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")) {
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")) {
                                    hastagEt4.setText("");
                                } else {
                                    hastagEt4.setText(editable.toString().trim());
                                }
                                hastagEt5.requestFocus();
                                return;
                            }
                        }
                    } else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt4.setText("");
                    }
                }
            }
        });

        hastagEt5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")) {
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")) {
                                    hastagEt5.setText("");
                                } else {
                                    hastagEt5.setText(editable.toString().trim());
                                }
                            }
                        }
                    } else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt5.setText("");
                    }
                }
            }
        });

        et_getCaption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")) {
                    if (!captionSB.toString().equalsIgnoreCase("")) {
                        editable.toString().replace(captionSB.toString(), "");
                    }
                    if (null != et_getCaption.getLayout() && et_getCaption.getLayout().getLineCount() > 5) {
                        et_getCaption.getText().delete(et_getCaption.getText().length() - 1, et_getCaption.getText().length());
                    }
                }
            }
        });

        add_location.setOnClickListener(view -> startAutocompleteActivity());
    }

    private class Downback extends AsyncTask<String, String, String> {
        String url;
        Downback(String url){
            this.url = url;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            final String vidurl = url;
            String a = downloadfile(vidurl);
            return a;

        }

        @Override
        protected void onPostExecute(String results){
            progressBar.setVisibility(View.GONE);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.loading_placeholder);
            requestOptions.error(R.drawable.loading_placeholder);
            Glide.with(EditPostActivity.this)
                    .load(path.get(0))
                    .fitCenter()
                    .centerCrop()
                    .apply(requestOptions)
                    .thumbnail(Glide.with(EditPostActivity.this).load(path.get(0)))
                    .into(imageView_selected);
            imageView_selected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    outVideoUri = "/storage/emulated/0/DCIM/Camera/newvideo122.mp4";
//                    startActivityForResult(VideoCropActivity.createIntent(EditPostActivity.this, path.get(0), outVideoUri), CROP_REQUEST);
                    startTrimActivity(Uri.fromFile(new File(path.get(0))));
                }
            });
        }


    }

    private void startTrimActivity(@NonNull Uri uri) {
        Intent intent = new Intent(EditPostActivity.this, VideoTrimmerActivity.class);
        intent.putExtra("EXTRA_VIDEO_PATH", FileUtils.getPath(this, uri));
        startActivityForResult(intent, REQUEST_VIDEO_TRIMMER_RESULT);
    }

    private String downloadfile(String vidurl) {
        SimpleDateFormat sd = new SimpleDateFormat("yymmhh");
        String date = sd.format(new Date());
        String name = "video" + date + ".mp4";
        try {
            String rootDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    + File.separator + "My_Video";
            File rootFile = new File(rootDir);
            rootFile.mkdir();
            File outFile = new File(rootDir+name);
            URL url = new URL(vidurl);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            FileOutputStream f = new FileOutputStream(outFile);
            InputStream in = c.getInputStream();
            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = in.read(buffer)) > 0) {
                f.write(buffer, 0, len1);
            }
            f.close();
            path.clear();
            path.add(outFile.getAbsolutePath());
            return outFile.getAbsoluteFile().toString();

        } catch (IOException e) {
            Log.d("Error....", e.toString());
            progressBar.setVisibility(View.GONE);
            return null;
        }
    }

    public static String queryName(ContentResolver resolver, Uri uri) {
        Cursor returnCursor =
                resolver.query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }

    public Uri getImageUri1(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 1, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "image1", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    public void cropImage(Uri sourceUri) {
        Uri destinationUri = Uri.fromFile(new File(getCacheDir(), queryName(getContentResolver(), sourceUri)));
        UCrop.Options options = new UCrop.Options();
        options.setCompressionQuality(80);
        options.setAspectRatioOptions(1,
                new AspectRatio("3:4", 3,4),
                new AspectRatio("16:9", 16,9),
                new AspectRatio("1:1", 1,1)
        );
        // applying UI theme
        options.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));

//        if (lockAspectRatio)
//            options.withAspectRatio(ASPECT_RATIO_X, ASPECT_RATIO_Y);

        if (true)
            options.withMaxResultSize(768, 432);

        UCrop.of(sourceUri, destinationUri)
                .withOptions(options)
                .start(this);
    }





    Callback<HastagMain> getSearchDataCallback1 = new Callback<HastagMain>() {
        @Override
        public void onResponse(Call<HastagMain> call, Response<HastagMain> response) {
            if (response.isSuccessful()) {
                if (response.body().getResult() != null && response.body().getResult().size() > 0) {
                    ArrayList<String> arrayList = new ArrayList<>();
                    for (int i = 0; i < response.body().getResult().size(); i++) {
                        arrayList.add(response.body().getResult().get(i).getTag());
                        captionList.add(response.body().getResult().get(i).getTag());
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EditPostActivity.this, android.R.layout.select_dialog_singlechoice, arrayList);
                    hastagEt1.setAdapter(arrayAdapter);
                    hastagEt2.setAdapter(arrayAdapter);
                    hastagEt3.setAdapter(arrayAdapter);
                    hastagEt4.setAdapter(arrayAdapter);
                    hastagEt5.setAdapter(arrayAdapter);
                }
            }
        }

        @Override
        public void onFailure(Call<HastagMain> call, Throwable t) {
        }
    };


    Callback<SearchUserListData> getSearchUserDataCallback1 = new Callback<SearchUserListData>() {
        @Override
        public void onResponse(Call<SearchUserListData> call, Response<SearchUserListData> response) {
            if (response.isSuccessful()) {
                if (response.body().getArrayList() != null && response.body().getArrayList().size() > 0) {
                    for (int i = 0; i < response.body().getArrayList().size(); i++) {
                        captionList.add("@" + response.body().getArrayList().get(i).getFirstName());
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(EditPostActivity.this, android.R.layout.select_dialog_singlechoice, captionList);
                    et_getCaption.setAdapter(arrayAdapter);
                }
            }
        }

        @Override
        public void onFailure(Call<SearchUserListData> call, Throwable t) {
        }
    };


    @OnClick(R.id.layout_close)
    public void closeClick(View view) {
        tv_addLocation.setText("Add Location");
    }


    @OnClick(R.id.imgView_commentBack)
    public void onBackClick(View view) {
        onBackPressed();
    }

    private void startAutocompleteActivity() {
        Places.initialize(getApplicationContext(), "AIzaSyCE_5xAkP50cQKk2suoeL0NByrstAR03gU");
        List<com.google.android.libraries.places.api.model.Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));

        List<TypeFilter> typeFilters = new ArrayList<>(Arrays.asList(TypeFilter.REGIONS));


// Create a RectangularBounds object.
        RectangularBounds bounds = RectangularBounds.newInstance(
                new LatLng(-33.880490, 151.184363),
                new LatLng(-33.858754, 151.229596));
        Intent autocompleteIntent =
                new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .setTypeFilter(typeFilters.get(0))

                        //.setInitialQuery("Search my data")
                        //.setTypeFilter(TypeFilter.ADDRESS)
                        .build(this);

        startActivityForResult(autocompleteIntent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    public Bitmap getBitmapFromURL(String src) {
        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    Log.e("TEST", "Place: " + place.getName() + "   " + place.getLatLng() + "  " + place.getAddress() + "  " + place.getId());
                    //tv_addLocation.setText(place.getAddress());
                    tv_addLocation.setText(place.getName());
                    latitude = place.getLatLng().latitude + "";
                    longitude = place.getLatLng().longitude + "";
                    place_id = place.getId();
                    getFullAddressFromLatAndLong(place.getLatLng().latitude, place.getLatLng().longitude);
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.

                if (data != null) {
                    Status status = Autocomplete.getStatusFromIntent(data);
                    Log.i("TEST", status.getStatusMessage());
                }

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
        else if (requestCode == UCrop.REQUEST_CROP){
//            if (resultCode == RESULT_OK) {
                handleUCropResult(data);
//            } else {
//                setResultCancelled();
//            }
        }
        else if(requestCode == CROP_REQUEST){
            try {
                if (!outVideoUri.equalsIgnoreCase("")) {
                    path.clear();
                    path.add(outVideoUri);
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.loading_placeholder);
                    requestOptions.error(R.drawable.loading_placeholder);
                    Glide.with(EditPostActivity.this)
                            .load(path.get(0))
                            .fitCenter()
                            .centerCrop()
                            .apply(requestOptions)
                            .thumbnail(Glide.with(EditPostActivity.this).load(path.get(0)))
                            .into(imageView_selected);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (requestCode == REQUEST_VIDEO_TRIMMER_RESULT) {
            try {
                Uri selectedVideoUri = data.getData();

                if (selectedVideoUri != null) {
                    String selectedVideoFile = data.getData().getPath();

                    ArrayList dataPath = new ArrayList();
                    dataPath.add(selectedVideoFile);
                    path.clear();
                    path.add(selectedVideoFile);
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.placeholder(R.drawable.loading_placeholder);
                    requestOptions.error(R.drawable.loading_placeholder);
                    Glide.with(EditPostActivity.this)
                            .load(path.get(0))
                            .fitCenter()
                            .centerCrop()
                            .apply(requestOptions)
                            .thumbnail(Glide.with(EditPostActivity.this).load(path.get(0)))
                            .into(imageView_selected);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void setResultCancelled() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
    }

    private void setResultOk(Uri imagePath) {
        Intent intent = new Intent();
        intent.putExtra("path", imagePath);
        setResult(Activity.RESULT_OK, intent);
    }

    private void handleUCropResult(Intent data) {
        try {
            if (data == null) {
                setResultCancelled();
                return;
            }
            final Uri resultUri = UCrop.getOutput(data);
//        path.clear();
//        File file = new File(resultUri.getPath());
//        path.add(file.toString());
//        path.add(resultUri.toString());
            Picasso.with(EditPostActivity.this)
                    .load(resultUri)
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(imageView_selected);
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(EditPostActivity.this.getContentResolver(), resultUri);


            AlertDialog.Builder mBuilder = new AlertDialog.Builder(EditPostActivity.this);
            @SuppressLint("InflateParams") View mView = getLayoutInflater().inflate(R.layout.dialog_black_and_white_filter, null);
            PhotoView photoView = mView.findViewById(R.id.imageView);
            CheckBox blackCB = mView.findViewById(R.id.blackCB);
            Button continueBtn = mView.findViewById(R.id.continueBtn);
            Glide.with(EditPostActivity.this).load(resultUri.toString()
            ).into(photoView);
            mBuilder.setView(mView);
            AlertDialog mDialog = mBuilder.create();
            mDialog.setCancelable(false);
            blackCB.setChecked(false);
            if (!blackCB.isChecked()) {
                continueBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Picasso.with(EditPostActivity.this)
                                .load(resultUri)
                                .placeholder(R.drawable.profile_placeholder)
                                .error(R.drawable.profile_placeholder)
                                .into(imageView_selected);
                        path.clear();
                        File file = new File(resultUri.getPath());
                        path.add(file.toString());
                        mDialog.dismiss();
                    }
                });
            }
            blackCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(EditPostActivity.this.getContentResolver(), resultUri);
                            Bitmap bitmap1 = toGrayscale(bitmap);
    //                                    photoView.setImageBitmap(bitmap1);
                            Uri uri1 = getImageUri1(EditPostActivity.this, bitmap1);
                            String a = getRealPathFromURI(uri1);
                            Glide.with(EditPostActivity.this).load(uri1.toString()
                            ).into(photoView);
                            continueBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Picasso.with(EditPostActivity.this)
                                            .load(uri1)
                                            .placeholder(R.drawable.profile_placeholder)
                                            .error(R.drawable.profile_placeholder)
                                            .into(imageView_selected);
                                    path.clear();
                                    File file = new File(a.toString());
                                    path.add(file.toString());
                                    mDialog.dismiss();
                                }
                            });

                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    } else {
                        Uri uri1 = getImageUri1(EditPostActivity.this, bitmap);
                        String a = getRealPathFromURI(uri1);
                        Glide.with(EditPostActivity.this).load(uri1.toString()
                        ).into(photoView);
                        continueBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Picasso.with(EditPostActivity.this)
                                        .load(uri1)
                                        .placeholder(R.drawable.profile_placeholder)
                                        .error(R.drawable.profile_placeholder)
                                        .into(imageView_selected);
                                path.clear();
                                File file = new File(a.toString());
                                path.add(file.toString());
                                mDialog.dismiss();
                            }
                        });
                    }
                }
            });
            mDialog.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    public void getFullAddressFromLatAndLong(double latitude, double longitude) {
        Locale aLocale = new Locale.Builder().setLanguage("en").setScript("Latn").setRegion("RS").build();
        Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            //System.out.println(addresses.get(0).getLocality());
            // addresses.get(0).getCountryName();
            country = addresses.get(0).getCountryName();
            state = addresses.get(0).getAdminArea();
            country_code = addresses.get(0).getCountryCode();
//            Log.e("hiiii", ""+addresses.get(0).getCountryCode());

//            Log.e("AllAddress=",addresses.get(0).getCountryName()+"  "+addresses.get(0).getAdminArea());


        }
//        else {
//            // do your stuff
//        }
    }


    String locationSend = "";

    @OnClick(R.id.shareImg)
    public void shareClick(View view){
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
//            Log.e("path=",path.get(0).toString());
            String location = tv_addLocation.getText().toString();
           /* try{
                File file=new File(path.get(0));

                String pathToReEncodedFile =
                        new VideoResolutionChanger().changeResolution(file);
                String s="test";
                Log.e("pathofvideo=",pathToReEncodedFile+s);
            }catch(Throwable t){*//* smth wrong :( *//*}*/

            if (location.equalsIgnoreCase("Add Location")){
                locationSend="";
            }else{
                locationSend = tv_addLocation.getText().toString();
            }
            if (state==null){
                state="";
            }
            String hastagStr1 = "", hastagStr2 = "", hastagStr3 = "", hastagStr4 = "", hastagStr5 = "";
            if (!hastagEt1.getText().toString().trim().equalsIgnoreCase("")){
                hastagStr1 = hastagEt1.getText().toString().trim();
//                hastagStr1 = hastagStr1.replace("#", "");
                hastagStr1 = hastagStr1.replaceAll("[-+.^:,'#]","");
            }
            if (!hastagEt2.getText().toString().trim().equalsIgnoreCase("")){
                hastagStr2 = hastagEt2.getText().toString().trim();
                hastagStr2 = hastagStr2.replaceAll("[-+.^:,'#]", "");
            }
            if (!hastagEt3.getText().toString().trim().equalsIgnoreCase("")){
                hastagStr3 = hastagEt3.getText().toString().trim();
                hastagStr3 = hastagStr3.replaceAll("[-+.^:,'#]", "");
            }
            if (!hastagEt4.getText().toString().trim().equalsIgnoreCase("")){
                hastagStr4 = hastagEt4.getText().toString().trim();
                hastagStr4 = hastagStr4.replaceAll("[-+.^:,'#]", "");
            }
            if (!hastagEt5.getText().toString().trim().equalsIgnoreCase("")){
                hastagStr5 = hastagEt5.getText().toString().trim();
                hastagStr5 = hastagStr5.replaceAll("[-+.^:,'#]", "");
            }
            RetrofitHelper.getInstance().updatePost(multipleImageMainCallback, path, userId,et_getCaption.getText().toString(),locationSend,latitude,longitude,country, country_code,state, place_id,
                    hastagStr1, hastagStr2, hastagStr3, hastagStr4, hastagStr5, postId);
        }else{
            Snackbar.make(tv_share, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<MultipleImageMain> multipleImageMainCallback = new Callback<MultipleImageMain>() {
        @Override
        public void onResponse(Call<MultipleImageMain> call, Response<MultipleImageMain> response) {
            try {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(EditPostActivity.this, MainActivity.class));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<MultipleImageMain> call, Throwable t) {
            t.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }
    };


}
