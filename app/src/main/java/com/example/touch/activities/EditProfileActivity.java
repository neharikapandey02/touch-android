package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.emoji.widget.EmojiEditText;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.SpinnerEditText;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.RealPathUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.ProfileVisibleResponse;
import com.example.touch.models.UpdateProfileMain;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.internal.Utils;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {
    private SharedPreferencesData sharedPreferencesData;
    @BindView(R.id.tv_firstName)
    EditText tv_firstName;

    @BindView(R.id.tv_lastName)
    EditText tv_lastName;

    @BindView(R.id.tv_number)
    EditText tv_number;

    @BindView(R.id.circularImageView_UserImage)
    ImageView circularImageView_UserImage;

    @BindView(R.id.coverImg)
    ImageView coverImg;

    @BindView(R.id.emailHideLL)
    LinearLayout emailHideLL;

    @BindView(R.id.genderLL)
    LinearLayout genderLL;

    @BindView(R.id.hairColorLL)
    LinearLayout hairColorLL;

    @BindView(R.id.weightLL)
    LinearLayout weightLL;

    @BindView(R.id.heightLL)
    LinearLayout heightLL;

    @BindView(R.id.breastSizeLL)
    LinearLayout breastSizeLL;

    @BindView(R.id.eyeColorLL)
    LinearLayout eyeColorLL;

    @BindView(R.id.hobbyLL)
    LinearLayout hobbyLL;

    @BindView(R.id.availabilityLL)
    LinearLayout availabilityLL;

    @BindView(R.id.ethnicityLL)
    LinearLayout ethnicityLL;

    @BindView(R.id.originLL)
    LinearLayout originLL;

    @BindView(R.id.countryLL)
    LinearLayout countryLL;

    @BindView(R.id.emailOnOffSwitch)
    Switch emailOnOffSwitch;

    @BindView(R.id.languageLL)
    LinearLayout languageLL;

    @BindView(R.id.outCallLL)
    LinearLayout outCallLL;

    @BindView(R.id.websiteLL)
    LinearLayout websiteLL;

    @BindView(R.id.aboutLL)
    LinearLayout aboutLL;

    @BindView(R.id.recordTextLL)
    LinearLayout recordTextLL;

    @BindView(R.id.recordBtnLL)
    LinearLayout recordBtnLL;

    @BindView(R.id.view1)
    View view1;


    @BindView(R.id.imgView_smiley)
    ImageView imgView_smiley;

    @BindView(R.id.btn_updateProfile)
    TextView btn_updateProfile;

    @BindView(R.id.btn_logout)
    Button btn_logout;

    @BindView(R.id.recordBtn)
    Button recordBtn;

    @BindView(R.id.playBtn)
    Button playBtn;


    @BindView(R.id.btn_changePassword)
    Button btn_changePassword;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private String userId;


    private Dialog dialog;
    private static final int PERMISSION_REQUEST_CODE_CAMERA = 101;
    private static final int PERMISSION_REQUEST_CODE_GALLERY = 102;
    private String userImage = "";

    @BindView(R.id.countDownTimerTv)
    TextView countDownTimerTv;

    @BindView(R.id.tv_email)
    EditText tv_email;

   /* @BindView(R.id.tv_dob)
    EditText tv_dob;*/

    @BindView(R.id.tv_gender)
    EditText tv_gender;


    @BindView(R.id.tv_about)
    EmojiEditText tv_about;

   /* @BindView(R.id.tv_hairColor)
    EditText tv_hairColor;*/


  /*  @BindView(R.id.tv_weight)
    EditText tv_weight;
    @BindView(R.id.tv_height)
    EditText tv_height;

    @BindView(R.id.tv_breast)
    EditText tv_breast;

    @BindView(R.id.tv_eyeColor)
    EditText tv_eyeColor;*/

    @BindView(R.id.tv_hobby)
    EditText tv_hobby;

    @BindView(R.id.tv_available)
    EditText tv_available;


   /* @BindView(R.id.tv_ethnicity)
    EditText tv_ethnicity;*/

    @BindView(R.id.tv_origin)
    EditText tv_origin;

    @BindView(R.id.tv_countryName)
    TextView tv_countryName;

    @BindView(R.id.tv_cityName)
    EditText tv_cityName;

    @BindView(R.id.tv_language)
    EditText tv_language;

   /* @BindView(R.id.tv_incall)
    EditText tv_incall;*/

    @BindView(R.id.tv_website)
    EditText tv_website;


    @BindView(R.id.scrollviewEditProfile)
    ScrollView scrollviewEditProfile;


    @BindView(R.id.spinner_age)
    Spinner spinner_age;

    @BindView(R.id.spinner_hairColor)
    Spinner spinner_hairColor;

    @BindView(R.id.spinner_weight)
    Spinner spinner_weight;

    @BindView(R.id.spinner_height)
    Spinner spinner_height;


    @BindView(R.id.spinner_breast)
    Spinner spinner_breast;

    @BindView(R.id.spinner_breastType)
    Spinner spinner_breastType;

    @BindView(R.id.spinner_eyeColor)
    Spinner spinner_eyeColor;

    @BindView(R.id.spinner_ethnicity)
    Spinner spinner_ethnicity;

    @BindView(R.id.spinner_callType)
    Spinner spinner_callType;


    @BindView(R.id.scrollview_smiley)
    ScrollView scrollview_smiley;


    String firstName;
    String lastName;
    String email;
    String phoneNumber;
    String dob;
    String gender;
    String userType;
    String about;
    String hairColor;
    String weight;
    String height;
    String breast;
    String eyeColor;
    String hobby;
    String available;
    String ethnicity;
    String origin;
    String countryName;
    String cityName;
    String language;
    String incall;
    String website;
    String profileImage;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1001;
    List ageSpinner, weightList, heightList, hairColorList, breastList, breastListType, eyeColorList, listEthnicity,listSpinner;
    EmojIconActions emojIcon;
    View rootView;
    MediaRecorder recorder;
    File audiofile = null;
    static final String TAG = "MediaRecording";
    MediaPlayer mediaPlayer;
    private String outputFile;
    private CounterClass counterClass;
    private int REQUEST_AUDIO_PERMISSION_RESULT = 100;
    int emailStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        viewFinds();
        recorder = new MediaRecorder();

        recordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recordBtn.getText().toString().equalsIgnoreCase(getString(R.string.record))){

                    try {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.RECORD_AUDIO) ==
                                    PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                                    == PackageManager.PERMISSION_GRANTED) {
                                // put your code for Version>=Marshmallow
                                recordBtn.setText(getString(R.string.stop));
                                startRecording(view);

                            } else {
                                if (shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO)) {
                                    Toast.makeText(EditProfileActivity.this,
                                            "App required access to audio", Toast.LENGTH_SHORT).show();
                                }

                                requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE
                                }, REQUEST_AUDIO_PERMISSION_RESULT);
                            }

                        } else {
                            startRecording(view);
                            // put your code for Version < Marshmallow
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    stopRecording(view);
                }
            }
        });

        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (playBtn.getText().toString().equalsIgnoreCase(getString(R.string.play))) {

                    mediaPlayer = new MediaPlayer();
                    try {
                        Log.e("audioF", outputFile);
                        mediaPlayer.setDataSource(outputFile);
                        mediaPlayer.prepare();
                        mediaPlayer.start();
                        playBtn.setText(getString(R.string.stop));
                        Toast.makeText(getApplicationContext(), "Playing Audio", Toast.LENGTH_LONG).show();

                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mediaPlayer) {
                                mediaPlayer.stop();
                                playBtn.setText(getString(R.string.play));
                            }
                        });
                    } catch (Exception e) {
                        // make something
                        e.printStackTrace();
                    }
                }else {
                    mediaPlayer.stop();
                    playBtn.setText(getString(R.string.play));
                    Toast.makeText(getApplicationContext(), "Stop Audio", Toast.LENGTH_LONG).show();
                }
            }
        });

        if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL_ON_OFF_STATUS).equalsIgnoreCase("1")) {
            emailStatus = 1;
            emailOnOffSwitch.setChecked(true);
        } else {
            emailOnOffSwitch.setChecked(false);
            emailStatus = 0;
        }

        emailOnOffSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    if (NetworkUtil.checkNetworkStatus(EditProfileActivity.this)) {
                        emailStatus = 1;
                        RetrofitHelper.getInstance().emailOnOff(profileVisible, userId, "1");
                    }
                }else {
                    if (NetworkUtil.checkNetworkStatus(EditProfileActivity.this)) {
                        emailStatus = 0;
                        RetrofitHelper.getInstance().emailOnOff(profileVisible, userId, "0");
                    }
                }
            }
        });
    }
    Callback<ProfileVisibleResponse> profileVisible = new Callback<ProfileVisibleResponse>() {
        @Override
        public void onResponse(Call<ProfileVisibleResponse> call, Response<ProfileVisibleResponse> response) {
            if (response.body().getSuccess() == 1){
                if (emailStatus == 1) {
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL_ON_OFF_STATUS, "1");
                    AlertDialog.Builder alert = new AlertDialog.Builder(EditProfileActivity.this);
                    alert.setTitle("Alert");
                    alert.setMessage("Email on successfully.");
                    alert.setPositiveButton("Ok", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    });
                    alert.show();
                }else {
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL_ON_OFF_STATUS, "0");
                    AlertDialog.Builder alert = new AlertDialog.Builder(EditProfileActivity.this);
                    alert.setTitle("Alert");
                    alert.setMessage("Email off successfully, nobody can see your email.");
                    alert.setPositiveButton("Ok", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    });
                    alert.show();
                }
            }
        }

        @Override
        public void onFailure(Call<ProfileVisibleResponse> call, Throwable t) {

        }
    };


    public void startRecording(View view) throws IOException {
        recordBtn.setEnabled(true);
        playBtn.setEnabled(false);
        outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.mp3";
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        recorder.setOutputFile(outputFile);

        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException ise) {
            // make something ...
        } catch (IOException ioe) {
            // make something
        }
        countDownTimerTv.setVisibility(View.VISIBLE);
        counterClass = new CounterClass(60000, 1000);
        counterClass.start();
        Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
    }

    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval){
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long l) {
            countDownTimerTv.setText(""+l / 1000);
        }

        @Override
        public void onFinish() {
            stopRecording(recordBtn);
        }
    }

    public void stopRecording(View view) {

        playBtn.setEnabled(true);
        recordBtn.setEnabled(false);

        try {
            recorder.stop();
        } catch(RuntimeException stopException) {
            // handle cleanup here
        }
        recorder.release();
        recorder = null;
        countDownTimerTv.setText("");
        countDownTimerTv.setVisibility(View.GONE);
        counterClass.cancel();
        Toast.makeText(getApplicationContext(), "Audio Recorder successfully", Toast.LENGTH_LONG).show();
    }
    protected void addRecordingToMediaLibrary() {
        //creating content values of size 4
        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
        values.put(MediaStore.Audio.Media.TITLE, "audio" + audiofile.getName());
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/3gpp");
        values.put(MediaStore.Audio.Media.DATA, audiofile.getAbsolutePath());

        //creating content resolver and storing it in the external content uri
        ContentResolver contentResolver = getContentResolver();
        Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Uri newUri = contentResolver.insert(base, values);

        //sending broadcast message to scan the media file so that it can be available
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, newUri));
        Toast.makeText(this, "Added File " + newUri, Toast.LENGTH_LONG).show();
    }




    @OnClick(R.id.tv_countryName)
    public void countryClick(View view) {
        startAutocompleteActivity();
    }


    private void startAutocompleteActivity() {
        Places.initialize(getApplicationContext(), "AIzaSyCE_5xAkP50cQKk2suoeL0NByrstAR03gU");
        List<Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));

        List<TypeFilter> typeFilters = new ArrayList<>(Arrays.asList(TypeFilter.REGIONS));

// Create a RectangularBounds object.
        RectangularBounds bounds = RectangularBounds.newInstance(
                new LatLng(-33.880490, 151.184363),
                new LatLng(-33.858754, 151.229596));
        Intent autocompleteIntent =
                new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .setTypeFilter(typeFilters.get(0))
                        .build(this);
        startActivityForResult(autocompleteIntent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }


    private void viewFinds() {

        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setSelectedItemId(R.id.navigation_profile);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });

        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());

        if (sharedPreferencesData != null) {
            tv_firstName.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.FIRST_NAME));
            tv_lastName.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.LAST_NAME));
            tv_number.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PHONE_NUMBER));
            tv_email.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL));
           // tv_dob.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DOB));
            tv_gender.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.GENDER));
            userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);

            tv_about.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ABOUT_US));
           // tv_hairColor.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HAIR_COLOR));
            //tv_weight.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.WEIGHT));
            //tv_height.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HEIGHT));
            //tv_breast.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.BREAST_SIZE));
            //tv_eyeColor.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EYE_COLOR));
            tv_hobby.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HOBBY));
            tv_available.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.AVAILABLE));
            //tv_ethnicity.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ETHNICITY));
            tv_origin.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ORIGIN));
            tv_countryName.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.CITY_NAME));
            tv_cityName.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.CITY_NAME));
            tv_language.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.LANGUAGE));
            incall = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.INCALL);
//Spinner data show here
            addSpinnerData(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DOB));
            addSpinnerHairColor(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HAIR_COLOR));
            addSpinnerWeight(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.WEIGHT));
            addSpinnerHeight(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HEIGHT));
            addSpinnerBreast(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.BREAST_SIZE));
            addSpinnerBreastType(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.BREAST_SIZE));
            addSpinnerEyeColor(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EYE_COLOR));
            addSpinnerEthnicity(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ETHNICITY));


            if (incall != null) {
                if (incall.equalsIgnoreCase("1")) {
                    incall = "In call";
                } else if (incall.equalsIgnoreCase("2")) {
                    incall = "Out Call";
                } else {
                    incall = "both";
                }
            }
            addSpinnerInCallOutCall(incall);
            //tv_incall.setText(incall);
            tv_website.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.WEBSITE));

            String userImage = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);

            if (userImage != null && (!userImage.equalsIgnoreCase(""))) {
                Picasso.with(getApplicationContext())
                        .load(userImage)
                        .placeholder(R.drawable.profile_placeholder)
                        .error(R.drawable.profile_placeholder)
                        .into(circularImageView_UserImage);
           Picasso.with(getApplicationContext())
                        .load(userImage)
                        .placeholder(R.drawable.profile_placeholder)
                        .error(R.drawable.profile_placeholder)
                        .into(coverImg);
            }

        }
        tv_about.setMovementMethod(new ScrollingMovementMethod());

        scrollviewEditProfile.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                tv_about.getParent().requestDisallowInterceptTouchEvent(false);

                return false;
            }
        });

        tv_about.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                tv_about.getParent().requestDisallowInterceptTouchEvent(true);

                return false;
            }
        });

        if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_TYPE).equalsIgnoreCase("1")){
            emailHideLL.setVisibility(View.GONE);
            genderLL.setVisibility(View.GONE);
            hairColorLL.setVisibility(View.GONE);
            weightLL.setVisibility(View.GONE);
            heightLL.setVisibility(View.GONE);
            breastSizeLL.setVisibility(View.GONE);
            eyeColorLL.setVisibility(View.GONE);
            hobbyLL.setVisibility(View.GONE);
            availabilityLL.setVisibility(View.GONE);
            ethnicityLL.setVisibility(View.GONE);
            originLL.setVisibility(View.GONE);
            countryLL.setVisibility(View.GONE);
            languageLL.setVisibility(View.GONE);
            outCallLL.setVisibility(View.GONE);
            websiteLL.setVisibility(View.GONE);
            aboutLL.setVisibility(View.GONE);
            recordTextLL.setVisibility(View.GONE);
            recordBtnLL.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.btn_logout)
    public void logoutClick(View view) {
        sharedPreferencesData.clearSharedPreferenceData(Constants.USER_LOGIN_SHARED);
        finish();
    }

    @OnClick(R.id.btn_updateProfile)
    public void updateButtonClick(View view) {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validate()) {
                progressBar.setVisibility(View.VISIBLE);
                sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
                firstName = tv_firstName.getText().toString();
                lastName = tv_lastName.getText().toString();
                email = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL);
                phoneNumber = tv_number.getText().toString();
                dob = spinner_age.getSelectedItem().toString();
                gender = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.GENDER);
                about = tv_about.getText().toString();
                if (spinner_hairColor.getSelectedItem().toString().equalsIgnoreCase(Constants.HAIR_COLOR_PROFILE)) {
                    hairColor = "";
                } else {
                    hairColor = spinner_hairColor.getSelectedItem().toString();
                }

                if (spinner_weight.getSelectedItem().toString().equalsIgnoreCase(Constants.WEIGHT_IN_CM)) {
                    weight = "";
                } else {
                    weight = spinner_weight.getSelectedItem().toString();
                }
                if (spinner_height.getSelectedItem().toString().equalsIgnoreCase(Constants.HEIGHT_IN_CM)) {
                    height = "";
                } else {
                    height = spinner_height.getSelectedItem().toString();
                }

                if (spinner_breast.getSelectedItem().toString().equalsIgnoreCase(Constants.BREAST_SIZE_PROFILE)) {
                    breast = "";
                } else {
                    if (spinner_breastType.getSelectedItem().toString().equalsIgnoreCase(Constants.SIZE)) {
                        breast = spinner_breast.getSelectedItem().toString() + " ";
                    } else {
                        breast = spinner_breast.getSelectedItem().toString() + " " + spinner_breastType.getSelectedItem().toString();
                    }
                }
                if (spinner_eyeColor.getSelectedItem().toString().equalsIgnoreCase(Constants.EYES)) {
                    eyeColor = "";
                } else {
                    eyeColor = spinner_eyeColor.getSelectedItem().toString();
                }


                hobby = tv_hobby.getText().toString();
                available = tv_available.getText().toString();

                if (spinner_ethnicity.getSelectedItem().toString().equalsIgnoreCase(Constants.ETHNICITY_PROFILE)) {
                    ethnicity = "";
                } else {
                    ethnicity = spinner_ethnicity.getSelectedItem().toString();
                }

                origin = tv_origin.getText().toString();
                countryName = tv_countryName.getText().toString();
                cityName = tv_cityName.getText().toString();
                language = tv_language.getText().toString();
                incall = (spinner_callType.getSelectedItemPosition() + 1) + "";
                website = tv_website.getText().toString();
//                Log.e("audioo", outputFile);
//                Log.e("audioo", userImage);
//                Log.e("audioo", incall);

                RetrofitHelper.getInstance().setUpdateProfile(updateProfileCallback, userImage, userId, firstName, lastName,
                        email, phoneNumber, dob, gender, about, hairColor, weight, height, breast, eyeColor, hobby, available,
                        ethnicity, origin, countryName, cityName, language, incall, website, outputFile);
            }
        }else {
            Snackbar.make(btn_updateProfile, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }

    //All spinner addede here for update profiles
    private void addSpinnerData(String age) {
        ageSpinner = new ArrayList();
        ageSpinner.add("Age");
        for (int i = 18; i <= 80; i++) {
            ageSpinner.add(i + "");
        }
        SpinnerEditText SpinnerEditText = new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, ageSpinner);
        spinner_age.setAdapter(SpinnerEditText);
        if (age!=null&&!age.equalsIgnoreCase("")){
            int spinnerPosition = SpinnerEditText.getPosition(age);
            spinner_age.setSelection(spinnerPosition);

        }else{
            if (age.equalsIgnoreCase("")){
                spinner_age.setSelection(0);
            }
        }
    }

    private void addSpinnerHairColor(String hairColor) {
        hairColorList = new ArrayList();
        hairColorList.add("Hair color");
        hairColorList.add("Brown");
        hairColorList.add("Blond");
        hairColorList.add("Red");
        hairColorList.add("Black");
        hairColorList.add("Light brown");
        hairColorList.add("Dark blond");
        SpinnerEditText SpinnerEditText = new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, hairColorList);
        spinner_hairColor.setAdapter(SpinnerEditText);
        if (hairColor!=null&&!hairColor.equalsIgnoreCase("")){
            int spinnerPosition = SpinnerEditText.getPosition(hairColor);
            spinner_hairColor.setSelection(spinnerPosition);
        }else{
            if (hairColor.equalsIgnoreCase("")){
                spinner_hairColor.setSelection(0);
            }
        }
    }

    private void addSpinnerHeight(String height) {
        heightList = new ArrayList();
        heightList.add("Height in cm");
        for (int i = 145; i <= 200; i++) {
            heightList.add(i + "");
        }
        SpinnerEditText SpinnerEditText = new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, heightList);
        spinner_height.setAdapter(SpinnerEditText);
        if (height!=null&&!height.equalsIgnoreCase("")){
            int spinnerPosition = SpinnerEditText.getPosition(height);
            spinner_height.setSelection(spinnerPosition);
        }else{
            if (height.equalsIgnoreCase("")){
                spinner_height.setSelection(0);
            }
        }
    }


    private void addSpinnerWeight(String weight) {
        weightList = new ArrayList();
        weightList.add("Weight in kg");
        for (int i = 40; i <= 100; i++) {
            weightList.add(i + "");
        }
        SpinnerEditText SpinnerEditText = new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, weightList);
        spinner_weight.setAdapter(SpinnerEditText);
        if (weight!=null&&!weight.equalsIgnoreCase("")){
            int spinnerPosition = SpinnerEditText.getPosition(weight);
            spinner_weight.setSelection(spinnerPosition);
        }else{
            if (weight.equalsIgnoreCase("")){
                spinner_weight.setSelection(0);
            }
        }
    }

    private void addSpinnerBreast(String breast) {
        String[] onlyB=breast.split(" ");
        breastList = new ArrayList();
        breastList.add("Breast size");
        for (int i = 70; i <= 100; i = i + 5) {
            breastList.add(i +"");
        }
        SpinnerEditText SpinnerEditText = new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, breastList);
        spinner_breast.setAdapter(SpinnerEditText);
        try {
            if (breast!=null&&!breast.equalsIgnoreCase("")){
                int spinnerPosition = SpinnerEditText.getPosition(onlyB[0]);
                spinner_breast.setSelection(spinnerPosition);
            }else{
                if (breast.equalsIgnoreCase("")){
                    spinner_breast.setSelection(0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addSpinnerBreastType(String breastType) {
        String[] onlyB=breastType.split(" ");
        breastListType = new ArrayList();
        breastListType.add("Size");
        breastListType.add("A");
        breastListType.add("B");
        breastListType.add("C");
        breastListType.add("D");
        breastListType.add("E");
        breastListType.add("F");

        SpinnerEditText SpinnerEditText = new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, breastListType);
        spinner_breastType.setAdapter(SpinnerEditText);
        if (breastType!=null&&!breastType.equalsIgnoreCase("")&&onlyB.length>1){
            int spinnerPosition = SpinnerEditText.getPosition(onlyB[1]);
            spinner_breastType.setSelection(spinnerPosition);
        }else{
            if (breastType.equalsIgnoreCase("")){
                spinner_breastType.setSelection(0);
            }
        }
    }


    private void addSpinnerEyeColor(String eyeColor) {
        eyeColorList = new ArrayList();
        eyeColorList.add("Eyes");
        eyeColorList.add("Black");
        eyeColorList.add("Brown");
        eyeColorList.add("Blue");
        eyeColorList.add("Green");
        eyeColorList.add("Grey");


        SpinnerEditText SpinnerEditText = new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, eyeColorList);
        spinner_eyeColor.setAdapter(SpinnerEditText);
        if (eyeColor!=null&&!eyeColor.equalsIgnoreCase("")){
            int spinnerPosition = SpinnerEditText.getPosition(eyeColor);
            spinner_eyeColor.setSelection(spinnerPosition);
        }else{
            if (eyeColor.equalsIgnoreCase("")){
                spinner_eyeColor.setSelection(0);
            }
        }
    }

    private void addSpinnerEthnicity(String ethnicity) {
        listEthnicity = new ArrayList();
        listEthnicity.add("Ethnicity");
        listEthnicity.add("Caucasian");
        listEthnicity.add("European");
        listEthnicity.add("African");
        listEthnicity.add("Arabian");
        listEthnicity.add("Latina");
        listEthnicity.add("Asian");
        listEthnicity.add("Indian");

        SpinnerEditText SpinnerEditText = new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, listEthnicity);
        spinner_ethnicity.setAdapter(SpinnerEditText);
        if (ethnicity!=null&&!ethnicity.equalsIgnoreCase("")){
            int spinnerPosition = SpinnerEditText.getPosition(ethnicity);
            spinner_ethnicity.setSelection(spinnerPosition);
        }else{
            if (ethnicity.equalsIgnoreCase("")){
                spinner_ethnicity.setSelection(0);
            }
        }
    }

    private void addSpinnerInCallOutCall(String calltype) {
        listSpinner=new ArrayList<>();
        listSpinner.add("In Call");
        listSpinner.add("Out Call");
        listSpinner.add("both");
        SpinnerEditText SpinnerEditText=new SpinnerEditText(getApplicationContext(),
                R.layout.custom_edittext_spinner, listSpinner);
        spinner_callType.setAdapter(SpinnerEditText);
        if (calltype!=null&&!calltype.equalsIgnoreCase("")){
            int spinnerPosition = SpinnerEditText.getPosition(calltype);
            spinner_callType.setSelection(spinnerPosition);
        }
    }


    Callback<UpdateProfileMain> updateProfileCallback = new Callback<UpdateProfileMain>() {

        @Override
        public void onResponse(Call<UpdateProfileMain> call, Response<UpdateProfileMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                //Snackbar.make(circularImageView_UserImage, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.FIRST_NAME, tv_firstName.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.LAST_NAME, tv_lastName.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PHONE_NUMBER, tv_number.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE, response.body().getUserImg());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL, tv_email.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DOB, dob);
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.GENDER, tv_gender.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ABOUT_US, tv_about.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HAIR_COLOR, hairColor);
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.WEIGHT, weight);
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HEIGHT, height);
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.BREAST_SIZE, breast);
                // sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.BREAST_TYPE, spinner_breastType.getSelectedItem().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EYE_COLOR,eyeColor);
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HOBBY, tv_hobby.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.AVAILABLE, tv_available.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ETHNICITY, ethnicity);
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ORIGIN, tv_origin.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.COUNTRY_NAME, tv_countryName.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.CITY_NAME, tv_countryName.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.LANGUAGE, tv_language.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.INCALL, incall);
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.WEBSITE, tv_website.getText().toString());
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.AUDIO, response.body().getUserData().getAudio());
                onBackPressed();

            }
        }

        @Override
        public void onFailure(Call<UpdateProfileMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.imgView_edit)
    public void editClick(View view) {
        dialog = new Dialog(EditProfileActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_select_dialogue);
        TextView tv_gallery = dialog.findViewById(R.id.tv_gallery);
        TextView tv_camera = dialog.findViewById(R.id.tv_camera);
        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED||ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {
                    requestGallery();
                } else {



                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PERMISSION_REQUEST_CODE_GALLERY);


                }


            }
        });
        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED) {
                    request();
                } else {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD&&android.os.Build.VERSION.SDK_INT <=android.os.Build.VERSION_CODES.P) {
                        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                        startActivityForResult(intent, PERMISSION_REQUEST_CODE_CAMERA);
                    }else{
                        cameraCall();
                    }



                }
            }
        });

        dialog.show();
    }

    File photoFile = null;
    private void cameraCall() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go

            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, PERMISSION_REQUEST_CODE_CAMERA);
            }
        }
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PERMISSION_REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imgv_check.setImageBitmap(imageBitmap);
        }
    }*/

    public void request() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_CAMERA);


    }

    public void requestGallery() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE_GALLERY);


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            dialog.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED &&ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED&& requestCode == PERMISSION_REQUEST_CODE_GALLERY) {

           /* Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, PERMISSION_REQUEST_CODE_GALLERY);*/

            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/*");
            startActivityForResult(intent, PERMISSION_REQUEST_CODE_GALLERY);


        } else if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_REQUEST_CODE_CAMERA) {


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD&&android.os.Build.VERSION.SDK_INT <=android.os.Build.VERSION_CODES.P) {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, PERMISSION_REQUEST_CODE_CAMERA);
            }else{
                cameraCall();
            }


        }
        else if (requestCode == REQUEST_AUDIO_PERMISSION_RESULT) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "Application will not have audio on record", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getApplicationContext(),
                        "Recording permission granted.", Toast.LENGTH_SHORT).show();
            }
            }
        else {
            Toast.makeText(getApplicationContext(), "Please allow permission", Toast.LENGTH_SHORT).show();
        }
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = circularImageView_UserImage.getWidth();
        int targetH = circularImageView_UserImage.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        Log.e("SizeOf",scaleFactor+"");

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = 4;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions);


        userImage=currentPhotoPath;
        circularImageView_UserImage.setImageBitmap(bitmap);
        coverImg.setImageBitmap(bitmap);

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (dialog!=null){
                dialog.cancel();
            }
            if (requestCode == PERMISSION_REQUEST_CODE_CAMERA) {


                Bitmap image;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD&& Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                    image= (Bitmap) data.getExtras().get("data");
                    circularImageView_UserImage.setImageBitmap(image);
                    coverImg.setImageBitmap(image);
                    Uri tempUri = getImageUri(getApplicationContext(),image);

                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    File finalFile = new File(getRealPathFromURI(tempUri));
                    userImage = finalFile.toString();
                    //imageList.add(finalFile.toString());
                    Log.e("pathOfImage", finalFile + "");
                }else{
                   /* Bundle extras = data.getExtras();
                    image = (Bitmap) extras.get("data");*/
                    setPic();
                }

               // imgv_check.setImageBitmap(imageBitmap);




               // Bitmap image = (Bitmap) data.getExtras().get("data");









            } else if (requestCode == PERMISSION_REQUEST_CODE_GALLERY && data != null) {
                //dialog.cancel();
                String realPath = "";
                // SDK < API11
                if (Build.VERSION.SDK_INT < 11) {
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getApplicationContext(), data.getData());
                    Log.e("pathOfImage", realPath + "");

                }

                // SDK >= 11 && SDK < 19
                else if (Build.VERSION.SDK_INT < 19) {
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(getApplicationContext(), data.getData());
                    Log.e("pathOfImage", realPath + "");
                }

                // SDK > 19 (Android 4.4)
                else {
                    try {
                        Bitmap image = getBitmapFromUri(data.getData());
                        Uri tempUri = getImageUri(getApplicationContext(), image);
                        File finalFile = new File(getRealPathFromURI(tempUri));

                        //realPath = RealPathUtil.getRealPathFromURI_API19(getApplicationContext(), data.getData());
                        //Log.e("pathOfImage", data.getData()+ "");
                        //realPath = getPath(getApplicationContext(), data.getData());
                        Log.e("pathOfImage", finalFile + "");
                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P){
                            realPath = finalFile.toString();
                        }else{
                            realPath = getPathFromInputStreamUri(getApplicationContext(),tempUri);
                        }


                    } catch (Exception e) {

                    }

                }

                userImage = realPath;
                Log.e("pathOfImage1", userImage + "");
                Uri selectedImage = data.getData();

                circularImageView_UserImage.setImageURI(selectedImage);
                coverImg.setImageURI(selectedImage);
                //getImages(data,requestCode,resultCode);

            } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        Log.i("TEST", "Place: " + place.getName() + ", " + place.getAddress());
                        tv_countryName.setText(place.getAddress());
                    }

                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    // TODO: Handle the error.

                    if (data != null) {
                        Status status = Autocomplete.getStatusFromIntent(data);
                        Log.i("TEST", status.getStatusMessage());
                    }

                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //getting path from gallery pics for sending on server
    public  String getPathFromInputStreamUri(Context context, Uri uri) {
        InputStream inputStream = null;
        String filePath = null;

        if (uri.getAuthority() != null) {
            try {
                inputStream = context.getContentResolver().openInputStream(uri);
                File photoFile = createTemporalFileFrom(inputStream);

                filePath = photoFile.getPath();

            } catch (FileNotFoundException e) {
              //  HttpLoggingInterceptor.Logger.printStackTrace(e);
            } catch (IOException e) {
             //   Logger.printStackTrace(e);
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return filePath;
    }

    private  File createTemporalFileFrom(InputStream inputStream) throws IOException {
        File targetFile = null;

        if (inputStream != null) {
            int read;
            byte[] buffer = new byte[8 * 1024];

            targetFile = createTemporalFile();
            OutputStream outputStream = new FileOutputStream(targetFile);

            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            outputStream.flush();

            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return targetFile;
    }

    private  File createTemporalFile() {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "_" + timeStamp + "_";
        return new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), imageFileName);
    }














    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    public File saveBitmapToFile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            Log.e("SizeOf",scale+"");

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }


    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private String getPath(Context context, Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }

                // TODO handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }



    public static String getDataColumn(Context context, Uri uri,
                                       String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getContentResolver() != null) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }

    @OnClick(R.id.btn_changePassword)
    public void changePasswordClick(View view) {
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            Intent intent = new Intent(getApplicationContext(), ChangePassword.class);
            startActivity(intent);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.login_first_change), Snackbar.LENGTH_LONG).show();
        }
    }

    public boolean validate() {
        if (tv_number.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_updateProfile, getResources().getString(R.string.please_enter_mobile_number), Snackbar.LENGTH_LONG).show();
            return false;

        }
        else if (tv_countryName.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_updateProfile, "Please enter city name", Snackbar.LENGTH_LONG).show();
            return false;

        }
        else if (tv_number.getText().toString().length()<10) {
            Snackbar.make(btn_updateProfile, getResources().getString(R.string.please_enter_valid_mobile), Snackbar.LENGTH_LONG).show();
            return false;

        } else if (spinner_age.getSelectedItemPosition()==0) {
            Snackbar.make(btn_updateProfile, getResources().getString(R.string.please_select_age), Snackbar.LENGTH_LONG).show();
            return false;
        }
//        else if (tv_about.getText().toString().equalsIgnoreCase("")) {
////            Snackbar.make(btn_updateProfile, getResources().getString(R.string.please_enter_about), Snackbar.LENGTH_LONG).show();
////            return false;
////        }
        return true;
    }
}
