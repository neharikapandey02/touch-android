package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.FollowersListAdapter;
import com.example.touch.adapters.FollowingListAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.constants.SingleShotLocationProvider;
import com.example.touch.models.FollowingMain;
import com.example.touch.models.FollowingMainData;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowersActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView_Following)
    RecyclerView recyclerView_Following;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.imgView_FollowingBack)
    ImageView imgView_FollowingBack;
    private String fromAnotherUserId;
    private boolean status=true;

    private static int PERMISSION_ACCESS_FINE_LOCATION=104;
    private SharedPreferencesData sharedPreferencesData;
    private float latitude;
    private float longitude;
    private String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_followers);
        ButterKnife.bind(this);
        viewFinds();
    }
    private void viewFinds() {
        fromAnotherUserId=getIntent().getStringExtra(Constants.ANOTHER_USER_ID);
        recyclerView_Following.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

            if (fromAnotherUserId!=null&&!fromAnotherUserId.equalsIgnoreCase("")){
                progressBar.setVisibility(View.VISIBLE);
                status=false;
                RetrofitHelper.getInstance().getFollowersList(followingListCallback,fromAnotherUserId);
            }else{
                progressBar.setVisibility(View.VISIBLE);
                status=false;
                RetrofitHelper.getInstance().getFollowersList(followingListCallback,userId);
            }


        }else{
            Snackbar.make(recyclerView_Following,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
        }

    }

    Callback<FollowingMain> followingListCallback=new Callback<FollowingMain>(){

        @Override
        public void onResponse(Call<FollowingMain> call, Response<FollowingMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                List<FollowingMainData> followingMainData=new ArrayList<>();
                followingMainData.clear();
                if (response.body().getResult()!=null){
                    followingMainData.addAll(response.body().getResult());



                    FollowersListAdapter followingListAdapter = new FollowersListAdapter(getApplicationContext(),followingMainData,progressBar,status);
                    recyclerView_Following.setAdapter(followingListAdapter);
                }else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_followers_user), Toast.LENGTH_SHORT).show();
                }

            }
        }

        @Override
        public void onFailure(Call<FollowingMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.imgView_FollowingBack)
    public void backClick(View view){
        onBackPressed();
    }
}
