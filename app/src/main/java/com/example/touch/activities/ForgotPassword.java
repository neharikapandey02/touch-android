package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.models.ForgotPasswordMain;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassword extends AppCompatActivity {
    @BindView(R.id.btn_login)
    Button btn_login;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);


    }
    //Continue OnClick Listner
    @OnClick(R.id.btn_login)
    public void continueButtonClick(View view) {
        if (!et_email.getText().toString().equalsIgnoreCase("")){
            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getForgotPassword(changePasswordCallback, et_email.getText().toString());

            } else {
                Snackbar.make(btn_login, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
        }else{
            Snackbar.make(btn_login,getResources().getString(R.string.please_enter_valid_email),Snackbar.LENGTH_LONG).show();
        }

    }


    //All Callback Here Below
    public Callback<ForgotPasswordMain> changePasswordCallback = new Callback<ForgotPasswordMain>() {
        @Override
        public void onResponse(Call<ForgotPasswordMain> call, Response<ForgotPasswordMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                Snackbar.make(btn_login, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                Log.e("Success=", response.body().getMessage());
            }

        }

        @Override
        public void onFailure(Call<ForgotPasswordMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


}
