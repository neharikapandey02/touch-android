package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.SearchLatestPostAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.models.ChangePasswordMain;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.ListImagesByLocation;
import com.example.touch.models.LocationSearchMain;
import com.example.touch.models.LocationSearchMainData;
import com.example.touch.models.SearchMainData;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageListByLocationActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView_ListImagesByLocation)
    RecyclerView recyclerView_ListImagesByLocation;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.tv_hastagsTitle)
    TextView tv_hastagsTitle;

    @BindView(R.id.latlongtest)
    TextView latlongtest;

    List<HomeMainData> listdata;
    String locationSearchKey;
    private String latitude;
    private String longitude, place_id;
    private String key;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_image_list_by_location);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setSelectedItemId(R.id.navigation_dashboard);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(ImageListByLocationActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(ImageListByLocationActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(ImageListByLocationActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(ImageListByLocationActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(ImageListByLocationActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });

        listdata=new ArrayList<>();
        //Intent intent = getIntent();
        //Bundle args = intent.getBundleExtra(Constants.BUNDLE);
        // listdata= (ArrayList<SearchMainData>) args.getSerializable(Constants.LIST_DATA);
        // Log.e("Size=",listdata.size()+"");
        key=getIntent().getStringExtra(Constants.KEY);
        latitude=getIntent().getStringExtra(Constants.LATITUDE);
        longitude=getIntent().getStringExtra(Constants.LONGITUDE);
        place_id=getIntent().getStringExtra("place_id");
        String countryCode = getIntent().getStringExtra("country_code");
        String countryType = getIntent().getStringExtra("country_type");

        //latlongtest.setText("LAT="+latitude+"  "+"LONG="+longitude+"  "+"key="+key);
        //locationSearchKey = getIntent().getStringExtra(Constants.BUNDLE);
        recyclerView_ListImagesByLocation.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getLocationImages(locationSearchCallback, latitude,longitude,key, place_id, countryCode, countryType);
        }else
            {
                Snackbar.make(imageViewBack, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
        }


    @OnClick(R.id.imageViewBack)
    public void backClicked(View view){
        onBackPressed();
    }



    Callback<LocationSearchMain> locationSearchCallback = new Callback<LocationSearchMain>() {

        @Override
        public void onResponse(Call<LocationSearchMain> call, Response<LocationSearchMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                //Snackbar.make(imageViewBack, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                if (response.body().getSuccess() == 1) {
                    listdata.clear();
                    for (int i=0;i<response.body().getResult().size();i++){
//                        if (response.body().getResult().get(i).getType().equalsIgnoreCase("image")){
                            listdata.add(response.body().getResult().get(i));
//                        }
                    }
                    SearchLatestPostAdapter adapter = new SearchLatestPostAdapter(ImageListByLocationActivity.this, listdata);
                    recyclerView_ListImagesByLocation.setAdapter(adapter);
//                    ListImagesByLocation listImagesByLocation = new ListImagesByLocation(getApplicationContext(), listdata);
//                    recyclerView_ListImagesByLocation.setAdapter(listImagesByLocation);
                }

            }
        }

        @Override
        public void onFailure(Call<LocationSearchMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
