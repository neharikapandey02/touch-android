package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.SearchLatestPostAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.DeleteMain;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.ImageListTagAndName;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImagesListByTagAndName extends AppCompatActivity {
    @BindView(R.id.recyclerView_ListImagesByTagName)
    RecyclerView recyclerView_ListImagesByTagName;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;

    @BindView(R.id.tv_hastagsTitle)
    TextView tv_hastagsTitle;


  /*  @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;*/

    private String tagId = "";
    private String tagName = "";
    List<HomeMainData> imagesListByTagAndNames;
    private SharedPreferencesData sharedPreferencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_images_list_by_tag_and_name);
        ButterKnife.bind(this);
        viewFinds();
    }

    @OnClick(R.id.imageViewBack)
    public void backClicked(View view){
        onBackPressed();
    }


    private void viewFinds() {
        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setSelectedItemId(R.id.navigation_dashboard);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(ImagesListByTagAndName.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(ImagesListByTagAndName.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(ImagesListByTagAndName.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(ImagesListByTagAndName.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(ImagesListByTagAndName.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());

        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);

        tagId = getIntent().getStringExtra(Constants.TAG_ID);
        tagName = getIntent().getStringExtra(Constants.TAG_NAME);

        if (tagName!=null&&!tagName.equalsIgnoreCase("")){
            tv_hastagsTitle.setText(tagName);
        }
        imagesListByTagAndNames = new ArrayList<>();
        recyclerView_ListImagesByTagName.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getListImageTagWithAddress(getListOfImages, tagId);
            RetrofitHelper.getInstance().addHastag(addHastagCallback, userId, tagName);

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }

    }

    Callback<DeleteMain> addHastagCallback = new Callback<DeleteMain>() {
        @Override
        public void onResponse(Call<DeleteMain> call, Response<DeleteMain> response) {

        }

        @Override
        public void onFailure(Call<DeleteMain> call, Throwable t) {

        }
    };

    Callback<ImageListTagAndName> getListOfImages = new Callback<ImageListTagAndName>() {
        @Override
        public void onResponse(Call<ImageListTagAndName> call, Response<ImageListTagAndName> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getResult()!=null){
                    imagesListByTagAndNames.clear();
                    imagesListByTagAndNames = response.body().getResult();
//                    for (int i=0;i<response.body().getResult().size();i++){
//                        if (response.body().getResult().get(i).getType().equalsIgnoreCase("image")){
//                            imagesListByTagAndNames.add(response.body().getResult().get(i));
//                        }
//
//                    }
                    SearchLatestPostAdapter adapter = new SearchLatestPostAdapter(ImagesListByTagAndName.this, imagesListByTagAndNames);
                    recyclerView_ListImagesByTagName.setAdapter(adapter);
//                    ListImagesTagsAndName listImagesTagsAndName = new ListImagesTagsAndName(getApplicationContext(), imagesListByTagAndNames);
//                    recyclerView_ListImagesByTagName.setAdapter(listImagesTagsAndName);
                }
            }
        }

        @Override
        public void onFailure(Call<ImageListTagAndName> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
