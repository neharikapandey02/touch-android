package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.ChatListAdapter;
import com.example.touch.adapters.LikeListAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.ChatListMain;
import com.example.touch.models.UserLikeMain;
import com.example.touch.models.UserLikeMainData;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LikesTotalList extends AppCompatActivity {
    @BindView(R.id.imgView_back)
    ImageView imgView_back;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.recyclerView_LikeLists)
    RecyclerView recyclerView_LikeLists;
    private String postId;



    private SharedPreferencesData sharedPreferencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_likes_total_list);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        postId=getIntent().getStringExtra(Constants.POSTID);
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        recyclerView_LikeLists.setHasFixedSize(true);
        recyclerView_LikeLists.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

            //String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);


            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getLikeTotal(getAllLikesCallback, postId);

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }
    Callback<UserLikeMain> getAllLikesCallback=new Callback<UserLikeMain>() {
        @Override
        public void onResponse(Call<UserLikeMain> call, Response<UserLikeMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getResult()!=null){
                    List<UserLikeMainData> userLikeMainData=new ArrayList<>();
                    userLikeMainData.clear();
                    userLikeMainData.addAll(response.body().getResult());
                   LikeListAdapter chatListAdapter = new LikeListAdapter(getApplicationContext(),userLikeMainData);
                    recyclerView_LikeLists.setAdapter(chatListAdapter);
                }
            }
        }

        @Override
        public void onFailure(Call<UserLikeMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
    @OnClick(R.id.imgView_back)
    public void backClick(View view){
        onBackPressed();
    }
}
