package com.example.touch.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.constants.SingleShotLocationProvider;
import com.example.touch.models.LoginMain;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static int PERMISSION_ACCESS_FINE_LOCATION = 103;
    @BindView(R.id.btn_login)
    Button btn_login;
    @BindView(R.id.tv_signup)
    TextView tv_signup;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_password)
    EditText et_password;
    @BindView(R.id.tv_forgotPassword)
    TextView tv_forgotPassword;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    SharedPreferencesData sharedPreferencesData;
    private float latitude;
    private float longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        et_email.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL));
        et_password.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PASSWORD));

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            SingleShotLocationProvider.requestSingleUpdate(getApplicationContext(),
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override
                        public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            Log.e("Location", "my location is " + location.latitude + "     " + location.longitude);
                            latitude = location.latitude;
                            longitude = location.longitude;

                        }
                    });
        } else {
            requestGallery();
        }

        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });
    }


    public void requestGallery() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_ACCESS_FINE_LOCATION) {
            SingleShotLocationProvider.requestSingleUpdate(getApplicationContext(),
                    location -> {
                        Log.e("Location", "my location is " + location.latitude + "     " + location.longitude);
                        latitude = location.latitude;
                        longitude = location.longitude;
                    });
        } else {
            latitude = 0;
            longitude = 0;
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.nearby), Toast.LENGTH_SHORT).show();
        }
    }


    @OnClick(R.id.btn_login)
    public void loginButtonClick(View view) {
        if (!et_email.getText().toString().equalsIgnoreCase("")) {
            if (!et_password.getText().toString().equalsIgnoreCase("")) {
                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    String device_id = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.DEVICE_ID);
                    RetrofitHelper.getInstance().getLogin(loginCallback, et_email.getText().toString(), et_password.getText().toString(), latitude + "", longitude + "", device_id);

                } else {
                    Snackbar.make(btn_login, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(btn_login, getResources().getString(R.string.please_enter_valid_password), Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(btn_login, getResources().getString(R.string.please_enter_valid_email), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.tv_signup)
    public void signupButtonClick(View view) {
        Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tv_forgotPassword)
    public void forgotPasswordClick(View view) {
        Intent intent = new Intent(getApplicationContext(), ForgotPassword.class);
        startActivity(intent);
    }

    Callback<LoginMain> loginCallback = new Callback<LoginMain>() {
        @Override
        public void onResponse(Call<LoginMain> call, Response<LoginMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getSuccess() == 1) {
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID, response.body().getUserDetails().getId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_TYPE, response.body().getUserDetails().getUserType());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.SOCIAL_MEDIA_ID, response.body().getUserDetails().getSocialMediaId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_NAME, response.body().getUserDetails().getUsername());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.FIRST_NAME, response.body().getUserDetails().getFirstName());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.LAST_NAME, response.body().getUserDetails().getLastName());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL, response.body().getUserDetails().getEmail());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE, response.body().getUserDetails().getProfileImage());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PASSWORD, response.body().getUserDetails().getPassword());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.COUNTRY_ID, response.body().getUserDetails().getCountryId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.LANGUAGE, response.body().getUserDetails().getLanguage());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PHONE_NUMBER, response.body().getUserDetails().getPhoneNumber());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DOB, response.body().getUserDetails().getDob());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.GENDER, response.body().getUserDetails().getGender());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.STATUS, response.body().getUserDetails().getStatus());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DEVICE_ID, response.body().getUserDetails().getDeviceId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DEVICE_TYPE, response.body().getUserDetails().getDeviceType());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ABOUT_US, response.body().getUserDetails().getAboutUs());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.CATEGORY_NAME, response.body().getUserDetails().getCategory());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.COUNTRY_NAME, response.body().getUserDetails().getCountry());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.CITY_NAME, response.body().getUserDetails().getCity());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HAIR_COLOR, response.body().getUserDetails().getHairColor());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.WEIGHT, response.body().getUserDetails().getWeight());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HEIGHT, response.body().getUserDetails().getHeight());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.AVAILABLE, response.body().getUserDetails().getAvailableFrom());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.BREAST_SIZE, response.body().getUserDetails().getBrestsize());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EYE_COLOR, response.body().getUserDetails().getEyeColor());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.HOBBY, response.body().getUserDetails().getHobby());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ETHNICITY, response.body().getUserDetails().getEthnicity());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ORIGIN, response.body().getUserDetails().getOrigin());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.COUNTRY_NAME, response.body().getUserDetails().getCountry());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.CITY_NAME, response.body().getUserDetails().getCity());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.LANGUAGE, response.body().getUserDetails().getLanguage());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.INCALL, response.body().getUserDetails().getServiceType());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.WEBSITE, response.body().getUserDetails().getWebsite());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_LAT, response.body().getUserDetails().getUserLang());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_LANG, response.body().getUserDetails().getUserLang());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PHONE_NUMBER_STATUS, response.body().getUserDetails().getDisplay_no());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PRICE_STATUS, response.body().getUserDetails().getDisplay_price());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.NOTIFICATION, "on");
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_STATUS, response.body().getUserDetails().getProfile_status());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.EMAIL_ON_OFF_STATUS, response.body().getUserDetails().getEmail_on());

                    Log.e("userId", response.body().getUserDetails().getId());

                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);

                } else {
                    Snackbar.make(btn_login, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<LoginMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }
}
