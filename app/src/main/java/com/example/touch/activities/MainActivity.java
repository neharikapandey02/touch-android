package com.example.touch.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.bumptech.glide.Glide;
import com.deep.videotrimmer.utils.FileUtils;
import com.example.touch.R;
import com.example.touch.constants.Constants;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.ReadChatCheckMain;
import com.example.touch.models.StatusMain;
import com.example.touch.utility.VideoTrimmerActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;

import net.vrgsoft.videcrop.VideoCropActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private SharedPreferencesData sharedPreferencesData;
    BottomNavigationView navView;
    String userId;
    ru.nikartm.support.ImageBadgeView imgView_ChatList;

    private static final int CROP_REQUEST = 200;
    String outVideoUri = "";
    private final int REQUEST_VIDEO_TRIMMER_RESULT = 342;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_main);


         navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications, R.id.navigation_likes, R.id.navigation_profile)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        try {
            String fragmentPage = getIntent().getStringExtra("fragment");
            assert fragmentPage != null;
            switch (fragmentPage){
                case "home":{
                    navController.navigate(R.id.navigation_home);
                    break;
                }
                case "profile":{
                    navController.navigate(R.id.navigation_profile);
                    break;
                }
                case "search":{
                    navController.navigate(R.id.navigation_dashboard);
                    break;
                }
                case "activity":{
                    navController.navigate(R.id.navigation_likes);
                    break;
                }
                case "post":{
                    navController.navigate(R.id.navigation_notifications);
                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
//        navController.navigate(R.id.navigation_dashboard);




     //navView.getOrCreateBadge(R.id.navigation_likes).setNumber(5);
     //navView.removeBadge(R.id.navigation_likes);

       /* Menu menu = navView.getMenu();
        menu.findItem(R.id.navigation_likes).setIcon(getResources().getDrawable(R.drawable.heart_icon_notification));
        navView.setItemIconTintList(null);
        ColorStateList csl = new ColorStateList(
                new int[][] {
                        new int[] {-android.R.attr.state_checked}, // unchecked
                        new int[] { android.R.attr.state_checked}  // checked
                },
                new int[] {
                        Color.BLACK,
                        Color.RED
                }
        );
        navView.setItemIconTintList(csl);*/
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);



    }
    public Bitmap getBitmap(String path) {
        Bitmap bitmap=null;
        try {
            File f= new File(path);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap ;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    for (LocalMedia media : selectList) {
//                        Log.e("afdfdfdf: isCompressed" , ""+media.isCompressed());
//                        Log.e( "afdfdfdf: getComPath" , ""+media.getCompressPath());
//                        Log.e( "afdfdfdf: getPath" , ""+media.getPath());
//                        Log.e( "afdfdfdf: isCut" , ""+media.isCut());
//                        Log.e( "afdfdfdf: getCutPath" , ""+media.getCutPath());
//                        Log.e( "afdfdfdf: isOriginal" , ""+media.isOriginal());
//                        Log.e( "afdfdfdf: getOriPath" , ""+media.getOriginalPath());
//                        Log.e( "afdfdfdf Q 特有Path:" , ""+media.getAndroidQToPath());
//                        Log.e( "afdfdfdf: getSize " , ""+media.getSize());
//                        Log.e( "afdfdfdf: getSize " , ""+media.getMimeType());

                        String path = "";
                        if (media.isCut()){
                            path = media.getCutPath();
                        }
                        else {
                            path = media.getPath();
                        }

                        if (media.getMimeType().contains("image")){
                            Uri uri = Uri.parse(media.getCutPath());

                            Bitmap bitmap = null;
                            bitmap = getBitmap(media.getCutPath());
                            String pathcamera = uri.toString();
                            AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
                    @SuppressLint("InflateParams") View mView = getLayoutInflater().inflate(R.layout.dialog_black_and_white_filter, null);
                    PhotoView photoView = mView.findViewById(R.id.imageView);
                    CheckBox blackCB = mView.findViewById(R.id.blackCB);
                    Button continueBtn = mView.findViewById(R.id.continueBtn);
                    Glide.with(MainActivity.this).load(uri.toString()
                    ).into(photoView);
                    mBuilder.setView(mView);
                    AlertDialog mDialog = mBuilder.create();
                    mDialog.setCancelable(false);
                    blackCB.setChecked(false);
                    Log.e("Urrri", uri.toString());
                    if (!blackCB.isChecked()) {
                        continueBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(MainActivity.this, UploadImageActivity.class);
                                intent.putExtra(Constants.IMAGEPATH, uri.toString());
                                intent.putExtra("crop", "1");
                                startActivity(intent);
                                mDialog.dismiss();
                            }
                        });
                    }
                            Bitmap finalBitmap = bitmap;
                            blackCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                Bitmap bitmap = getBitmap(media.getCutPath());

                                Bitmap bitmap1 = toGrayscale(bitmap);
//                                    photoView.setImageBitmap(bitmap1);
                                Uri uri1 = getImageUri1(getApplicationContext(), bitmap1);
                                String a = getRealPathFromURI(uri1);
                                Log.e("Urrri", a.toString());
                                Glide.with(MainActivity.this).load(uri1.toString()
                                ).into(photoView);
                                continueBtn.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          Intent intent = new Intent(MainActivity.this, UploadImageActivity.class);
                                          intent.putExtra(Constants.IMAGEPATH, a.toString());
                                          intent.putExtra("crop", "1");
                                          startActivity(intent);
                                          mDialog.dismiss();
                                      }
                                  });

                            } else {
//                                Uri uri1 = getImageUri1(MainActivity.this.getApplicationContext(), finalBitmap);
//                                String a = getRealPathFromURI(uri1);
//                                Log.e("Urrri", a.toString());
                                Glide.with(MainActivity.this).load(uri.toString()
                                ).into(photoView);
                                continueBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(MainActivity.this, UploadImageActivity.class);
                                        intent.putExtra(Constants.IMAGEPATH, uri.toString());
                                        intent.putExtra("crop", "1");
                                        startActivity(intent);
                                        mDialog.dismiss();
                                    }
                                });
                            }
                        }
                    });
                    mDialog.show();

                        }else {
//                            outVideoUri = "/storage/emulated/0/DCIM/Camera/newvideo1212.mp4";

                            startTrimActivity(Uri.fromFile(new File(media.getPath())));
                        }

//                        Intent intent = new Intent(MainActivity.this, UploadImageActivity.class);
//                        intent.putExtra(Constants.IMAGEPATH, path);
//                        intent.putExtra("crop", "1");
//                        startActivity(intent);

                    }
                    break;

                case REQUEST_VIDEO_TRIMMER_RESULT:
            try {
                Uri selectedVideoUri = data.getData();

                if (selectedVideoUri != null) {
                    String selectedVideoFile = data.getData().getPath();
    //                selectedVideoName = data.getData().getLastPathSegment();
    //                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(selectedVideoUri.getPath(),
    //                        MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);
    //                String path = getRealPathFromURI(selectedVideoUri);
                    ArrayList dataPath=new ArrayList();
                    dataPath.add(selectedVideoFile);
                    Intent intent=new Intent(MainActivity.this, UploadImageActivity.class);
                    intent.putStringArrayListExtra(Constants.IMAGEUPLOAD, dataPath);
                    startActivity(intent);
     }
            } catch (Exception e) {
                e.printStackTrace();
            }
            break;

            }
        }

    }

    private void startTrimActivity(@NonNull Uri uri) {
        Intent intent = new Intent(MainActivity.this, VideoTrimmerActivity.class);
        intent.putExtra("EXTRA_VIDEO_PATH", FileUtils.getPath(this, uri));
        startActivityForResult(intent, REQUEST_VIDEO_TRIMMER_RESULT);
    }

    public String getRealPathFromURI(Uri contentUri) {
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return "";
        }
    }
    public Uri getImageUri1(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 1, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "image1", null);
        return Uri.parse(path);
    }
    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //m_Handler.removeCallbacksAndMessages(null);
    }
    Callback<ReadChatCheckMain> statusChatCallback = new Callback<ReadChatCheckMain>() {

        @Override
        public void onResponse(Call<ReadChatCheckMain> call, Response<ReadChatCheckMain> response) {
            //progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {

                if (response.body().getStatus().toString().equalsIgnoreCase("0")) {
                    imgView_ChatList.clearBadge();
                   imgView_ChatList.visibleBadge(false);
                } else {
                    Log.e("InsideChatCheck=",response.body().getCount()+"");
                    imgView_ChatList.visibleBadge(true);
                    imgView_ChatList.setBadgeValue(response.body().getCount());
                   // imgView_ChatList.clearBadge();


                }
            }


        }

        @Override
        public void onFailure(Call<ReadChatCheckMain> call, Throwable t) {
            //progressBar.setVisibility(View.GONE);
        }
    };
    Callback<StatusMain> getLikeCommentStatus=new Callback<StatusMain>(){

        @Override
        public void onResponse(Call<StatusMain> call, Response<StatusMain> response) {
            if (response.isSuccessful()){
                if (response.body().getStatus().toString().equalsIgnoreCase("0")){
                    navView.removeBadge(R.id.navigation_likes);
                }else{
                    navView.getOrCreateBadge(R.id.navigation_likes).setNumber(response.body().getCount());
                    //Menu menu = navView.getMenu();
                    // menu.findItem(R.id.navigation_likes).setIcon(getResources().getDrawable(R.drawable.hart_icon_notification));
                }
            }


        }

        @Override
        public void onFailure(Call<StatusMain> call, Throwable t) {
            //progressBar.setVisibility(View.GONE);
        }
    };


    @Override
    public void onBackPressed(){
    super.onBackPressed();
    finishAffinity();
}

}
