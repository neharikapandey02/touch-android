package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.HomeProfileAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.HomeMain;
import com.example.touch.models.HomeMainData;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPostLists extends AppCompatActivity {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.recyclerView_ProfileDetails)
    RecyclerView recyclerView_ProfileDetails;

    @BindView(R.id.imgView_backIcon)
    ImageView imgView_backIcon;

    private SharedPreferencesData sharedPreferencesData;
    private String userId;
    private String to_positon;
    private String anotherUserId;
    private boolean flag=false;
    MediaController mc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_my_post_lists);
        ButterKnife.bind(this);
        viewFinds();

    }

    private void viewFinds() {
        mc= new MediaController(MyPostLists.this);
        anotherUserId=getIntent().getStringExtra(Constants.ANOTHER_USER_ID);
        to_positon=getIntent().getStringExtra(Constants.TO_POSITION);
        recyclerView_ProfileDetails.setLayoutManager(new LinearLayoutManager(MyPostLists.this, LinearLayoutManager.VERTICAL, false));
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (anotherUserId!=null&&!anotherUserId.equalsIgnoreCase("")){
                flag=false;
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getProfilePostOnly(profileListCallback,userId,anotherUserId);
            }else{
                flag=true;
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getProfilePostOnly(profileListCallback,userId,"");
            }


        }else{
            Snackbar.make(progressBar,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<HomeMain> profileListCallback=new Callback<HomeMain>(){

        @Override
        public void onResponse(Call<HomeMain> call, Response<HomeMain> response) {
            progressBar.setVisibility(View.GONE);
            List<HomeMainData> followersListMainData=new ArrayList<>();
            followersListMainData.clear();
            if (response.isSuccessful()){
                if (response.body().getResult()!=null){
                    followersListMainData.addAll(response.body().getResult());
                    HomeProfileAdapter homeProfileAdapter=new HomeProfileAdapter(MyPostLists.this,followersListMainData,mc);
                    recyclerView_ProfileDetails.setAdapter(homeProfileAdapter);
                    if (flag){
                        recyclerView_ProfileDetails.scrollToPosition(Integer.parseInt(to_positon));
                    }else{
                        for(int i=0;i<followersListMainData.size();i++){
                            if (followersListMainData.get(i).getId().equalsIgnoreCase(to_positon)){
                                recyclerView_ProfileDetails.scrollToPosition(i);
                                break;
                            }
                        }
                    }


                }else{

                }
            }else{

            }



        }

        @Override
        public void onFailure(Call<HomeMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.imgView_backIcon)
    public void backClick(View view){
        onBackPressed();
    }
}
