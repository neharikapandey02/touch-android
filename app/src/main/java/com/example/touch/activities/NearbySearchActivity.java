package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.SearchLatestPostAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.models.HomeMain;
import com.example.touch.models.HomeMainData;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NearbySearchActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    SearchLatestPostAdapter adapter;
    ArrayList<HomeMainData> arrayList;
    String lat, lng;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_nearby_search);
        recyclerView = findViewById(R.id.recyclerview);
        progressBar = findViewById(R.id.progressBar);
        arrayList = new ArrayList<>();

        GridLayoutManager gridLayoutManager= new GridLayoutManager(NearbySearchActivity.this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);

        lat = getIntent().getStringExtra(Constants.SEARCH_LAT);
        lng = getIntent().getStringExtra(Constants.SEARCH_LANG);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getNearMe(nearMeCallback, lat, lng);
        } else {
            Snackbar.make(recyclerView, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
//        Log.e("lat", lat);
//        Log.e("lat", lng);
//        lat = "28.7041";
//        lng = "77.1025";
//        String latLng = lat+","+lng;
//        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latLng+"&key=AIzaSyCE_5xAkP50cQKk2suoeL0NByrstAR03gU";
//        DownloadTask downloadTask = new DownloadTask();
//        downloadTask.execute(url);

    }

    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(result);
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                JSONObject placeObj = jsonArray.getJSONObject(0);
                String placeId = placeObj.optString("place_id");
                Log.e("placcce", placeId);
//                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
//                    progressBar.setVisibility(View.VISIBLE);
//                    RetrofitHelper.getInstance().getNearMe(nearMeCallback, placeId);
//                } else {
//                    Snackbar.make(recyclerView, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
//                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(NearbySearchActivity.this, "Please try again later.", Toast.LENGTH_SHORT).show();
            }
//            ParserTask parserTask = new ParserTask();
//
//            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);


            } catch (Exception e) {
                e.printStackTrace();
            }


            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(2);
                lineOptions.color(Color.RED);
            }


        }
    }



    Callback<HomeMain> nearMeCallback = new Callback<HomeMain>() {
        @Override
        public void onResponse(Call<HomeMain> call, Response<HomeMain> response) {
            if (response.isSuccessful()) {
                progressBar.setVisibility(View.GONE);
                if (response.body().getSuccess() == 1) {
                    if (response.body().getResult() != null) {
                        arrayList.clear();
                        arrayList.addAll(response.body().getResult());
                        adapter = new SearchLatestPostAdapter(NearbySearchActivity.this, arrayList);
                        recyclerView.setAdapter(adapter);
                    }
                } else {
                    Toast.makeText(NearbySearchActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void onFailure(Call<HomeMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
