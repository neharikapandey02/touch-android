package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.DisplayPhoneSuccess;
import com.example.touch.models.UpdatePriceMain;
import com.example.touch.models.UpdatePriceMainData;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PriceListActivity extends AppCompatActivity {
    @BindView(R.id.imageView_back)
    ImageView imageView_back;

    @BindView(R.id.btn_addPrice)
    Button btn_addPrice;

    @BindView(R.id.et_oneHours)
    EditText et_oneHours;

    @BindView(R.id.et_twoHours)
    EditText et_twoHours;

    @BindView(R.id.et_threeHours)
    EditText et_threeHours;


    @BindView(R.id.et_fourHours)
    EditText et_fourHours;

    @BindView(R.id.et_12Hours)
    EditText et_12Hours;

    @BindView(R.id.et_24Hours)
    EditText et_24Hours;


    @BindView(R.id.et_fromMinum)
    EditText et_fromMinum;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    int value = 0;
    String jsonData;
    private SharedPreferencesData sharedPreferencesData;
    String userId;

    @BindView(R.id.switcher)
    Switch switcher;

    private boolean flag=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_price_list);
        ButterKnife.bind(this);

        viewFinds();




        //Log.e("jsonArray", jsonArray.toString());
    }

    private void viewFinds() {

        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setSelectedItemId(R.id.navigation_profile);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(PriceListActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(PriceListActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(PriceListActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(PriceListActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(PriceListActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getUpdatePrice(updatePriceCallback,userId,"");

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }

        String status = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PRICE_STATUS);
        if (status != null){
            if (status.equalsIgnoreCase("0")){
                switcher.setChecked(false);
            }else {
                switcher.setChecked(true);
            }
        }
        switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (NetworkUtil.checkNetworkStatus(PriceListActivity.this.getApplicationContext())) {
                    if (isChecked) {
                        RetrofitHelper.getInstance().displayPrice(displayPhoneCallback, userId, "1");
                    }else {
                        RetrofitHelper.getInstance().displayPrice(displayPhoneCallback, userId, "0");
                    }

                } else {
                    Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }

            }
        });
    }


    Callback<DisplayPhoneSuccess> displayPhoneCallback = new Callback<DisplayPhoneSuccess>() {
        @Override
        public void onResponse(Call<DisplayPhoneSuccess> call, Response<DisplayPhoneSuccess> response) {
            try {
                if (response.body() != null){
                    if (response.body().getSuccess() == 1) {
                        Toast.makeText(PriceListActivity.this, "Updated successfully.", Toast.LENGTH_SHORT).show();
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PRICE_STATUS, response.body().getDisplay_price());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<DisplayPhoneSuccess> call, Throwable t) {

        }
    };


    /*public void Add_Line() {
        LinearLayout ll = findViewById(R.id.layout_addNew);
        // add edittext
        EditText et = new EditText(this);
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        et.setLayoutParams(p);
        et.setText("Text");
        et.setId(numberOfLines + 1);
        ll.addView(et);
        ll.removeViewAt(numberOfLines);
        numberOfLines++;
    }*/

    @OnClick(R.id.btn_addPrice)
    public void addNewClick(View view) {

        String oneHours = et_oneHours.getText().toString();
        String twoHours = et_twoHours.getText().toString();
        String threeHours = et_threeHours.getText().toString();
        String fourHours = et_fourHours.getText().toString();
        String twelveHours = et_12Hours.getText().toString();
        String twentyFourHours = et_24Hours.getText().toString();
        String fromMinumum = et_fromMinum.getText().toString();

        JSONArray jsonArray = new JSONArray();


        try {
             if (validate()){
                /* if (!fromMinumum.equalsIgnoreCase("")) {
                     jsonArray.put(new JSONObject()
                             .put("price_id", 0)
                             .put("price", et_fromMinum.getText().toString()));
                 }
                 if (!oneHours.equalsIgnoreCase("")) {
                     jsonArray.put(new JSONObject()
                             .put("price_id", 1)
                             .put("price", et_oneHours.getText().toString()));
                 }
                 if (!twoHours.equalsIgnoreCase("")) {
                     jsonArray.put(new JSONObject()
                             .put("price_id", 2)
                             .put("price", et_twoHours.getText().toString()));
                 }
                 if (!threeHours.equalsIgnoreCase("")) {
                     jsonArray.put(new JSONObject()
                             .put("price_id", 3)
                             .put("price", et_threeHours.getText().toString()));
                 }
                 if (!fourHours.equalsIgnoreCase("")) {
                     jsonArray.put(new JSONObject()
                             .put("price_id", 4)
                             .put("price", et_fourHours.getText().toString()));
                 }
                 if (!twelveHours.equalsIgnoreCase("")) {
                     jsonArray.put(new JSONObject()
                             .put("price_id", 5)
                             .put("price", et_12Hours.getText().toString()));
                 }
                 if (!twentyFourHours.equalsIgnoreCase("")) {
                     jsonArray.put(new JSONObject()
                             .put("price_id", 6)
                             .put("price", et_24Hours.getText().toString()));
                 }*/
                 jsonArray.put(new JSONObject()
                         .put("price_id", 0)
                         .put("price", et_fromMinum.getText().toString()));
                 jsonArray.put(new JSONObject()
                         .put("price_id", 1)
                         .put("price", et_oneHours.getText().toString()));
                 jsonArray.put(new JSONObject()
                         .put("price_id", 2)
                         .put("price", et_twoHours.getText().toString()));
                 jsonArray.put(new JSONObject()
                         .put("price_id", 3)
                         .put("price", et_threeHours.getText().toString()));
                 jsonArray.put(new JSONObject()
                         .put("price_id", 4)
                         .put("price", et_fourHours.getText().toString()));
                 jsonArray.put(new JSONObject()
                         .put("price_id", 5)
                         .put("price", et_12Hours.getText().toString()));
                 jsonArray.put(new JSONObject()
                         .put("price_id", 6)
                         .put("price", et_24Hours.getText().toString()));

                 if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                     flag=true;
                     progressBar.setVisibility(View.VISIBLE);
                     RetrofitHelper.getInstance().getUpdatePrice(updatePriceCallback,userId,jsonArray.toString());

                 } else {
                     Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                 }
             }


        } catch (Exception e) {


        }

    }

    @OnClick(R.id.imageView_back)
    public void backClick(View v) {
        onBackPressed();
    }

    Callback<UpdatePriceMain> updatePriceCallback=new Callback<UpdatePriceMain>(){

        @Override
        public void onResponse(Call<UpdatePriceMain> call, Response<UpdatePriceMain> response) {
         progressBar.setVisibility(View.GONE);
        /* if (flag){
             //Snackbar.make(progressBar, getResources().getString(R.string.price_updated), Snackbar.LENGTH_LONG).show();
             flag=false;
         }*/

            if (response.isSuccessful()){
                if (response.body().getResult()!=null){
                    List<UpdatePriceMainData> updatePriceMainData=new ArrayList<>();
                    updatePriceMainData.clear();
                    updatePriceMainData.addAll(response.body().getResult());
                    for (int i=0;i<updatePriceMainData.size();i++){
                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("0")){
                            et_fromMinum.setText(updatePriceMainData.get(i).getPrice());
                        }else {
                            //tv_priceOneHour.setText("No Price");
                        }
                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("1")){
                            et_oneHours.setText(updatePriceMainData.get(i).getPrice());
                        }else {
                            //tv_priceOneHour.setText("No Price");
                        }

                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("2")){
                            et_twoHours.setText(updatePriceMainData.get(i).getPrice());
                        }else {
                            //tv_priceTwoHours.setText("No Price");
                        }

                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("3")){
                            et_threeHours.setText(updatePriceMainData.get(i).getPrice());
                        }else {
                            //tv_priceThreeHours.setText("No Price");
                        }

                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("4")){
                            et_fourHours.setText(updatePriceMainData.get(i).getPrice());
                        }else {
                            // tv_priceFourHours.setText("No Price");
                        }

                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("5")){
                            et_12Hours.setText(updatePriceMainData.get(i).getPrice());
                        }else {
                            // tv_price12Hours.setText("No Price");
                        }

                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("6")){
                            et_24Hours.setText(updatePriceMainData.get(i).getPrice());
                        }else{
                            // tv_price24Hours.setText("No Price");
                        }
                    }







                }else{
                    et_fromMinum.setText("No Price");
                    et_oneHours.setText("No Price");
                    et_twoHours.setText("No Price");
                    et_threeHours.setText("No Price");
                    et_fourHours.setText("No Price");
                    et_12Hours.setText("No Price");
                    et_24Hours.setText("No Price");
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_price),Toast.LENGTH_LONG).show();
                }
                if (flag){
                    //Snackbar.make(progressBar, getResources().getString(R.string.price_updated), Snackbar.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.price_updated),Toast.LENGTH_LONG).show();
                    flag=false;
                    onBackPressed();
                }
            }
        }

        @Override
        public void onFailure(Call<UpdatePriceMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    public boolean validate(){
        if (et_fromMinum.getText().toString().equalsIgnoreCase("")&&et_oneHours.getText().toString().equalsIgnoreCase("")&&
                et_twoHours.getText().toString().equalsIgnoreCase("")&&
                et_threeHours.getText().toString().equalsIgnoreCase("")&&
                et_fourHours.getText().toString().equalsIgnoreCase("")&&
                et_12Hours.getText().toString().equalsIgnoreCase("")&&
                et_24Hours.getText().toString().equalsIgnoreCase("") ){
            Snackbar.make(imageView_back,getResources().getString(R.string.please_enter_atlease),Snackbar.LENGTH_LONG).show();
            return false;

        }/*else if (et_twoHours.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(imageView_back,getResources().getString(R.string.price_two),Snackbar.LENGTH_LONG).show();
            return false;

        }else if (et_threeHours.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(imageView_back,getResources().getString(R.string.price_three),Snackbar.LENGTH_LONG).show();
            return false;

        }else if (et_fourHours.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(imageView_back,getResources().getString(R.string.price_four),Snackbar.LENGTH_LONG).show();
            return false;

        }else if (et_12Hours.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(imageView_back,getResources().getString(R.string.price_twelve),Snackbar.LENGTH_LONG).show();
            return false;

        }else if (et_24Hours.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(imageView_back,getResources().getString(R.string.price_twenty),Snackbar.LENGTH_LONG).show();
            return false;
        }*/

        return true;
    }
}
