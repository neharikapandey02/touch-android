package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.models.UpdatePriceMain;
import com.example.touch.models.UpdatePriceMainData;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PriceShowActivity extends AppCompatActivity {
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private String anotherUserId="";

    @BindView(R.id.tv_priceOneHour)
    TextView tv_priceOneHour;

    @BindView(R.id.tv_priceTwoHours)
    TextView tv_priceTwoHours;

    @BindView(R.id.tv_priceThreeHours)
    TextView tv_priceThreeHours;

    @BindView(R.id.tv_priceFourHours)
    TextView tv_priceFourHours;

    @BindView(R.id.tv_price12Hours)
    TextView tv_price12Hours;

    @BindView(R.id.tv_price24Hours)
    TextView tv_price24Hours;


    @BindView(R.id.tv_minimumPrice)
    TextView tv_minimumPrice;


    @BindView(R.id.imageView_back)
    ImageView imageView_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_price_show);
        ButterKnife.bind(this);

        viewFinds();
    }

    private void viewFinds() {
        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(PriceShowActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(PriceShowActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(PriceShowActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(PriceShowActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(PriceShowActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });
        anotherUserId=getIntent().getStringExtra(Constants.ANOTHER_USER_ID);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getUpdatePrice(updatePriceCallback,anotherUserId,"");

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<UpdatePriceMain> updatePriceCallback=new Callback<UpdatePriceMain>(){

        @Override
        public void onResponse(Call<UpdatePriceMain> call, Response<UpdatePriceMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getResult()!=null){
                    List<UpdatePriceMainData> updatePriceMainData=new ArrayList<>();
                    updatePriceMainData.clear();
                    updatePriceMainData.addAll(response.body().getResult());
                    for (int i=0;i<updatePriceMainData.size();i++){
                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("0")){
                            tv_minimumPrice.setText(updatePriceMainData.get(i).getPrice()+" Euros");
                        }else {
                            //tv_priceOneHour.setText("No Price");
                        }
                        if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("1")){
                            tv_priceOneHour.setText(updatePriceMainData.get(i).getPrice()+" Euros");
                        }else {
                            //tv_priceOneHour.setText("No Price");
                        }

                            if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("2")){
                            tv_priceTwoHours.setText(updatePriceMainData.get(i).getPrice()+" Euros");
                        }else {
                                //tv_priceTwoHours.setText("No Price");
                            }

                            if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("3")){
                            tv_priceThreeHours.setText(updatePriceMainData.get(i).getPrice()+" Euros");
                        }else {
                                //tv_priceThreeHours.setText("No Price");
                            }

                            if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("4")){
                            tv_priceFourHours.setText(updatePriceMainData.get(i).getPrice()+" Euros");
                        }else {
                               // tv_priceFourHours.setText("No Price");
                            }

                            if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("5")){
                            tv_price12Hours.setText(updatePriceMainData.get(i).getPrice()+" Euros");
                        }else {
                               // tv_price12Hours.setText("No Price");
                            }

                            if (updatePriceMainData.get(i).getPriceId().equalsIgnoreCase("6")){
                            tv_price24Hours.setText(updatePriceMainData.get(i).getPrice()+" Euros");
                        }else{
                               // tv_price24Hours.setText("No Price");
                        }
                    }







                }else{
                    tv_priceOneHour.setText("No Price");
                    tv_priceTwoHours.setText("No Price");
                    tv_priceThreeHours.setText("No Price");
                    tv_priceFourHours.setText("No Price");
                    tv_price12Hours.setText("No Price");
                    tv_price24Hours.setText("No Price");
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_price),Toast.LENGTH_LONG).show();
                }
            }

        }

        @Override
        public void onFailure(Call<UpdatePriceMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.imageView_back)
    public void backClick(View view){
        onBackPressed();
    }
}
