package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.emoji.widget.EmojiTextView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.touch.R;
import com.example.touch.constants.Constants;
import com.example.touch.constants.MySpannable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {
    String firstName;
    String lastName;
    String email;
    String phoneNumber;
    String dob;
    String gender;
    String userType;
    String about;
    String hairColor;
    String weight;
    String height;
    String breast;
    String eyeColor;
    String hobby;
    String available;
    String ethnicity;
    String origin;
    String countryName;
    String cityName;
    String language;
    String incall;
    String website;
    String profileImage;
    String tag1;
    String tag2;
    String tag3;
    String tag4;
    String tag5;

    @BindView(R.id.scrollview)
    ScrollView scrollview;


    @BindView(R.id.circularImageView_UserImage)
    CircularImageView circularImageView_UserImage;

    @BindView(R.id.backgroundImg)
    ImageView backgroundImg;

    @BindView(R.id.tv_firstName)
    TextView tv_firstName;


    @BindView(R.id.tv_lastName)
    TextView tv_lastName;


    @BindView(R.id.tv_number)
    TextView tv_number;

/*

    @BindView(R.id.tv_hairColor)
     TextView tv_hairColor;

    @BindView(R.id.tv_weight)
     TextView tv_weight;


    @BindView(R.id.tv_height)
     TextView tv_height;
*/


    @BindView(R.id.btn_moreInfo)
    Button btn_moreInfo;


    @BindView(R.id.imgView_back)
    ImageView imgView_back;


    @BindView(R.id.linearLayout_moreInfo)
    LinearLayout linearLayout_moreInfo;

    @BindView(R.id.linearLayout_moreInfoButton)
    LinearLayout linearLayout_moreInfoButton;

    @BindView(R.id.tv_email)
    TextView tv_email;

    @BindView(R.id.tv_dob)
    TextView tv_dob;

    @BindView(R.id.tv_gender)
    TextView tv_gender;

    @BindView(R.id.tv_aboutUs)
     EmojiTextView tv_aboutUs;

    @BindView(R.id.tv_hairColor)
    TextView tv_hairColor;

    @BindView(R.id.tv_weight)
    TextView tv_weight;

    @BindView(R.id.tv_height)
    TextView tv_height;

    @BindView(R.id.tv_breast)
    TextView tv_breast;

    @BindView(R.id.tv_eyeColor)
    TextView tv_eyeColor;

    @BindView(R.id.tv_hobby)
    TextView tv_hobby;

    @BindView(R.id.tv_available)
    TextView tv_available;

    @BindView(R.id.tv_ethnicity)
    TextView tv_ethnicity;

    @BindView(R.id.tv_origin)
    TextView tv_origin;

    @BindView(R.id.tv_country)
    TextView tv_country;

    @BindView(R.id.tv_city)
    TextView tv_city;

    @BindView(R.id.tv_language)
    TextView tv_language;

    @BindView(R.id.tv_call)
    TextView tv_call;

    @BindView(R.id.tv_website)
    TextView tv_website;

    HashMap<String, String> userDetailsHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {

        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });

        Intent intent = getIntent();
        userDetailsHashMap = (HashMap<String, String>) intent.getSerializableExtra(Constants.USER_DETAILS);
        firstName = userDetailsHashMap.get(Constants.FIRST_NAME);
        lastName = userDetailsHashMap.get(Constants.LAST_NAME);
        email = userDetailsHashMap.get(Constants.EMAIL);
        phoneNumber = userDetailsHashMap.get(Constants.PHONE_NUMBER);
        dob = userDetailsHashMap.get(Constants.DOB);
        gender = userDetailsHashMap.get(Constants.GENDER);
        userType = userDetailsHashMap.get(Constants.USER_TYPE);
        about = userDetailsHashMap.get(Constants.ABOUT_US);
        hairColor = userDetailsHashMap.get(Constants.HAIR_COLOR);
        weight = userDetailsHashMap.get(Constants.WEIGHT);
        height = userDetailsHashMap.get(Constants.HEIGHT);
        breast = userDetailsHashMap.get(Constants.BREAST_SIZE);
        eyeColor = userDetailsHashMap.get(Constants.EYE_COLOR);
        hobby = userDetailsHashMap.get(Constants.HOBBY);
        available = userDetailsHashMap.get(Constants.AVAILABLE);
        ethnicity = userDetailsHashMap.get(Constants.ETHNICITY);
        origin = userDetailsHashMap.get(Constants.ORIGIN);
        countryName = userDetailsHashMap.get(Constants.COUNTRY_NAME);
        cityName = userDetailsHashMap.get(Constants.CITY_NAME);
        language = userDetailsHashMap.get(Constants.LANGUAGE);
        incall = userDetailsHashMap.get(Constants.SERVICE_TYPE);

        if (incall!=null){
            if (incall.equalsIgnoreCase("0")) {
                incall = "Incall";
            } else if (incall.equalsIgnoreCase("1")) {
                incall = "OutCall";
            } else {
                incall = "both";
            }
        }



        website = userDetailsHashMap.get(Constants.WEBSITE);
        profileImage = userDetailsHashMap.get(Constants.PROFILE_IMAGE);

        tv_firstName.setText(firstName);
        tv_lastName.setText(lastName);
        tv_number.setText(phoneNumber);
        tv_email.setText(email);
        tv_dob.setText(dob);
        tv_gender.setText(gender);
        tv_aboutUs.setText(about);
        tv_hairColor.setText(hairColor);
        tv_weight.setText(weight);
        tv_height.setText(height);
        tv_breast.setText(breast);
        tv_eyeColor.setText(eyeColor);
        tv_hobby.setText(hobby);
        tv_available.setText(available);
        tv_ethnicity.setText(ethnicity);
        tv_origin.setText(origin);
        tv_country.setText(countryName);
        tv_city.setText(cityName);
        tv_language.setText(language);
        tv_call.setText(incall);
        tv_website.setText(website);

       // tv_aboutUs.setMovementMethod(new ScrollingMovementMethod());


        //Log.e("HashMapTest", hashMap.get(Constants.FIRST_NAME)+" "+hashMap.get(Constants.LAST_NAME));

        if (profileImage != null && !profileImage.equalsIgnoreCase("")) {
            Picasso.with(getApplicationContext())
                    .load(profileImage)
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(circularImageView_UserImage);

            Picasso.with(getApplicationContext())
                    .load(profileImage)
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(backgroundImg);

        }

        /*scrollview.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                tv_aboutUs.getParent().requestDisallowInterceptTouchEvent(false);

                return false;
            }
        });*/

       /* tv_aboutUs.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                tv_aboutUs.getParent().requestDisallowInterceptTouchEvent(true);

                return false;
            }
        });*/




    }





    public static void makeTextViewResizable(final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {

                        int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                        String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                        tv.setText(text);
                        tv.setMovementMethod(LinkMovementMethod.getInstance());
                        tv.setText(
                                addClickablePartTextViewResizable(Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                        viewMore), TextView.BufferType.SPANNABLE);


                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {


            ssb.setSpan(new MySpannable(false){
                @Override
                public void onClick(View widget) {
                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "See Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 3, ".. See More", true);
                    }
                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

        }
        return ssb;

    }



    @OnClick(R.id.imgView_back)
    public void backIconClick(View view) {
        onBackPressed();
    }


    @OnClick(R.id.tv_website)
    public void websiteOpne(View view) {
        String url=tv_website.getText().toString();
        if (!url.startsWith("http://") && !url.startsWith("https://")){
            url = "http://" + url;
        }

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));

        startActivity(browserIntent);
    }


    @OnClick(R.id.btn_moreInfo)
    public void moreButtonClick(View view) {
        linearLayout_moreInfo.setVisibility(View.VISIBLE);
        linearLayout_moreInfoButton.setVisibility(View.GONE);
        if (about!=null&&about.length()>20){
            makeTextViewResizable(tv_aboutUs, 2, "See More", true);
        }


    }
}
