package com.example.touch.activities;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.SearchAdapterByName;
import com.example.touch.adapters.SearchAdapterBySuggestion;
import com.example.touch.adapters.SearchImageListResultAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.SearchAdapter;
import com.example.touch.models.SearchAdapterByHasTag;
import com.example.touch.models.SearchMain;
import com.example.touch.models.SearchMainData;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {
    @BindView(R.id.et_search)
    EditText et_search;

    @BindView(R.id.layoutLinear_search)
    LinearLayout layoutLinear_search;


    @BindView(R.id.recyclerView_SearchData)
    RecyclerView recyclerView_SearchData;


    @BindView(R.id.recyclerView_ImageSearchData)
    RecyclerView recyclerView_ImageSearchData;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;


    @BindView(R.id.imgView_searchFind)
    ImageView imgView_searchFind;


    @BindView(R.id.imgView_back)
    ImageView imgView_back;
    private SharedPreferencesData sharedPreferencesData;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.top_layout)
    RelativeLayout top_layout;


    @BindView(R.id.layout_titleBar)
    RelativeLayout layout_titleBar;


    @BindView(R.id.tv_chat_title)
    TextView tv_chat_title;


    @BindView(R.id.tv_message)
    TextView tv_message;

    @BindView(R.id.top_layoutHashtag)
    RelativeLayout top_layoutHashtag;

    @BindView(R.id.view_menu)
    View view_menu;

    @BindView(R.id.view_name)
    View view_name;

    @BindView(R.id.view_hashtage)
    View view_hashtage;

    @BindView(R.id.view_location)
    View view_location;


    @BindView(R.id.linearLayout_Menu)
    LinearLayout linearLayout_Menu;

    @BindView(R.id.layoutLinear_Name)
    LinearLayout layoutLinear_Name;


    @BindView(R.id.linearLayout_Hashtage)
    LinearLayout linearLayout_Hashtage;

    @BindView(R.id.linearLayout_Location)
    LinearLayout linearLayout_Location;

    @BindView(R.id.relative_searchMain)
    RelativeLayout relative_searchMain;

    String byTypeSearch="by_suggestion";
    String userId = "";

    private String searchText = "";
    private String searchLat = "";
    private String searchLang = "";
    private String searchType="";
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE=1001;
    private String latitude;
    private String longitude, place_id = "", countryType = "";
    private String country;
    private String state, country_code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {

        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());

        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);

        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        nav_view.setSelectedItemId(R.id.navigation_dashboard);
        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(SearchActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });
        recyclerView_SearchData.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        searchText = getIntent().getStringExtra(Constants.SEARCH_TEXT);
//        searchLat = getIntent().getStringExtra(Constants.SEARCH_LAT);
//        searchLang = getIntent().getStringExtra(Constants.SEARCH_LANG);



        //Adapter for list image show search result  tab
        recyclerView_ImageSearchData.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));



//        if (searchText != null && !searchText.equalsIgnoreCase("")) {
//            top_layout.setVisibility(View.GONE);
//            top_layoutHashtag.setVisibility(View.GONE);
//
//
//            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
//                if (searchText.equalsIgnoreCase("male") || searchText.equalsIgnoreCase("female") || searchText.equalsIgnoreCase("trasander")) {
//                    layout_titleBar.setVisibility(View.VISIBLE);
//                    tv_chat_title.setText(searchText);
//                    progressBar.setVisibility(View.VISIBLE);
//                    searchType="Images";
//                    RetrofitHelper.getInstance().getSearchResult(getSearchDataCallback, searchText, "", "", "");
//                } else {
//                    layout_titleBar.setVisibility(View.VISIBLE);
//                    tv_chat_title.setText(searchText);
//                    progressBar.setVisibility(View.VISIBLE);
//                    searchType="Images";
//                    RetrofitHelper.getInstance().getSearchResult(getSearchDataCallback, "", "", "", searchText);
//                }
//            } else {
//                Snackbar.make(et_search, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
//            }
//        } else if (searchLat != null && !searchLat.equalsIgnoreCase("")) {
//            top_layout.setVisibility(View.GONE);
//            top_layoutHashtag.setVisibility(View.GONE);
//            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
//                layout_titleBar.setVisibility(View.VISIBLE);
//                tv_chat_title.setText("NearBy You");
//                searchType="Images";
//                progressBar.setVisibility(View.VISIBLE);
//                RetrofitHelper.getInstance().getSearchResult(getSearchDataCallback, "", searchLat, searchLang, "");
//                //Log.e("LatLng=", searchLat + " " + searchLang);
//
//
//            } else {
//                Snackbar.make(et_search, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
//            }
//        }

        if (searchLat == null || searchLat.equalsIgnoreCase("")) {
            if (byTypeSearch.equalsIgnoreCase("by_suggestion")) {
                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                    String key = et_search.getText().toString();
                    String lat = getIntent().getStringExtra(Constants.SEARCH_LAT);
                    String lng = getIntent().getStringExtra(Constants.SEARCH_LANG);

                    RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession1(getSearchDataCallback, key, byTypeSearch, lat, lng, userId);
                }
            }
        }

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (byTypeSearch.equalsIgnoreCase("by_suggestion")){
                    if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                        String key = et_search.getText().toString();
                        String lat = getIntent().getStringExtra(Constants.SEARCH_LAT);
                        String lng = getIntent().getStringExtra(Constants.SEARCH_LANG);
                        RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession1(getSearchDataCallback, key, byTypeSearch, lat, lng, userId);
                    }
                }else if (byTypeSearch.equalsIgnoreCase("by_hashtag")){
                    if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                        String key = et_search.getText().toString();
                        RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession(getSearchDataCallback, key, byTypeSearch, userId);
                    }
                }
            }
        });


        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.e("Inside=",editable.toString());
                    if (searchLat == null || searchLat.equalsIgnoreCase("")) {

                        if (byTypeSearch.equalsIgnoreCase("by_suggestion")) {
                            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                                String key = et_search.getText().toString();
                                String lat = getIntent().getStringExtra(Constants.SEARCH_LAT);
                                String lng = getIntent().getStringExtra(Constants.SEARCH_LANG);

                                RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession1(getSearchDataCallback, key, byTypeSearch, lat, lng, userId);
                            }
                        } else {
                            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                                String key = et_search.getText().toString();
                                RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession(getSearchDataCallback, key, byTypeSearch, userId);

                            }
                        }
                    }

//                    Log.e("Inside","InsideTextChanger");
//                    layoutLinear_search.setVisibility(View.GONE);
//                    List<SearchMainData> searchMainDataList1=new ArrayList<>();
//                    searchMainDataList1.clear();
//                    // Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_user),Toast.LENGTH_LONG).show();
//                    //onBackPressed();
//                    tv_message.setVisibility(View.VISIBLE);
//                    SearchAdapter searchAdapter = new SearchAdapter(getApplicationContext(), searchMainDataList1);
//                    recyclerView_SearchData.setAdapter(searchAdapter);

//                } else {
//                    layoutLinear_search.setVisibility(View.VISIBLE);
//                    if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
//                        String key = et_search.getText().toString();
//                        searchType = "Profiles";
//                        //progressBar.setVisibility(View.VISIBLE);
//                        RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession(getSearchDataCallback, key, byTypeSearch);
//                        Log.e("forSearch=",key+"  "+byTypeSearch);
//
//                    } else {
//                        Snackbar.make(et_search, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
//                    }
//                }
            }
        });

    }

    @OnClick(R.id.imgView_searchFind)
    public void searchClick(View view) {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            String key = et_search.getText().toString();
            searchType="Profiles";
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession(getSearchDataCallback, key, byTypeSearch, userId);

        } else {
            Snackbar.make(et_search, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.imgView_back)
    public void backClicked(View view) {
       onBackPressed();
    }
    List<SearchMainData> searchMainDataList;

    Callback<SearchMain> getSearchDataCallback = new Callback<SearchMain>() {
        @Override
        public void onResponse(Call<SearchMain> call, Response<SearchMain> response) {
            if (swipeRefresh.isRefreshing()){
                swipeRefresh.setRefreshing(false);
            }
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                searchMainDataList= new ArrayList<>();
                if (response.body().getResult() != null) {
                    tv_message.setVisibility(View.GONE);

                    searchMainDataList.clear();


                    if (searchType.equalsIgnoreCase("Images")){
                        Log.e("Inside=","Images");
                        searchMainDataList.clear();
                        try {
                            for (int i=0;i<response.body().getResult().size();i++){
                                if (response.body().getResult().get(i).getType().equalsIgnoreCase("image")){
                                    searchMainDataList.add(response.body().getResult().get(i));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        SearchImageListResultAdapter searchAdapter = new SearchImageListResultAdapter(getApplicationContext(), searchMainDataList);
                        recyclerView_ImageSearchData.setAdapter(searchAdapter);

                    }else{
                        if (byTypeSearch.equalsIgnoreCase("by_suggestion")){
                            searchMainDataList.addAll(response.body().getResult());
                            SearchAdapterBySuggestion searchAdapter = new SearchAdapterBySuggestion(getApplicationContext(), searchMainDataList);
                            recyclerView_SearchData.setAdapter(searchAdapter);
                        }else if (byTypeSearch.equalsIgnoreCase("by_name")){
                            Log.e("Inside=","profiles"+byTypeSearch);
                            searchMainDataList.addAll(response.body().getResult());
                            SearchAdapterByName searchAdapter = new SearchAdapterByName(getApplicationContext(), searchMainDataList);
                            recyclerView_SearchData.setAdapter(searchAdapter);

                        }else if (byTypeSearch.equalsIgnoreCase("by_hashtag")){
                            searchMainDataList.addAll(response.body().getResult());

                            SearchAdapterByHasTag searchAdapterByHasTag=new SearchAdapterByHasTag(getApplicationContext(),
                                    searchMainDataList, response.body().getUser_hastag(), userId);
                            recyclerView_SearchData.setAdapter(searchAdapterByHasTag);
                        }else if (byTypeSearch.equalsIgnoreCase("by_location")){
                            Log.e("Inside=","profiles");
                            searchMainDataList.addAll(response.body().getResult());
                            SearchAdapter searchAdapter = new SearchAdapter(getApplicationContext(), searchMainDataList);
                            recyclerView_SearchData.setAdapter(searchAdapter);
                        }

                    }

                } else {
                    Log.e("Inside=","Nothing");
                    searchMainDataList.clear();
                    // Toast.makeText(getApplicationContext(),getResources().getString(R.string.no_user),Toast.LENGTH_LONG).show();
                    //onBackPressed();
                    tv_message.setVisibility(View.VISIBLE);
                    SearchAdapter searchAdapter = new SearchAdapter(getApplicationContext(), searchMainDataList);
                    recyclerView_SearchData.setAdapter(searchAdapter);
                }
            }
        }

        @Override
        public void onFailure(Call<SearchMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
            if (swipeRefresh.isRefreshing()){
                swipeRefresh.setRefreshing(false);
            }
        }
    };
//all view underline clickListner
    @OnClick(R.id.linearLayout_Menu)
    public void menuClick(View view) {
        et_search.setClickable(true);
        et_search.setEnabled(true);
        recyclerView_SearchData.setVisibility(View.VISIBLE);
        byTypeSearch = "by_suggestion";
        view_name.setVisibility(View.INVISIBLE);
        view_hashtage.setVisibility(View.INVISIBLE);
        view_location.setVisibility(View.INVISIBLE);
        view_menu.setVisibility(View.VISIBLE);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            String key = et_search.getText().toString();
            String lat = getIntent().getStringExtra(Constants.SEARCH_LAT);
            String lng = getIntent().getStringExtra(Constants.SEARCH_LANG);
            RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession1(getSearchDataCallback, key, byTypeSearch, lat, lng, userId);
        }
    }

    @OnClick(R.id.layoutLinear_Name)
    public void nameClick(View view) {
        et_search.setClickable(true);
        et_search.setEnabled(true);
        recyclerView_SearchData.setVisibility(View.VISIBLE);
        byTypeSearch = "by_name";
        view_menu.setVisibility(View.INVISIBLE);
        view_hashtage.setVisibility(View.INVISIBLE);
        view_location.setVisibility(View.INVISIBLE);
        view_name.setVisibility(View.VISIBLE);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            String key = et_search.getText().toString();
            RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession(getSearchDataCallback, key, byTypeSearch, userId);
        }
    }

    @OnClick(R.id.linearLayout_Hashtage)
    public void hashtageClick(View view) {
        et_search.setClickable(true);
        et_search.setEnabled(true);
        recyclerView_SearchData.setVisibility(View.VISIBLE);
        byTypeSearch = "by_hashtag";
        view_name.setVisibility(View.INVISIBLE);
        view_menu.setVisibility(View.INVISIBLE);
        view_location.setVisibility(View.INVISIBLE);
        view_hashtage.setVisibility(View.VISIBLE);
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            String key = et_search.getText().toString();
            RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession(getSearchDataCallback, key, byTypeSearch, userId);
        }
    }

    @OnClick(R.id.linearLayout_Location)
    public void locationClick(View view) {
        et_search.setClickable(false);
        et_search.setEnabled(false);
        et_search.setText("");
        recyclerView_SearchData.setVisibility(View.GONE);
        if (searchMainDataList!=null){
            searchMainDataList.clear();


        }

        byTypeSearch = "by_location";
        view_name.setVisibility(View.INVISIBLE);
        view_hashtage.setVisibility(View.INVISIBLE);
        view_menu.setVisibility(View.INVISIBLE);
        view_location.setVisibility(View.VISIBLE);
        startAutocompleteActivity();

    }




    @OnClick(R.id.top_layout)
    public void editTextSearchClick(View view) {

       if (byTypeSearch.equalsIgnoreCase("by_location")){
           et_search.setClickable(false);
           et_search.setEnabled(false);
           et_search.setText("");
           startAutocompleteActivity();
       }

    }

    @Override
    protected void onResume() {
        super.onResume();
        relative_searchMain.setVisibility(View.VISIBLE);
    }

    private void startAutocompleteActivity() {
        Places.initialize(getApplicationContext(), "AIzaSyCE_5xAkP50cQKk2suoeL0NByrstAR03gU");
        List<com.google.android.libraries.places.api.model.Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));

        List<TypeFilter> typeFilters = new ArrayList<>(Arrays.asList(TypeFilter.REGIONS));


// Create a RectangularBounds object.
        RectangularBounds bounds = RectangularBounds.newInstance(
                new LatLng(-33.880490, 151.184363),
                new LatLng(-33.858754, 151.229596));
        Intent autocompleteIntent =
                new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .setTypeFilter(typeFilters.get(0))

                        //.setInitialQuery("Search my data")
                        //.setTypeFilter(TypeFilter.ADDRESS)
                        .build(this);

        startActivityForResult(autocompleteIntent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    relative_searchMain.setVisibility(View.GONE);
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    Log.e("TEST", "Place: " + place.getName()+"   "+place.getLatLng()+"  "+place.getAddress());
                    //tv_addLocation.setText(place.getAddress());
                    //tv_addLocation.setText(place.getName());
                    latitude=place.getLatLng().latitude+"";
                    longitude=place.getLatLng().longitude+"";
                    place_id=place.getId();
                    countryType = place.getTypes().get(0).toString();
                    getFullAddressFromLatAndLong(place.getLatLng().latitude,place.getLatLng().longitude);

                    Log.e("LATLNG=",latitude+"   "+longitude);
                    if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                        Intent intent=new Intent(getApplicationContext(),ImageListByLocationActivity.class);
                        intent.putExtra(Constants.LATITUDE,latitude);
                        intent.putExtra(Constants.LONGITUDE,longitude);
                        intent.putExtra(Constants.KEY,place.getName());
                        intent.putExtra("place_id",place.getId());
                        intent.putExtra("country_code",country_code);
                        intent.putExtra("country_type",countryType);
                        startActivity(intent);
                        //String key = et_search.getText().toString();
                        //RetrofitHelper.getInstance().getSearchByTagLocationNameSuggession(getSearchDataCallback, key, byTypeSearch);
                    }

                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Log.e("ErrorCheck=","InsideResultError");
                // TODO: Handle the error.

                if (data != null) {
                    Status status = Autocomplete.getStatusFromIntent(data);
                    Log.i("TEST", status.getStatusMessage());
                }

            } else if (resultCode == RESULT_CANCELED) {
                Log.e("ErrorCheck=","InsideCanceled");
                // The user canceled the operation.
            }
        }
    }


    public void getFullAddressFromLatAndLong(double latitude,double longitude){
        Locale aLocale = new Locale.Builder().setLanguage("en").setScript("Latn").setRegion("RS").build();
        Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (addresses.size() > 0) {
                //System.out.println(addresses.get(0).getLocality());
                // addresses.get(0).getCountryName();
                country=addresses.get(0).getCountryName();
                state=addresses.get(0).getAdminArea();
                country_code=addresses.get(0).getCountryCode();


                Log.e("AllAddress=",addresses.get(0).getCountryName()+"  "+addresses.get(0).getAdminArea());


            }
            else {
                // do your stuff
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
