package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.FollowingListAdapter;
import com.example.touch.adapters.SearchImageDetailsAdapter;
import com.example.touch.adapters.SearchListImageAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.HomeMain;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.SearchDetailsMain;
import com.example.touch.models.SearchDetailsMainData;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchImageDetailsActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView_SearchDetails)
    RecyclerView recyclerView_SearchDetails;

    @BindView(R.id.imgView_back)
    ImageView imgView_back;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_search_image_details);
        ButterKnife.bind(this);
        userId=getIntent().getStringExtra("userID");

        //Adapter for list following show
        recyclerView_SearchDetails.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));


        viewFinds();
    }

    private void viewFinds() {
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            //String userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getDetailsSearchData(userDetailsImageCallback,userId);

        }else{
            Snackbar.make(progressBar,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.imgView_back)
    public void backClick(View view){
        onBackPressed();
    }
    Callback<SearchDetailsMain> userDetailsImageCallback=new Callback<SearchDetailsMain>(){

        @Override
        public void onResponse(Call<SearchDetailsMain> call, Response<SearchDetailsMain> response) {
            progressBar.setVisibility(View.GONE);
            List<SearchDetailsMainData> followersListMainData=new ArrayList<>();
            followersListMainData.clear();

            if (response.body().getResult()!=null){
                followersListMainData.addAll(response.body().getResult());
                SearchImageDetailsAdapter searchImageDetailsAdapter = new SearchImageDetailsAdapter(getApplicationContext(),followersListMainData,userId);
                recyclerView_SearchDetails.setAdapter(searchImageDetailsAdapter);
            }else{

            }

        }

        @Override
        public void onFailure(Call<SearchDetailsMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
