package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.touch.R;
import com.example.touch.constants.Constants;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.fragments.SearchFragment;
import com.example.touch.models.GooglePlace;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchLocationActivity extends AppCompatActivity {
    @BindView(R.id.recyclerview_location)
    RecyclerView recyclerview_location;

    private SharedPreferencesData sharedPreferencesData;

    @BindView(R.id.et_search)
    EditText et_search;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_search_location);
        ButterKnife.bind(this);
        recyclerview_location.setHasFixedSize(true);
        recyclerview_location.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                googleplaces = new googleplaces();
                googleplaces.execute(s);

                if (s.toString().length()<1){
                    recyclerview_location.setVisibility(View.GONE);
                }else{
                    recyclerview_location.setVisibility(View.VISIBLE);
                }
            }
        });
    }
    final String GOOGLE_KEY = "AIzaSyCE_5xAkP50cQKk2suoeL0NByrstAR03gU";
    ArrayList<GooglePlace> arrayListLocation = new ArrayList();
    private LocationRecyclerView locationRecyclerViewAdapter;
    googleplaces googleplaces;

    private class googleplaces extends AsyncTask {

        String temp;


        @Override
        protected Object doInBackground(Object[] objects) {
            Log.e("objectzero", objects[0].toString());
            String findLocationText = objects[0].toString();
            String first = "https://maps.googleapis.com/maps/api/place/search/json?location=\" + latitude + \",\" + longtitude + \"&radius=100&sensor=true&key=\" + GOOGLE_KEY";
            String second = null;
            try {
                second = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + URLEncoder.encode(findLocationText, "UTF-8") + "+main+street&key=" + GOOGLE_KEY;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            temp = makeCall(second);
            return temp;
        }

        @Override
        protected void onPreExecute() {
            if (temp == null) {
                // we have an error to the call
                // we can also stop the progress bar
            } else {
                // all things went right

                // parse Google places search result
                arrayListLocation = parseGoogleParse(temp);


            }
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            arrayListLocation.clear();
            arrayListLocation = parseGoogleParse(o.toString());
            Log.e("searchResultData", arrayListLocation.size() + "");
            locationRecyclerViewAdapter = new LocationRecyclerView(arrayListLocation);
            recyclerview_location.setAdapter(locationRecyclerViewAdapter);
            locationRecyclerViewAdapter.notifyDataSetChanged();
            //googleplaces.cancel(true);
        }
    }

    public static String makeCall(String url) {

        // string buffers the url
        StringBuffer buffer_string = new StringBuffer(url);
        String replyString = "";

        // instanciate an HttpClient
        HttpClient httpclient = new DefaultHttpClient();
        // instanciate an HttpGet
        ;
        HttpGet httpget = new HttpGet(buffer_string.toString());

        try {
            // get the responce of the httpclient execution of the url
            HttpResponse response = httpclient.execute(httpget);
            InputStream is = response.getEntity().getContent();

            // buffer input stream the result
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);
            int current = 0;
            while ((current = bis.read()) != -1) {
                baf.append((byte) current);
            }
            // the result as a string is ready for parsing
            replyString = new String(baf.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(replyString);

        // trim the whitespaces
        return replyString.trim();
    }

    private static ArrayList parseGoogleParse(final String response) {

        ArrayList temp = new ArrayList();
        try {

            // make an jsonObject in order to parse the response
            JSONObject jsonObject = new JSONObject(response);

            // make an jsonObject in order to parse the response
            if (jsonObject.has("results")) {

                JSONArray jsonArray = jsonObject.getJSONArray("results");



                for (int i = 0; i < jsonArray.length(); i++) {
                    GooglePlace poi = new GooglePlace();
                    JSONObject geometry=jsonArray.getJSONObject(i);
                    JSONObject geometry1=geometry.getJSONObject("geometry");
                    JSONObject location=geometry1.getJSONObject("location");
                    poi.setLat(location.optString("lat"));
                    poi.setLng(location.optString("lng"));



                    if (jsonArray.getJSONObject(i).has("name")) {
                        poi.setAddress(jsonArray.getJSONObject(i).optString("formatted_address"));
                        poi.setName(jsonArray.getJSONObject(i).optString("name"));
                        poi.setRating(jsonArray.getJSONObject(i).optString("rating", " "));

                        if (jsonArray.getJSONObject(i).has("opening_hours")) {
                            if (jsonArray.getJSONObject(i).getJSONObject("opening_hours").has("open_now")) {
                                if (jsonArray.getJSONObject(i).getJSONObject("opening_hours").getString("open_now").equals("true")) {
                                    poi.setOpenNow("YES");
                                } else {
                                    poi.setOpenNow("NO");
                                }
                            }
                        } else {
                            poi.setOpenNow("Not Known");
                        }
                        if (jsonArray.getJSONObject(i).has("types")) {
                            JSONArray typesArray = jsonArray.getJSONObject(i).getJSONArray("types");

                            for (int j = 0; j < typesArray.length(); j++) {
                                poi.setCategory(typesArray.getString(j) + ", " + poi.getCategory());
                            }
                        }
                    }
                    temp.add(poi);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList();
        }
        return temp;

    }
    public class LocationRecyclerView extends RecyclerView.Adapter<LocationRecyclerView.ViewHolder> {
        ArrayList<GooglePlace> arrayListLocation;

        public LocationRecyclerView(ArrayList<GooglePlace> arrayListLocation) {
            this.arrayListLocation=arrayListLocation;
        }

        @Override
        public LocationRecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.item_location_set, parent, false);
            LocationRecyclerView.ViewHolder viewHolder = new LocationRecyclerView.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(LocationRecyclerView.ViewHolder holder, final int position) {
            //Log.e("insideRecyclerView=",arrayListLocation.get(position).toString()+"");
            if(arrayListLocation!=null&&arrayListLocation.size()>0&&position<arrayListLocation.size()){
                holder.tv_location.setText(arrayListLocation.get(position).getAddress());
                //Log.e("insideRecyclerView=",arrayListLocation.get(position).toString()+"");
            }


            holder.layoutLinear_location.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View v) {
                    Log.e("position=",arrayListLocation.get(position).getLat()+"");
                    et_search.setText(arrayListLocation.get(position).getAddress());
                    recyclerview_location.setVisibility(View.GONE);
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.LOCATION,arrayListLocation.get(position).getAddress());
                    onBackPressed();
                    /*et_getLocation.setText(arrayListLocation.get(position).getAddress());
                    sharedPreferences=getSharedPreferences(Constants.MYLOCATIONPREF,MODE_PRIVATE);
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putString(Constants.LAT,arrayListLocation.get(position).getLat());
                    editor.putString(Constants.LNG,arrayListLocation.get(position).getLng());
                    editor.commit();
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.location_set),Toast.LENGTH_LONG).show();
                    finish();*/
                }
            });
        }


        @Override
        public int getItemCount() {
            return arrayListLocation.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView tv_location;
            private LinearLayout layoutLinear_location,layoutLinear_top;
            private ImageView imgV_marker;


            public ViewHolder(View itemView) {
                super(itemView);
                tv_location=itemView.findViewById(R.id.tv_location);
                layoutLinear_location=itemView.findViewById(R.id.layoutLinear_location);
                layoutLinear_top=itemView.findViewById(R.id.layoutLinear_top);
                imgV_marker=itemView.findViewById(R.id.imgV_marker);


            }
        }
    }
}
