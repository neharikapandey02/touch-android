package com.example.touch.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.touch.R;
import com.example.touch.models.AnotherUserProfileResult;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.MultipleImageGetMainData;
import com.example.touch.video_pk.SimpleAdapter1;
import com.example.touch.video_pk.SimpleAdapterForAnotherUserProfile;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import im.ene.toro.widget.Container;

public class SearchPostActivity extends AppCompatActivity {

    @BindView(R.id.container)
    Container container;

    private SimpleAdapter1 simpleAdapter;
    ArrayList<HomeMainData> arrayList;
    ArrayList<AnotherUserProfileResult> arrayList1;
    ArrayList<MultipleImageGetMainData> arrayList2;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide();
        setContentView(R.layout.activity_search_post);
        ButterKnife.bind(this);
        BottomNavigationView nav_view = findViewById(R.id.nav_view);
        try {
                String bottomNav = getIntent().getStringExtra("bottom_nav");
                assert bottomNav != null;
                switch (bottomNav){
                    case "home":{
                        nav_view.setSelectedItemId(R.id.navigation_home);
                        break;
                    }
                    case "profile":{
                        nav_view.setSelectedItemId(R.id.navigation_profile);
                        break;
                    }
                    case "search":{
                        nav_view.setSelectedItemId(R.id.navigation_dashboard);
                        break;
                    }
                    case "activity":{
                        nav_view.setSelectedItemId(R.id.navigation_likes);
                        break;
                    }
                    case "post":{
                        nav_view.setSelectedItemId(R.id.navigation_notifications);
                        break;
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        nav_view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home: {
                        Intent intent = new Intent(SearchPostActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "home");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_profile:{
                        Intent intent = new Intent(SearchPostActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "profile");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_dashboard:{
                        Intent intent = new Intent(SearchPostActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "search");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_likes:{
                        Intent intent = new Intent(SearchPostActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "activity");
                        startActivity(intent);
                        break;
                    }
                    case R.id.navigation_notifications:{
                        Intent intent = new Intent(SearchPostActivity.this, MainActivity.class);
                        intent.putExtra("fragment", "post");
                        startActivity(intent);
                        break;
                    }
                }
                return false;
            }
        });
        layoutManager = new LinearLayoutManager(SearchPostActivity.this);
        container.setLayoutManager(layoutManager);
        arrayList = new ArrayList<>();
        arrayList1 = new ArrayList<>();
        arrayList2 = new ArrayList<>();
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundle");
        assert bundle != null;
        try {
            arrayList = (ArrayList<HomeMainData>)bundle.getSerializable("arraylist");
            arrayList1 = (ArrayList<AnotherUserProfileResult>)bundle.getSerializable("arraylist");
//            arrayList2 = (ArrayList<MultipleImageGetMainData>)bundle.getSerializable("arraylist");
        } catch (Exception e) {
            e.printStackTrace();
        }
        int pos = intent.getIntExtra("position", 0);
        String page = intent.getStringExtra("page");
        if (page != null && page.equalsIgnoreCase("another")){

            SimpleAdapterForAnotherUserProfile adapter = new SimpleAdapterForAnotherUserProfile(SearchPostActivity.this, arrayList1);
            container.setAdapter(adapter);
            container.scrollToPosition(pos);
        }
        else {
            simpleAdapter = new SimpleAdapter1(SearchPostActivity.this, arrayList);
            container.setAdapter(simpleAdapter);
            container.scrollToPosition(pos);
        }

    }
}
