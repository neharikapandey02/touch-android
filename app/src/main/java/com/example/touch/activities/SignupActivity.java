package com.example.touch.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.SpinnerCallAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.models.CategoryAdapter;
import com.example.touch.models.CategoryMain;
import com.example.touch.models.CategoryMainData;
import com.example.touch.models.SignUpMain;
import com.example.touch.models.ValidateEmailMain;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {
   /* @BindView(R.id.linear_layoutDatePicker)
    LinearLayout linear_layoutDatePicker;

    @BindView(R.id.tv_dateShow)
    TextView tv_dateShow;*/

    @BindView(R.id.et_firstName)
    EditText et_firstName;

    @BindView(R.id.et_lastName)
    EditText et_lastName;

    @BindView(R.id.et_email)
    EditText et_email;

    @BindView(R.id.et_password)
    EditText et_password;

    @BindView(R.id.confirmPasswordEt)
    EditText confirmPasswordEt;

    @BindView(R.id.et_phoneNumber)
    EditText et_phoneNumber;

    @BindView(R.id.et_countryCode)
    EditText et_countryCode;

    @BindView(R.id.radioButtonMale)
    RadioButton radioButtonMale;

    @BindView(R.id.radioButtonFemale)
    RadioButton radioButtonFemale;

    @BindView(R.id.btn_SignUp)
    Button btn_SignUp;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;


    @BindView(R.id.spinner_category)
    Spinner spinner_category;

    @BindView(R.id.radioGroupType)
    RadioGroup radioGroupType;


    @BindView(R.id.checkbox_term)
    CheckBox checkbox_term;


    @BindView(R.id.tv_term_condition_text)
    TextView tv_term_condition_text;


    @BindView(R.id.tv_doWhat)
    TextView tv_doWhat;


    @BindView(R.id.linearLayout_gender)
    LinearLayout linearLayout_gender;


    @BindView(R.id.spinner_age)
    Spinner spinner_age;

    private String gender;

    List<CategoryMainData> categoryMainDataList;

    private String userType;

    private String selectedCategoryId;

    private int userAgeCount = 0;
    private boolean statusTermAndCondition=false;
    List ageSpinner;

    @BindView(R.id.ccp)
    CountryCodePicker ccp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        viewFinds();

//        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
//            @Override
//            public void onCountrySelected() {
//                Toast.makeText(SignupActivity.this, ""+ccp.getSelectedCountryCodeWithPlus(), Toast.LENGTH_SHORT).show();
//            }
//        });
        //makeText(SignupActivity.this,"THis is test for me",4000).show();

    }


    public static Snackbar makeText(Context context, String message, int duration) {
        Activity activity = (Activity) context;
        View layout;
        Snackbar snackbar = Snackbar
                .make(activity.findViewById(android.R.id.content), message, duration);
        layout = snackbar.getView();
        //setting background color
        layout.setBackgroundColor(context.getResources().getColor(R.color.main_red));
        android.widget.TextView text = (android.widget.TextView) layout.findViewById(R.id.snackbar_text);
        //setting font color
        text.setTextColor(context.getResources().getColor(R.color.white));
        Typeface font = null;
        //Setting font
       // font = Typeface.createFromAsset(context.get(), "dejavusus_bold.ttf");
      //  text.setTypeface(font);
        return snackbar;

    }


    private void viewFinds() {
        userType="1";
        tv_doWhat.setText(getResources().getString(R.string.dowhatuser));
        linearLayout_gender.setVisibility(View.GONE);
        categoryMainDataList=new ArrayList<>();

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String countryIso = telephonyManager.getSimCountryIso().toUpperCase();
        et_countryCode.setText(countryIso);
        //comment previous get category blond bigtits and others
       /* if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getCategoryList(getCategoryCallback);

        }else {
            Snackbar.make(radioGroup, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }*/
        radioGroupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                if (checkedId==R.id.radioButtonUser){
                    spinner_category.setVisibility(View.GONE);
                    userType="1";
                    btn_SignUp.setText("Sign Up");
                    tv_doWhat.setText(getResources().getString(R.string.dowhatuser));
                    linearLayout_gender.setVisibility(View.GONE);
                }else{
                    spinner_category.setVisibility(View.GONE);
                    userType="2";
                    userType="2";
                    btn_SignUp.setText("Next");
                    tv_doWhat.setText(getResources().getString(R.string.dowhatescortuser));
                    linearLayout_gender.setVisibility(View.VISIBLE);
                }
            }
        });
        checkbox_term.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    statusTermAndCondition = true;
                }else{
                    statusTermAndCondition = false;
                }
                Log.e("checkedCheckbox=", b + "");
            }
        });
        //Spinner for age
        addSpinnerData();
    }

    private void addSpinnerData() {
        ageSpinner=new ArrayList();
        ageSpinner.add("Age");
        for (int i=18;i<=80;i++){
            ageSpinner.add(i+"");
        }
        SpinnerCallAdapter spinnerCallAdapter = new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, ageSpinner);
        spinner_age.setAdapter(spinnerCallAdapter);
    }

    Callback<CategoryMain> getCategoryCallback=new Callback<CategoryMain>() {
        @Override
        public void onResponse(Call<CategoryMain> call, Response<CategoryMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getData()!=null){
                    categoryMainDataList.clear();
                    categoryMainDataList.addAll(response.body().getData());
                    CategoryAdapter adapter = new CategoryAdapter(getApplicationContext(),
                            R.layout.customspinneritem, categoryMainDataList);
                    spinner_category.setAdapter(adapter);
                }
            }
        }
        @Override
        public void onFailure(Call<CategoryMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

   /* @OnClick(R.id.linear_layoutDatePicker)
    public void selectDate(View view) {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        //getAge(mYear,mMonth,mDay);

                        tv_dateShow.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        //tv_dateShow.setText(getAge(year,monthOfYear,dayOfMonth));
                        getAge(year,monthOfYear,dayOfMonth);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }*/
    String firstName;
    String lastName;
    String email;
    String password;
    String number;
    String dob;

    @OnClick(R.id.btn_SignUp)
    public void signUpButtonClick(View view) {

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validate()) {
                progressBar.setVisibility(View.VISIBLE);
                firstName = et_firstName.getText().toString();
                lastName= et_lastName.getText().toString();
                email = et_email.getText().toString();
                password = et_password.getText().toString();
                number= ccp.getSelectedCountryCodeWithPlus()+" "+et_phoneNumber.getText().toString().trim();
                dob =spinner_age.getSelectedItem()+"";
                Log.e("TAG","AGE="+dob);
                //String country = et_country.getText().toString();
                //String city = et_city.getText().toString();
                //String aboutus = et_about.getText().toString();
               // String hairColor = et_hairColor.getText().toString();
                //String weight = et_weight.getText().toString();
                //String height = et_height.getText().toString();
                //String availability = et_available.getText().toString();
                int selectedId = radioGroup.getCheckedRadioButtonId();


                if (selectedId == R.id.radioButtonMale) {
                    gender = "male";
                } else if(selectedId==R.id.radioButtonFemale) {
                    gender = "female";
                }else{
                    gender="trasander";
                }


                if (!userType.equalsIgnoreCase("1")){

                    for (int i=0;i<categoryMainDataList.size();i++){


                        if (i==spinner_category.getSelectedItemPosition()){
                           // Log.e("selectedSpinnerItem=","  "+categoryMainDataList.get(i).getId());
                            selectedCategoryId=categoryMainDataList.get(i).getId();
                        }
                    }
                }else{
                    selectedCategoryId="";
                }



                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().getEmailValidate(checkEmailValidateCallback, email,firstName);







                //Log.e("Gender=", gender);
                //RetrofitHelper.getInstance().getSignUp(signupCalling, firstName, lastName, email, password, number, dob, gender,country,city,userType,selectedCategoryId,aboutus,hairColor,weight,height,availability);
            }


        } else {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }




        Callback<ValidateEmailMain> checkEmailValidateCallback = new Callback<ValidateEmailMain>() {
        @Override
        public void onResponse(Call<ValidateEmailMain> call, Response<ValidateEmailMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getStatus()){
                    if (userType.equalsIgnoreCase("2")){
                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put(Constants.FIRST_NAME, firstName);
                        hashMap.put(Constants.LAST_NAME, lastName);
                        hashMap.put(Constants.EMAIL, email);
                        hashMap.put(Constants.PASSWORD, password);
                        hashMap.put(Constants.PHONE_NUMBER, number);
                        hashMap.put(Constants.DOB, dob);
                        hashMap.put(Constants.GENDER, gender);
                        hashMap.put(Constants.USER_TYPE, userType);
                        hashMap.put(Constants.CATEGORY_ID, selectedCategoryId);




                        Intent intent=new Intent(getApplicationContext(),SignupSecondPage.class);
                        intent.putExtra(Constants.SIGNUP_FIRST, hashMap);
                        startActivity(intent);
                    }else{
                        Log.e("UserTypeCheck=",userType);
                        RetrofitHelper.getInstance().getSignUp(signupCalling, firstName, lastName, email, password, number, dob, gender, selectedCategoryId, userType,"","", "", "","","","","","","","","","","","");
                    }
                }else{
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_LONG).show();
                }
                }

        }

        @Override
        public void onFailure(Call<ValidateEmailMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    Callback<SignUpMain> signupCalling = new Callback<SignUpMain>() {
        @Override
        public void onResponse(Call<SignUpMain> call, Response<SignUpMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {

                //Log.e("Message=",response.body().getMessage());
                if (response.body().getSuccess() == 1) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }

        @Override
        public void onFailure(Call<SignUpMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    public boolean validate() {
        if (et_firstName.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_user_name), Snackbar.LENGTH_LONG).show();
            return false;
        }  else if (isValidEmail(et_email.getText().toString())) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_valid_email), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_password.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_password), Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (et_password.getText().toString().trim().length()<8) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.password_should_min_8_char), Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (confirmPasswordEt.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_confirm_password), Snackbar.LENGTH_LONG).show();
            return false;
        }
        else if (!et_password.getText().toString().trim().equalsIgnoreCase(confirmPasswordEt.getText().toString().trim())){
            Snackbar.make(btn_SignUp, getResources().getString(R.string.password_not_matched), Snackbar.LENGTH_LONG).show();
            return false;
        }
//        else if (et_countryCode.getText().toString().trim().equalsIgnoreCase("")){
//            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_country_code), Snackbar.LENGTH_LONG).show();
//            return false;
//        }
        else if (et_phoneNumber.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_mobile_number), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (et_phoneNumber.getText().toString().length()<9) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_valid_mobile), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (spinner_age.getSelectedItemPosition()==0) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_select_age), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (!statusTermAndCondition) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_accept_term_and_condition), Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    public static boolean isValidEmail(CharSequence target) {
        return !(!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
    private String getAge(int year, int month, int day){
        //Log.e("Date=",month+"");

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }


        Integer ageInt = new Integer(age);
        userAgeCount = ageInt;
        String ageS = ageInt.toString();
        //Log.e("Date=",ageS);
        return ageS;
    }

    @OnClick(R.id.tv_term_condition_text)
    public void termtextClick(View view){
       Intent intent=new Intent(getApplicationContext(),SignupTermAndCondtion.class);
       startActivity(intent);
    }

}
