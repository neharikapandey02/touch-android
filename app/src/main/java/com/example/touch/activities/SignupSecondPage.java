package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.emoji.widget.EmojiEditText;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.example.touch.R;
import com.example.touch.adapters.SpinnerCallAdapter;
import com.example.touch.constants.Constants;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import hani.momanii.supernova_emoji_library.Helper.EmojiconEditText;

public class SignupSecondPage extends AppCompatActivity {
    @BindView(R.id.btn_SignUp)
    Button btn_SignUp;


    @BindView(R.id.et_about)
    EmojiEditText et_about;

   /* @BindView(R.id.et_hairColor)
    EditText et_hairColor;*/


   /* @BindView(R.id.et_weight)
    EditText et_weight;*/

    /*   @BindView(R.id.et_height)
    EditText et_height;
*/
   /* @BindView(R.id.et_breastSize)
    EditText et_breastSize;*/

   /* @BindView(R.id.et_eyeColor)
    EditText et_eyeColor;*/

    @BindView(R.id.et_hobby)
    EditText et_hobby;

    @BindView(R.id.spinner_weight)
    Spinner spinner_weight;

    @BindView(R.id.spinner_height)
    Spinner spinner_height;


    @BindView(R.id.spinner_hairColor)
    Spinner spinner_hairColor;


    @BindView(R.id.spinner_breast)
    Spinner spinner_breast;


    @BindView(R.id.spinner_breastType)
    Spinner spinner_breastType;

    @BindView(R.id.spinner_eyeColor)
    Spinner spinner_eyeColor;

    @BindView(R.id.et_available)
    EditText et_available;


    HashMap<String, String> hashMap;
    List weightList, heightList, hairColorList, breastList, breastListType, eyeColorList;

    EmojIconActions emojIcon;
    View rootView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_signup_second_page);
        ButterKnife.bind(this);

        viewFinds();


    }


    private void viewFinds() {





        Intent intent = getIntent();
        hashMap = (HashMap<String, String>) intent.getSerializableExtra(Constants.SIGNUP_FIRST);
        Log.e("HashMapTest", hashMap.get(Constants.FIRST_NAME) + " " + hashMap.get(Constants.LAST_NAME));
        addSpinnerHairColor();
        addSpinnerWeight();
        addSpinnerHeight();
        addSpinnerBreast();
        addSpinnerBreastType();
        addSpinnerEyeColor();
    }

    private void addSpinnerHairColor() {
        hairColorList = new ArrayList();
        hairColorList.add("Hair color");
        hairColorList.add("Brown");
        hairColorList.add("Blond");
        hairColorList.add("Red");
        hairColorList.add("Black");
        hairColorList.add("Light brown");
        hairColorList.add("Dark blond");
        SpinnerCallAdapter spinnerCallAdapter = new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, hairColorList);
        spinner_hairColor.setAdapter(spinnerCallAdapter);
    }

    private void addSpinnerHeight() {
        heightList = new ArrayList();
        heightList.add("Height in cm");
        for (int i = 145; i <= 200; i++) {
            heightList.add(i + "");
        }
        SpinnerCallAdapter spinnerCallAdapter = new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, heightList);
        spinner_height.setAdapter(spinnerCallAdapter);
    }


    private void addSpinnerWeight() {
        weightList = new ArrayList();
        weightList.add("Weight in kg");
        for (int i = 40; i <= 100; i++) {
            weightList.add(i + "");
        }
        SpinnerCallAdapter spinnerCallAdapter = new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, weightList);
        spinner_weight.setAdapter(spinnerCallAdapter);
    }

    private void addSpinnerBreast() {
        breastList = new ArrayList();
        breastList.add("Breast size");
        for (int i = 70; i <= 100; i = i + 5) {
            breastList.add(i + "");
        }
        SpinnerCallAdapter spinnerCallAdapter = new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, breastList);
        spinner_breast.setAdapter(spinnerCallAdapter);
    }

    private void addSpinnerBreastType() {
        breastListType = new ArrayList();
        breastListType.add("Size");
        breastListType.add("A");
        breastListType.add("B");
        breastListType.add("C");
        breastListType.add("D");
        breastListType.add("E");
        breastListType.add("F");

        SpinnerCallAdapter spinnerCallAdapter = new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, breastListType);
        spinner_breastType.setAdapter(spinnerCallAdapter);
    }


    private void addSpinnerEyeColor() {
        eyeColorList = new ArrayList();
        eyeColorList.add("Eyes");
        eyeColorList.add("Black");
        eyeColorList.add("Brown");
        eyeColorList.add("Blue");
        eyeColorList.add("Green");
        eyeColorList.add("Grey");


        SpinnerCallAdapter spinnerCallAdapter = new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, eyeColorList);
        spinner_eyeColor.setAdapter(spinnerCallAdapter);
    }

    @OnClick(R.id.btn_SignUp)
    public void btnSignupClick(View view) {
        if (validate()) {
            Log.e("TAG", "Weight" + spinner_weight.getSelectedItem() + "  " + spinner_height.getSelectedItem() + spinner_breast.getSelectedItem() + " " + spinner_breastType.getSelectedItem() + spinner_eyeColor.getSelectedItem());
            hashMap.put(Constants.ABOUT_US, et_about.getText().toString());
            if (spinner_hairColor.getSelectedItem().toString().equalsIgnoreCase("Hair color")) {
                hashMap.put(Constants.HAIR_COLOR, "");
            } else {
                hashMap.put(Constants.HAIR_COLOR, spinner_hairColor.getSelectedItem() + "");
            }

            if (spinner_weight.getSelectedItem().toString().equalsIgnoreCase("Weight in kg")) {
                hashMap.put(Constants.WEIGHT, "");
            } else {
                hashMap.put(Constants.WEIGHT, spinner_weight.getSelectedItem() + "");
            }
            if (spinner_height.getSelectedItem().toString().equalsIgnoreCase("Height in cm")) {
                hashMap.put(Constants.HEIGHT, "");
            } else {
                hashMap.put(Constants.HEIGHT, spinner_height.getSelectedItem() + "");
            }
            if (spinner_breast.getSelectedItem().toString().equalsIgnoreCase("Breast size")) {
                hashMap.put(Constants.BREAST_SIZE, "");
            } else {
                if (spinner_breastType.getSelectedItem().toString().equalsIgnoreCase("size")) {
                    hashMap.put(Constants.BREAST_SIZE, spinner_breast.getSelectedItem() + "");
                } else {
                    hashMap.put(Constants.BREAST_SIZE, spinner_breast.getSelectedItem() + " " + spinner_breastType.getSelectedItem());
                }

            }
            if (spinner_eyeColor.getSelectedItem().toString().equalsIgnoreCase("Eyes")) {
                hashMap.put(Constants.EYE_COLOR, "");
            } else {
                hashMap.put(Constants.EYE_COLOR, spinner_eyeColor.getSelectedItem() + "");

            }


            hashMap.put(Constants.HOBBY, et_hobby.getText().toString());
            hashMap.put(Constants.AVAILABLE, et_available.getText().toString());
            Intent intent = new Intent(getApplicationContext(), SignupThird.class);
            intent.putExtra(Constants.SIGNUP_FIRST, hashMap);
            startActivity(intent);
        }

    }

    public boolean validate() {
        if (et_about.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_about), Snackbar.LENGTH_LONG).show();
            return false;
        } /*else if (spinner_hairColor.getSelectedItemPosition()==0) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_select_hair_color), Snackbar.LENGTH_LONG).show();
            return false;
        }  else if (spinner_weight.getSelectedItemPosition()==0) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_select_weight), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (spinner_height.getSelectedItemPosition()==0) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_select_height), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (spinner_breast.getSelectedItemPosition()==0) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_select_breast_size), Snackbar.LENGTH_LONG).show();
            return false;
        }else if (spinner_breastType.getSelectedItemPosition()==0) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_select_breast_type), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (spinner_eyeColor.getSelectedItemPosition()==0) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_select_eye_color), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_hobby.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_hobby), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_available.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_available), Snackbar.LENGTH_LONG).show();
            return false;
        }*/
        return true;
    }
}
