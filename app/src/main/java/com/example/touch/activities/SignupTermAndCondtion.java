package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.example.touch.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupTermAndCondtion extends AppCompatActivity {
    @BindView(R.id.imgview_backIcons)
    ImageView imgview_backIcons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_signup_term_and_condtion);
        ButterKnife.bind(this);

    }
    @OnClick(R.id.imgview_backIcons)
    public void backClick(View view){
        onBackPressed();
    }
}
