package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.SpinnerCallAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.fragments.SearchFragment;
import com.example.touch.models.GooglePlace;
import com.example.touch.models.SignUpMain;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.snackbar.Snackbar;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupThird extends AppCompatActivity {

    @BindView(R.id.btn_SignUp)
    Button btn_SignUp;

   /* @BindView(R.id.et_ethinicity)
    EditText et_ethinicity;*/

    @BindView(R.id.et_origin)
    EditText et_origin;

    @BindView(R.id.et_country)
    TextView et_country;

    @BindView(R.id.et_city)
    EditText et_city;

    @BindView(R.id.et_language)
    EditText et_language;



    @BindView(R.id.et_website)
    EditText et_website;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.spinner_callType)
    Spinner spinner_callType;


    @BindView(R.id.spinner_ethnicity)
    Spinner spinner_ethnicity;

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String number;
    private String dob;
    private String gender;
    private String userType;
    private String selectedCategoryId;
    private String aboutus;
    private String hairColor;
    private String weight;
    private String height;
    private String breastSize;
    private String eyeColor;
    private String hobby;
    private String availability;
    private String ethnicity;
    private String origin;
    private String country;
    private String city;
    private String language;
    private String incall;
    private String website;
    HashMap<String, String> hashMap;
    List listEthnicity;

    private List<String> listSpinner;
    private SharedPreferencesData sharedPreferencesData;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE=1001;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_signup_third);
        ButterKnife.bind(this);

        viewFinds();
    }

    private void addSpinnerEyeColor() {
        listEthnicity=new ArrayList();
        listEthnicity.add("Ethnicity");
        listEthnicity.add("Caucasian");
        listEthnicity.add("European");
        listEthnicity.add("African");
        listEthnicity.add("Arabian");
        listEthnicity.add("Latina");
        listEthnicity.add("Asian");
        listEthnicity.add("Indian");

        SpinnerCallAdapter spinnerCallAdapter=new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, listEthnicity);
        spinner_ethnicity.setAdapter(spinnerCallAdapter);
    }

    private void viewFinds() {
        listSpinner=new ArrayList<>();
        listSpinner.add("In Call");
        listSpinner.add("Out Call");
        listSpinner.add("both");
        SpinnerCallAdapter spinnerCallAdapter=new SpinnerCallAdapter(getApplicationContext(),
                R.layout.customspinneritem, listSpinner);
        spinner_callType.setAdapter(spinnerCallAdapter);

        Intent intent = getIntent();
        hashMap= (HashMap<String, String>)intent.getSerializableExtra(Constants.SIGNUP_FIRST);
        if (hashMap!=null){
            firstName=hashMap.get(Constants.FIRST_NAME);
            lastName=hashMap.get(Constants.LAST_NAME);
            email=hashMap.get(Constants.EMAIL);
            password=hashMap.get(Constants.PASSWORD);
            number=hashMap.get(Constants.PHONE_NUMBER);
            dob=hashMap.get(Constants.DOB);
            gender=hashMap.get(Constants.GENDER);
            userType=hashMap.get(Constants.USER_TYPE);
            selectedCategoryId=hashMap.get(Constants.CATEGORY_ID);
            aboutus=hashMap.get(Constants.ABOUT_US);
            hairColor=hashMap.get(Constants.HAIR_COLOR);
            weight=hashMap.get(Constants.WEIGHT);
            height=hashMap.get(Constants.HEIGHT);
            breastSize=hashMap.get(Constants.BREAST_SIZE);
            eyeColor=hashMap.get(Constants.EYE_COLOR);
            hobby=hashMap.get(Constants.HOBBY);
            availability=hashMap.get(Constants.AVAILABLE);
        }


        addSpinnerEyeColor();

    }



    @OnClick(R.id.btn_SignUp)
    public void signUpClick(View view){
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            if (validate()) {
                //spinner_callType.getSelectedItemPosition();
                if (spinner_ethnicity.getSelectedItem().toString().equalsIgnoreCase("ethnicity")){
                    ethnicity="";
                }else{
                    ethnicity=spinner_ethnicity.getSelectedItem()+"";
                }
                origin=et_origin.getText().toString();
                country=et_country.getText().toString();
                city=et_country.getText().toString();
                language=et_language.getText().toString();
                incall=spinner_callType.getSelectedItemPosition()+1+"";
                Log.e("spinner=",incall);
                website=et_website.getText().toString();
                RetrofitHelper.getInstance().getSignUp(signupCalling, firstName, lastName, email, password, number, dob, gender, selectedCategoryId, userType,aboutus,hairColor, weight, height,breastSize,eyeColor,hobby,availability,ethnicity,origin,country,city,language,incall,website);
            }
        }
    }


    Callback<SignUpMain> signupCalling = new Callback<SignUpMain>() {
        @Override
        public void onResponse(Call<SignUpMain> call, Response<SignUpMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                //Log.e("Message=",response.body().getMessage());
                if (response.body().getSuccess() == 1) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }

        @Override
        public void onFailure(Call<SignUpMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onRes=","Onresume");
        //sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
       // if (sharedPreferencesData!=null)
       // et_country.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.LOCATION));
      //  et_city.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.LOCATION));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        if (sharedPreferencesData!=null){
            sharedPreferencesData.clearSingleFieldSharedData(Constants.USER_LOGIN_SHARED,Constants.LOCATION);
        }
            //et_country.setText(sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.LOCATION));

    }

    public boolean validate() {
        /*if (spinner_ethnicity.getSelectedItemPosition()==0) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_select_ethinicity), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_origin.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_origin), Snackbar.LENGTH_LONG).show();
            return false;
        } else
        if (et_country.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_country), Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_language.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, getResources().getString(R.string.please_enter_language), Snackbar.LENGTH_LONG).show();
            return false;
        }*/

        if (et_country.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_SignUp, "Please enter location(city)", Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @OnClick(R.id.et_country)
    public void countryClick(View view){
      // Intent intent=new Intent(getApplicationContext(),SearchLocationActivity.class);
       //startActivity(intent);
        startAutocompleteActivity();
    }

    private void startAutocompleteActivity() {
        Places.initialize(getApplicationContext(), "AIzaSyCE_5xAkP50cQKk2suoeL0NByrstAR03gU");
        List<com.google.android.libraries.places.api.model.Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));

        List<TypeFilter> typeFilters = new ArrayList<>(Arrays.asList(TypeFilter.REGIONS));

// Create a RectangularBounds object.
        RectangularBounds bounds = RectangularBounds.newInstance(
                new LatLng(-33.880490, 151.184363),
                new LatLng(-33.858754, 151.229596));
        Intent autocompleteIntent =
                new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .setTypeFilter(typeFilters.get(0))
                        .build(this);
        startActivityForResult(autocompleteIntent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    Log.i("TEST", "Place: " + place.getName() + ", " + place.getAddress());
                    et_country.setText(place.getAddress());
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.

                if (data != null) {
                    Status status = Autocomplete.getStatusFromIntent(data);
                    Log.i("TEST", status.getStatusMessage());
                }

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }





}
