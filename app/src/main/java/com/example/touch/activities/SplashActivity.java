package com.example.touch.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.se.omapi.Session;
import android.util.Log;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.constants.textview_animate.HTextView;
import com.example.touch.models.LoginMain;
import com.example.touch.models.SessionResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @BindView(R.id.tv_touch)
    HTextView tv_touch;
    SharedPreferencesData sharedPreferencesData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.e("device_id", ""+task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DEVICE_ID, token);
                        // Log and toast
                        Log.e("device_id", token);
//                        Toast.makeText(SplashActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });

        tv_touch.setAnimationListener(new SimpleAnimationListener(this));
        tv_touch.animateText(getResources().getString(R.string.touch));
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            RetrofitHelper.getInstance().getSession(sessionCallback, sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.DEVICE_ID));

        }


//            if (sharedPreferencesData!=null&&!sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID).equalsIgnoreCase("")){
//            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
//                String email=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.EMAIL);
//                String password=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.PASSWORD);
//                String latitude=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.USER_LAT);
//                String longitude=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.USER_LANG);
//                String device_id = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.DEVICE_ID);
//
//                RetrofitHelper.getInstance().getLogin(loginCallback, email, password,latitude+"",longitude+"", device_id);
//
//            } else {
//                //Snackbar.make(tv_touch, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
//            }
//        }

       /* RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(2000);
        rotate.setInterpolator(new LinearInterpolator());

       // ImageView image= (ImageView) findViewById(R.id.imageView);

        tv_touch.startAnimation(rotate);*/
/*
        Animation anim = new CircularRotateAnimation(tv_touch, 20);
//duration of animation
        anim.setDuration(3000);
//start the animation
        tv_touch.startAnimation(anim);*/




    }

    Callback<SessionResponse> sessionCallback = new Callback<SessionResponse>() {
        @Override
        public void onResponse(Call<SessionResponse> call, Response<SessionResponse> response) {
            try {
                if (response.body().getStatus() == 1){
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.TOKEN_ID, response.body().getValue());

                        if (sharedPreferencesData!=null&&!sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID).equalsIgnoreCase("")){
                            if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                                Log.e("vallue", "55 "+response.body().getValue());
                                String email=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.EMAIL);
                                String password=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.PASSWORD);
                                String latitude=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.USER_LAT);
                                String longitude=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.USER_LANG);
                                String device_id = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.DEVICE_ID);

                                RetrofitHelper.getInstance().getLogin(loginCallback, email, password,latitude+"",longitude+"", device_id);

                            }
                        }


                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (sharedPreferencesData!=null&&!sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID).equalsIgnoreCase("")){
                                    Intent mainActivity=new Intent(getApplicationContext(),MainActivity.class);
                                    startActivity(mainActivity);
                                    finish();
                                }else{
                                    Intent mainActivity = new Intent(getApplicationContext(), TermAndCondition.class);
                                    startActivity(mainActivity);
                                    finish();
                                }
                            }
                        }, SPLASH_DISPLAY_LENGTH);
                    }
                else {

                        RetrofitHelper.getInstance().getSession(sessionCallback, sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DEVICE_ID));
                }

            }catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        public void onFailure(Call<SessionResponse> call, Throwable t) {

        }
    };

    Callback<LoginMain> loginCallback=new Callback<LoginMain>() {
        @Override
        public void onResponse(Call<LoginMain> call, Response<LoginMain> response) {
            //progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                if (response.body().getSuccess()==1){
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID,response.body().getUserDetails().getId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.SOCIAL_MEDIA_ID,response.body().getUserDetails().getSocialMediaId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.USER_NAME,response.body().getUserDetails().getUsername());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.FIRST_NAME,response.body().getUserDetails().getFirstName());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.LAST_NAME,response.body().getUserDetails().getLastName());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.EMAIL,response.body().getUserDetails().getEmail());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.PROFILE_IMAGE,response.body().getUserDetails().getProfileImage());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.PASSWORD,response.body().getUserDetails().getPassword());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.COUNTRY_ID,response.body().getUserDetails().getCountryId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.LANGUAGE,response.body().getUserDetails().getLanguage());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.PHONE_NUMBER,response.body().getUserDetails().getPhoneNumber());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.DOB,response.body().getUserDetails().getDob());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.GENDER,response.body().getUserDetails().getGender());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.STATUS,response.body().getUserDetails().getStatus());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.DEVICE_ID,response.body().getUserDetails().getDeviceId());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.DEVICE_TYPE,response.body().getUserDetails().getDeviceType());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ABOUT_US,response.body().getUserDetails().getAboutUs());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.CATEGORY_NAME,response.body().getUserDetails().getCategory());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.COUNTRY_NAME,response.body().getUserDetails().getCountry());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.CITY_NAME,response.body().getUserDetails().getCity());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.HAIR_COLOR,response.body().getUserDetails().getHairColor());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.WEIGHT,response.body().getUserDetails().getWeight());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.HEIGHT,response.body().getUserDetails().getHeight());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.AVAILABLE,response.body().getUserDetails().getAvailableFrom());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.BREAST_SIZE,response.body().getUserDetails().getBrestsize());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.EYE_COLOR,response.body().getUserDetails().getEyeColor());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.HOBBY,response.body().getUserDetails().getHobby());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ETHNICITY,response.body().getUserDetails().getEthnicity());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ORIGIN,response.body().getUserDetails().getOrigin());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.COUNTRY_NAME,response.body().getUserDetails().getCountry());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.CITY_NAME,response.body().getUserDetails().getCity());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.LANGUAGE,response.body().getUserDetails().getLanguage());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.INCALL,response.body().getUserDetails().getServiceType());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.WEBSITE,response.body().getUserDetails().getWebsite());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.USER_LAT,response.body().getUserDetails().getUserLat());
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.USER_LANG,response.body().getUserDetails().getUserLang());
                    Log.e("userId",response.body().getUserDetails().getId());

                }else{
                    Snackbar.make(tv_touch,response.body().getMessage(),Snackbar.LENGTH_LONG).show();
                }




            }
        }

        @Override
        public void onFailure(Call<LoginMain> call, Throwable t) {
            //progressBar.setVisibility(View.GONE);
        }
    };

    class SimpleAnimationListener implements Animation.AnimationListener {

        private Context context;

        public SimpleAnimationListener(Context context) {
            this.context = context;
        }


        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    }

}
