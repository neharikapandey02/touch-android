package com.example.touch.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.QuickContactBadge;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.customGallery.SubFragment;
import com.example.touch.models.PackageMainData;
import com.example.touch.models.PaymentDoneMainData;
import com.google.android.material.snackbar.Snackbar;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardMultilineWidget;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StripePaymentActivity extends AppCompatActivity {
    @BindView(R.id.imgView_back)
    ImageView imgView_back;

 @BindView(R.id.image)
    ImageView image;


    @BindView(R.id.et_firstName)
    EditText et_firstName;


    @BindView(R.id.et_phoneNumber)
    EditText et_phoneNumber;


    @BindView(R.id.et_layout_city)
    EditText et_layout_city;

    @BindView(R.id.card_widget)
    CardMultilineWidget card_widget;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.btn_pay)
    Button btn_pay;
    private String user_id;

    private SharedPreferencesData sharedPreferencesData;

    @BindView(R.id.radioGroup_Package)
    RadioGroup radioGroup_Package;

    private String amount = "5";
    private String booking_id;
    private String package_id = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_stripe_payment);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        user_id = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(StripePaymentActivity.this, TipsActivity.class));
            }
        });


        radioGroup_Package.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton_OneWeek = group.findViewById(checkedId);
                RadioButton radioButton_OneMonth = group.findViewById(checkedId);
                RadioButton radioButton_ThreeMonth = group.findViewById(checkedId);
                if (checkedId == R.id.radioButton_OneWeek) {
                    amount = "5";
                    package_id = "1";
                } else if (checkedId == R.id.radioButton_OneMonth) {
                    amount = "17";
                    package_id = "2";
                } else if (checkedId == R.id.radioButton_ThreeMonth) {
                    amount = "45";
                    package_id = "3";
                }

                Log.e("amount=", amount);

            }
        });
    }

    @OnClick(R.id.imgView_back)
    public void backClick(View view) {
        onBackPressed();
    }

    private void onCardSaved() {

        final Card cardToSave = card_widget.getCard();
        if (cardToSave != null) {
            //String cardNumber=cardToSave.validateCardNumber();
            Log.e("CardNumber=", card_widget.validateAllFields() + "");
            // tokenize card (see below)
            //tokenizeCard(cardToSave);
        }
    }

    public void createCardTokenForPayment() {

        Stripe stripe = new Stripe(StripePaymentActivity.this, getString(R.string.stripe_key));
        final Card cardToSave = card_widget.getCard();
        if (cardToSave != null) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.wait), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.VISIBLE);
            stripe.createToken(cardToSave, new TokenCallback() {
                @Override
                public void onError(Exception error) {
                    Toast.makeText(StripePaymentActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onSuccess(Token token) {
                    if (NetworkUtil.checkNetworkStatus(StripePaymentActivity.this)) {

                            String description = "Payment success";
                            Log.e("getTokenId=", token.getId());
                            //Toast.makeText(StripePaymentActivity.this, "Payment Success", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.VISIBLE);
                            RetrofitHelper.getInstance().doStripePayment(stripePaymentCallback, user_id, token.getId(), amount, "1", package_id, "THis is for test");


                    } else {

                    }
                }
            });
        }
    }

    private boolean validation() {
        if (et_firstName.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_pay,getResources().getString(R.string.please_enter_name),Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_phoneNumber.getText().toString().equalsIgnoreCase("")) {
            Snackbar.make(btn_pay,getResources().getString(R.string.please_enter_mobile_number),Snackbar.LENGTH_LONG).show();
            return false;
        } else if (et_phoneNumber.getText().toString().length() < 10) {
            Snackbar.make(btn_pay,getResources().getString(R.string.please_enter_valid_mobile),Snackbar.LENGTH_LONG).show();
            return false;

        }else if(et_layout_city.getText().toString().equalsIgnoreCase("")){
            Snackbar.make(btn_pay,getResources().getString(R.string.please_enter_city),Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


    @OnClick(R.id.btn_pay)
    public void payClick(View view) {
        // onCardSaved();
        if (validation()) {
            createCardTokenForPayment();
        }

    }

    RelativeLayout areaContainer;
    SubFragment subFragment;
    View view;

    Callback<PaymentDoneMainData> stripePaymentCallback = new Callback<PaymentDoneMainData>() {

        @Override
        public void onResponse(Call<PaymentDoneMainData> call, Response<PaymentDoneMainData> response) {
            progressBar.setVisibility(View.GONE);
            if (response.body() != null) {
                if (response.body().getSuccess() == true) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                    onBackPressed();
                    //areaContainer = findViewById(R.id.area_container1);
                    //subFragment = new SubFragment();
                    //getSupportFragmentManager().beginTransaction().replace(R.id.area_container1, subFragment).commit();

                } else {
                    Snackbar.make(btn_pay, response.body().getMessage(), Snackbar.LENGTH_LONG).show();
                }
            }

        }

        @Override
        public void onFailure(Call<PaymentDoneMainData> call, Throwable t) {
            //progressBar.setVisibility(View.GONE);
        }
    };
}
