package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.touch.R;
import com.example.touch.constants.Constants;
import com.example.touch.constants.SharedPreferencesData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TermAndCondition extends AppCompatActivity {
    @BindView(R.id.tv_accept)
    TextView tv_accept;
    private SharedPreferencesData sharedPreferencesData;
    private String loginCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_term_and_condition);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        sharedPreferencesData=new SharedPreferencesData(getApplicationContext());
        loginCheck=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);

    }

    @OnClick(R.id.tv_accept)
    public void onAcceptTermsAndCondition(View view){
        Intent mainActivity=new Intent(getApplicationContext(),MainActivity.class);
        startActivity(mainActivity);
        finish();
    }
}
