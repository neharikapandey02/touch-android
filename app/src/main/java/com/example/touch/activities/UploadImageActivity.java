package com.example.touch.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.HastagAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.HastagMain;
import com.example.touch.models.HastagMainData;
import com.example.touch.models.MultipleImageMain;
import com.example.touch.models.PostUploadCountResponse;
import com.example.touch.models.SearchUserListData;
import com.example.touch.utility.SpaceTokenizer;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadImageActivity extends AppCompatActivity {
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1001;
    @BindView(R.id.imageView_selected)
    ImageView imageView_selected;
    @BindView(R.id.imageView_close)
    ImageView imageView_close;
    @BindView(R.id.circularImageView_UserImage)
    ImageView circularImageView_UserImage;
    @BindView(R.id.add_location)
    RelativeLayout add_location;
    @BindView(R.id.tv_addLocation)
    TextView tv_addLocation;
    String tv_addLocationString = "";
    @BindView(R.id.tv_share)
    TextView tv_share;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.layout_close)
    LinearLayout layout_close;
    @BindView(R.id.recylerview_hastag)
    RecyclerView recylerview_hastag;
    @BindView(R.id.et_getCaption)
    MultiAutoCompleteTextView et_getCaption;
    @BindView(R.id.hastagEt1)
    AutoCompleteTextView hastagEt1;
    @BindView(R.id.hastagEt2)
    AutoCompleteTextView hastagEt2;
    @BindView(R.id.hastagEt3)
    AutoCompleteTextView hastagEt3;
    @BindView(R.id.hastagEt4)
    AutoCompleteTextView hastagEt4;
    @BindView(R.id.hastagEt5)
    AutoCompleteTextView hastagEt5;
    @BindView(R.id.hastag1CloseImg)
    ImageView hastag1CloseImg;
    @BindView(R.id.hastag2CloseImg)
    ImageView hastag2CloseImg;
    @BindView(R.id.hastag3CloseImg)
    ImageView hastag3CloseImg;
    @BindView(R.id.hastag4CloseImg)
    ImageView hastag4CloseImg;
    @BindView(R.id.hastag5CloseImg)
    ImageView hastag5CloseImg;
    @BindView(R.id.scrollview_smiley)
    ScrollView scrollview_smiley;
    ArrayList<String> path;
    String trimData;
    String searchData;
    private List<HastagMainData> hastagMainData;
    private String userId;
    private SharedPreferencesData sharedPreferencesData;
    private String latitude = "";
    private String longitude = "";
    private String state = "", country = "", place_id = "", country_code = "";
    private StringBuffer captionSB;
    ArrayList<String> captionList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_upload_image);
        ButterKnife.bind(this);
        viewFinds();
    }

    private void viewFinds() {
        captionSB = new StringBuffer();
        et_getCaption.setThreshold(2);
        et_getCaption.setTokenizer(new SpaceTokenizer());

        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        recylerview_hastag.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        String cropCheck = "";
        try {
            cropCheck = getIntent().getStringExtra("crop");
            if (cropCheck.equalsIgnoreCase("1")) {

                Log.e("Urrri1", getIntent().getStringExtra(Constants.IMAGEPATH));
                Glide.with(UploadImageActivity.this).load(getIntent().getStringExtra(Constants.IMAGEPATH)).into(imageView_selected);
                path = new ArrayList<>();
                Uri uri = Uri.parse(getIntent().getStringExtra(Constants.IMAGEPATH));
                File file = new File(uri.getPath());
                path.add(file.toString());
                Log.e("Urrri1", file.toString());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        if (cropCheck == null || cropCheck.equalsIgnoreCase("")) {
            path = getIntent().getStringArrayListExtra(Constants.IMAGEUPLOAD);
            if (path != null && path.size() > 0) {
                if (path.size() > 1) {
                    Bitmap bmImg = BitmapFactory.decodeFile(path.get(path.size() - 1));
                    String path1 = path.get(path.size() - 1).substring(path.get(path.size() - 1).lastIndexOf("."));
                    Log.e("ExtensionFind=", path1);

                    if (path1.equalsIgnoreCase(".jpg") || path1.equalsIgnoreCase(".png") || path1.equalsIgnoreCase(".jpeg") || path1.equalsIgnoreCase(".gif")) {
                        imageView_selected.setImageBitmap(bmImg);
                    } else {
                        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(path.get(path.size() - 1),
                                MediaStore.Images.Thumbnails.MINI_KIND);
                        imageView_selected.setImageBitmap(thumb);
                    }

                } else {
                    Bitmap bmImg = BitmapFactory.decodeFile(path.get(0));
                    String path1 = path.get(0).substring(path.get(0).lastIndexOf("."));
                    Log.e("ExtensionFind=", path1 + "   " + path.get(0));
                    if (path1.equalsIgnoreCase(".jpg") || path1.equalsIgnoreCase(".png") || path1.equalsIgnoreCase(".jpeg") || path1.equalsIgnoreCase(".gif")) {
                        imageView_selected.setImageBitmap(bmImg);
                    } else {
                        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(path.get(0),
                                MediaStore.Images.Thumbnails.MINI_KIND);
                        imageView_selected.setImageBitmap(thumb);
                    }
                }
            }
        }
        hastag1CloseImg.setOnClickListener(view -> hastagEt1.setText(""));
        hastag2CloseImg.setOnClickListener(view -> hastagEt2.setText(""));
        hastag3CloseImg.setOnClickListener(view -> hastagEt3.setText(""));
        hastag4CloseImg.setOnClickListener(view -> hastagEt4.setText(""));
        hastag5CloseImg.setOnClickListener(view -> hastagEt5.setText(""));
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            captionList = new ArrayList<>();
            RetrofitHelper.getInstance().getSearchByHastag(getSearchDataCallback1, "");
            RetrofitHelper.getInstance().getSearchByUsername(getSearchUserDataCallback1, "");
        }

        hastagEt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")) {
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")) {
                                    hastagEt1.setText("");
                                } else {
                                    hastagEt1.setText(editable.toString().trim());
                                }
                                hastagEt2.requestFocus();
                                return;
                            }
                        }
                    } else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt1.setText("");
                    }
                }
            }
        });

        hastagEt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")) {
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")) {
                                    hastagEt2.setText("");
                                } else {
                                    hastagEt2.setText(editable.toString().trim());
                                }
                                hastagEt3.requestFocus();
                            }
                        }
                    } else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt2.setText("");
                    }
                }
            }
        });

        hastagEt3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")){
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")){
                                    hastagEt3.setText("");
                                }else {
                                    hastagEt3.setText(editable.toString().trim());
                                }
                                hastagEt4.requestFocus();
                            }
                        }
                    }else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt3.setText("");
                    }
                }
            }
        });

        hastagEt4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")){
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")){
                                    hastagEt4.setText("");
                                }else {
                                    hastagEt4.setText(editable.toString().trim());
                                }
                                hastagEt5.requestFocus();
                                return;
                            }
                        }
                    }else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt4.setText("");
                    }
                }
            }
        });

        hastagEt5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")){
                    if (editable.toString().startsWith("#")) {
                        if (editable.length() > 0) {
                            if (editable.toString().contains(" ")) {
                                if (editable.toString().trim().equalsIgnoreCase("#")){
                                    hastagEt5.setText("");
                                }else {
                                    hastagEt5.setText(editable.toString().trim());
                                }
                            }
                        }
                    }else {
                        Snackbar.make(tv_share, getResources().getString(R.string.please_type_hash_first), Snackbar.LENGTH_LONG).show();
                        hastagEt5.setText("");
                    }
                }
            }
        });

        et_getCaption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().equalsIgnoreCase("")){
                    if (!captionSB.toString().equalsIgnoreCase("")){
                        editable.toString().replace(captionSB.toString(), "");

                    }
                    if (null != et_getCaption.getLayout() && et_getCaption.getLayout().getLineCount() > 5) {
                        et_getCaption.getText().delete(et_getCaption.getText().length() - 1, et_getCaption.getText().length());
                    }

                }
            }
        });

//        et_getCaption.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//                Log.e("Inside=",editable.toString());
//                if (editable.toString().equalsIgnoreCase("")) {
//
//                    Log.e("Inside","InsideTextChangerEMPTY");
//                    RetrofitHelper.getInstance().getSearchByHastag(getSearchDataCallback,"a");
//
//                } else {
//                    //layoutLinear_search.setVisibility(View.VISIBLE);
//                    if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
//                        Log.e("Inside","value"+" "+editable.toString());
//                        String[] split=editable.toString().split("#");
//                        if (editable.toString().startsWith("#")&&split.length==1){
//                            Log.e("Inside","StartWithHastag");
//                            RetrofitHelper.getInstance().getSearchByHastag(getSearchDataCallback,editable.toString());
//                            trimData=editable.toString();
//                        }else{
//
//                            Log.e("Inside","Size="+split.length);
//                            if (split.length>1){
//                                searchData=split[split.length-1];
//
//                                RetrofitHelper.getInstance().getSearchByHastag(getSearchDataCallback,searchData.trim());
//
//
//                                Log.e("Inside","secondHastage"+" "+searchData);
//                                trimData="#"+searchData;
//                            }
//
//                        }
//
//
//                    }else{
//                        Snackbar.make(tv_share,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
//                    }
//                }
//            }
//        });

        add_location.setOnClickListener(view -> startAutocompleteActivity());
    }



    private void setPic() {
        // Get the dimensions of the View
        int targetW = imageView_selected.getWidth();
        int targetH = imageView_selected.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
//        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
        //Log.e("SizeOf",scaleFactor+"");

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = 4;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path.get(path.size()-1), bmOptions);


        //userImage=path;
        imageView_selected.setImageBitmap(bitmap);

    }

    Callback<PostUploadCountResponse> postUploadCountCallback = new Callback<PostUploadCountResponse>() {
        @Override
        public void onResponse(Call<PostUploadCountResponse> call, Response<PostUploadCountResponse> response) {
            try {
                if (response.isSuccessful()){
                    if (response.body().getSuccess() == 1){
                        AlertDialog.Builder alert = new AlertDialog.Builder(UploadImageActivity.this);
                        alert.setTitle("Today post count " + response.body().getToday_count());
                        alert.setMessage(response.body().getMessage());
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                                    progressBar.setVisibility(View.VISIBLE);
//            Log.e("path=",path.get(0).toString());
                                    String location = tv_addLocation.getText().toString();
           /* try{
                File file=new File(path.get(0));

                String pathToReEncodedFile =
                        new VideoResolutionChanger().changeResolution(file);
                String s="test";
                Log.e("pathofvideo=",pathToReEncodedFile+s);
            }catch(Throwable t){*//* smth wrong :( *//*}*/

                                    if (location.equalsIgnoreCase("Add Location")){
                                        locationSend="";
                                    }else{
                                        locationSend = tv_addLocation.getText().toString();
                                    }
                                    if (state==null){
                                        state="";
                                    }
                                    String hastagStr1 = "", hastagStr2 = "", hastagStr3 = "", hastagStr4 = "", hastagStr5 = "";
                                    if (!hastagEt1.getText().toString().trim().equalsIgnoreCase("")){
                                        hastagStr1 = hastagEt1.getText().toString().trim();
//                hastagStr1 = hastagStr1.replace("#", "");
                                        hastagStr1 = hastagStr1.replaceAll("[-+.^:,'#]","");
                                    }
                                    if (!hastagEt2.getText().toString().trim().equalsIgnoreCase("")){
                                        hastagStr2 = hastagEt2.getText().toString().trim();
                                        hastagStr2 = hastagStr2.replaceAll("[-+.^:,'#]", "");
                                    }
                                    if (!hastagEt3.getText().toString().trim().equalsIgnoreCase("")){
                                        hastagStr3 = hastagEt3.getText().toString().trim();
                                        hastagStr3 = hastagStr3.replaceAll("[-+.^:,'#]", "");
                                    }
                                    if (!hastagEt4.getText().toString().trim().equalsIgnoreCase("")){
                                        hastagStr4 = hastagEt4.getText().toString().trim();
                                        hastagStr4 = hastagStr4.replaceAll("[-+.^:,'#]", "");
                                    }
                                    if (!hastagEt5.getText().toString().trim().equalsIgnoreCase("")){
                                        hastagStr5 = hastagEt5.getText().toString().trim();
                                        hastagStr5 = hastagStr5.replaceAll("[-+.^:,'#]", "");
                                    }
                                    RetrofitHelper.getInstance().setMultipleImages(multipleImageMainCallback, path, userId,et_getCaption.getText().toString(),tv_addLocationString,latitude,longitude,country, country_code,state, place_id,
                                            hastagStr1, hastagStr2, hastagStr3, hastagStr4, hastagStr5);
                                }else{
                                    Snackbar.make(tv_share, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                                }
                            }
                        });
                        alert.show();
                    }else if (response.body().getSuccess() == 0){
                        AlertDialog.Builder alert = new AlertDialog.Builder(UploadImageActivity.this);
                        alert.setTitle("Today post count " + response.body().getToday_count());
                        alert.setMessage(response.body().getMessage());
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onBackPressed();
                            }
                        });
                        alert.show();
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<PostUploadCountResponse> call, Throwable t) {

        }
    };

    String locationSend = "";

    @OnClick(R.id.shareImg)
    public void shareClick(View view){
        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
            RetrofitHelper.getInstance().getPostUploadCount(postUploadCountCallback, userId);
        }
    }

    Callback<MultipleImageMain> multipleImageMainCallback = new Callback<MultipleImageMain>() {
        @Override
        public void onResponse(Call<MultipleImageMain> call, Response<MultipleImageMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                onBackPressed();
            }
        }

        @Override
        public void onFailure(Call<MultipleImageMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    public void addhastag(String tag){

        String privous = et_getCaption.getText().toString();
        String newData = privous.replace(trimData, tag) + " ";


        if (newData!=null&&newData.length()>0){

            et_getCaption.setText(newData);
            hastagMainData.clear();
            searchAdapter = new HastagAdapter(getApplicationContext(), hastagMainData, UploadImageActivity.this);
            recylerview_hastag.setAdapter(searchAdapter);
            et_getCaption.setSelection(et_getCaption.getText().length());
        }else{
           // et_getCaption.setText(tag);
            recylerview_hastag.setVisibility(View.GONE);
        }


    }


    HastagAdapter searchAdapter;
    Callback<HastagMain> getSearchDataCallback = new Callback<HastagMain>() {
        @Override
        public void onResponse(Call<HastagMain> call, Response<HastagMain> response) {
           // progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                hastagMainData= new ArrayList<>();
                if (response.body().getResult() != null&&response.body().getResult().size()>0) {
//                    recylerview_hastag.setVisibility(View.VISIBLE);
                    //tv_message.setVisibility(View.GONE);
                    hastagMainData.clear();
                    hastagMainData.addAll(response.body().getResult());
                    ArrayList<String> arrayList = new ArrayList<>();
                    for (int i=0;i<response.body().getResult().size();i++){
                        arrayList.add(response.body().getResult().get(i).getTag());
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter(UploadImageActivity.this, android.R.layout.select_dialog_singlechoice, arrayList);
//                    et_getCaption.setAdapter(arrayAdapter);
                    //tv_message.setVisibility(View.VISIBLE);
//                    searchAdapter = new HastagAdapter(getApplicationContext(), hastagMainData,UploadImageActivity.this);
//                    recylerview_hastag.setAdapter(searchAdapter);
                }else{
                    Log.e("Inside=","InsideEmptyAdapter");
                    hastagMainData.clear();
//                    searchAdapter = new HastagAdapter(getApplicationContext(), hastagMainData,UploadImageActivity.this);
//                    recylerview_hastag.setAdapter(searchAdapter);
                }
            }
        }

        @Override
        public void onFailure(Call<HastagMain> call, Throwable t) {
            //progressBar.setVisibility(View.GONE);
        }
    };



    Callback<HastagMain> getSearchDataCallback1 = new Callback<HastagMain>() {
        @Override
        public void onResponse(Call<HastagMain> call, Response<HastagMain> response) {
            if (response.isSuccessful()) {
                if (response.body().getResult() != null&&response.body().getResult().size()>0) {
                    ArrayList<String> arrayList = new ArrayList<>();
                    for (int i=0;i<response.body().getResult().size();i++){
                        arrayList.add(response.body().getResult().get(i).getTag());
                        captionList.add(response.body().getResult().get(i).getTag());
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UploadImageActivity.this, android.R.layout.select_dialog_singlechoice, arrayList);
                    hastagEt1.setAdapter(arrayAdapter);
                    hastagEt2.setAdapter(arrayAdapter);
                    hastagEt3.setAdapter(arrayAdapter);
                    hastagEt4.setAdapter(arrayAdapter);
                    hastagEt5.setAdapter(arrayAdapter);
                }
            }
        }
        @Override
        public void onFailure(Call<HastagMain> call, Throwable t) {
        }
    };


    Callback<SearchUserListData> getSearchUserDataCallback1 = new Callback<SearchUserListData>() {
        @Override
        public void onResponse(Call<SearchUserListData> call, Response<SearchUserListData> response) {
            if (response.isSuccessful()) {
                if (response.body().getArrayList() != null&&response.body().getArrayList().size()>0) {
//                    ArrayList<String> arrayList = new ArrayList<>();
                    for (int i=0;i<response.body().getArrayList().size();i++){
//                        arrayList.add("@"+response.body().getArrayList().get(i).getFirstName());
                        captionList.add("@"+response.body().getArrayList().get(i).getFirstName());
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(UploadImageActivity.this, android.R.layout.select_dialog_singlechoice, captionList);
                    et_getCaption.setAdapter(arrayAdapter);
                }
            }
        }
        @Override
        public void onFailure(Call<SearchUserListData> call, Throwable t) {
        }
    };




    @OnClick(R.id.layout_close)
    public void closeClick(View view){
        tv_addLocation.setText("Add Location");
    }


    @OnClick(R.id.imgView_commentBack)
    public void onBackClick(View view){
        onBackPressed();
    }

    private void startAutocompleteActivity() {
        Places.initialize(getApplicationContext(), "AIzaSyCE_5xAkP50cQKk2suoeL0NByrstAR03gU");
        List<com.google.android.libraries.places.api.model.Place.Field> placeFields = new ArrayList<>(Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.values()));

        List<TypeFilter> typeFilters = new ArrayList<>(Arrays.asList(TypeFilter.REGIONS));


// Create a RectangularBounds object.
        RectangularBounds bounds = RectangularBounds.newInstance(
                new LatLng(-33.880490, 151.184363),
                new LatLng(-33.858754, 151.229596));
        Intent autocompleteIntent =
                new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, placeFields)
                        .setTypeFilter(typeFilters.get(0))

                        //.setInitialQuery("Search my data")
                        //.setTypeFilter(TypeFilter.ADDRESS)
                        .build(this);

        startActivityForResult(autocompleteIntent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    Log.e("TEST", "Place: " + place.getName()+"   "+place.getLatLng()+"  "+place.getAddress() +"  "+place.getId());
                    //tv_addLocation.setText(place.getAddress());
                    tv_addLocationString = place.getName();
                    tv_addLocation.setText(place.getAddress());
                    latitude=place.getLatLng().latitude+"";
                    longitude=place.getLatLng().longitude+"";
                    place_id = place.getId();
                    getFullAddressFromLatAndLong(place.getLatLng().latitude,place.getLatLng().longitude);
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.

                if (data != null) {
                    Status status = Autocomplete.getStatusFromIntent(data);
                    Log.i("TEST", status.getStatusMessage());
                }

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    public void getFullAddressFromLatAndLong(double latitude,double longitude){
        Locale aLocale = new Locale.Builder().setLanguage("en").setScript("Latn").setRegion("RS").build();
        Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(latitude, longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            //System.out.println(addresses.get(0).getLocality());
           // addresses.get(0).getCountryName();
            country = addresses.get(0).getCountryName();
            state=addresses.get(0).getAdminArea();
            country_code = addresses.get(0).getCountryCode();
//            Log.e("hiiii", ""+addresses.get(0).getCountryCode());

//            Log.e("AllAddress=",addresses.get(0).getCountryName()+"  "+addresses.get(0).getAdminArea());


        }
//        else {
//            // do your stuff
//        }
    }

    public void checkImageRotatingType(String pathOfImageFile){
        try {
            ExifInterface exif = new ExifInterface(pathOfImageFile);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            int rotate = 0;
            switch(orientation) {
                case  ExifInterface.ORIENTATION_ROTATE_270:
                    rotate-=90;break;
                case  ExifInterface.ORIENTATION_ROTATE_180:
                    rotate-=90;break;
                case  ExifInterface.ORIENTATION_ROTATE_90:
                    rotate-=90;break;
            }
            Log.e("Fragment", "EXIF info for file " + pathOfImageFile + ": " + rotate);
        } catch (IOException e) {
            Log.e("Fragment", "Could not get EXIF info for file " + pathOfImageFile + ": " + e);
        }
    }

}
