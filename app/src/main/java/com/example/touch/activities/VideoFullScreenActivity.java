package com.example.touch.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.widget.MediaController;
import android.widget.VideoView;

import com.example.touch.R;

import java.util.Objects;

public class VideoFullScreenActivity extends AppCompatActivity {
    VideoView videoView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_video_full_screen);

        videoView = findViewById(R.id.videoView);

        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setVideoPath(getIntent().getStringExtra("video"));
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) videoView.getLayoutParams();
        params.width = metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;
        videoView.setLayoutParams(params);
        videoView.start();
    }

    @Override
    public void onPause(){
        videoView.pause();
        super.onPause();
    }

    @Override
    public void onStop(){
        videoView.stopPlayback();
        super.onStop();
    }

    @Override
    public void onBackPressed(){
        videoView.stopPlayback();
        finish();
        super.onBackPressed();
    }
}