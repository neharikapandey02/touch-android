package com.example.touch.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.AddAnotherImageListAdapter1;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.AnotherUserMain;
import com.example.touch.models.BlockUserResponse;
import com.example.touch.models.GetAnotherUserProfileData;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitorProfileActivity extends AppCompatActivity {

    @BindView(R.id.imgv_top)
    ImageView imgv_top;

    @BindView(R.id.settingImg)
    ImageView settingImg;


    @BindView(R.id.circularImageView_UserImage)
   CircularImageView circularImageView_UserImage;

   @BindView(R.id.tv_firstName)
    TextView tv_firstName;
   @BindView(R.id.blockTv)
    TextView blockTv;
    private String userId = "";
    private SharedPreferencesData sharedPreferencesData;
    private String userIdAnotherUser = "";
    private String firstName = "";
    private String profileImage = "", blockStatus = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        Objects.requireNonNull(getSupportActionBar()).hide();
        setContentView(R.layout.activity_visitor_profile);
        ButterKnife.bind(this);
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        userIdAnotherUser = getIntent().getStringExtra(Constants.ANOTHER_USER_ID);

        if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {

            RetrofitHelper.getInstance().getAnotherUserProfile(callback, userId, "1", "5", getIntent().getStringExtra(Constants.ANOTHER_USER_ID));

        } else {
            Snackbar.make(tv_firstName, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }

        settingImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(VisitorProfileActivity.this)
                        .setTitle("Block user")
                        .setMessage("Do you want to block this user?")
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (NetworkUtil.checkNetworkStatus(getApplicationContext())) {
                                    tv_firstName.setVisibility(View.VISIBLE);
                                    RetrofitHelper.getInstance().doBlockUser(blockUserCallback, userId, userIdAnotherUser);

                                } else {
                                    Snackbar.make(tv_firstName, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .show();
            }
        });

    }


    Callback<BlockUserResponse> blockUserCallback = new Callback<BlockUserResponse>() {
        @Override
        public void onResponse(Call<BlockUserResponse> call, Response<BlockUserResponse> response) {

            if (response.isSuccessful()){
                if (response.body().getSuccess() == 1){
                    Toast.makeText(VisitorProfileActivity.this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }

        @Override
        public void onFailure(Call<BlockUserResponse> call, Throwable t) {

        }
    };



    @OnClick(R.id.imgView_back)
    public void backClick(View view) {
        onBackPressed();
    }

    @OnClick(R.id.chatBtn)
    public void priceButtonClick(View view) {

        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {

            if (!sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase(userIdAnotherUser)) {
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra(Constants.FIRST_NAME, firstName);
                intent.putExtra(Constants.RECEIVER_ID, userIdAnotherUser);
                intent.putExtra(Constants.PROFILE_IMAGE, profileImage);
                intent.putExtra("block_status", blockStatus);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.same_user), Toast.LENGTH_LONG).show();
            }

        } else {
            Snackbar.make(tv_firstName, getResources().getString(R.string.please_login_to_chat), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<GetAnotherUserProfileData> callback = new Callback<GetAnotherUserProfileData>() {
        @Override
        public void onResponse(@NotNull Call<GetAnotherUserProfileData> call, Response<GetAnotherUserProfileData> response) {

            if (response.isSuccessful()) {
                assert response.body() != null;
                blockStatus = response.body().getBlock_status().toString();
                if (response.body().getUserDetail() != null && response.body().getUserDetail().getProfileImage() != null) {
                    Picasso.with(getApplicationContext())
                            .load(response.body().getUserDetail().getProfileImage())
                            .placeholder(R.drawable.profile_back)
                            .error(R.drawable.profile_back)
                            .into(imgv_top);
                    Picasso.with(getApplicationContext())
                            .load(response.body().getUserDetail().getProfileImage())
                            .placeholder(R.drawable.profile_placeholder)
                            .error(R.drawable.profile_placeholder)
                            .into(circularImageView_UserImage);
                }

                if (response.body().getUserDetail() != null) {
                    firstName = response.body().getUserDetail().getFirstName();
                    profileImage = response.body().getUserDetail().getProfileImage();

                    tv_firstName.setText(response.body().getUserDetail().getFirstName());

                }
            }

        }

        @Override
        public void onFailure(@NotNull Call<GetAnotherUserProfileData> call, @NotNull Throwable t) {
        }
    };



}