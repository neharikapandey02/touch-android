package com.example.touch.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.touch.R;
import com.example.touch.activities.MyPostLists;
import com.example.touch.constants.Constants;
import com.example.touch.models.MultipleImageGetMainData;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AddAnotherImageListAdapter extends RecyclerView.Adapter<AddAnotherImageListAdapter.addImageList> {
    Context context;
    List<MultipleImageGetMainData> multipleImageGetMainData;
    private Dialog dialog;
    MediaController mc;
    public AddAnotherImageListAdapter(Context context, List<MultipleImageGetMainData> multipleImageGetMainData, MediaController mc){
        this.context=context;
        this.multipleImageGetMainData=multipleImageGetMainData;
        this.mc=mc;

    }

    @NonNull
    @Override
    public AddAnotherImageListAdapter.addImageList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_multiple,parent,false);
        AddAnotherImageListAdapter.addImageList addImageList=new AddAnotherImageListAdapter.addImageList(view);

        return addImageList;
    }

    @Override
    public void onBindViewHolder(@NonNull AddAnotherImageListAdapter.addImageList holder, int position) {
        Picasso.with(context)
                .load(multipleImageGetMainData.get(position).getImage())
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .centerCrop()
                .error(R.drawable.loading_placeholder)
                .into(holder.imageView_image);

        holder.imageView_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context, MyPostLists.class);
                intent.putExtra(Constants.TO_POSITION,multipleImageGetMainData.get(position).getId());
                intent.putExtra(Constants.ANOTHER_USER_ID,multipleImageGetMainData.get(position).getUserId());
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
               /* dialog =new  Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialogue_image_show_profile);
                dialog.setCancelable(true);
                ImageView imgView_Expand=dialog.findViewById(R.id.imgView_Expand);
                TextView tv_UploadedDate=dialog.findViewById(R.id.tv_UploadedDate);
                tv_UploadedDate.setText(parseDate(multipleImageGetMainData.get(position).getAddedAt()));
                Picasso.with(context)
                        .load(multipleImageGetMainData.get(position).getBigImage())
                        .placeholder(R.drawable.profile_back)
                        .error(R.drawable.profile_back)
                        .into(imgView_Expand);
                dialog.show();*/
            }
        });
        holder.imageView_thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context, MyPostLists.class);
                intent.putExtra(Constants.TO_POSITION,multipleImageGetMainData.get(position).getId());
                intent.putExtra(Constants.ANOTHER_USER_ID,multipleImageGetMainData.get(position).getUserId());
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
               /* dialog =new  Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialogue_image_show_profile);
                dialog.setCancelable(true);
                ImageView imgView_Expand=dialog.findViewById(R.id.imgView_Expand);
                TextView tv_UploadedDate=dialog.findViewById(R.id.tv_UploadedDate);
                tv_UploadedDate.setText(parseDate(multipleImageGetMainData.get(position).getAddedAt()));
                Picasso.with(context)
                        .load(multipleImageGetMainData.get(position).getBigImage())
                        .placeholder(R.drawable.profile_back)
                        .error(R.drawable.profile_back)
                        .into(imgView_Expand);
                dialog.show();*/
            }
        });


        //for video play in this adapter
        if (multipleImageGetMainData.get(position).getType().equalsIgnoreCase("image")){
            holder.imageView_image.setVisibility(View.VISIBLE);
            holder.videoView.setVisibility(View.GONE);
            holder.imageView_thumbnail.setVisibility(View.GONE);
            holder.imgview_Video.setVisibility(View.GONE);
        }else{
           /* Log.e("VideoInside=",multipleImageGetMainData.get(position).getImage());
            holder.imageView_image.setVisibility(View.GONE);
            holder.videoView.setVisibility(View.VISIBLE);
            //holder.videoView.setMediaController(mc);
            String str = multipleImageGetMainData.get(position).getImage();
            Uri uri = Uri.parse(str);
            // mc.setAnchorView(holder.videoView);
            holder.videoView.setVideoURI(uri);
            holder.videoView.requestFocus();
            holder.videoView.start();*/


            holder.imgview_Video.setVisibility(View.VISIBLE);
            holder.imageView_thumbnail.setVisibility(View.VISIBLE);
            holder.imageView_image.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.loading_placeholder);
            requestOptions.error(R.drawable.loading_placeholder);
            Glide.with(context)
                    .load(multipleImageGetMainData.get(position).getImage())
                    .fitCenter()
                    .centerCrop()
                    .apply(requestOptions)
                    .thumbnail(Glide.with(context).load(multipleImageGetMainData.get(position).getImage()))
                    .into(holder.imageView_thumbnail);
        }
        /*holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                holder.videoView.start();
                mp.setLooping(true);
                mp.setVolume(0f, 0f);
            }
        });*/

        holder.videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Intent intent=new Intent(context, MyPostLists.class);
                intent.putExtra(Constants.TO_POSITION,multipleImageGetMainData.get(position).getId());
                intent.putExtra(Constants.ANOTHER_USER_ID,multipleImageGetMainData.get(position).getUserId());
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return multipleImageGetMainData.size();
    }
    public class addImageList extends RecyclerView.ViewHolder{
        ImageView imageView_image;
        ImageView imageView_thumbnail;
        ImageView imgview_Video;
        ImageView linearLayout_postImage;
        VideoView videoView;
        public addImageList(@NonNull View itemView) {
            super(itemView);
            videoView=itemView.findViewById(R.id.videoView);
            imageView_image=itemView.findViewById(R.id.imageView_image_multiple);
            imgview_Video=itemView.findViewById(R.id.imgview_Video);
            imageView_thumbnail=itemView.findViewById(R.id.imageView_thumbnail);
            //linearLayout_postImage=itemView.findViewById(R.id.linearLayout_postImage);
        }
    }

    public static String parseDate(String inputDateString ) {
        Date date = null;
        String outputDateString = null;
        SimpleDateFormat inputDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat outputDateFormat=new SimpleDateFormat("EEE dd-MMM-yyyy");
        try {
            date = inputDateFormat.parse(inputDateString);
            outputDateString = outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDateString;
    }
}
