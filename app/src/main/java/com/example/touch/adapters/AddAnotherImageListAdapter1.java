package com.example.touch.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.touch.R;
import com.example.touch.activities.MyPostLists;
import com.example.touch.activities.SearchPostActivity;
import com.example.touch.constants.Constants;
import com.example.touch.models.AnotherUserProfileResult;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AddAnotherImageListAdapter1 extends RecyclerView.Adapter<AddAnotherImageListAdapter1.addImageList> {
    Context context;
    private List<AnotherUserProfileResult> multipleImageGetMainData;
    private Dialog dialog;
    private MediaController mc;

    public AddAnotherImageListAdapter1(Context context, List<AnotherUserProfileResult> multipleImageGetMainData, MediaController mc){
        this.context = context;
        this.multipleImageGetMainData = multipleImageGetMainData;
        this.mc = mc;
    }

    @NonNull
    @Override
    public AddAnotherImageListAdapter1.addImageList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_multiple, parent, false);
        return new addImageList(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddAnotherImageListAdapter1.addImageList holder, int position) {
        Picasso.with(context)
                .load(multipleImageGetMainData.get(position).getBigImage())
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .centerCrop()
                .error(R.drawable.loading_placeholder)
                .into(holder.imageView_image);

        holder.imageView_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(context, SearchPostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("arraylist", (Serializable) multipleImageGetMainData);
                intent.putExtra("position", position);
                intent.putExtra("page", "another");
                intent.putExtra("bundle", bundle);
                context.startActivity(intent);

            }
        });
        holder.imageView_thumbnail.setOnClickListener(view -> {

            Intent intent = new Intent(context, SearchPostActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("arraylist", (Serializable) multipleImageGetMainData);
            intent.putExtra("position", position);
            intent.putExtra("page", "another");
            intent.putExtra("bundle", bundle);
            context.startActivity(intent);


        });


        //for video play in this adapter
        if (multipleImageGetMainData.get(position).getType().equalsIgnoreCase("image")){
            holder.imageView_image.setVisibility(View.VISIBLE);
            holder.videoView.setVisibility(View.GONE);
            holder.imageView_thumbnail.setVisibility(View.GONE);
            holder.imgview_Video.setVisibility(View.GONE);
        }else{
           /* Log.e("VideoInside=",multipleImageGetMainData.get(position).getImage());
            holder.imageView_image.setVisibility(View.GONE);
            holder.videoView.setVisibility(View.VISIBLE);
            //holder.videoView.setMediaController(mc);
            String str = multipleImageGetMainData.get(position).getImage();
            Uri uri = Uri.parse(str);
            // mc.setAnchorView(holder.videoView);
            holder.videoView.setVideoURI(uri);
            holder.videoView.requestFocus();
            holder.videoView.start();*/


            holder.imgview_Video.setVisibility(View.VISIBLE);
            holder.imageView_thumbnail.setVisibility(View.VISIBLE);
            holder.imageView_image.setVisibility(View.VISIBLE);

//            Bitmap bitmap = null;
//            try {
//                bitmap = retriveVideoFrameFromVideo(multipleImageGetMainData.get(position).getBigImage());
//            } catch (Throwable throwable) {
//                throwable.printStackTrace();
//            }
//            if (bitmap != null) {
//                bitmap = Bitmap.createScaledBitmap(bitmap, 240, 240, false);
//                holder.imageView_thumbnail.setImageBitmap(bitmap);
//            }


            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.loading_placeholder);
            requestOptions.error(R.drawable.loading_placeholder);
            Glide.with(context)
                    .load(multipleImageGetMainData.get(position).getBigImage())
                    .fitCenter()
                    .centerCrop()
                    .apply(requestOptions)
                    .thumbnail(Glide.with(context).load(multipleImageGetMainData.get(position).getBigImage()))
                    .into(holder.imageView_thumbnail);
        }

        holder.videoView.setOnTouchListener((view, motionEvent) -> {
            Intent intent = new Intent(context, MyPostLists.class);
            intent.putExtra(Constants.TO_POSITION, multipleImageGetMainData.get(position).getId());
            intent.putExtra(Constants.ANOTHER_USER_ID, multipleImageGetMainData.get(position).getUserId());
            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return false;
        });
    }


    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);

            bitmap = mediaMetadataRetriever.getFrameAtTime(1, MediaMetadataRetriever.OPTION_CLOSEST);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable(
                    "Exception in retriveVideoFrameFromVideo(String videoPath)"
                            + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    @Override
    public int getItemCount() {
        return multipleImageGetMainData.size();
    }

    public class addImageList extends RecyclerView.ViewHolder {
        ImageView imageView_image;
        ImageView imageView_thumbnail;
        ImageView imgview_Video;
        ImageView linearLayout_postImage;
        VideoView videoView;

        public addImageList(@NonNull View itemView) {
            super(itemView);
            videoView = itemView.findViewById(R.id.videoView);
            imageView_image = itemView.findViewById(R.id.imageView_image_multiple);
            imgview_Video = itemView.findViewById(R.id.imgview_Video);
            imageView_thumbnail = itemView.findViewById(R.id.imageView_thumbnail);
            //linearLayout_postImage=itemView.findViewById(R.id.linearLayout_postImage);
        }
    }

    public static String parseDate(String inputDateString) {
        Date date = null;
        String outputDateString = null;
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputDateFormat = new SimpleDateFormat("EEE dd-MMM-yyyy");
        try {
            date = inputDateFormat.parse(inputDateString);
            outputDateString = outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDateString;
    }
}
