package com.example.touch.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.touch.R;
import com.example.touch.activities.SearchPostActivity;
import com.example.touch.models.AnotherUserProfileResult;
import com.example.touch.models.CheckedUncheckedImageDelete;
import com.example.touch.models.MultipleImageGetMainData;
import com.example.touch.ui.profile.ProfileFragment;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddImagesListAdapter extends RecyclerView.Adapter<AddImagesListAdapter.addImageList> implements View.OnLongClickListener {
    private Context context;
    private List<AnotherUserProfileResult> multipleImageGetMainData;
    private Dialog dialog;
    private boolean flag = false;
    private View view;
    private List<CheckedUncheckedImageDelete> checkedUncheckedImageDeletes;

    private LinearLayout linearLayout;
    private Button btn_cancel, btn_delete;
    private ProfileFragment classContext;

    public AddImagesListAdapter(Context context, List<AnotherUserProfileResult> multipleImageGetMainData, View view) {
        this.context = context;
        this.multipleImageGetMainData = multipleImageGetMainData;
        this.view = view;
        linearLayout = view.findViewById(R.id.linearlayout_deleteCancel);
        btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_delete = view.findViewById(R.id.btn_delete);
        checkedUncheckedImageDeletes = new ArrayList<>();
        CheckedUncheckedImageDelete checkedUncheckedImageDelete = new CheckedUncheckedImageDelete();
        checkedUncheckedImageDelete.setFlag(false);
        checkedUncheckedImageDelete.setId("0");
        for (int i = 0; i < multipleImageGetMainData.size(); i++) {
            checkedUncheckedImageDeletes.add(checkedUncheckedImageDelete);
        }


    }

    public AddImagesListAdapter(Context context, List<AnotherUserProfileResult> multipleImageGetMainData, View view, ProfileFragment profileFragment, MediaController mc) {
        this.context = context;
        this.multipleImageGetMainData = multipleImageGetMainData;
        this.view = view;
        linearLayout = view.findViewById(R.id.linearlayout_deleteCancel);
        btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_delete = view.findViewById(R.id.btn_delete);
        checkedUncheckedImageDeletes = new ArrayList<>();
        CheckedUncheckedImageDelete checkedUncheckedImageDelete = new CheckedUncheckedImageDelete();
        checkedUncheckedImageDelete.setFlag(false);
        checkedUncheckedImageDelete.setId("0");
        for (int i = 0; i < multipleImageGetMainData.size(); i++) {
            checkedUncheckedImageDeletes.add(checkedUncheckedImageDelete);
        }
        classContext = profileFragment;
    }

    @NonNull
    @Override
    public AddImagesListAdapter.addImageList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_add_image_list, parent, false);
        AddImagesListAdapter.addImageList addImageList = new AddImagesListAdapter.addImageList(view);

        return addImageList;
    }

    @Override
    public void onBindViewHolder(@NonNull AddImagesListAdapter.addImageList holder, int position) {
        Picasso.with(context)
                .load(multipleImageGetMainData.get(position).getBigImage())
                .placeholder(R.drawable.loading_placeholder)
                //.resize(250,250)
                .fit()
                .centerCrop()
                .error(R.drawable.loading_placeholder)
                .into(holder.imageView_image);

        holder.imageView_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SearchPostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("arraylist", (Serializable) multipleImageGetMainData);
                intent.putExtra("position", position);
                intent.putExtra("page", "another");
                intent.putExtra("bundle", bundle);
                context.startActivity(intent);
            }
        });
        holder.imageView_thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SearchPostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("arraylist", (Serializable) multipleImageGetMainData);
                intent.putExtra("position", position);
                intent.putExtra("page", "another");
                intent.putExtra("bundle", bundle);
                context.startActivity(intent);
            }
        });


        if (flag) {
            holder.checkBox_deleted.setVisibility(View.VISIBLE);
            linearLayout.setVisibility(View.VISIBLE);
            holder.checkBox_deleted.setChecked(false);
        } else {
            holder.checkBox_deleted.setVisibility(View.GONE);
            linearLayout.setVisibility(View.GONE);
            holder.checkBox_deleted.setChecked(false);
        }
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flag = false;

                for (int i = 0; i < checkedUncheckedImageDeletes.size(); i++) {
                    CheckedUncheckedImageDelete checkedUncheckedImageDelete = new CheckedUncheckedImageDelete();
                    checkedUncheckedImageDelete.setFlag(false);
                    checkedUncheckedImageDelete.setId("0");
                    checkedUncheckedImageDeletes.set(i, checkedUncheckedImageDelete);
                }
                notifyDataSetChanged();
                //Log.e("InsideCancel=","cancel");
            }
        });


        holder.checkBox_deleted.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Log.e("checkedCheck=", b + " " + position);
                if (b) {
                    CheckedUncheckedImageDelete checkedUncheckedImageDelete = new CheckedUncheckedImageDelete();
                    checkedUncheckedImageDelete.setFlag(true);
                    checkedUncheckedImageDelete.setId(multipleImageGetMainData.get(position).getId());
                    checkedUncheckedImageDeletes.set(position, checkedUncheckedImageDelete);

                } else {
                    CheckedUncheckedImageDelete checkedUncheckedImageDelete = new CheckedUncheckedImageDelete();
                    checkedUncheckedImageDelete.setFlag(false);
                    checkedUncheckedImageDelete.setId("0");
                    checkedUncheckedImageDeletes.set(position, checkedUncheckedImageDelete);

                }
            }
        });
//        holder.imageView_image.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                Log.e("LongPress=", position + "");
//                holder.checkBox_deleted.setVisibility(View.VISIBLE);
//                notifyDataSetChanged();
//                flag = true;
//                return true;
//            }
//        });

//        holder.imageView_thumbnail.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                Log.e("LongPress=", position + "");
//                holder.checkBox_deleted.setVisibility(View.VISIBLE);
//                notifyDataSetChanged();
//                flag = true;
//                return true;
//            }
//        });

//        holder.videoView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                Log.e("LongPress=", position + "");
//                holder.checkBox_deleted.setVisibility(View.VISIBLE);
//                notifyDataSetChanged();
//                flag = true;
//                return true;
//            }
//        });

        //for video play in this adapter
        if (multipleImageGetMainData.get(position).getType().equalsIgnoreCase("image")) {
            holder.imageView_image.setVisibility(View.VISIBLE);
            holder.imageView_thumbnail.setVisibility(View.GONE);
            holder.imgview_Video.setVisibility(View.GONE);
            //holder.videoView.setVisibility(View.GONE);

        } else {
           /* Log.e("VideoInside=",multipleImageGetMainData.get(position).getImage());
            holder.imageView_image.setVisibility(View.GONE);
            holder.videoView.setVisibility(View.VISIBLE);
            //holder.videoView.setMediaController(mc);
            String str = multipleImageGetMainData.get(position).getImage();
            Uri uri = Uri.parse(str);
            // mc.setAnchorView(holder.videoView);
            holder.videoView.setVideoURI(uri);
            holder.videoView.requestFocus();
            holder.videoView.start();*/


            holder.imgview_Video.setVisibility(View.VISIBLE);
            holder.imageView_thumbnail.setVisibility(View.VISIBLE);
            holder.imageView_image.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.loading_placeholder);
            requestOptions.error(R.drawable.loading_placeholder);
            Glide.with(context)
                    .load(multipleImageGetMainData.get(position).getBigImage())
                    .fitCenter()
                    .centerCrop()
                    .apply(requestOptions)
                    .thumbnail(Glide.with(context).load(multipleImageGetMainData.get(position).getBigImage()))
                    .into(holder.imageView_thumbnail);
        }
       /* holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                holder.videoView.start();
                mp.setLooping(true);
                mp.setVolume(0f, 0f);
            }
        });*/

       /* holder.videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                return false;
            }
        });*/
       /* holder.videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, MyPostLists.class);
                intent.putExtra(Constants.TO_POSITION,position+"");
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });*/
    }


    @Override
    public int getItemCount() {
        return multipleImageGetMainData.size();
    }

    @Override
    public boolean onLongClick(View view) {
        //Log.e("LongPress=",view.)
        return true;
    }

    public class addImageList extends RecyclerView.ViewHolder {
        ImageView imageView_image;
        ImageView linearLayout_postImage;
        ImageView imageView_thumbnail;
        ImageView imgview_Video;
        CheckBox checkBox_deleted;
        VideoView videoView;

        public addImageList(@NonNull View itemView) {
            super(itemView);
            checkBox_deleted = itemView.findViewById(R.id.checkBox_deleted);
            imgview_Video = itemView.findViewById(R.id.imgview_Video);
            imageView_image = itemView.findViewById(R.id.imageView_image);
            imageView_thumbnail = itemView.findViewById(R.id.imageView_thumbnail);
            videoView = itemView.findViewById(R.id.videoView);

            //linearLayout_postImage=itemView.findViewById(R.id.linearLayout_postImage);
            btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int j = 0;
                    test = new String[checkedUncheckedImageDeletes.size()];
                    for (int i = 0; i < checkedUncheckedImageDeletes.size(); i++) {

                        if (checkedUncheckedImageDeletes.get(i).isFlag()) {
                            Log.e("Test=", checkedUncheckedImageDeletes.get(i).isFlag() + "");
                            test[j++] = checkedUncheckedImageDeletes.get(i).getId();
                        }

                    }
                    keyId = "";
                    //Log.e("Flag=",test);
                    for (int i = 0; i < test.length; i++) {
                        if (test[i] != null) {
                            if (i == 0) {
                                keyId = test[i];
                            } else {
                                keyId = keyId + "," + test[i];

                            }
                        }

                    }
                    //Log.e("Key=",keyId+"  test");

                    if (keyId.equalsIgnoreCase("")) {
                        Snackbar.make(imageView_image, context.getResources().getString(R.string.please_select_images_to_delete), Snackbar.LENGTH_LONG).show();
                    } else {
                        classContext.callDeleteMethod(keyId);
                        //linearLayout.setVisibility(View.GONE);
                    }

                }
            });

        }

    }

    String test[];
    String keyId = "";

    public static String parseDate(String inputDateString) {
        Date date = null;
        String outputDateString = null;
        SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("EEE dd-MMM-yyyy");
        try {
            date = inputDateFormat.parse(inputDateString);
            outputDateString = outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputDateString;
    }
}
