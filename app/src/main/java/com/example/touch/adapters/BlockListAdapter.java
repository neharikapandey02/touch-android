package com.example.touch.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.BlockUserListActivity;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.models.BlockListData;
import com.example.touch.models.BlockUserResponse;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlockListAdapter extends RecyclerView.Adapter<BlockListAdapter.ViewHolder> {


    private Context context;
    ArrayList<BlockListData.BlockList> arrayList;
    private String userId;
    Button button;

    public BlockListAdapter(Context context, ArrayList<BlockListData.BlockList> arrayList, String user_id){
        this.context = context;
        this.arrayList = arrayList;
        this.userId = user_id;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(context).inflate(R.layout.block_list_layout,parent,false);
        BlockListAdapter.ViewHolder search=new BlockListAdapter.ViewHolder(view);

        return search;

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.nameTv.setText(arrayList.get(position).getFirst_name());

        Picasso.with(context)
                .load(arrayList.get(position).getProfile_image())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.imageView);

        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button = holder.btn;
                if (NetworkUtil.checkNetworkStatus(context)) {
                    RetrofitHelper.getInstance().doUnblock(unblockCallback, userId, arrayList.get(position).getId());
                } else {
                    Snackbar.make(holder.btn, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }

    Callback<BlockUserResponse> unblockCallback = new Callback<BlockUserResponse>() {
        @Override
        public void onResponse(Call<BlockUserResponse> call, Response<BlockUserResponse> response) {
            if (response.isSuccessful()){
                if (response.body().getSuccess() == 1){
                    button.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onFailure(Call<BlockUserResponse> call, Throwable t) {

        }
    };

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTv;
        CircularImageView imageView;
        Button btn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nameTv = itemView.findViewById(R.id.nameTv);
            imageView = itemView.findViewById(R.id.imageView);
            btn = itemView.findViewById(R.id.btn);

        }
    }
}
