package com.example.touch.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.emoji.widget.EmojiEditText;
import androidx.emoji.widget.EmojiTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.ChatActivity;
import com.example.touch.activities.VisitorProfileActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.EmojiAppCompatTextView;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.ChatListMain;
import com.example.touch.models.ChatListMainData;
import com.example.touch.models.MultipleImageGetMainData;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconMultiAutoCompleteTextView;
import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ChatListAdapter extends RecyclerView.Adapter<ChatListAdapter.ChatList> {
    Context context;
    List<ChatListMainData> chatListMainDataList;
    private SharedPreferencesData sharedPreferencesData;
    private Dialog dialog;

    public ChatListAdapter(Context context, List<ChatListMainData> chatListMainDataList) {
        this.context = context;
        this.chatListMainDataList = chatListMainDataList;
        sharedPreferencesData = new SharedPreferencesData(context);

    }

    @NonNull
    @Override
    public ChatListAdapter.ChatList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_chat_list_adapter, parent, false);
        ChatListAdapter.ChatList addImageList = new ChatListAdapter.ChatList(view);

        return addImageList;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatListAdapter.ChatList holder, int position) {
        if(chatListMainDataList.get(position).getProfileImage()!=null&&!chatListMainDataList.get(position).getProfileImage().equalsIgnoreCase("")){
            Picasso.with(context)
                    .load(chatListMainDataList.get(position).getProfileImage())
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(holder.circularImageView_chatUser);
        }

        if (!chatListMainDataList.get(position).getFirstName().equalsIgnoreCase("")) {
            holder.tv_receiverName.setText(chatListMainDataList.get(position).getFirstName());
        }else {
            holder.tv_receiverName.setText("User Profile Deleted");
            holder.tv_receiverName.setTextColor(context.getResources().getColor(R.color.light_red));
        }
        try {
            int a = Integer.parseInt(chatListMainDataList.get(position).getUnread_count());
            if (a>0) {
                holder.tv_lastChats.setTextColor(context.getResources().getColor(R.color.black));
                holder.countTv.setVisibility(View.VISIBLE);
                holder.countTv.setText(chatListMainDataList.get(position).getUnread_count());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        holder.circularImageView_chatUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chatListMainDataList.get(position).getUser_type().equalsIgnoreCase("2")){
                    Intent intent=new Intent(context, AnotherUserProfile.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID,chatListMainDataList.get(position).getAnotherId());
                    context.startActivity(intent);
                }else{
                    Intent intent=new Intent(context, VisitorProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID,chatListMainDataList.get(position).getAnotherId());
                    context.startActivity(intent);
                }
            }
        });

        holder.layout_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra(Constants.RECEIVER_ID, chatListMainDataList.get(position).getAnotherId());
                intent.putExtra(Constants.PROFILE_IMAGE, chatListMainDataList.get(position).getProfileImage());
                intent.putExtra(Constants.FIRST_NAME, chatListMainDataList.get(position).getFirstName());
                intent.putExtra("block_status", chatListMainDataList.get(position).getBlock_status().toString());
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        if (chatListMainDataList.get(position).getType().equalsIgnoreCase("1")) {
            holder.tv_lastChats.setText(chatListMainDataList.get(position).getMsg());
        }else {
            holder.tv_lastChats.setText("Audio");
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD&&android.os.Build.VERSION.SDK_INT <=android.os.Build.VERSION_CODES.P) {
            //holder.tv_lastChats.setEmojiconSize(50);
            holder.tv_lastChats.setTextSize(17);
            ///holder.tv_lastChats.setLineSpacing(3,3);
        }else{
            //holder.tv_lastChats.setEmojiconSize(80);
            holder.tv_lastChats.setTextSize(17);
            //holder.tv_lastChats.setLineSpacing(3,3);
        }

        //holder.tv_lastChats.setMaxLines(1);





    }

    @Override
    public int getItemCount() {
        return chatListMainDataList.size();
    }

    public class ChatList extends RecyclerView.ViewHolder {
        ImageView imageView_image;
        CircularImageView circularImageView_chatUser;
        TextView tv_receiverName;
        TextView countTv;

        EmojiTextView tv_lastChats;
        RelativeLayout layout_top;

        public ChatList(@NonNull View itemView) {
            super(itemView);
            circularImageView_chatUser = itemView.findViewById(R.id.circularImageView_chatUser);
            tv_receiverName = itemView.findViewById(R.id.tv_receiverName);
            countTv = itemView.findViewById(R.id.countTv);
            layout_top = itemView.findViewById(R.id.layout_top);
            tv_lastChats = itemView.findViewById(R.id.tv_lastChats);
        }
    }


}