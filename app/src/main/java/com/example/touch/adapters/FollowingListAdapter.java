package com.example.touch.adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.VisitorProfileActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.FollowModel;
import com.example.touch.models.FollowUnfollowMain;
import com.example.touch.models.FollowingMainData;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class FollowingListAdapter extends RecyclerView.Adapter<FollowingListAdapter.FollowingList> {
    Context context;
    List<FollowingMainData> followModelList;
    ProgressBar progressBar;
    private SharedPreferencesData sharedPreferencesData;
    private String userId;
    private boolean status;



    public FollowingListAdapter(Context context,List<FollowingMainData> followModelList,ProgressBar progressBar,boolean status) {
        this.context = context;
        this.followModelList=followModelList;
        this.progressBar=progressBar;
        sharedPreferencesData=new SharedPreferencesData(context);
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);
        this.status=status;
    }

    @NonNull
    @Override
    public FollowingListAdapter.FollowingList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_following_list, parent, false);
        FollowingListAdapter.FollowingList followingList = new FollowingListAdapter.FollowingList(view);
        return followingList;
    }

    @Override
    public void onBindViewHolder(@NonNull FollowingListAdapter.FollowingList holder, int position) {
        Picasso.with(context)
                .load(followModelList.get(position).getProfileImage())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_image)
                .into(holder.circularImageView_UserImage);
        holder.tv_name.setText(followModelList.get(position).getFirstName());

        if (followModelList.get(position).getStatus()==1) {
            holder.btn_followUnfollow.setText("Unfollow");
            holder.btn_followUnfollow.setBackgroundResource(R.drawable.button_corner_unfollow);
        } else {
            holder.btn_followUnfollow.setText("Follow");
            holder.btn_followUnfollow.setBackgroundResource(R.drawable.following_corner);
            holder.btn_followUnfollow.setTextColor(context.getResources().getColor(R.color.white));
        }

        if (status){
            holder.btn_followUnfollow.setVisibility(View.VISIBLE);
        }else{
            holder.btn_followUnfollow.setVisibility(View.GONE);
        }

        holder.circularImageView_UserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent=new Intent(context, AnotherUserProfile.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID, followModelList.get(position).getId());
                    context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return followModelList.size();
    }

    public class FollowingList extends RecyclerView.ViewHolder {
        Button btn_followUnfollow;
        CircularImageView circularImageView_UserImage;
        TextView tv_name;

        public FollowingList(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            circularImageView_UserImage = itemView.findViewById(R.id.circularImageView_UserImage);
            btn_followUnfollow = itemView.findViewById(R.id.btn_followUnfollow);
            btn_followUnfollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (sharedPreferencesData!=null&&!sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID).equalsIgnoreCase("")){
                        FollowingMainData followModel = new FollowingMainData();
                        followModel.setFirstName(followModelList.get(getAdapterPosition()).getFirstName());
                        followModel.setLastName(followModelList.get(getAdapterPosition()).getLastName());
                        followModel.setId(followModelList.get(getAdapterPosition()).getId());


                        if (followModelList.get(getAdapterPosition()).getStatus()==1) {
                            followModel.setStatus(2);
                            followModelList.set(getAdapterPosition(), followModel);
                            btn_followUnfollow.setText("Follow");
                            btn_followUnfollow.setBackgroundResource(R.drawable.following_corner);
                            btn_followUnfollow.setTextColor(context.getResources().getColor(R.color.white));
                            Animation animFadeIn = AnimationUtils.loadAnimation(context,
                                    R.anim.fade_in);
                            btn_followUnfollow.startAnimation(animFadeIn);


                            if (NetworkUtil.checkNetworkStatus(context)) {
                                Log.e("adapter=",followModelList.get(getAdapterPosition()).getId()+"");
                                progressBar.setVisibility(View.VISIBLE);
                                RetrofitHelper.getInstance().getFollowUnfollow(followUnfollowCallback,userId,followModelList.get(getAdapterPosition()).getId(),"2");

                            }else{
                                Snackbar.make(btn_followUnfollow,context.getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            followModel.setStatus(1);
                            followModelList.set(getAdapterPosition(), followModel);
                            btn_followUnfollow.setText("Unfollow");
                            btn_followUnfollow.setBackgroundResource(R.drawable.button_corner_unfollow);
                            btn_followUnfollow.setTextColor(context.getResources().getColor(R.color.black));
                            Animation animFadeIn = AnimationUtils.loadAnimation(context,
                                    R.anim.fade_in);
                            btn_followUnfollow.startAnimation(animFadeIn);
                            if (NetworkUtil.checkNetworkStatus(context)) {
                                progressBar.setVisibility(View.VISIBLE);

                                RetrofitHelper.getInstance().getFollowUnfollow(followUnfollowCallback,userId,followModelList.get(getAdapterPosition()).getId(),"1");

                            }else{
                                Snackbar.make(btn_followUnfollow,context.getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }else{
                        Snackbar.make(btn_followUnfollow,context.getResources().getString(R.string.login_first_to_follow),Snackbar.LENGTH_LONG).show();
                    }



                }
            });


        }
    }
    Callback<FollowUnfollowMain> followUnfollowCallback=new Callback<FollowUnfollowMain>(){

        @Override
        public void onResponse(Call<FollowUnfollowMain> call, Response<FollowUnfollowMain> response) {
            progressBar.setVisibility(View.GONE);
             if (response.isSuccessful()){
                 Snackbar.make(progressBar,response.body().getMessage(),Snackbar.LENGTH_LONG).show();

             }
        }

        @Override
        public void onFailure(Call<FollowUnfollowMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };
}
