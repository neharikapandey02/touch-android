package com.example.touch.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.UploadImageActivity;
import com.example.touch.constants.Constants;
import com.example.touch.models.HastagMainData;
import com.example.touch.models.SearchAdapter;
import com.example.touch.models.SearchMainData;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;
import java.util.List;

public class HastagAdapter extends RecyclerView.Adapter<HastagAdapter.Search> {
    Context context;
    List<HastagMainData> hastagMainData;
    Activity activity;


    public HastagAdapter(Context context, List<HastagMainData> hastagMainData,Activity activity){
        this.hastagMainData=hastagMainData;
        this.context=context;
        this.activity=activity;
    }


    @NonNull
    @Override
    public HastagAdapter.Search onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_hastag,parent,false);
        HastagAdapter.Search search=new HastagAdapter.Search(view);

        return search;
    }

    @Override
    public void onBindViewHolder(@NonNull HastagAdapter.Search holder, int position) {
        holder.tv_HastageName.setText(hastagMainData.get(position).getTag());
       // holder.tv_locationName.setText(searchMainDataList.get(position).getCountry()+","+searchMainDataList.get(position).getCity());
       /* Picasso.with(context)
                .load(searchMainDataList.get(position).getProfileImage())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.circularImageView_UserImage);

        holder.tv_name.setText(searchMainDataList.get(position).getFirstName()+" "+searchMainDataList.get(position).getLastName());
        holder.tv_email.setText(searchMainDataList.get(position).getEmail());*/
        holder.top_circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((UploadImageActivity) activity).addhastag(hastagMainData.get(position).getTag());
            }
        });

    }

    @Override
    public int getItemCount() {
        return hastagMainData.size();
    }

    public class Search extends RecyclerView.ViewHolder{
        //CircularImageView circularImageView_UserImage;
        TextView tv_HastageName;
        //TextView tv_email;
       // TextView tv_userName;
       // TextView tv_locationName;
       // RelativeLayout layout_relativeTop;
        LinearLayout top_circle;

        public Search(@NonNull View itemView) {
            super(itemView);
            //circularImageView_UserImage=itemView.findViewById(R.id.circularImageView_UserImage);
            tv_HastageName=itemView.findViewById(R.id.tv_HastageName);
           // tv_email=itemView.findViewById(R.id.tv_email);
           // layout_relativeTop=itemView.findViewById(R.id.layout_relativeTop);
            top_circle=itemView.findViewById(R.id.top_circle);
           // tv_locationName=itemView.findViewById(R.id.tv_locationName);
           // tv_userName=itemView.findViewById(R.id.tv_userName);
        }
    }
}
