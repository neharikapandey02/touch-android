package com.example.touch.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.constants.Constants;
import com.example.touch.models.FollowersListMainData;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class HomeNameAdapter extends RecyclerView.Adapter<HomeNameAdapter.HomeName> {
    Context context;
    private List<FollowersListMainData> followersListMainData;

    public HomeNameAdapter(Context context, List<FollowersListMainData> followersListMainData) {
        this.context = context;
        this.followersListMainData = followersListMainData;
    }

    @NonNull
    @Override
    public HomeNameAdapter.HomeName onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_home_name_adapter, parent, false);
        return new HomeName(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeNameAdapter.HomeName holder, int position) {
        holder.tv_firstName.setText(followersListMainData.get(position).getFirstName());
        holder.tv_firstName.setOnClickListener(view -> {
            Intent intent = new Intent(context, AnotherUserProfile.class);
            intent.putExtra(Constants.ANOTHER_USER_ID, followersListMainData.get(position).getId());
            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        });
        Picasso.with(context)
                .load(followersListMainData.get(position).getProfileImage())
                .placeholder(R.drawable.profile_placeholder)
                //.resize(0, 520)
                //.fit()
                .error(R.drawable.profile_placeholder)
                .into(holder.circularImageView_UserImage);

        holder.LinearLayout_topName.setOnClickListener(view -> {
            Intent intent = new Intent(context, AnotherUserProfile.class);
            intent.putExtra(Constants.ANOTHER_USER_ID, followersListMainData.get(position).getId());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return followersListMainData.size();
    }

    class HomeName extends RecyclerView.ViewHolder {
        TextView tv_firstName;
        ImageView circularImageView_UserImage;
        LinearLayout LinearLayout_topName;

        HomeName(@NonNull View itemView) {
            super(itemView);
            tv_firstName = itemView.findViewById(R.id.tv_firstName);
            circularImageView_UserImage = itemView.findViewById(R.id.circularImageView_UserImage);
            LinearLayout_topName = itemView.findViewById(R.id.LinearLayout_topName);
        }
    }
}
