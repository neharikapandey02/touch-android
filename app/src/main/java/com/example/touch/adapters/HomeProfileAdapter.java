package com.example.touch.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.emoji.widget.EmojiTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.ChatActivity;
import com.example.touch.activities.CommentsActivity;
import com.example.touch.activities.LikesTotalList;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.forvideofiles.BaseVideoItem;
import com.example.touch.forvideofiles.DirectLinkVideoItem;
import com.example.touch.forvideofiles.ItemFactory;
import com.example.touch.interfaces.StatusMuteUnmute;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.LikesMain;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class HomeProfileAdapter extends RecyclerView.Adapter<HomeProfileAdapter.HomeProfile> {
    static Context context;
    List<HomeMainData> followersListMainData;
    int i = 0;
    HomeProfileAdapter.HomeProfile holder;
    private int positionImage;
    private SharedPreferencesData sharedPreferencesData;
    private final int LIKE_DISPLAY_LENGTH = 700;
    private String userImagePath;
    private  List<DirectLinkVideoItem> mList1=new ArrayList<>();
    VideoPlayerManager mVideoPlayerManager;
    MediaController mc;
    AudioManager amanager;


    public HomeProfileAdapter(Context context, List<HomeMainData> followersListMainData) {
        this.context = context;
        this.followersListMainData = followersListMainData;

        sharedPreferencesData = new SharedPreferencesData(context);
        if (sharedPreferencesData!=null){
            userImagePath = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);
        }

    }
    public HomeProfileAdapter(Context context, List<HomeMainData> followersListMainData,MediaController mc) {
        this.mc=mc;
        this.context = context;
        this.followersListMainData = followersListMainData;
        StatusMuteUnmute.muteunmuteStatus=true;

       // amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
        sharedPreferencesData = new SharedPreferencesData(context);
        if (sharedPreferencesData!=null){
            userImagePath = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);
        }

    }
    public HomeProfileAdapter(Context context, List<HomeMainData> followersListMainData, VideoPlayerManager videoPlayerManager, Activity activity,List<DirectLinkVideoItem> mList) {
        this.context = context;
        mVideoPlayerManager=videoPlayerManager;
        this.followersListMainData = followersListMainData;
        mList1=mList;
        try {
            mList1.add(ItemFactory.createItemFromAsset1("video_sample_1.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));
            mList1.add(ItemFactory.createItemFromAsset1("video_sample_2.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));
            mList1.add(ItemFactory.createItemFromAsset1("video_sample_3.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4", activity, videoPlayerManager));
            mList1.add(ItemFactory.createItemFromAsset1("video_sample_4.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));
            mList1.add(ItemFactory.createItemFromAsset1("video_sample_4.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));

            mList1.add(ItemFactory.createItemFromAsset1("video_sample_1.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));
            mList1.add(ItemFactory.createItemFromAsset1("video_sample_2.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));
            mList1.add(ItemFactory.createItemFromAsset1("video_sample_3.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" ,activity, videoPlayerManager));
            mList1.add(ItemFactory.createItemFromAsset1("video_sample_4.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));


            mList1.add(ItemFactory.createItemFromAsset1("video_sample_1.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));
           // mList1.add(ItemFactory.createItemFromAsset1("video_sample_2.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));
            //mList1.add(ItemFactory.createItemFromAsset1("video_sample_3.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" ,activity, videoPlayerManager));
            //mList1.add(ItemFactory.createItemFromAsset1("video_sample_4.mp4", R.drawable.search_icon,"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",activity, videoPlayerManager));
        }catch (Exception e){

        }

        sharedPreferencesData = new SharedPreferencesData(context);
        if (sharedPreferencesData != null) {
            userImagePath = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);
        }

    }

    @NonNull
    @Override
    public HomeProfileAdapter.HomeProfile onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Log.e("ViewType=",viewType+"");
       // View view = LayoutInflater.from(context).inflate(R.layout.item_home_profile_adapter, parent, false);
       // HomeProfileAdapter.HomeProfile homeProfile = new HomeProfileAdapter.HomeProfile(view);
        //426*240

        if (mList1!=null&&mList1.size()>0){
            BaseVideoItem videoItem = mList1.get(viewType);
            View resultView = videoItem.createView(parent, context.getResources().getDisplayMetrics().widthPixels);
            return new HomeProfile(resultView) ;
        }else{
            View view = LayoutInflater.from(context).inflate(R.layout.item_home_profile_adapter, parent, false);
            HomeProfileAdapter.HomeProfile homeProfile = new HomeProfileAdapter.HomeProfile(view);
            return homeProfile;
        }

    }




    @Override
    public void onBindViewHolder(@NonNull HomeProfileAdapter.HomeProfile holder, int position) {
        if (mList1!=null&&mList1.size()>0){
            BaseVideoItem videoItem = mList1.get(position);
            videoItem.update(position, holder, mVideoPlayerManager);
        }else{

        }
        holder.progressBarImages.setVisibility(View.VISIBLE);
        Glide.with(context).load(followersListMainData.get(position).getBig())
                .placeholder(R.drawable.loading_placeholder)
                .error(R.drawable.loading_placeholder)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                       holder. progressBarImages.setVisibility(View.GONE);

                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        holder.progressBarImages.setVisibility(View.GONE);

                        return false;
                    }
                })
                .into(holder.imgView_profile);



       /* Picasso.with(context)
                .load(followersListMainData.get(position).getBig())
                .placeholder(R.drawable.loading_placeholder)
                .error(R.drawable.loading_placeholder)
                .into(holder.imgView_profile);*/

        /*Picasso.with(context)
                .load(followersListMainData.get(position).getBig())
                .placeholder(R.drawable.loading_placeholder)
                .skipMemoryCache()
                //.resize(1080,1920 )
                //.onlyScaleDown()
               // .noFade()

                 //.centerCrop()


                //.resize(0, 920)
                //.fit()
                .error(R.drawable.loading_placeholder)
                .into(holder.imgView_profile);*/
       // holder.imgView_profile.setScaleType(ImageView.ScaleType.FIT_XY);

        Picasso.with(context)
                .load(followersListMainData.get(position).getProfile_image())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.circularImageView_Profile);

        holder.circularImageView_Profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, AnotherUserProfile.class);
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.ANOTHER_USER_ID,followersListMainData.get(position).getPost_user_id());
                context.startActivity(intent);
            }
        });


        RelativeLayout.LayoutParams layoutParams=(RelativeLayout.LayoutParams)holder.tv_userName.getLayoutParams();;
       if (followersListMainData.get(position).getKeyword()!=null){
           holder.tv_hastags.setText(followersListMainData.get(position).getKeyword());
           //holder.tv_hastags.setEmojiconSize(40);
           holder.tv_hastags.setTextSize(13);
           holder.tv_hastags.setVisibility(View.VISIBLE);
       }else{
           holder.tv_hastags.setVisibility(View.GONE);
       }
       if (followersListMainData.get(position).getLocation()!=null&&!followersListMainData.get(position).getLocation().equalsIgnoreCase("")){
           Log.e("callLocation","Inside NotNull"+position+"");
           layoutParams.setMargins(5,10,0,0);
           holder.tv_userName.setLayoutParams(layoutParams);
           holder.tv_Address.setVisibility(View.VISIBLE);
           holder.tv_Address.setText(followersListMainData.get(position).getLocation());


       }else{
           Log.e("callLocation","Inside Null"+position+"");
           holder.tv_Address.setVisibility(View.GONE);
           layoutParams.setMargins(5,30,0,0);
           holder.tv_userName.setLayoutParams(layoutParams);


       }

        if (mc!=null){
            if (followersListMainData.get(position).getType().equalsIgnoreCase("image")){
                holder.imgView_profile.setVisibility(View.VISIBLE);
                holder.videoView.setVisibility(View.GONE);


            }else{
                Log.d("testmc", "*******"+mc);
                    Log.e("VideoInside=", followersListMainData.get(position).getBig());
                    holder.imgView_profile.setVisibility(View.GONE);
                    holder.videoView.setVisibility(View.VISIBLE);
                    //holder.videoView.setMediaController(mc);
                    String str = followersListMainData.get(position).getBig();
                    Uri uri = Uri.parse(str);
                    // mc.setAnchorView(holder.videoView);

                    //holder.videoView.setVideoUri(uri);
                    //holder.videoView.setVideoURI(uri);
                    holder.videoView.requestFocus();
                    //holder.videoView.setPreviewImage(holder.videoView.getBitmap());

                    // holder.videoView.setRepeatMode(100);
                    //holder.videoView.setOnPreparedListener();
                    //holder.videoView.setRepeatMode();
                    holder.videoView.setVideoPath(followersListMainData.get(position).getBig());
                    holder.videoView.start();

                    //holder.videoView.setRepeatMode(100);
                    // holder.videoView.setVideoPath(followersListMainData.get(position).getBig());
                    holder.progressBarHome.setVisibility(View.VISIBLE);
            }
            holder.videoView.setOnCompletionListener(new OnCompletionListener() {
                @Override
                public void onCompletion() {
                    //holder.videoView.setVideoPath(followersListMainData.get(position).getBig());
                    holder.videoView.requestFocus();
                    holder.videoView.restart();

                    Log.e("InsideOnComplete=","onComplete");
                }
            });

            holder.videoView.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared() {
                    holder.videoView.start();
                    //holder.videoView.setRepeatMode(100);
                    holder.progressBarHome.setVisibility(View.GONE);
                }
            });
           /* holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                    holder.videoView.start();
                    holder.progressBarHome.setVisibility(View.GONE);
                    //mp.setVolume(10,10);

                }
            });*/
        }
        holder.tv_userName.setText(followersListMainData.get(position).getFirstName());
        positionImage = position;

        if (followersListMainData.get(position).getIsLike() == 1) {
            holder.imgView_likeRed.setVisibility(View.VISIBLE);
            holder.imgView_likeBlack.setVisibility(View.INVISIBLE);
        } else {
            holder.imgView_likeRed.setVisibility(View.GONE);
            holder.imgView_likeBlack.setVisibility(View.VISIBLE);
        }

        if (followersListMainData.get(position).getCount_comment() == 0) {
            holder.tv_totalComments.setVisibility(View.GONE);
        } else {
            holder.tv_totalComments.setVisibility(View.VISIBLE);
            holder.tv_totalComments.setText("View All " + followersListMainData.get(position).getCount_comment() + " Comments");
        }

        holder.tv_totalLikes.setText(followersListMainData.get(position).getCountLike() + " " + "Likes");
        holder.tv_totalLikes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                    if (followersListMainData.get(position).getCountLike()!=0){
                        Intent intent = new Intent(context, LikesTotalList.class);
                        intent.putExtra(Constants.POSTID, followersListMainData.get(position).getId());
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }else{
                        Snackbar.make(holder.imgView_Chat,context.getResources().getString(R.string.no_like_yet),Snackbar.LENGTH_LONG).show();
                    }

                }else{
                    Snackbar.make(holder.imgView_Chat,context.getResources().getString(R.string.please_login_to_see_likes),Snackbar.LENGTH_LONG).show();
                }
            }
        });
        holder.imgView_Chat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                Log.e("userId=", userId + "  " + followersListMainData.get(position).getUserId());
                if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                    if (!userId.equalsIgnoreCase(followersListMainData.get(position).getUserId())) {
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra(Constants.RECEIVER_ID, followersListMainData.get(position).getPost_user_id());
                        intent.putExtra(Constants.PROFILE_IMAGE, followersListMainData.get(position).getProfile_image());
                        intent.putExtra(Constants.FIRST_NAME, followersListMainData.get(position).getFirstName());
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    } else {
                        Toast.makeText(context, context.getResources().getString(R.string.same_user), Toast.LENGTH_LONG).show();
                    }

                }else{
                    Snackbar.make(holder.imgView_Chat,context.getResources().getString(R.string.please_login_to_chat),Snackbar.LENGTH_LONG).show();

                }
            }
        });

        /*holder.videoView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                i++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        i = 0;
                    }
                };

                if (i == 1) {
                    //Single click
                    handler.postDelayed(r, 250);
                    Log.e("handler=", "handler");
                } else if (i == 2) {
                    //Double click
                    i = 0;
                    Log.e("doubleClick=", "Double Click");
                    //status 1 for like 2 for unlike mind it
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        String status = followersListMainData.get(position).getIsLike().toString();
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            if (status.equalsIgnoreCase("1")) {
                                holder.like_unlike.setVisibility(View.VISIBLE);
                                handlerForLikeUnlikeShow(holder);
                                positionImage = position;
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, followersListMainData.get(position).getId(), "2");
                            } else {
                                holder.like_unlike.setVisibility(View.VISIBLE);
                                handlerForLikeUnlikeShow(holder);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, followersListMainData.get(position).getId(), "1");
                                positionImage = position;
                            }

                        } else {
                            Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }

                }
                return false;
            }
        }) ;*/


        holder.imgView_profile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                i++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {

                    @Override
                    public void run() {
                        i = 0;
                    }
                };

                if (i == 1) {
                    //Single click
                    handler.postDelayed(r, 250);
                    Log.e("handler=", "handler");
                } else if (i == 2) {
                    //Double click
                    i = 0;
                    Log.e("doubleClick=", "Double Click");
                    //status 1 for like 2 for unlike mind it
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        String status = followersListMainData.get(position).getIsLike().toString();
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            if (status.equalsIgnoreCase("1")) {
                                holder.like_unlike.setVisibility(View.VISIBLE);
                                handlerForLikeUnlikeShow(holder);
                                positionImage = position;
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, followersListMainData.get(position).getId(), "2");
                            } else {
                                holder.like_unlike.setVisibility(View.VISIBLE);
                                handlerForLikeUnlikeShow(holder);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, followersListMainData.get(position).getId(), "1");
                                positionImage = position;
                            }

                        } else {
                            Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }

                }


            }
        });

        this.holder = holder;
      holder.imgView_likeBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtil.checkNetworkStatus(context)) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    String status = followersListMainData.get(position).getIsLike().toString();
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        if (status.equalsIgnoreCase("1")) {
                            positionImage = position;
                            holder.imgView_likeRed.setVisibility(View.GONE);
                            holder.imgView_likeBlack.setVisibility(View.VISIBLE);
                            RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, followersListMainData.get(position).getId(), "2");
                        } else {
                            positionImage = position;
                            holder.imgView_likeRed.setVisibility(View.VISIBLE);
                            holder.imgView_likeBlack.setVisibility(View.INVISIBLE);
                            RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, followersListMainData.get(position).getId(), "1");

                        }

                    } else {
                        Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                    }


                } else {
                    Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }

            }
        });
        holder.imgView_likeRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtil.checkNetworkStatus(context)) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    String status = followersListMainData.get(position).getIsLike().toString();
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        if (status.equalsIgnoreCase("1")) {
                            positionImage = position;
                            holder.imgView_likeRed.setVisibility(View.GONE);
                            holder.imgView_likeBlack.setVisibility(View.VISIBLE);
                            RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, followersListMainData.get(position).getId(), "2");
                        } else {
                            positionImage = position;
                            holder.imgView_likeRed.setVisibility(View.VISIBLE);
                            holder.imgView_likeBlack.setVisibility(View.INVISIBLE);
                            RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, followersListMainData.get(position).getId(), "1");

                        }

                    } else {
                        Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                    }


                } else {
                    Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        });
        holder.imgView_Comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtil.checkNetworkStatus(context)) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        Intent intent = new Intent(context, CommentsActivity.class);
                        intent.putExtra(Constants.POSTID, followersListMainData.get(position).getId());
                        intent.putExtra("post_user_id", followersListMainData.get(position).getUserId());
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    } else {
                        Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.please_login_to_comment), Snackbar.LENGTH_LONG).show();
                    }


                } else {
                    Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }

            }
        });

        holder.tv_totalComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtil.checkNetworkStatus(context)) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        Intent intent = new Intent(context, CommentsActivity.class);
                        intent.putExtra(Constants.POSTID, followersListMainData.get(position).getId());
                        intent.putExtra("post_user_id", followersListMainData.get(position).getUserId());
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);

                    } else {
                        Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.please_login_to_comment), Snackbar.LENGTH_LONG).show();
                    }


                } else {
                    Snackbar.make(holder.imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        //here manage all audio mute unmute method and click listners

        amanager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

        if (followersListMainData.get(position).getType().equalsIgnoreCase("video")){
            if (StatusMuteUnmute.muteunmuteStatus){
                holder.imgview_unmute.setVisibility(View.GONE);
                holder.imgview_mute.setVisibility(View.VISIBLE);
                Log.e("Mute=","Inside MuteVisisble");

            }else{
                holder.imgview_unmute.setVisibility(View.VISIBLE);
                holder.imgview_mute.setVisibility(View.GONE);
                Log.e("Mute=","Inside UnmuteVisisble");
            }

        }else{
            holder.imgview_mute.setVisibility(View.GONE);
            holder.imgview_unmute.setVisibility(View.GONE);
        }

        holder.imgview_mute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                StatusMuteUnmute.muteunmuteStatus=false;
                holder.imgview_unmute.setVisibility(View.VISIBLE);
                holder.imgview_mute.setVisibility(View.GONE);
                if (position==0){
                    notifyItemRangeChanged(position+1,followersListMainData.size());
                }else{
                    notifyItemRangeChanged(0,position);
                    notifyItemRangeChanged(position+1,followersListMainData.size());
                }
            }
        });


        holder.imgview_unmute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                StatusMuteUnmute.muteunmuteStatus=true;
                holder.imgview_unmute.setVisibility(View.GONE);
                holder.imgview_mute.setVisibility(View.VISIBLE);
                if (position==0){
                    notifyItemRangeChanged(position+1,followersListMainData.size());
                }else {
                    notifyItemRangeChanged(0,position);
                    notifyItemRangeChanged(position+1,followersListMainData.size());
                }





            }
        });



    }
    RecyclerView mRecyclerView;


    @Override
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        mRecyclerView = recyclerView;

        // Handle key up and key down and attempt to move selection
        recyclerView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();

                // Return false if scrolled to the bounds and allow focus to move off the list
                if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
                    Log.e("Volume=","Volume22");
                    int music_volume_level = amanager.getStreamVolume(AudioManager.STREAM_MUSIC);

                    // Get the device music maximum volume level
                    int max = amanager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                    Log.e("Volume=",music_volume_level+"    "+max);

                }

                return false;
            }
        });
    }


    @Override
    public int getItemCount() {
        return followersListMainData.size();
    }
    static Handler handler ;
    static SurfaceHolder holder1 ;



    public static class HomeProfile extends RecyclerView.ViewHolder {

        ImageView imgview_unmute;
        ImageView imgview_mute;
        ImageView imgView_Chat;
        ImageView imgView_Comment;
        ImageView like_unlike;
        ImageView imgView_profile;
        ImageView imgView_likeRed;
        ImageView imgView_likeBlack;
        TextView tv_userName;
        TextView tv_totalComments;
        TextView tv_totalLikes;
        TextView tv_Address;
        EmojiTextView tv_hastags;
        CircularImageView circularImageView_Profile;
        VideoView videoView_show;
         public final VideoPlayerView mPlayer;
        com.devbrackets.android.exomedia.ui.widget.VideoView videoView;
         ProgressBar progressBarHome;
         ProgressBar progressBarImages;
        SurfaceView surfaceView;
        public HomeProfile(@NonNull View itemView) {
            super(itemView);
            videoView =  itemView.findViewById(R.id.videoView);
            progressBarImages =  itemView.findViewById(R.id.progressBarImages);
            progressBarHome =  itemView.findViewById(R.id.progressBarHome);
            mPlayer =  itemView.findViewById(R.id.player);
            //videoView_show = itemView.findViewById(R.id.videoView_show);
            tv_hastags = itemView.findViewById(R.id.tv_hastags);
            tv_Address = itemView.findViewById(R.id.tv_Address);
            imgView_Chat = itemView.findViewById(R.id.imgView_Chat);
            tv_totalComments = itemView.findViewById(R.id.tv_totalComments);
            imgView_Comment = itemView.findViewById(R.id.imgView_Comment);
            tv_totalLikes = itemView.findViewById(R.id.tv_totalLikes);
            like_unlike = itemView.findViewById(R.id.like_unlike);
            imgView_likeRed = itemView.findViewById(R.id.imgView_likeRed);
            imgView_likeBlack = itemView.findViewById(R.id.imgView_likeBlack);
            imgView_profile = itemView.findViewById(R.id.imgView_profile);
            tv_userName = itemView.findViewById(R.id.tv_userName);
            circularImageView_Profile = itemView.findViewById(R.id.circularImageView_Profile);
            imgview_mute = itemView.findViewById(R.id.imgview_mute);
            imgview_unmute = itemView.findViewById(R.id.imgview_unmute);



        }
    }

    public void handlerForLikeUnlikeShow(HomeProfileAdapter.HomeProfile holder) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                holder.like_unlike.setVisibility(View.GONE);


            }
        }, LIKE_DISPLAY_LENGTH);
    }

    Callback<LikesMain> profileLikeCallback = new Callback<LikesMain>() {

        @Override
        public void onResponse(Call<LikesMain> call, Response<LikesMain> response) {

            if (response.isSuccessful()) {
                if (response.body().getStatus().equalsIgnoreCase("1")) {
                    holder.imgView_likeRed.setVisibility(View.VISIBLE);
                    holder.imgView_likeBlack.setVisibility(View.INVISIBLE);
                    HomeMainData homeMainData = new HomeMainData();
                    homeMainData.setId(followersListMainData.get(positionImage).getId());
                    homeMainData.setUserId(followersListMainData.get(positionImage).getUserId());
                    homeMainData.setFirstName(followersListMainData.get(positionImage).getFirstName());
                    homeMainData.setLastName(followersListMainData.get(positionImage).getLastName());
                    homeMainData.setPostDate(followersListMainData.get(positionImage).getPostDate());
                    homeMainData.setCountLike(followersListMainData.get(positionImage).getCountLike() + 1);
                    homeMainData.setThumb(followersListMainData.get(positionImage).getThumb());
                    homeMainData.setBig(followersListMainData.get(positionImage).getBig());
                    homeMainData.setCount_comment(followersListMainData.get(positionImage).getCount_comment());
                    homeMainData.setProfile_image(followersListMainData.get(positionImage).getProfile_image());
                    homeMainData.setLocation(followersListMainData.get(positionImage).getLocation());
                    homeMainData.setKeyword(followersListMainData.get(positionImage).getKeyword());
                    homeMainData.setType(followersListMainData.get(positionImage).getType());
                    homeMainData.setIsLike(1);


                    followersListMainData.set(positionImage, homeMainData);
                    holder.tv_totalLikes.setText(followersListMainData.get(positionImage).getCountLike() + " " + "Likes");

                    notifyDataSetChanged();
                    holder.like_unlike.setVisibility(View.GONE);

                } else {

                    holder.imgView_likeRed.setVisibility(View.GONE);
                    holder.imgView_likeBlack.setVisibility(View.VISIBLE);
                    HomeMainData homeMainData = new HomeMainData();
                    homeMainData.setId(followersListMainData.get(positionImage).getId());
                    homeMainData.setUserId(followersListMainData.get(positionImage).getUserId());
                    homeMainData.setFirstName(followersListMainData.get(positionImage).getFirstName());
                    homeMainData.setLastName(followersListMainData.get(positionImage).getLastName());
                    homeMainData.setPostDate(followersListMainData.get(positionImage).getPostDate());
                    homeMainData.setCountLike(followersListMainData.get(positionImage).getCountLike() - 1);
                    homeMainData.setThumb(followersListMainData.get(positionImage).getThumb());
                    homeMainData.setBig(followersListMainData.get(positionImage).getBig());
                    homeMainData.setCount_comment(followersListMainData.get(positionImage).getCount_comment());
                    homeMainData.setCount_comment(followersListMainData.get(positionImage).getCount_comment());
                    homeMainData.setProfile_image(followersListMainData.get(positionImage).getProfile_image());
                    homeMainData.setLocation(followersListMainData.get(positionImage).getLocation());
                    homeMainData.setKeyword(followersListMainData.get(positionImage).getKeyword());
                    homeMainData.setType(followersListMainData.get(positionImage).getType());
                    homeMainData.setIsLike(2);


                    followersListMainData.set(positionImage, homeMainData);
                    holder.tv_totalLikes.setText(followersListMainData.get(positionImage).getCountLike() + " " + "Likes");
                    notifyDataSetChanged();
                    holder.like_unlike.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onFailure(Call<LikesMain> call, Throwable t) {
            // holder.like_unlike.setVisibility(View.GONE);
        }
    };

    @Override
    public void onViewAttachedToWindow(@NonNull HomeProfile holder) {
        super.onViewAttachedToWindow(holder);
        if (holder.videoView!=null){
            //Log.e("BaseUrl=",followersListMainData.get(getItemId()).getBig());
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
