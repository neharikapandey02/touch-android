package com.example.touch.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.AnotherFreeUserProfile;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.VisitorProfileActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.FollowUnfollowMain;
import com.example.touch.models.FollowingMainData;
import com.example.touch.models.LikeCommentFollowMainData;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.material.snackbar.Snackbar;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LikeComFollowAdapter extends RecyclerView.Adapter<LikeComFollowAdapter.FollowingList> {
    Context context;
    List<LikeCommentFollowMainData> followModelList;
    ProgressBar progressBar;
    private SharedPreferencesData sharedPreferencesData;
    private String userId;
    private boolean status;
    private LayoutInflater inflater;


    public LikeComFollowAdapter(Context context,List<LikeCommentFollowMainData> followModelList,ProgressBar progressBar,boolean status) {
        this.context = context;
        this.followModelList=followModelList;
        this.progressBar=progressBar;
        sharedPreferencesData=new SharedPreferencesData(context);
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);
        this.status=status;

    }

    @NonNull
    @Override
    public LikeComFollowAdapter.FollowingList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_follow_like_comment, null);

        LikeComFollowAdapter.FollowingList followingList = new LikeComFollowAdapter.FollowingList(view);
        return followingList;
    }

    @Override
    public void onBindViewHolder(@NonNull LikeComFollowAdapter.FollowingList holder, int position) {
        if (followModelList.get(position).getProfileImage()!=null&&!followModelList.get(position).getProfileImage().equalsIgnoreCase("")) {
            Picasso.with(context)
                    .load(followModelList.get(position).getProfileImage())
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_image)
                    .into(holder.circularImageView_UserImage);
        }

        if (followModelList.get(position).getFlag().equalsIgnoreCase("like_profile")||followModelList.get(position).getFlag().equalsIgnoreCase("comment")
                ||followModelList.get(position).getFlag().equalsIgnoreCase("like")) {
            holder.postImg.setVisibility(View.VISIBLE);
            if (followModelList.get(position).getType().equalsIgnoreCase("image")) {
                Picasso.with(context).load(followModelList.get(position).getBig()).placeholder(R.drawable.profile_placeholder)
                        .error(R.drawable.profile_image).into(holder.postImg);
            }else if (followModelList.get(position).getType().equalsIgnoreCase("video")){
                RequestOptions requestOptions = new RequestOptions();
                requestOptions.placeholder(R.drawable.loading_placeholder);
                requestOptions.error(R.drawable.loading_placeholder);
                Glide.with(context)
                        .load(followModelList.get(position).getBig())
                        .fitCenter()
                        .centerCrop()
                        .apply(requestOptions)
                        .thumbnail(Glide.with(context).load(followModelList.get(position).getBig()))
                        .into(holder.postImg);
            }

            holder.postImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
                    View mView = inflater.inflate(R.layout.image_preview_new, null);
                    PhotoView photoView = mView.findViewById(R.id.imageView);
                    VideoView videoView = mView.findViewById(R.id.videoView);
                    if (followModelList.get(position).getType().equalsIgnoreCase("image")){
                    Glide.with(context).load(followModelList.get(position).getBig()
                    ).placeholder(R.drawable.profile_placeholder).into(photoView);
                    photoView.setVisibility(View.VISIBLE);
                    }
                    else {
                        videoView.setVisibility(View.VISIBLE);
                        videoView.setVideoPath(followModelList.get(position).getBig());
                        videoView.start();
                    }
                    mBuilder.setView(mView);
                    AlertDialog mDialog = mBuilder.create();
                    mDialog.show();
                }
            });
        }
        else {
            holder.postImg.setVisibility(View.GONE);
        }


        holder.tv_name.setText(followModelList.get(position).getMsg());
        holder.relative_mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (followModelList.get(position).getUser_type().equalsIgnoreCase("2")){
                    Intent intent=new Intent(context, AnotherUserProfile.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID,followModelList.get(position).getUserId());
                    context.startActivity(intent);
                }else{
                    Intent intent=new Intent(context, VisitorProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID,followModelList.get(position).getUserId());
                    context.startActivity(intent);
                }
            }
        });
    }







    @Override
    public int getItemCount() {
        return followModelList.size();
    }

    public class FollowingList extends RecyclerView.ViewHolder {
        Button btn_followUnfollow;
        CircularImageView circularImageView_UserImage;
        TextView tv_name;
        RelativeLayout relative_mainLayout;
        ImageView postImg;

        public FollowingList(@NonNull View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
            circularImageView_UserImage = itemView.findViewById(R.id.circularImageView_UserImage);
            btn_followUnfollow = itemView.findViewById(R.id.btn_followUnfollow);
            relative_mainLayout = itemView.findViewById(R.id.relative_mainLayout);
            postImg = itemView.findViewById(R.id.postImg);



        }
    }

}
