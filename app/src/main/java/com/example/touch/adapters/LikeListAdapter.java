package com.example.touch.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.ChatActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.ChatListMainData;
import com.example.touch.models.UserLikeMain;
import com.example.touch.models.UserLikeMainData;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class LikeListAdapter extends RecyclerView.Adapter<LikeListAdapter.LikeList> {
    Context context;
    List<UserLikeMainData> userLikeMainData;
    private SharedPreferencesData sharedPreferencesData;
    private Dialog dialog;

    public LikeListAdapter(Context context,List<UserLikeMainData> userLikeMainData) {
        this.context = context;
        this.userLikeMainData=userLikeMainData;
        sharedPreferencesData = new SharedPreferencesData(context);

    }

    @NonNull
    @Override
    public LikeListAdapter.LikeList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_likelist_adapter, parent, false);
        LikeListAdapter.LikeList addImageList = new LikeListAdapter.LikeList(view);

        return addImageList;
    }

    @Override
    public void onBindViewHolder(@NonNull LikeListAdapter.LikeList holder, int position) {
        Picasso.with(context)
                .load(userLikeMainData.get(position).getProfileImage())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.circularImageView_chatUser);
        holder.tv_receiverName.setText(userLikeMainData.get(position).getFirstName());



    }

    @Override
    public int getItemCount() {
        return userLikeMainData.size();
    }

    public class LikeList extends RecyclerView.ViewHolder {
        ImageView imageView_image;
        CircularImageView circularImageView_chatUser;
        TextView tv_receiverName;
        RelativeLayout layout_top;

        public LikeList(@NonNull View itemView) {
            super(itemView);
            circularImageView_chatUser = itemView.findViewById(R.id.circularImageView_chatUser);
            tv_receiverName = itemView.findViewById(R.id.tv_receiverName);
            layout_top = itemView.findViewById(R.id.layout_top);
        }
    }


}