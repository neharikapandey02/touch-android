package com.example.touch.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.constants.Constants;
import com.example.touch.models.SearchMainData;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchAdapterBySuggestion extends RecyclerView.Adapter<SearchAdapterBySuggestion.SearchAdapter> {

    Context context;
    List<SearchMainData> searchMainDataList;

    public SearchAdapterBySuggestion(Context context, List<SearchMainData> searchMainDataList){
        this.searchMainDataList=searchMainDataList;
        this.context=context;
    }
    @NonNull
    @Override
    public SearchAdapterBySuggestion.SearchAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_search_by_suggestion,parent,false);
        SearchAdapterBySuggestion.SearchAdapter searchAdapter=new SearchAdapterBySuggestion.SearchAdapter(view);

        return searchAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapterBySuggestion.SearchAdapter holder, int position) {
        holder.tv_userName.setText(searchMainDataList.get(position).getFirstName());
        holder.tv_locationName.setText(searchMainDataList.get(position).getDistance());
       // holder.tv_locationName.setText(searchMainDataList.get(position).getLastName());
        Picasso.with(context)
                .load(searchMainDataList.get(position).getProfileImage())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.circularImageView_UserImage);

        /* holder.tv_name.setText(searchMainDataList.get(position).getFirstName()+" "+searchMainDataList.get(position).getLastName());
        holder.tv_email.setText(searchMainDataList.get(position).getEmail());*/
        holder.layout_relativeTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, AnotherUserProfile.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.ANOTHER_USER_ID,searchMainDataList.get(position).getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchMainDataList.size();
    }

    public class SearchAdapter extends RecyclerView.ViewHolder{
        CircularImageView circularImageView_UserImage;
        TextView tv_name;
        TextView tv_email;
        TextView tv_userName;
        TextView tv_locationName;
        RelativeLayout layout_relativeTop;
        LinearLayout top_circle;
        public SearchAdapter(@NonNull View itemView) {
            super(itemView);
            circularImageView_UserImage=itemView.findViewById(R.id.circularImageView_ByNameUser);
            tv_name=itemView.findViewById(R.id.tv_name);
            tv_email=itemView.findViewById(R.id.tv_email);
            layout_relativeTop=itemView.findViewById(R.id.layout_relativeTop);
            top_circle=itemView.findViewById(R.id.top_circle);
            tv_locationName=itemView.findViewById(R.id.tv_locationName);
            tv_userName=itemView.findViewById(R.id.tv_userName);
        }
    }
}
