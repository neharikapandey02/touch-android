package com.example.touch.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.constants.Constants;
import com.example.touch.models.SearchDetailsMainData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SearchImageDetailsAdapter extends RecyclerView.Adapter<SearchImageDetailsAdapter.SearchImage> {
    Context context;
    List<SearchDetailsMainData> followersListMainData;
    String userId;
    public SearchImageDetailsAdapter(Context context, List<SearchDetailsMainData> followersListMainData,String userId){
        this.context=context;
        this.followersListMainData=followersListMainData;
        this.userId=userId;
    }
    @NonNull
    @Override
    public SearchImageDetailsAdapter.SearchImage onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_search_details_adapter,parent,false);
        SearchImageDetailsAdapter.SearchImage searchImage=new SearchImageDetailsAdapter.SearchImage(view);
        return searchImage;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchImageDetailsAdapter.SearchImage holder, int position) {
        Picasso.with(context)
                .load(followersListMainData.get(position).getImage())
                .placeholder(R.drawable.loading_placeholder)
                .error(R.drawable.loading_placeholder)
                .into(holder.imgView_Details);
        holder.linearLayout_fullImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, AnotherUserProfile.class);
                intent.putExtra(Constants.ANOTHER_USER_ID,userId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return followersListMainData.size();
    }
    public class SearchImage extends RecyclerView.ViewHolder{
        ImageView imgView_Details;
        LinearLayout linearLayout_fullImage;
        public SearchImage(@NonNull View itemView) {
            super(itemView);
            imgView_Details=itemView.findViewById(R.id.imgView_Details);
            linearLayout_fullImage=itemView.findViewById(R.id.linearLayout_fullImage);
        }
    }
}
