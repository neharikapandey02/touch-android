package com.example.touch.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.constants.Constants;
import com.example.touch.models.SearchMainData;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchImageListResultAdapter extends RecyclerView.Adapter<SearchImageListResultAdapter.Search> {
    Context context;
    List<SearchMainData> searchMainDataList;

    public SearchImageListResultAdapter(Context context, List<SearchMainData> searchMainDataList) {
        this.searchMainDataList = searchMainDataList;
        this.context = context;
    }


    @NonNull
    @Override
    public SearchImageListResultAdapter.Search onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_search_image_adapter, parent, false);
        SearchImageListResultAdapter.Search search = new SearchImageListResultAdapter.Search(view);

        return search;
    }

    @Override
    public void onBindViewHolder(@NonNull Search holder, int position) {
        if (searchMainDataList.get(position).getProfileImage()!=null){
            Picasso.with(context)
                    .load(searchMainDataList.get(position).getProfileImage())
                    .placeholder(R.drawable.loading_placeholder)
                    .fit()
                    .centerCrop()
                    .error(R.drawable.loading_placeholder)
                    .into(holder.imageView_image);
        }


        holder.layout_relativeTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AnotherUserProfile.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.ANOTHER_USER_ID, searchMainDataList.get(position).getId());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchMainDataList.size();
    }

    public class Search extends RecyclerView.ViewHolder {

        RelativeLayout layout_relativeTop;
        ImageView imageView_image;

        public Search(@NonNull View itemView) {
            super(itemView);

            layout_relativeTop = itemView.findViewById(R.id.layout_relativeTop);
            imageView_image = itemView.findViewById(R.id.imageView_image);
        }
    }
}
