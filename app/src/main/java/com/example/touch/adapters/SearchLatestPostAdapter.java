package com.example.touch.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.touch.R;
import com.example.touch.activities.MyPostLists;
import com.example.touch.activities.SearchPostActivity;
import com.example.touch.constants.Constants;
import com.example.touch.models.AnotherUserProfileResult;
import com.example.touch.models.HomeMain;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.LatestPostData;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SearchLatestPostAdapter extends RecyclerView.Adapter<SearchLatestPostAdapter.addImageList> {
    Context context;
    List<HomeMainData> multipleImageGetMainData;
    private Dialog dialog;
    public SearchLatestPostAdapter(Context context, List<HomeMainData> multipleImageGetMainData){
        this.context=context;
        this.multipleImageGetMainData=multipleImageGetMainData;

    }

    @NonNull
    @Override
    public SearchLatestPostAdapter.addImageList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_multiple,parent,false);
        SearchLatestPostAdapter.addImageList addImageList=new SearchLatestPostAdapter.addImageList(view);

        return addImageList;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchLatestPostAdapter.addImageList holder, int position) {
        Picasso.with(context)
                .load(multipleImageGetMainData.get(position).getBig())
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .centerCrop()
                .error(R.drawable.loading_placeholder)
                .into(holder.imageView_image);

        holder.imageView_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SearchPostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("arraylist", (Serializable) multipleImageGetMainData);
                intent.putExtra("position", position);
                intent.putExtra("bottom_nav", "search");
                intent.putExtra("bundle", bundle);
                context.startActivity(intent);

            }
        });
        holder.imageView_thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SearchPostActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("arraylist", (Serializable) multipleImageGetMainData);
                intent.putExtra("position", position);
                intent.putExtra("page", "search");
                intent.putExtra("bottom_nav", "search");
                intent.putExtra("bundle", bundle);
                context.startActivity(intent);
            }
        });


        if (multipleImageGetMainData.get(position).getType().equalsIgnoreCase("image")){
            holder.imageView_image.setVisibility(View.VISIBLE);
            holder.imageView_thumbnail.setVisibility(View.GONE);
            holder.imgview_Video.setVisibility(View.GONE);
        }else{
            holder.imgview_Video.setVisibility(View.VISIBLE);
            holder.imageView_thumbnail.setVisibility(View.VISIBLE);
            holder.imageView_image.setVisibility(View.VISIBLE);

            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.loading_placeholder);
            requestOptions.error(R.drawable.loading_placeholder);
            Glide.with(context)
                    .load(multipleImageGetMainData.get(position).getBig())
                    .fitCenter()
                    .centerCrop()
                    .apply(requestOptions)
                    .thumbnail(Glide.with(context).load(multipleImageGetMainData.get(position).getBig()))
                    .into(holder.imageView_thumbnail);
        }
    }



    @Override
    public int getItemCount() {
        return multipleImageGetMainData.size();
    }
    public class addImageList extends RecyclerView.ViewHolder{
        ImageView imageView_image;
        ImageView imageView_thumbnail;
        ImageView imgview_Video;
        public addImageList(@NonNull View itemView) {
            super(itemView);
            imageView_image=itemView.findViewById(R.id.imageView_image_multiple);
            imgview_Video=itemView.findViewById(R.id.imgview_Video);
            imageView_thumbnail=itemView.findViewById(R.id.imageView_thumbnail);
        }
    }

}
