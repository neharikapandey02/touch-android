package com.example.touch.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.MyPostLists;
import com.example.touch.activities.SearchImageDetailsActivity;
import com.example.touch.constants.Constants;
import com.example.touch.models.HomeMainData;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SearchListImageAdapter extends RecyclerView.Adapter<SearchListImageAdapter.searchListImage> {
    Context context;
    List<HomeMainData> followersListMainData;

    public SearchListImageAdapter(Context context, List<HomeMainData> followersListMainData) {
        this.context = context;
        this.followersListMainData = followersListMainData;
    }

    @NonNull
    @Override
    public SearchListImageAdapter.searchListImage onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_search_image_adapter, parent, false);
        SearchListImageAdapter.searchListImage searchListImage = new SearchListImageAdapter.searchListImage(view);

        return searchListImage;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchListImageAdapter.searchListImage holder, int position) {
        holder.imageView_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MyPostLists.class);
                intent.putExtra(Constants.ANOTHER_USER_ID,followersListMainData.get(position).getUserId());
                intent.putExtra(Constants.TO_POSITION,followersListMainData.get(position).getId());
                context.startActivity(intent);
            }
        });
        Picasso.with(context)
                .load(followersListMainData.get(position).getThumb())
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .centerCrop()
                //.resize(250,350)
                .error(R.drawable.loading_placeholder)
                .into(holder.imageView_image);
    }

    @Override
    public int getItemCount() {
        return followersListMainData.size();
    }

    public class searchListImage extends RecyclerView.ViewHolder {
        ImageView imageView_image;

        public searchListImage(@NonNull View itemView) {
            super(itemView);
            imageView_image = itemView.findViewById(R.id.imageView_image);
        }
    }
}
