package com.example.touch.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.SearchActivity;
import com.example.touch.constants.Constants;
import com.example.touch.models.CategoryMainData;

import java.util.List;

public class SearchTabAdapter extends RecyclerView.Adapter<SearchTabAdapter.SearchAdapter> {
    Context context;
    List<CategoryMainData> categoryMainData;
    public SearchTabAdapter(Context context, List<CategoryMainData> categoryMainData){
        this.context=context;
        this.categoryMainData=categoryMainData;
    }

    @NonNull
    @Override
    public SearchTabAdapter.SearchAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_search_adapter,parent,false);
        SearchTabAdapter.SearchAdapter searchTabAdapter=new SearchTabAdapter.SearchAdapter(view);
        return searchTabAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchTabAdapter.SearchAdapter holder, int position) {
        holder.tv_name.setText(categoryMainData.get(position).getCategory());
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, SearchActivity.class);
                if (position==0){
                    intent.putExtra(Constants.SEARCH_TEXT,"male");
                }else if (position==1){
                    intent.putExtra(Constants.SEARCH_TEXT,"female");
                }else if (position==2){
                    intent.putExtra(Constants.SEARCH_TEXT,"transgender");
                }else{
                    intent.putExtra(Constants.SEARCH_TEXT,categoryMainData.get(position).getCategory());
                }

                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return categoryMainData.size();
    }

    public class SearchAdapter extends RecyclerView.ViewHolder{
        TextView tv_name;

        public SearchAdapter(@NonNull View itemView) {
            super(itemView);
            tv_name=itemView.findViewById(R.id.tv_name);
        }
    }
}
