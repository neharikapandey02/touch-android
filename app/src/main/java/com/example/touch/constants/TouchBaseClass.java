package com.example.touch.constants;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.provider.FontRequest;
import androidx.emoji.text.EmojiCompat;
import androidx.emoji.text.FontRequestEmojiCompatConfig;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;

public class TouchBaseClass extends Application {

    Context touch;

    @Override
    public void onCreate() {
        super.onCreate();
        touch = this.getApplicationContext();
        RetrofitHelper.getInstance().init(touch);
        int CERTIFICATES= R.array.com_google_android_gms_fonts_certs;
        FontRequest fontRequest = new FontRequest(
                "com.google.android.gms.fonts",
                "com.google.android.gms",
                "Noto Color Emoji Compat",
                CERTIFICATES);
        EmojiCompat.Config config = new FontRequestEmojiCompatConfig(this, fontRequest);
        config.setReplaceAll(true)
                .registerInitCallback(new EmojiCompat.InitCallback() {
                    @Override
                    public void onInitialized() {
                        Log.i("", "EmojiCompat initialized");
                    }

                    @Override
                    public void onFailed(@Nullable Throwable throwable) {
                        Log.e("", "EmojiCompat initialization failed", throwable);
                    }
                });

        EmojiCompat.init(config);
//        EmojiCompat.init(new BundledEmojiCompatConfig(this));
    }
}