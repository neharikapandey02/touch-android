package com.example.touch.constants.textview_animate;

public class CharacterDiffResult {
    public char c;
    public int  fromIndex;
    public int  moveIndex;
}
