package com.example.touch.constants.textview_animate;

import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.animation.Animation;

public interface IHText {
    void init(HTextView hTextView, AttributeSet attrs, int defStyle);

    void animateText(CharSequence text);

    void onDraw(Canvas canvas);

    void setAnimationListener(Animation.AnimationListener listener);
}
