package com.example.touch.customGallery;

import android.net.Uri;

import java.util.ArrayList;

interface BaseProperty {
    FishBunCreator setSelectedImages(ArrayList<Uri> arrayPaths);

    FishBunCreator setPickerCount(int count);

    FishBunCreator setMaxCount(int count);

    FishBunCreator setMinCount(int count);

    FishBunCreator setRequestCode(int RequestCode);

    FishBunCreator setReachLimitAutomaticClose(boolean isAutomaticClose);

    FishBunCreator exceptGif(boolean isExcept);

    void startAlbum();
}
