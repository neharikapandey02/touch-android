package com.example.touch.customGallery;

import android.net.Uri;
import android.widget.ImageView;

public interface ImageAdapter {
    void loadImage(ImageView target, Uri loadUrl);
    void loadDetailImage(ImageView target, Uri loadUrl);
}
