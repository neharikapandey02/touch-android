package com.example.touch.customGallery;

import android.net.Uri;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

class ImageController {
    ImageView imgMain;

    ImageController(ImageView imgMain) {
        this.imgMain = imgMain;
    }

    void setImgMain(Uri path) {
        Picasso.with(imgMain.getContext())
                .load(path)
                //.fit()
               // .centerCrop()
                .into(imgMain);
    }
}
