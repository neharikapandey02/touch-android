package com.example.touch.customGallery;

import android.net.Uri;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PicassoAdapter implements ImageAdapter {
    @Override
    public void loadImage(ImageView target, Uri loadUrl) {
        Picasso
                .with(target.getContext())
                .load(loadUrl)
                .fit()
                .centerCrop()
                .into(target);
    }

    @Override
    public void loadDetailImage(ImageView target, Uri loadUrl) {
        Picasso .with(target.getContext())
                .load(loadUrl)
                .fit()
                .centerInside()
                .into(target);
    }
}