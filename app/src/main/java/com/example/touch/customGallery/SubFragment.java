package com.example.touch.customGallery;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.UploadImageActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.MultipleImageMain;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubFragment extends Fragment {

    ArrayList<Uri> path = new ArrayList<>();
    ArrayList<String> storagePath=new ArrayList<>();
    ImageView imgMain;
    Button btnAddImages;
    RecyclerView recyclerView;
    LinearLayoutManager linearLayoutManager;
    ImageAdapter1 imageAdapter;
    ImageController withActivityController;
    private String userId;
    private ProgressBar progressBar;

    private SharedPreferencesData sharedPreferencesData;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sub, container, false);
        // Inflate the layout for this fragment
        progressBar = rootView.findViewById(R.id.progressBar);
        imgMain = (ImageView) rootView.findViewById(R.id.img_main);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerview);
        btnAddImages = (Button) rootView.findViewById(R.id.btn_add_images);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        withActivityController = new ImageController(imgMain);
        imageAdapter = new ImageAdapter1(getActivity(), withActivityController, path);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(imageAdapter);
        FishBun.with(SubFragment.this)
                .setImageAdapter(new PicassoAdapter())
                .setMaxCount(5)
                .setActionBarColor(getResources().getColor(R.color.colorPrimaryDark), getResources().getColor(R.color.colorPrimaryDark))
                .setSelectedImages(path)
                .textOnImagesSelectionLimitReached("You can't select  more than 5 pics")
                .setCamera(true)
                .exceptGif(false)

                //.setSelectCircleStrokeColor(getResources().getColor(R.color.main_red))
                .startAlbum();

        btnAddImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferencesData = new SharedPreferencesData(getContext());
                userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                storagePath.clear();
                Log.e("totalSize=",path.size()+"");
                if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                    /*for (int i = 0; i < path.size(); i++) {
                        storagePath.add(getPath(getContext(), path.get(i)));
                    }
                    if (storagePath != null && storagePath.size() > 0) {
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().setMultipleImages(multipleImageMainCallback, storagePath, userId);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.please_select_images), Toast.LENGTH_LONG).show();
                    }*/
                    if (storagePath!=null){

                        for (int i = 0; i < path.size(); i++) {
                            storagePath.add(getPath(getContext(), path.get(i)));
                        }


                        if (storagePath.size()>0){
                            Intent intent=new Intent(getActivity(), UploadImageActivity.class);
                            intent.putStringArrayListExtra(Constants.IMAGEUPLOAD,storagePath);
                            startActivity(intent);
                        }else {
                            Snackbar.make(btnAddImages,getResources().getString(R.string.please_select_images),Snackbar.LENGTH_LONG).show();
                        }

                    }



                /*FishBun.with(SubFragment.this)
                        .setImageAdapter(new PicassoAdapter())
                        .setPickerCount(5)
                        .setActionBarColor(Color.parseColor("#3F51B5"), Color.parseColor("#303F9F"))
                        .setSelectedImages(path)
                        .setCamera(true)
                        .startAlbum();*/
                }else{
                    Snackbar.make(btnAddImages,getResources().getString(R.string.please_login_to_upload_image),Snackbar.LENGTH_LONG).show();
                }
            }
        });

        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Define.ALBUM_REQUEST_CODE:
                if (resultCode == getActivity().RESULT_OK) {
                    path = data.getParcelableArrayListExtra(Define.INTENT_PATH);
                    imageAdapter.changePath(path);
                    break;
                }
        }
    }
    Callback<MultipleImageMain> multipleImageMainCallback = new Callback<MultipleImageMain>() {
        @Override
        public void onResponse(Call<MultipleImageMain> call, Response<MultipleImageMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                Snackbar.make(btnAddImages,response.body().getMessage(),Snackbar.LENGTH_LONG).show();

            }

        }

        @Override
        public void onFailure(Call<MultipleImageMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

// DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
// ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }

// TODO handle non-primary volumes
                }
// DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
// MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[] {
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
// MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                return getDataColumn(context, uri, null, null);
            }
// File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
// File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
}
