package com.example.touch.customGallery;

import android.os.Bundle;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.touch.R;

public class WithFragmentActivity extends AppCompatActivity {

    RelativeLayout areaContainer;
    SubFragment subFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.fragment_notifications);

        areaContainer = (RelativeLayout)findViewById(R.id.area_container1);
        subFragment = new SubFragment();

        getSupportFragmentManager().beginTransaction().add(areaContainer.getId(), subFragment).commit();

    }
}
