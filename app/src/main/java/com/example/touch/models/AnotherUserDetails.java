package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnotherUserDetails {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("user_type")
    @Expose
    public String userType;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("social_media_id")
    @Expose
    public Object socialMediaId;
    @SerializedName("username")
    @Expose
    public Object username;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("profile_image")
    @Expose
    public String profileImage;
    @SerializedName("about_us")
    @Expose
    public String aboutUs;
    @SerializedName("password")
    @Expose
    public String password;
    @SerializedName("country_id")
    @Expose
    public Object countryId;
    @SerializedName("language")
    @Expose
    public String language;
    @SerializedName("phone_number")
    @Expose
    public String phoneNumber;
    @SerializedName("dob")
    @Expose
    public String dob;
    @SerializedName("gender")
    @Expose
    public String gender;
    @SerializedName("hair_color")
    @Expose
    public String hairColor;
    @SerializedName("height")
    @Expose
    public String height;
    @SerializedName("weight")
    @Expose
    public String weight;
    @SerializedName("available_from")
    @Expose
    public String availableFrom;
    @SerializedName("ethnicity")
    @Expose
    public String ethnicity;
    @SerializedName("origin")
    @Expose
    public String origin;
    @SerializedName("eye_color")
    @Expose
    public String eyeColor;
    @SerializedName("hobby")
    @Expose
    public String hobby;
    @SerializedName("brestsize")
    @Expose
    public String brestsize;
    @SerializedName("website")
    @Expose
    public String website;
    @SerializedName("service_type")
    @Expose
    public String serviceType;
    @SerializedName("user_lat")
    @Expose
    public String userLat;
    @SerializedName("user_lang")
    @Expose
    public String userLang;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("device_id")
    @Expose
    public String deviceId;
    @SerializedName("device_type")
    @Expose
    public String deviceType;
    @SerializedName("verify_status")
    @Expose
    public String verifyStatus;
    @SerializedName("added_at")
    @Expose
    public String addedAt;
    @SerializedName("audio")
    @Expose
    public String audio;
    @SerializedName("profile_like_status")
    @Expose
    public Integer profileLikeStatus;

    @SerializedName("display_no")
    @Expose
    public String display_no;

    @SerializedName("display_price")
    @Expose
    public String display_price;

    public String getDisplay_no() {
        return display_no;
    }

    public void setDisplay_no(String display_no) {
        this.display_no = display_no;
    }

    public String getDisplay_price() {
        return display_price;
    }

    public void setDisplay_price(String display_price) {
        this.display_price = display_price;
    }


    public Integer getProfileLikeStatus() {
        return profileLikeStatus;
    }

    public void setProfileLikeStatus(Integer profileLikeStatus) {
        this.profileLikeStatus = profileLikeStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Object getSocialMediaId() {
        return socialMediaId;
    }

    public void setSocialMediaId(Object socialMediaId) {
        this.socialMediaId = socialMediaId;
    }

    public Object getUsername() {
        return username;
    }

    public void setUsername(Object username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Object getCountryId() {
        return countryId;
    }

    public void setCountryId(Object countryId) {
        this.countryId = countryId;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getAvailableFrom() {
        return availableFrom;
    }

    public void setAvailableFrom(String availableFrom) {
        this.availableFrom = availableFrom;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public String getBrestsize() {
        return brestsize;
    }

    public void setBrestsize(String brestsize) {
        this.brestsize = brestsize;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getUserLat() {
        return userLat;
    }

    public void setUserLat(String userLat) {
        this.userLat = userLat;
    }

    public String getUserLang() {
        return userLang;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public String getAddedAt() {
        return addedAt;
    }

    public void setAddedAt(String addedAt) {
        this.addedAt = addedAt;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }


}
