package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AnotherUserMain {
    public List<TagMainData> getTags() {
        return tags;
    }

    public void setTags(List<TagMainData> tags) {
        this.tags = tags;
    }

    @SerializedName("tags")
    @Expose
    private List<TagMainData> tags = null;


    @SerializedName("user_detail")
    @Expose
    private AnotherUserDetailsData userDetail;
    @SerializedName("result")
    @Expose
    private List<MultipleImageGetMainData> result = null;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    public AnotherUserDetailsData getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(AnotherUserDetailsData userDetail) {
        this.userDetail = userDetail;
    }

    public List<MultipleImageGetMainData> getResult() {
        return result;
    }

    public void setResult(List<MultipleImageGetMainData> result) {
        this.result = result;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
