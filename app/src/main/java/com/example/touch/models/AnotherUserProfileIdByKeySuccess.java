package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnotherUserProfileIdByKeySuccess {

    @SerializedName("user_data")
    @Expose
    private String user_data;

    @SerializedName("success")
    @Expose
    private String success;

    public String getUser_data() {
        return user_data;
    }

    public void setUser_data(String user_data) {
        this.user_data = user_data;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
