package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AnotherUserProfileResult implements Serializable {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("user_id")
    @Expose
    public String userId;
    @SerializedName("tag_id")
    @Expose
    public String tagId;
    @SerializedName("keyword")
    @Expose
    public String keyword;
    @SerializedName("location")
    @Expose
    public String location;
    @SerializedName("state")
    @Expose
    public String state;
    @SerializedName("country")
    @Expose
    public String country;
    @SerializedName("main_country")
    @Expose
    public String mainCountry;
    @SerializedName("latitude")
    @Expose
    public String latitude;
    @SerializedName("longitude")
    @Expose
    public String longitude;
    @SerializedName("hashtag_one")
    @Expose
    public String hashtagOne;
    @SerializedName("hashtag_two")
    @Expose
    public String hashtagTwo;
    @SerializedName("hashtag_three")
    @Expose
    public String hashtagThree;
    @SerializedName("hashtag_four")
    @Expose
    public String hashtagFour;
    @SerializedName("hashtag_five")
    @Expose
    public String hashtagFive;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("profile_image")
    @Expose
    public String profileImage;
    @SerializedName("post_date")
    @Expose
    public String postDate;
    @SerializedName("count_like")
    @Expose
    public Integer countLike;
    @SerializedName("is_like")
    @Expose
    public Integer isLike;
    @SerializedName("count_comment")
    @Expose
    public Integer countComment;
    @SerializedName("post_user_id")
    @Expose
    public String postUserId;
    @SerializedName("post_time")
    @Expose
    public String postTime;
    @SerializedName("big_image")
    @Expose
    public String bigImage;
    @SerializedName("type")
    @Expose
    public String type;

    @SerializedName("audio")
    @Expose
    public String audio;

@SerializedName("country_code")
    @Expose
    public String country_code;

@SerializedName("place_id")
    @Expose
    public String place_id;

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTagId() {
        return tagId;
    }

    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMainCountry() {
        return mainCountry;
    }

    public void setMainCountry(String mainCountry) {
        this.mainCountry = mainCountry;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getHashtagOne() {
        return hashtagOne;
    }

    public void setHashtagOne(String hashtagOne) {
        this.hashtagOne = hashtagOne;
    }

    public String getHashtagTwo() {
        return hashtagTwo;
    }

    public void setHashtagTwo(String hashtagTwo) {
        this.hashtagTwo = hashtagTwo;
    }

    public String getHashtagThree() {
        return hashtagThree;
    }

    public void setHashtagThree(String hashtagThree) {
        this.hashtagThree = hashtagThree;
    }

    public String getHashtagFour() {
        return hashtagFour;
    }

    public void setHashtagFour(String hashtagFour) {
        this.hashtagFour = hashtagFour;
    }

    public String getHashtagFive() {
        return hashtagFive;
    }

    public void setHashtagFive(String hashtagFive) {
        this.hashtagFive = hashtagFive;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public Integer getCountLike() {
        return countLike;
    }

    public void setCountLike(Integer countLike) {
        this.countLike = countLike;
    }

    public Integer getIsLike() {
        return isLike;
    }

    public void setIsLike(Integer isLike) {
        this.isLike = isLike;
    }

    public Integer getCountComment() {
        return countComment;
    }

    public void setCountComment(Integer countComment) {
        this.countComment = countComment;
    }

    public String getPostUserId() {
        return postUserId;
    }

    public void setPostUserId(String postUserId) {
        this.postUserId = postUserId;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getBigImage() {
        return bigImage;
    }

    public void setBigImage(String bigImage) {
        this.bigImage = bigImage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
