package com.example.touch.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.touch.R;

import java.util.ArrayList;
import java.util.List;

public class CategoryAdapter extends ArrayAdapter<CategoryMainData> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<CategoryMainData> items;
    private final int mResource;

    public CategoryAdapter(@NonNull Context context, @LayoutRes int resource,
                              @NonNull List<CategoryMainData> items) {
        super(context, resource, 0, items);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        this.items = items;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);
        TextView tv_category=view.findViewById(R.id.tv_category_name);
        tv_category.setText(items.get(position).getCategory());


        return view;
    }
}