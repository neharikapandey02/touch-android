package com.example.touch.models;

import android.content.Context;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconTextView;
import com.rygelouv.audiosensei.player.AudioSenseiListObserver;
import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;
import com.rygelouv.audiosensei.player.OnPlayerViewClickListener;
import com.vanniktech.emoji.EmojiEditText;
import com.vanniktech.emoji.EmojiTextView;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import hani.momanii.supernova_emoji_library.Helper.EmojiconMultiAutoCompleteTextView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.chat> {
    private Context context;
    private List<ChatMainData> chatMainDataList;
    public  void addChat(ChatMainData commentMainData){
        chatMainDataList.add(commentMainData);
        notifyDataSetChanged();
    }
    public  void removeLastIndexData(){
        chatMainDataList.remove(chatMainDataList.size()-1);
        notifyDataSetChanged();

    }

    public ChatAdapter(Context context,List<ChatMainData> chatMainDataList){
        this.chatMainDataList = chatMainDataList;
        this.context = context;
    }
    @NonNull
    @Override
    public ChatAdapter.chat onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_chat_adapter,parent,false);
        ChatAdapter.chat chat=new ChatAdapter.chat(view);

        return chat;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatAdapter.chat holder, int position) {
        if (chatMainDataList.get(position).getMe()==0){
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD&&android.os.Build.VERSION.SDK_INT <=android.os.Build.VERSION_CODES.P) {
              holder.layout_receiver.setVisibility(View.VISIBLE);
              holder.layout_sender.setVisibility(View.GONE);

              if (chatMainDataList.get(position).getType().equalsIgnoreCase("1")) {
                  holder.tv_receiver.setVisibility(View.VISIBLE);
                  holder.audio_player.setVisibility(View.GONE);
                  holder.durationLeftTv.setVisibility(View.GONE);
                  holder.tv_receiver.setTextSize(17);
                  holder.tv_receiver.setText(chatMainDataList.get(position).getMsg());
              }
              else if (chatMainDataList.get(position).getType().equalsIgnoreCase("2")){
                  holder.tv_receiver.setVisibility(View.GONE);
                  String dur = getDuration(chatMainDataList.get(position).getAudio());
                  holder.durationLeftTv.setText(dur+ " sec");
                  holder.durationLeftTv.setVisibility(View.VISIBLE);
                  holder.audio_player_left.setAudioTarget(chatMainDataList.get(position).getAudio());
                  holder.audio_player_left.setVisibility(View.VISIBLE);
              }
              holder.tv_receiverTime.setVisibility(View.VISIBLE);
              holder.tv_receiverTime.setText(localTimeZoneConverter(chatMainDataList.get(position).getDate()));
              holder.tv_senderTime.setVisibility(View.GONE);
          }else{
              holder.layout_receiver.setVisibility(View.VISIBLE);
              holder.layout_sender.setVisibility(View.GONE);
              //holder.tv_receiver.setEmojiconSize(80);
              //holder.tv_receiver.setLineSpacing(2,1.5f);
              //holder.tv_receiver.setEmojiSize(60);
              if (chatMainDataList.get(position).getType().equalsIgnoreCase("1")) {
                  holder.tv_receiver.setVisibility(View.VISIBLE);
                  holder.audio_player.setVisibility(View.GONE);
                  holder.durationLeftTv.setVisibility(View.GONE);
                  holder.tv_receiver.setTextSize(17);
                  holder.tv_receiver.setText(chatMainDataList.get(position).getMsg());
              }
              else if (chatMainDataList.get(position).getType().equalsIgnoreCase("2")){
                  holder.tv_receiver.setVisibility(View.GONE);

                  String dur = getDuration(chatMainDataList.get(position).getAudio());
                  holder.durationLeftTv.setText(dur+ " sec");
                  holder.durationLeftTv.setVisibility(View.VISIBLE);
                  holder.audio_player_left.setVisibility(View.VISIBLE);
                  holder.audio_player_left.setAudioTarget(chatMainDataList.get(position).getAudio());
                  holder.audio_player_left.setVisibility(View.VISIBLE);

              }
              holder.tv_receiverTime.setVisibility(View.VISIBLE);
              holder.tv_receiverTime.setText(localTimeZoneConverter(chatMainDataList.get(position).getDate()));
              holder.tv_senderTime.setVisibility(View.GONE);
          }

      }else{
          if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD&&android.os.Build.VERSION.SDK_INT <=android.os.Build.VERSION_CODES.P) {
              holder.layout_receiver.setVisibility(View.GONE);
              holder.layout_sender.setVisibility(View.VISIBLE);

              if (chatMainDataList.get(position).getType().equalsIgnoreCase("1")) {
                  holder.tv_sender.setVisibility(View.VISIBLE);
                  holder.audio_player.setVisibility(View.GONE);
                  holder.durationRightTv.setVisibility(View.GONE);
                  holder.tv_sender.setTextSize(17);
                  holder.tv_sender.setText(chatMainDataList.get(position).getMsg());
              }
              else if (chatMainDataList.get(position).getType().equalsIgnoreCase("2")){
                  holder.tv_sender.setVisibility(View.GONE);
                  holder.audio_player.setVisibility(View.VISIBLE);

                      String dur = getDuration(chatMainDataList.get(position).getAudio());
                      holder.durationRightTv.setText(dur+ " sec");
                      holder.durationRightTv.setVisibility(View.VISIBLE);
                  holder.audio_player.setAudioTarget(chatMainDataList.get(position).getAudio());


              }
              holder.tv_senderTime.setVisibility(View.VISIBLE);
              holder.tv_senderTime.setText(localTimeZoneConverter(chatMainDataList.get(position).getDate()));
              holder.tv_receiverTime.setVisibility(View.GONE);
          }else{
              holder.layout_receiver.setVisibility(View.GONE);
              holder.layout_sender.setVisibility(View.VISIBLE);

              if (chatMainDataList.get(position).getType().equalsIgnoreCase("1")) {
                  holder.tv_sender.setVisibility(View.VISIBLE);
                  holder.audio_player.setVisibility(View.GONE);
                  holder.durationRightTv.setVisibility(View.GONE);
                  holder.tv_sender.setTextSize(17);
                  holder.tv_sender.setText(chatMainDataList.get(position).getMsg());
              }
              else if (chatMainDataList.get(position).getType().equalsIgnoreCase("2")){
                  holder.tv_sender.setVisibility(View.GONE);
                  holder.audio_player.setVisibility(View.VISIBLE);
                  String dur = getDuration(chatMainDataList.get(position).getAudio());
                  holder.durationRightTv.setText(dur+ " sec");
                  holder.durationRightTv.setVisibility(View.VISIBLE);
                  holder.audio_player.setAudioTarget(chatMainDataList.get(position).getAudio());
              }
              holder.tv_senderTime.setVisibility(View.VISIBLE);
              holder.tv_senderTime.setText(localTimeZoneConverter(chatMainDataList.get(position).getDate()));
              holder.tv_receiverTime.setVisibility(View.GONE);
          }
      }

//      holder.audio_player.getPlayerRootView().

    }

    @Override
    public int getItemCount() {
        return chatMainDataList.size();
    }
    public class chat extends RecyclerView.ViewHolder{
       RelativeLayout layout_receiver;
       RelativeLayout layout_sender;
        androidx.emoji.widget.EmojiTextView tv_receiver;
        androidx.emoji.widget.EmojiTextView tv_sender;
        TextView tv_receiverTime;
        TextView tv_senderTime, durationRightTv, durationLeftTv;
        AudioSenseiPlayerView audio_player;
        AudioSenseiPlayerView audio_player_left;
        public chat(@NonNull View itemView) {
            super(itemView);
            layout_receiver=itemView.findViewById(R.id.layout_receiver);
            layout_sender=itemView.findViewById(R.id.layout_sender);
            tv_receiver=itemView.findViewById(R.id.tv_receiver);
            tv_sender=itemView.findViewById(R.id.tv_sender);
            tv_receiverTime=itemView.findViewById(R.id.tv_receiverTime);
            tv_senderTime=itemView.findViewById(R.id.tv_senderTime);
            durationRightTv=itemView.findViewById(R.id.durationRightTv);
            durationLeftTv=itemView.findViewById(R.id.durationLeftTv);
            audio_player = itemView.findViewById(R.id.audio_player);
            audio_player_left = itemView.findViewById(R.id.audio_player_left);
//            AudioSenseiListObserver.getInstance().registerLifecycle(getLifecycle());

        }
    }

    private static String getDuration(String file) {
        try {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(file, new HashMap<String, String>());
            String durationStr = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            int seconds = (int) ((Long.parseLong(durationStr) % (1000 * 60 * 60)) % (1000 * 60) / 1000);
            return String.valueOf(seconds);
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    public String convertDate(String date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("HH:mm");
        String targetdatevalue= targetFormat.format(sourceDate);
        return targetdatevalue;
    }

    public String localTimeZoneConverter(String date1){
        String dateStr = date1;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        df.setTimeZone(TimeZone.getDefault());
        SimpleDateFormat targetFormat = new SimpleDateFormat("HH:mm");
        String targetdatevalue= targetFormat.format(date);
        return targetdatevalue;


        //String formattedDate = df.format(date);

       // return  formattedDate;


    }
}
