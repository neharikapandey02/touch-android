package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatFromUserMain {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("return_msg")
    @Expose
    private String returnMsg;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private ChatFromUserMainData result;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ChatFromUserMainData getResult() {
        return result;
    }

    public void setResult(ChatFromUserMainData result) {
        this.result = result;
    }
}
