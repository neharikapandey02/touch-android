package com.example.touch.models;

import android.content.Intent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatListMainData {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("reciver_id")
    @Expose
    private String reciverId;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("c_date")
    @Expose
    private String cDate;
    @SerializedName("my_id")
    @Expose
    private String myId;
    @SerializedName("another_id")
    @Expose
    private String anotherId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;

   @SerializedName("type")
    @Expose
    private String type;

   @SerializedName("user_type")
    @Expose
    private String user_type;

   @SerializedName("audio")
    @Expose
    private String audio;

    @SerializedName("unread_count")
    @Expose
    private String unread_count;

    @SerializedName("profile_on")
    @Expose
    private String profile_on;
    @SerializedName("block_status")
    @Expose
    private Integer block_status;

    @SerializedName("user_blocked_status")
    @Expose
    private Integer user_blocked_status;

    public Integer getBlock_status() {
        return block_status;
    }

    public void setBlock_status(Integer block_status) {
        this.block_status = block_status;
    }

    public Integer getUser_blocked_status() {
        return user_blocked_status;
    }

    public void setUser_blocked_status(Integer user_blocked_status) {
        this.user_blocked_status = user_blocked_status;
    }

    public String getProfile_on() {
        return profile_on;
    }

    public void setProfile_on(String profile_on) {
        this.profile_on = profile_on;
    }



    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getUnread_count() {
        return unread_count;
    }

    public void setUnread_count(String unread_count) {
        this.unread_count = unread_count;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReciverId() {
        return reciverId;
    }

    public void setReciverId(String reciverId) {
        this.reciverId = reciverId;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getImage() {
        return image;
    }

    public void setImage(Object image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCDate() {
        return cDate;
    }

    public void setCDate(String cDate) {
        this.cDate = cDate;
    }

    public String getMyId() {
        return myId;
    }

    public void setMyId(String myId) {
        this.myId = myId;
    }

    public String getAnotherId() {
        return anotherId;
    }

    public void setAnotherId(String anotherId) {
        this.anotherId = anotherId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }
}
