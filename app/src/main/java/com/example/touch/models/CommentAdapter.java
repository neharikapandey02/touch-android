package com.example.touch.models;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.emoji.widget.EmojiTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherFreeUserProfile;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.VisitorProfileActivity;
import com.example.touch.constants.Constants;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hani.momanii.supernova_emoji_library.Helper.EmojiconTextView;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.Comment> {

    Context context;
    List<CommentMainData> commentMainDataList;

    public  void addComments(CommentMainData commentMainData){
        commentMainDataList.add(commentMainData);
        notifyDataSetChanged();

    }
    public CommentAdapter(Context context){
        this.context=context;
    }

    public  CommentAdapter(Context context,List<CommentMainData> commentMainDataList){
        this.context=context;
        this.commentMainDataList=commentMainDataList;
    }
    @NonNull
    @Override
    public CommentAdapter.Comment onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_comment_adapter,parent,false);
        CommentAdapter.Comment comment=new CommentAdapter.Comment(view);
        return comment;
    }

    @Override
    public void onBindViewHolder(@NonNull CommentAdapter.Comment holder, int position) {
        Picasso.with(context)
                .load(commentMainDataList.get(position).getUserImage())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.circularImageView_UserImage);

        holder.tv_userName.setText(commentMainDataList.get(position).getFirstName());
        holder.tv_comments.setText(commentMainDataList.get(position).getComment());
        //holder.tv_comments.setEmojiconSize(49);

        holder.circularImageView_UserImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentMainDataList.get(position).getUser_type().equalsIgnoreCase("2")){
                    Intent intent=new Intent(context, AnotherUserProfile.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID,commentMainDataList.get(position).getUserId());
                    context.startActivity(intent);
                }else{
                    Intent intent=new Intent(context, VisitorProfileActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID,commentMainDataList.get(position).getUserId());
                    context.startActivity(intent);
                }
            }
        });

        holder.tv_comments.setTextSize(13);
        holder.tv_hours.setText(commentMainDataList.get(position).getCommented_before());
        holder.linearLayout_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (commentMainDataList.get(position).getUser_type().equalsIgnoreCase("2")){
                    Intent intent=new Intent(context, AnotherUserProfile.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID,commentMainDataList.get(position).getUserId());
                    context.startActivity(intent);
                }else{
                    Intent intent=new Intent(context, AnotherFreeUserProfile.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID,commentMainDataList.get(position).getUserId());
                    context.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return commentMainDataList.size();
    }

    public class Comment extends RecyclerView.ViewHolder{
        TextView tv_userName,tv_hours;
        EmojiTextView tv_comments;
        CircularImageView circularImageView_UserImage;
        LinearLayout linearLayout_comment;

        public Comment(@NonNull View itemView) {
            super(itemView);
            tv_hours=itemView.findViewById(R.id.tv_hours);
            tv_userName=itemView.findViewById(R.id.tv_userName);
            tv_comments=itemView.findViewById(R.id.tv_comments);
            circularImageView_UserImage=itemView.findViewById(R.id.circularImageView_UserImage);
            linearLayout_comment=itemView.findViewById(R.id.linearLayout_comment);
        }
    }
}
