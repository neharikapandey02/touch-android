package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentedMain {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("comment_id")
    @Expose
    private Integer commentId;
    @SerializedName("result")
    @Expose
    private List<CommentMainData> result = null;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public List<CommentMainData> getResult() {
        return result;
    }

    public void setResult(List<CommentMainData> result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
