package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CountListFollowIngPost {
    @SerializedName("following_count")
    @Expose
    private Integer followingCount;
    @SerializedName("followers_count")
    @Expose
    private Integer followersCount;
    @SerializedName("post_count")
    @Expose
    private Integer postCount;

    @SerializedName("like_count")
    @Expose
    private Integer likeCount;


    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }

    public Integer getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Integer followingCount) {
        this.followingCount = followingCount;
    }

    public Integer getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(Integer followersCount) {
        this.followersCount = followersCount;
    }

    public Integer getPostCount() {
        return postCount;
    }

    public void setPostCount(Integer postCount) {
        this.postCount = postCount;
    }
}
