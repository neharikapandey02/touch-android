package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisplayPhoneSuccess {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("display_no")
    @Expose
    private String display_no;

    @SerializedName("display_price")
    @Expose
    private String display_price;

    @SerializedName("success")
    @Expose
    private Integer success;

    public String getDisplay_price() {
        return display_price;
    }

    public void setDisplay_price(String display_price) {
        this.display_price = display_price;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDisplay_no() {
        return display_no;
    }

    public void setDisplay_no(String display_no) {
        this.display_no = display_no;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
}
