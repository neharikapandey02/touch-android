package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAnotherUserProfileData {
    @SerializedName("user_detail")
    @Expose
    public AnotherUserDetails userDetail;
    @SerializedName("result")
    @Expose
    public List<AnotherUserProfileResult> result = null;
    @SerializedName("success")
    @Expose
    public Integer success;

    @SerializedName("block_status")
    @Expose
    public Integer block_status;

    @SerializedName("block_status_by_user")
    @Expose
    public Integer block_status_by_user;

    @SerializedName("user_blocked_status")
    @Expose
    public Integer user_blocked_status;



    @SerializedName("message")
    @Expose
    public String message;


    public Integer getBlock_status() {
        return block_status;
    }

    public void setBlock_status(Integer block_status) {
        this.block_status = block_status;
    }

    public Integer getBlock_status_by_user() {
        return block_status_by_user;
    }

    public void setBlock_status_by_user(Integer block_status_by_user) {
        this.block_status_by_user = block_status_by_user;
    }

    public Integer getUser_blocked_status() {
        return user_blocked_status;
    }

    public void setUser_blocked_status(Integer user_blocked_status) {
        this.user_blocked_status = user_blocked_status;
    }

    public AnotherUserDetails getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(AnotherUserDetails userDetail) {
        this.userDetail = userDetail;
    }

    public List<AnotherUserProfileResult> getResult() {
        return result;
    }

    public void setResult(List<AnotherUserProfileResult> result) {
        this.result = result;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
