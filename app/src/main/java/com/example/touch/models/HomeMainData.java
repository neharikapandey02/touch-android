package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class HomeMainData implements Serializable {

    private boolean muteunmutecheck;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("post_user_id")
    @Expose
    private String post_user_id;
    @SerializedName("keyword")
    @Expose
    private String keyword;
    @SerializedName("big_image")
    @Expose
    private String big_image;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("profile_image")
    @Expose
    private String profile_image;
    @SerializedName("count_comment")
    @Expose
    private Integer count_comment;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("post_date")
    @Expose
    private String postDate;
    @SerializedName("post_time")
    @Expose
    private String postTime;
    @SerializedName("hashtag_one")
    @Expose
    private String hashtag_one;
    @SerializedName("hashtag_two")
    @Expose
    private String hashtag_two;
    @SerializedName("hashtag_three")
    @Expose
    private String hashtag_three;
    @SerializedName("hashtag_four")
    @Expose
    private String hashtag_four;
    @SerializedName("hashtag_five")
    @Expose
    private String hashtag_five;
    @SerializedName("count_like")
    @Expose
    private Integer countLike;
    @SerializedName("is_like")
    @Expose
    private Integer isLike;
    @SerializedName("thumb")
    @Expose
    private String thumb;
    @SerializedName("big")
    @Expose
    private String big;

    @SerializedName("big_width")
    @Expose
    private Integer big_width;

    @SerializedName("big_height")
    @Expose
    private Integer big_height;

    public Integer getBig_width() {
        return big_width;
    }

    public void setBig_width(Integer big_width) {
        this.big_width = big_width;
    }

    public Integer getBig_height() {
        return big_height;
    }

    public void setBig_height(Integer big_height) {
        this.big_height = big_height;
    }

    public boolean isMuteunmutecheck() {
        return muteunmutecheck;
    }

    public void setMuteunmutecheck(boolean muteunmutecheck) {
        this.muteunmutecheck = muteunmutecheck;
    }

    public String getPost_user_id() {
        return post_user_id;
    }

    public void setPost_user_id(String post_user_id) {
        this.post_user_id = post_user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBig_image() {
        return big_image;
    }

    public void setBig_image(String big_image) {
        this.big_image = big_image;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public Integer getCount_comment() {
        return count_comment;
    }

    public void setCount_comment(Integer count_comment) {
        this.count_comment = count_comment;
    }

    public String getHashtag_one() {
        return hashtag_one;
    }

    public void setHashtag_one(String hashtag_one) {
        this.hashtag_one = hashtag_one;
    }

    public String getHashtag_two() {
        return hashtag_two;
    }

    public void setHashtag_two(String hashtag_two) {
        this.hashtag_two = hashtag_two;
    }

    public String getHashtag_three() {
        return hashtag_three;
    }

    public void setHashtag_three(String hashtag_three) {
        this.hashtag_three = hashtag_three;
    }

    public String getHashtag_four() {
        return hashtag_four;
    }

    public void setHashtag_four(String hashtag_four) {
        this.hashtag_four = hashtag_four;
    }

    public String getHashtag_five() {
        return hashtag_five;
    }

    public void setHashtag_five(String hashtag_five) {
        this.hashtag_five = hashtag_five;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

    public Integer getCountLike() {
        return countLike;
    }

    public void setCountLike(Integer countLike) {
        this.countLike = countLike;
    }

    public Integer getIsLike() {
        return isLike;
    }

    public void setIsLike(Integer isLike) {
        this.isLike = isLike;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

}
