package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LatestPostData {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("tag_id")
    @Expose
    private String tag_id;

    @SerializedName("keyword")
    @Expose
    private String keyword;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("state")
    @Expose
    private String state;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("main_country")
    @Expose
    private String main_country;

    @SerializedName("latitude")
    @Expose
    private String latitude;

    @SerializedName("longitude")
    @Expose
    private String longitude;

    @SerializedName("hashtag_one")
    @Expose
    private String hashtag_one;

    @SerializedName("hashtag_two")
    @Expose
    private String hashtag_two;

    @SerializedName("hashtag_three")
    @Expose
    private String hashtag_three;

    @SerializedName("hashtag_four")
    @Expose
    private String hashtag_four;

    @SerializedName("hashtag_five")
    @Expose
    private String hashtag_five;

    @SerializedName("first_name")
    @Expose
    private String first_name;

    @SerializedName("last_name")
    @Expose
    private String last_name;

    @SerializedName("profile_image")
    @Expose
    private String profile_image;

    @SerializedName("post_date")
    @Expose
    private String post_date;

    @SerializedName("count_like")
    @Expose
    private String count_like;

    @SerializedName("is_like")
    @Expose
    private String is_like;

    @SerializedName("count_comment")
    @Expose
    private String count_comment;

    @SerializedName("post_user_id")
    @Expose
    private String post_user_id;

    @SerializedName("months")
    @Expose
    private String months;


    @SerializedName("days")
    @Expose
    private String days;


    @SerializedName("thumb")
    @Expose
    private String thumb;


    @SerializedName("big")
    @Expose
    private String big;

    @SerializedName("type")
    @Expose
    private String type;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTag_id() {
        return tag_id;
    }

    public void setTag_id(String tag_id) {
        this.tag_id = tag_id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMain_country() {
        return main_country;
    }

    public void setMain_country(String main_country) {
        this.main_country = main_country;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getHashtag_one() {
        return hashtag_one;
    }

    public void setHashtag_one(String hashtag_one) {
        this.hashtag_one = hashtag_one;
    }

    public String getHashtag_two() {
        return hashtag_two;
    }

    public void setHashtag_two(String hashtag_two) {
        this.hashtag_two = hashtag_two;
    }

    public String getHashtag_three() {
        return hashtag_three;
    }

    public void setHashtag_three(String hashtag_three) {
        this.hashtag_three = hashtag_three;
    }

    public String getHashtag_four() {
        return hashtag_four;
    }

    public void setHashtag_four(String hashtag_four) {
        this.hashtag_four = hashtag_four;
    }

    public String getHashtag_five() {
        return hashtag_five;
    }

    public void setHashtag_five(String hashtag_five) {
        this.hashtag_five = hashtag_five;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getPost_date() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public String getCount_like() {
        return count_like;
    }

    public void setCount_like(String count_like) {
        this.count_like = count_like;
    }

    public String getIs_like() {
        return is_like;
    }

    public void setIs_like(String is_like) {
        this.is_like = is_like;
    }

    public String getCount_comment() {
        return count_comment;
    }

    public void setCount_comment(String count_comment) {
        this.count_comment = count_comment;
    }

    public String getPost_user_id() {
        return post_user_id;
    }

    public void setPost_user_id(String post_user_id) {
        this.post_user_id = post_user_id;
    }

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}


