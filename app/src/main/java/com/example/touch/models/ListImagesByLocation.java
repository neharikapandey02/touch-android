package com.example.touch.models;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.constants.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListImagesByLocation extends RecyclerView.Adapter<ListImagesByLocation.searchListImage> {
    Context context;
    List<LocationSearchMainData> followersListMainData;

    public ListImagesByLocation(Context context, List<LocationSearchMainData> followersListMainData) {
        this.context = context;
        this.followersListMainData = followersListMainData;
    }

    @NonNull
    @Override
    public ListImagesByLocation.searchListImage onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_search_image_adapter, parent, false);
        ListImagesByLocation.searchListImage searchListImage = new ListImagesByLocation.searchListImage(view);

        return searchListImage;
    }

    @Override
    public void onBindViewHolder(@NonNull ListImagesByLocation.searchListImage holder, int position) {
        holder.imageView_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AnotherUserProfile.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.ANOTHER_USER_ID,followersListMainData.get(position).getUserId());
                context.startActivity(intent);
            }
        });
        Picasso.with(context)
                .load(followersListMainData.get(position).getImage())
                .placeholder(R.drawable.loading_placeholder)
                .fit()
                .centerCrop()
                //.resize(250,350)
                .error(R.drawable.loading_placeholder)
                .into(holder.imageView_image);
    }

    @Override
    public int getItemCount() {
        return followersListMainData.size();
    }

    public class searchListImage extends RecyclerView.ViewHolder {
        ImageView imageView_image;

        public searchListImage(@NonNull View itemView) {
            super(itemView);
            imageView_image = itemView.findViewById(R.id.imageView_image);
        }
    }
}
