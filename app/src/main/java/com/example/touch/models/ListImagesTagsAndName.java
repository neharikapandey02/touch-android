package com.example.touch.models;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.constants.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListImagesTagsAndName extends RecyclerView.Adapter<ListImagesTagsAndName.searchListImage> {
    private Context context;
    private List<ImageListTagAndNameData> followersListMainData;

    public ListImagesTagsAndName(Context context, List<ImageListTagAndNameData> followersListMainData) {
        this.context = context;
        this.followersListMainData = followersListMainData;
    }

    @NonNull
    @Override
    public ListImagesTagsAndName.searchListImage onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_search_image_adapter, parent, false);
        ListImagesTagsAndName.searchListImage searchListImage = new ListImagesTagsAndName.searchListImage(view);

        return searchListImage;
    }

    @Override
    public void onBindViewHolder(@NonNull ListImagesTagsAndName.searchListImage holder, int position) {
        holder.imageView_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AnotherUserProfile.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.ANOTHER_USER_ID, followersListMainData.get(position).getId());
                context.startActivity(intent);
            }
        });
        if (followersListMainData.get(position).getType().equalsIgnoreCase("image")) {
            Picasso.with(context)
                    .load(followersListMainData.get(position).getImage())
                    .placeholder(R.drawable.loading_placeholder)
                    .fit()
                    .centerCrop()
                    //.resize(250,350)
                    .error(R.drawable.loading_placeholder)
                    .into(holder.imageView_image);
        }else {
            holder.imgview_Video.setVisibility(View.VISIBLE);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.loading_placeholder);
            requestOptions.error(R.drawable.loading_placeholder);
            Glide.with(context)
                    .load(followersListMainData.get(position).getImage())
                    .fitCenter()
                    .centerCrop()
                    .apply(requestOptions)
                    .thumbnail(Glide.with(context).load(followersListMainData.get(position).getImage()))
                    .into(holder.imageView_image);

        }
    }

    @Override
    public int getItemCount() {
        return followersListMainData.size();
    }

    public class searchListImage extends RecyclerView.ViewHolder {
        ImageView imageView_image, imgview_Video;

        public searchListImage(@NonNull View itemView) {
            super(itemView);
            imageView_image = itemView.findViewById(R.id.imageView_image);
            imgview_Video = itemView.findViewById(R.id.imgview_Video);
        }
    }
}
