package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginMain {
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("user_details")
    @Expose
    private LoginMainData userDetails;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public LoginMainData getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(LoginMainData userDetails) {
        this.userDetails = userDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
