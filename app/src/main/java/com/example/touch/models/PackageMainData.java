package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackageMainData {
    @SerializedName("check_status")
    @Expose
    private Integer checkStatus;
    @SerializedName("expired_on")
    @Expose
    private Integer expiredOn;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("status")
    @Expose
    private Integer status;

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getExpiredOn() {
        return expiredOn;
    }

    public void setExpiredOn(Integer expiredOn) {
        this.expiredOn = expiredOn;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
