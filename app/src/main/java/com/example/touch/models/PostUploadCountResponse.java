package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostUploadCountResponse {

    @SerializedName("success")
    @Expose
    private Integer success;

    @SerializedName("today_count")
    @Expose
    private Integer today_count;

    @SerializedName("message")
    @Expose
    private String message;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getToday_count() {
        return today_count;
    }

    public void setToday_count(Integer today_count) {
        this.today_count = today_count;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
