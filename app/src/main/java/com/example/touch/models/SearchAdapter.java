package com.example.touch.models;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.ImageListByLocationActivity;
import com.example.touch.constants.Constants;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.Search> {
    Context context;
    List<SearchMainData> searchMainDataList;

    public SearchAdapter(Context context, List<SearchMainData> searchMainDataList){
      this.searchMainDataList=searchMainDataList;
      this.context=context;
    }


    @NonNull
    @Override
    public SearchAdapter.Search onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_search_adapter_list,parent,false);
        SearchAdapter.Search search=new SearchAdapter.Search(view);

        return search;
    }

    @Override
    public void onBindViewHolder(@NonNull Search holder, int position) {
        holder.tv_userName.setText(searchMainDataList.get(position).getFirstName());
        holder.tv_locationName.setText(searchMainDataList.get(position).getCity());
       /* Picasso.with(context)
                .load(searchMainDataList.get(position).getProfileImage())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.circularImageView_UserImage);

        holder.tv_name.setText(searchMainDataList.get(position).getFirstName()+" "+searchMainDataList.get(position).getLastName());
        holder.tv_email.setText(searchMainDataList.get(position).getEmail());*/
        holder.layout_relativeTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ImageListByLocationActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //Bundle args = new Bundle();
                //args.putSerializable(Constants.LIST_DATA, (Serializable) searchMainDataList);
                intent.putExtra(Constants.BUNDLE,searchMainDataList.get(position).getCity());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchMainDataList.size();
    }

    public class Search extends RecyclerView.ViewHolder{
        CircularImageView circularImageView_UserImage;
        TextView tv_name;
        TextView tv_email;
        TextView tv_userName;
        TextView tv_locationName;
        RelativeLayout layout_relativeTop;
        LinearLayout top_circle;

        public Search(@NonNull View itemView) {
            super(itemView);
            circularImageView_UserImage=itemView.findViewById(R.id.circularImageView_UserImage);
            tv_name=itemView.findViewById(R.id.tv_name);
            tv_email=itemView.findViewById(R.id.tv_email);
            layout_relativeTop=itemView.findViewById(R.id.layout_relativeTop);
            top_circle=itemView.findViewById(R.id.top_circle);
            tv_locationName=itemView.findViewById(R.id.tv_locationName);
            tv_userName=itemView.findViewById(R.id.tv_userName);
        }
    }
}
