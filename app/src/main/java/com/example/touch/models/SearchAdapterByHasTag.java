package com.example.touch.models;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.ImagesListByTagAndName;
import com.example.touch.adapters.SearchAdapterByName;
import com.example.touch.constants.Constants;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchAdapterByHasTag extends RecyclerView.Adapter <SearchAdapterByHasTag.SearchAdapter>{
    Context context;
    List<SearchMainData> searchMainDataList;
    int userHastag;
    String userId;
    int pos;

    public SearchAdapterByHasTag(Context context, List<SearchMainData> searchMainDataList, int userHastag, String userId){
        this.searchMainDataList=searchMainDataList;
        this.context=context;
        this.userHastag = userHastag;
        this.userId = userId;
    }

    @NonNull
    @Override
    public SearchAdapterByHasTag.SearchAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.item_search_byhastag,parent,false);
        SearchAdapterByHasTag.SearchAdapter searchAdapter=new SearchAdapterByHasTag.SearchAdapter(view);

        return searchAdapter;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapterByHasTag.SearchAdapter holder, int position) {
        holder.tv_userName.setText("#"+searchMainDataList.get(position).getTag());
        if (userHastag == 1){
            holder.removeIcon.setVisibility(View.VISIBLE);
        }
        else {
            holder.removeIcon.setVisibility(View.GONE);
        }

        holder.removeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;


                RetrofitHelper.getInstance().deleteHastag(deleteHastagCallback, userId, searchMainDataList.get(position).getTag());

            }
        });
        //holder.tv_locationName.setText(searchMainDataList.get(position).getCountry()+","+searchMainDataList.get(position).getCity());
        //holder.tv_locationName.setText(searchMainDataList.get(position).getFirstName());
       /* Picasso.with(context)
                .load(searchMainDataList.get(position).getProfileImage())
                .placeholder(R.drawable.profile_placeholder)
                .error(R.drawable.profile_placeholder)
                .into(holder.circularImageView_UserImage);*/

        /* holder.tv_name.setText(searchMainDataList.get(position).getFirstName()+" "+searchMainDataList.get(position).getLastName());
        holder.tv_email.setText(searchMainDataList.get(position).getEmail());*/
        holder.layout_relativeTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, ImagesListByTagAndName.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra(Constants.TAG_ID,searchMainDataList.get(position).getTag_id());
                intent.putExtra(Constants.TAG_NAME,searchMainDataList.get(position).getTag());
                context.startActivity(intent);
            }
        });
    }

    Callback<DeleteMain> deleteHastagCallback = new Callback<DeleteMain>() {
        @Override
        public void onResponse(Call<DeleteMain> call, Response<DeleteMain> response) {

            try {
                if (response.body().getSuccess() == 1){
                    searchMainDataList.remove(pos);
                    notifyDataSetChanged();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<DeleteMain> call, Throwable t) {

        }
    };

    @Override
    public int getItemCount() {
        return searchMainDataList.size();
    }
    public class SearchAdapter extends RecyclerView.ViewHolder{
        CircularImageView circularImageView_UserImage;
        TextView tv_name;
        TextView tv_email;
        TextView tv_userName;
        ImageView removeIcon;
        TextView tv_locationName;
        RelativeLayout layout_relativeTop;
        RelativeLayout top_circle;
        public SearchAdapter(@NonNull View itemView) {
            super(itemView);
            circularImageView_UserImage=itemView.findViewById(R.id.circularImageView_ByNameUser);
            tv_name=itemView.findViewById(R.id.tv_name);
            tv_email=itemView.findViewById(R.id.tv_email);
            layout_relativeTop=itemView.findViewById(R.id.layout_relativeTop);
            top_circle=itemView.findViewById(R.id.top_circle);
            tv_locationName=itemView.findViewById(R.id.tv_locationName);
            tv_userName=itemView.findViewById(R.id.tv_userName);
            removeIcon=itemView.findViewById(R.id.removeIcon);
        }
    }
}
