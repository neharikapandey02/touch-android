package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchMain {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("user_hastag")
    @Expose
    private Integer user_hastag;
    @SerializedName("result")
    @Expose
    private List<SearchMainData> result = null;

    public Integer getUser_hastag() {
        return user_hastag;
    }

    public void setUser_hastag(Integer user_hastag) {
        this.user_hastag = user_hastag;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SearchMainData> getResult() {
        return result;
    }

    public void setResult(List<SearchMainData> result) {
        this.result = result;
    }

}
