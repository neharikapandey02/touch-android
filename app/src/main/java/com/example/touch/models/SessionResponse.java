package com.example.touch.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SessionResponse {

    @SerializedName("success")
    @Expose
    private Integer status;

    @SerializedName("token")
    @Expose
    private String value;

    @SerializedName("invalid_token")
    @Expose
    private Integer invalid_token;

   @SerializedName("msg")
    @Expose
    private String msg;

    public Integer getInvalid_token() {
        return invalid_token;
    }

    public void setInvalid_token(Integer invalid_token) {
        this.invalid_token = invalid_token;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
