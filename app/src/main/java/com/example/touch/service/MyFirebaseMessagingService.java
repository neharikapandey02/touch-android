package com.example.touch.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.example.touch.R;
import com.example.touch.activities.ChatActivity;
import com.example.touch.activities.MainActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.SharedPreferencesData;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Objects;


@SuppressLint("MissingFirebaseInstanceTokenRefresh")
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private SharedPreferencesData sharedPreferencesData;

    @Override
    public void onNewToken(String token) {
        Log.e("device_id", token);
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());
        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.DEVICE_ID, token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
//        sendRegistrationToServer(token);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sharedPreferencesData = new SharedPreferencesData(getApplicationContext());

        if (remoteMessage.getData().size() > 0) {
            Log.e("mj4", "" + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e("mj5", "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.NOTIFICATION).equalsIgnoreCase("on")){
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData());
                JSONObject jsonObject1 = new JSONObject(Objects.requireNonNull(remoteMessage.getData().get("data")));
                String page = jsonObject1.optString("page");
                Log.e("pagge", "hi"+page);

                if (page.equalsIgnoreCase("activity")){
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.putExtra("fragment", "activity");
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    sendNotification(jsonObject, intent);
                }
                else if (page.equalsIgnoreCase("chat")){
                    Intent intent = new Intent(this, ChatActivity.class);
                    intent.putExtra(Constants.FIRST_NAME, jsonObject1.optString("first_name"));
                    intent.putExtra(Constants.RECEIVER_ID, jsonObject1.optString("post_user_id"));
                    intent.putExtra(Constants.PROFILE_IMAGE, jsonObject1.optString("profile_image"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    sendNotification(jsonObject, intent);
                }
                else {
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    sendNotification(jsonObject, intent);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    private void sendNotification(JSONObject remoteMessage, Intent intent) {


        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "1";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Bitmap bitmap_image = BitmapFactory.decodeResource(getResources(), R.mipmap.launcher_by_app_use);
        NotificationCompat.BigPictureStyle bpStyle = new NotificationCompat.BigPictureStyle();
        bpStyle.bigPicture(bitmap_image)
                .bigLargeIcon(null).build();
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(MyFirebaseMessagingService.this, channelId)
                        .setContentTitle(remoteMessage.optString("title"))
                        .setContentText(Html.fromHtml(remoteMessage.optString("message")))
                        .setAutoCancel(true)
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
//                        .setStyle(bpStyle);


        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("chatactivity");
        broadcastIntent.putExtra("key",
                remoteMessage.optString("body"));
        sendBroadcast(broadcastIntent);


        Intent mainIntnt = new Intent();
        mainIntnt.setAction("MainActivity");
        mainIntnt.putExtra("key",
                remoteMessage.optString("body"));
//        mainIntnt.putExtra("key_title",
//                remoteMessage.optString("title"));
        mainIntnt.putExtra("key_title",
                remoteMessage.optString("option"));

        sendBroadcast(mainIntnt);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            notificationBuilder.setSmallIcon(R.mipmap.launcher_by_app_use);
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
            notificationBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            notificationBuilder.setLights(Color.RED, 3000, 3000);

        } else {
            notificationBuilder.setSmallIcon(R.mipmap.launcher_by_app_use);
            notificationBuilder.setColor(getResources().getColor(R.color.colorAccent));
            notificationBuilder.setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
            notificationBuilder.setLights(Color.RED, 3000, 3000);
        }
        NotificationManager notificationManager =
                (NotificationManager) MyFirebaseMessagingService.this.getSystemService(Context.NOTIFICATION_SERVICE);

// Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(1, notificationBuilder.build());
    }

}