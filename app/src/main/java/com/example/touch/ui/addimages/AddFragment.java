package com.example.touch.ui.addimages;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.exifinterface.media.ExifInterface;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.deep.videotrimmer.utils.FileUtils;
import com.example.touch.ImageProcessing.GifSizeFilter;
import com.example.touch.ImageProcessing.Glide4Engine;
import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.StripePaymentActivity;
import com.example.touch.activities.UploadImageActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.customGallery.SubFragment;
import com.example.touch.models.MultipleImageMain;
import com.example.touch.models.PackageMainData;
import com.example.touch.utility.GlideCacheEngine;
import com.example.touch.utility.GlideEngine;
import com.example.touch.utility.ImagePickerActivity;
import com.example.touch.utility.VideoTrimmerActivity;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.snackbar.Snackbar;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.instagram.InsGallery;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.filter.Filter;

import net.vrgsoft.videcrop.VideoCropActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static androidx.core.content.FileProvider.getUriForFile;

public class AddFragment extends Fragment {
    private static final int REQUEST_IMAGE = 100;
    private static final int PERMISSION_REQUEST_CODE_GALLERY = 102;
    private static final int PERMISSION_REQUEST_CODE_CAMERA = 106;
    private static final int CAMERA_REQUEST_CODE_VEDIO = 107;
    private static final int PICKER_REQUEST_CODE = 1;
    String outVideoUri = "";
    private static final int PICKER_REQUEST_CODE_VIDEO = 2;
    private AddViewModel addViewModel;
    private ArrayList<String> images;
    private List imageList;
    private String imageEncoded;
    private static final int CROP_REQUEST = 200;
    private List<String> imagesEncodedList;
    public static final String EXTRA_VIDEO_PATH = "EXTRA_VIDEO_PATH";
    private final int REQUEST_VIDEO_TRIMMER_RESULT = 342;
    private String selectedVideoName = null,selectedVideoFile = null;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.imgView_uploadImages)
    ImageView imgView_uploadImages;

    @BindView(R.id.recyclerView_Following)
    RecyclerView recyclerView_Following;

    private String userId;
    private String userType;

    private SharedPreferencesData sharedPreferencesData;
    List<String> mSelected;List<Uri> mSelectedUri;
    private Dialog dialog;
    private String extensionType=".jpg";
    private RequestOptions simpleOptions;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        addViewModel =
                ViewModelProviders.of(this).get(AddViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
        ButterKnife.bind(this, root);
        return root;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        simpleOptions = new RequestOptions()
                .centerCrop()
                .placeholder(R.color.blackOverlay)
                .error(R.color.blackOverlay)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        viewFinds(view);
    }
    RelativeLayout areaContainer;
    SubFragment subFragment;
    View view;

    private void viewFinds(View view) {
        this.view = view;
        sharedPreferencesData = new SharedPreferencesData(getContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        userType = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_TYPE);

      /*  Intent intent=new Intent(getContext(), StripePaymentActivity.class);
        startActivity(intent);*/

        if (NetworkUtil.checkNetworkStatus(getContext())) {
            if (userId != null && !userId.equalsIgnoreCase("")) {
                if (userType != null && userType.equalsIgnoreCase("2")) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().getPackageUpdateInfo(getPackageInfoCallback, userId);
                } else {
                    Snackbar.make(progressBar, getResources().getString(R.string.you_cant_upload_images), Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.login_first_to_upload), Snackbar.LENGTH_LONG).show();
            }
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    public void openDialogForCamerAndGallerySelection(){
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_select_dialogue1);
//        dialog.setCancelable(false);
        TextView tv_gallery = dialog.findViewById(R.id.tv_gallery);
        TextView tv_videoGallery = dialog.findViewById(R.id.tv_videoGallery);
        TextView tv_camera = dialog.findViewById(R.id.tv_camera);
        TextView tv_video = dialog.findViewById(R.id.tv_video);

        tv_videoGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] PERMISSIONS = {
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                };

                if(hasPermissions( getContext(),PERMISSIONS)){
//                    ShowPicker();
                    launchVideoGalleryIntent();
                }else{
                    requestPermissions(PERMISSIONS, PICKER_REQUEST_CODE_VIDEO);
                }

            }
        });

        tv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {
                    request();
                } else {
                    Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivityForResult(takeVideoIntent,
                                CAMERA_REQUEST_CODE_VEDIO);
                    }
                }
            }
        });

        tv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] PERMISSIONS = {
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                };

                if(hasPermissions( getContext(),PERMISSIONS)){
//                    ShowPicker();
                    launchGalleryIntent();
                }else{
                    requestPermissions(PERMISSIONS, PICKER_REQUEST_CODE);
                }


            }
        });
        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED||
                        ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED||
                        ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_DENIED) {
                    request();
                } else {

//                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD && android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.P) {
//                        sendTakePictureIntent();
//                    } else {
//                        cameraCall();
//                    }
                    launchCameraIntent();
                }
            }
        });
        dialog.show();

    }

    public boolean checkAndRequestPermissions(final Activity context) {
        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermissions(listPermissionsNeeded
                            .toArray(new String[listPermissionsNeeded.size()]),
                    PERMISSION_REQUEST_CODE_CAMERA);
            return false;
        }
        return true;
    }


    public static boolean checkAndRequestPermissionsOnlyCheck(final Activity context) {
        int ExtstorePermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        int cameraPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (cameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ExtstorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded
                    .add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            return false;
        }
        return true;
    }

    private void showCamera() {
        dialog.cancel();
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, PERMISSION_REQUEST_CODE_CAMERA);
    }

    public void request() {
        requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE_CAMERA);
    }



    Callback<PackageMainData> getPackageInfoCallback = new Callback<PackageMainData>() {
        @Override
        public void onResponse(Call<PackageMainData> call, Response<PackageMainData> response) {
            progressBar.setVisibility(View.GONE);
            if (response.body() != null) {



                if (response.body().getCheckStatus() == 1) {
//                    openDialogForCamerAndGallerySelection();
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                            == PackageManager.PERMISSION_DENIED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED) {
                        request();
                    } else {
                        ArrayList<LocalMedia> arrayList = new ArrayList<>();
                        InsGallery.openGallery(getActivity(), GlideEngine.createGlideEngine(), GlideCacheEngine.createCacheEngine(), arrayList);
                    }
                    //areaContainer = (RelativeLayout)view.findViewById(R.id.area_container1);
                    //subFragment = new SubFragment();
                    //getActivity().getSupportFragmentManager().beginTransaction().add(areaContainer.getId(), subFragment).commit();
                } else {
                    if(response.body().getExpiredOn() == 0){
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("You can't post content actually. Please upgrade your profile to be online and post photos & videos.");
                        alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Toast.makeText(getContext(), getResources().getString(R.string.please_payment), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getContext(), StripePaymentActivity.class);
                                startActivity(intent);
                            }
                        });
                        alertDialog.show();
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<PackageMainData> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    public void ShowPicker()
    {
        dialog.cancel();
            Matisse.from(AddFragment.this)
                    .choose(MimeType.ofAll())
                    .countable(true)
                    .maxSelectable(5)
                    .addFilter(new GifSizeFilter(426, 240, 5 * Filter.K * Filter.K))
                    .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .imageEngine(new Glide4Engine())
                    .forResult(PICKER_REQUEST_CODE);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        dialog.cancel();
        switch (requestCode) {
            case PICKER_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    /*Toast.makeText(
                            getContext(),
                            "Permission granted! Please click on pick a file once again.",
                            Toast.LENGTH_SHORT
                    ).show();*/
//                    ShowPicker();
                    launchGalleryIntent();
                } else {
                    Toast.makeText(
                            getContext(),
                            "Permission denied to read your External storage :(",
                            Toast.LENGTH_SHORT
                    ).show();
                }

                return;
            }
            case PICKER_REQUEST_CODE_VIDEO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    launchVideoGalleryIntent();
                } else {
                    Toast.makeText(
                            getContext(),
                            "Permission denied to read your External storage :(",
                            Toast.LENGTH_SHORT
                    ).show();
                }
                break;
            }
            case PERMISSION_REQUEST_CODE_CAMERA:{
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_REQUEST_CODE_CAMERA) {


//                   if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD&&android.os.Build.VERSION.SDK_INT <=android.os.Build.VERSION_CODES.P) {
//                       sendTakePictureIntent();
//                   }else{
//                       cameraCall();
//                   }
//                   launchCameraIntent();
                   ArrayList<LocalMedia> arrayList = new ArrayList<>();
                   InsGallery.openGallery(getActivity(), GlideEngine.createGlideEngine(), GlideCacheEngine.createCacheEngine(), arrayList);



               } else {
                    request();
                    Toast.makeText(getContext(), "Permission denied to for camera :(", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void launchCameraIntent () {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void cameraCall() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, PERMISSION_REQUEST_CODE_CAMERA);
            }
        }
    }
    String currentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir =getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }
    public void requestCamera() {
        //requestPermissions(getContext(), new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST_CODE_CAMERA);

        requestPermissions( //Method of Fragment
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE_CAMERA
        );
    }

    @OnClick(R.id.imgView_uploadImages)
    public void uploadIconClick(View view){

    }

    public void requestGallery() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE_GALLERY);


    }
    /* @Override
   public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_REQUEST_CODE_CAMERA) {


            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            startActivityForResult(intent, PERMISSION_REQUEST_CODE_CAMERA);


        } else {
            Toast.makeText(getContext(), "Please allow permission", Toast.LENGTH_SHORT).show();
        }

*//*
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_REQUEST_CODE_GALLERY) {

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PERMISSION_REQUEST_CODE_GALLERY);


        }  else {
            Toast.makeText(getContext(), "Please allow permission", Toast.LENGTH_SHORT).show();
        }*//*
    }*/
    public static int count = 0;
    Uri outputFileUri;
    private void sendTakePictureIntent() {
        final String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();
// and likewise.
        count++;
        String file = dir+count+".jpg";
        File newfile = new File(file);

        try {
            newfile.createNewFile();
        }
        catch (IOException e)
        {
        }

        //Uri outputFileUri = Uri.fromFile(newfile);
        outputFileUri= FileProvider.getUriForFile(getContext(), getContext().getPackageName() + ".provider", newfile);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(cameraIntent, PERMISSION_REQUEST_CODE_CAMERA);
    }

    private void launchVideoGalleryIntent(){
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, CAMERA_REQUEST_CODE_VEDIO);
    }

    private void launchGalleryIntent () {
        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    String pathImage;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //imageList.clear();
//        if (dialog!=null){
//            dialog.cancel();
//        }

//        if (requestCode == REQUEST_IMAGE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri uri = data.getParcelableExtra("path");
//                try {
//                    // You can update this bitmap to your server
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
////                    pathcamera1 = getRealPathFromURI(RegisterProfileActivity.this, uri);
//                    String pathcamera = uri.toString();
//
//                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
//                    @SuppressLint("InflateParams") View mView = getLayoutInflater().inflate(R.layout.dialog_black_and_white_filter, null);
//                    PhotoView photoView = mView.findViewById(R.id.imageView);
//                    CheckBox blackCB = mView.findViewById(R.id.blackCB);
//                    Button continueBtn = mView.findViewById(R.id.continueBtn);
//                    Glide.with(getActivity()).load(uri.toString()
//                    ).into(photoView);
//                    mBuilder.setView(mView);
//                    AlertDialog mDialog = mBuilder.create();
//                    mDialog.setCancelable(false);
//                    blackCB.setChecked(false);
//                    Log.e("Urrri", uri.toString());
//                    if (!blackCB.isChecked()) {
//                        continueBtn.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                Intent intent = new Intent(getContext(), UploadImageActivity.class);
//                                intent.putExtra(Constants.IMAGEPATH, uri.toString());
//                                intent.putExtra("crop", "1");
//                                startActivity(intent);
//                                mDialog.dismiss();
//                            }
//                        });
//                    }
//                    blackCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                        @Override
//                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                            if (isChecked) {
//                                try {
//                                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                                    Bitmap bitmap1 = toGrayscale(bitmap);
////                                    photoView.setImageBitmap(bitmap1);
//                                    Uri uri1 = getImageUri1(getActivity().getApplicationContext(), bitmap1);
//                                    String a = getRealPathFromURI(uri1);
//                                    Log.e("Urrri", a.toString());
//                                    Glide.with(getActivity()).load(uri1.toString()
//                                    ).into(photoView);
//                                  continueBtn.setOnClickListener(new View.OnClickListener() {
//                                        @Override
//                                        public void onClick(View v) {
//                                            Intent intent = new Intent(getContext(), UploadImageActivity.class);
//                                            intent.putExtra(Constants.IMAGEPATH, a.toString());
//                                            intent.putExtra("crop", "1");
//                                            startActivity(intent);
//                                            mDialog.dismiss();
//                                        }
//                                    });
//
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//
//                            } else {
//                                Uri uri1 = getImageUri1(getActivity().getApplicationContext(), bitmap);
//                                String a = getRealPathFromURI(uri1);
//                                Log.e("Urrri", a.toString());
//                                Glide.with(getActivity()).load(uri1.toString()
//                                ).into(photoView);
//                                continueBtn.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        Intent intent = new Intent(getContext(), UploadImageActivity.class);
//                                        intent.putExtra(Constants.IMAGEPATH, a.toString());
//                                        intent.putExtra("crop", "1");
//                                        startActivity(intent);
//                                        mDialog.dismiss();
//                                    }
//                                });
//                            }
//                        }
//                    });
//                    mDialog.show();
//
//                    // loading profile image from local cache
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//        else if (requestCode == PERMISSION_REQUEST_CODE_CAMERA) {
//
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
//                    try {
//                        //Log.e("Extra=",getRealPathFromURI(outputFileUri));
//
//                        Bitmap mImageBitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), Uri.parse(outputFileUri.toString()));
//                        //image.setImageBitmap(mImageBitmap);
//                        String device_name = android.os.Build.MANUFACTURER;
//                        Uri tempUri11;
//                        if (device_name.equalsIgnoreCase("samsung")) {
//
//                            tempUri11 = getImageUri(getContext(), getCameraPhotoOrientation(mImageBitmap, mImageBitmap.getWidth(), mImageBitmap.getHeight()));
//                        } else {
//                            tempUri11 = getImageUri(getContext(), mImageBitmap);
//                        }
//                        Log.e("urri", "hi"+tempUri11);
//
//                        // CALL THIS METHOD TO GET THE ACTUAL PATH
//                        File finalFile1 = new File(getRealPathFromURI(tempUri11));
//                        String pathImage1 = saveBitmapToFile(finalFile1).toString();
//
//                        Log.e("Extra=", pathImage1);
//                        ArrayList dataPath = new ArrayList();
//                        dataPath.clear();
//                        dataPath.add(pathImage1);
//                        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
//                        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, 3);
//                        intent.putExtra("file_name", tempUri11.toString());
//                        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
//                        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1);
//                        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
//                        startActivityForResult(intent, 99);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    try {
//                        Bitmap bmImg = BitmapFactory.decodeFile(currentPhotoPath);
////                        Bitmap bmImg = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), Uri.parse(outputFileUri.toString()));
//
//                        Uri tempUri11;
//                        String device_name = android.os.Build.MANUFACTURER;
//                        if (device_name.equalsIgnoreCase("samsung")) {
//                            tempUri11 = getImageUri(getContext(), getCameraPhotoOrientation(bmImg, bmImg.getWidth(), bmImg.getHeight()));
//                        } else {
//                            // tempUri11=getImageUri(getContext(),bmImg);
//                            tempUri11 = getImageUri(getContext(), bmImg);
////                            tempUri11 = getImageUri(getContext(), getCameraPhotoOrientation(bmImg, bmImg.getWidth(), bmImg.getHeight()));
//                        }
//
//                        Log.e("urri", "hi"+tempUri11);
//
////                        File finalFile1 = new File(getRealPathFromURI(tempUri11));
////                        String pathImage1;
////                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
////                            pathImage1 = finalFile1.toString();
////                        } else {
////                            pathImage1 = getPathFromInputStreamUri(getActivity(), tempUri11);
////                        }
//
////                        String pathImage1 = saveBitmapToFile(finalFile1).toString();
////                        ArrayList<String> dataPath = new ArrayList<String>();
////
////                        dataPath.add(pathImage1);
//                        Intent intent = new Intent(getActivity(), ImagePickerActivity.class);
//                        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, 3);
//                        intent.putExtra("file_name", tempUri11.toString());
//                        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
//                        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1);
//                        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
//                        startActivityForResult(intent, 99);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//        }
//
//        else if (requestCode == CAMERA_REQUEST_CODE_VEDIO) {
//            try {
//                Uri videoUri = data.getData();
////                String b = getRealPathFromURI(videoUri);
////                String fileName = System.currentTimeMillis() + ".mp4";
//
////                File path = new File(getActivity().getExternalCacheDir(), "camera");
////                if (!path.exists()) path.mkdirs();
////                File image = new File(path, fileName);
////                Uri uri =  getUriForFile(getActivity(), getActivity().getPackageName() + ".provider", image);
//                String a = getPath(getContext(), videoUri);
//                Log.e("bdfdfd", "dfdf "+a.toString());
////                Log.e("bdfdfd", "dfdf "+uri.toString());
////                Log.e("bdfdfd", "dfdaf "+b);
////                Log.e("bdfdfd", "dfda "+videoUri.getPath());
//                outVideoUri = "/storage/emulated/0/DCIM/Camera/newvideo1212.mp4";
//                startActivityForResult(VideoCropActivity.createIntent(getActivity(), a.toString(), outVideoUri), CROP_REQUEST);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        else if(requestCode == CROP_REQUEST){
//            try {
//                if (!outVideoUri.equalsIgnoreCase("")) {
//                    ArrayList dataPath=new ArrayList();
//                    dataPath.add(outVideoUri);
//                    Intent intent=new Intent(getActivity(), UploadImageActivity.class);
//                    intent.putStringArrayListExtra(Constants.IMAGEUPLOAD, dataPath);
//                    startActivity(intent);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//    else if (requestCode == 99)
//    {
//        try {
//            if (data != null) {
//                Uri uri = data.getParcelableExtra("path");
////                Log.e("uri33", uri.toString());
////        File finalFile1 = new File(getRealPathFromURI(uri));
////        String pathImage1=saveBitmapToFile(finalFile1).toString();
////                ArrayList dataPath = new ArrayList();
////                dataPath.clear();
////                dataPath.add(uri.toString());
//
//                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
//                @SuppressLint("InflateParams") View mView = getLayoutInflater().inflate(R.layout.dialog_black_and_white_filter, null);
//                PhotoView photoView = mView.findViewById(R.id.imageView);
//                CheckBox blackCB = mView.findViewById(R.id.blackCB);
//                Button continueBtn = mView.findViewById(R.id.continueBtn);
//                Glide.with(getActivity()).load(uri.toString()
//                ).into(photoView);
//                mBuilder.setView(mView);
//                AlertDialog mDialog = mBuilder.create();
//                mDialog.setCancelable(false);
//                blackCB.setChecked(false);
//                if (!blackCB.isChecked()) {
//                    continueBtn.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            String pathImage1 = null;
//                            try {
//                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                                String device_name = Build.MANUFACTURER;
//                                Uri uri1;
//                                if (device_name.equalsIgnoreCase("samsung")) {
//
//                                    uri1 = getImageUri(getContext(), getCameraPhotoOrientation(bitmap, bitmap.getWidth(), bitmap.getHeight()));
//                                } else {
//                                    uri1 = getImageUri(getContext(), bitmap);
//                                }
//                                File finalFile1 = new File(getRealPathFromURI(uri1));
//                                pathImage1 = saveBitmapToFile(finalFile1).toString();
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//                            Intent intent = new Intent(getContext(), UploadImageActivity.class);
//                            intent.putExtra(Constants.IMAGEPATH, pathImage1);
//                            intent.putExtra("crop", "1");
//                            startActivity(intent);
//                            mDialog.dismiss();
//                        }
//                    });
//                }
//                blackCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                    @Override
//                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                        if (isChecked) {
//                            try {
//                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//                                Bitmap bitmap1 = toGrayscale(bitmap);
//                                photoView.setImageBitmap(bitmap1);
//                                String device_name = Build.MANUFACTURER;
//                                Uri uri1;
//                                if (device_name.equalsIgnoreCase("samsung")) {
//
//                                    uri1 = getImageUri(getContext(), getCameraPhotoOrientation(bitmap1, bitmap1.getWidth(), bitmap1.getHeight()));
//                                } else {
//                                    uri1 = getImageUri(getContext(), bitmap1);
//                                }
//                                File finalFile1 = new File(getRealPathFromURI(uri1));
//                                String pathImage1 = saveBitmapToFile(finalFile1).toString();
//                                continueBtn.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        Intent intent = new Intent(getContext(), UploadImageActivity.class);
//                                        intent.putExtra(Constants.IMAGEPATH, pathImage1);
//                                        intent.putExtra("crop", "1");
//                                        startActivity(intent);
//                                        mDialog.dismiss();
//                                    }
//                                });
//
//                            } catch (IOException e) {
//                                e.printStackTrace();
//                            }
//
//                        } else {
//                            Glide.with(getActivity()).load(uri.toString()
//                            ).into(photoView);
//                        }
//                    }
//                });
//                mDialog.show();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
////        Intent intent = new Intent(getContext(), UploadImageActivity.class);
////        intent.putExtra(Constants.IMAGEPATH, uri.toString());
////        intent.putExtra("crop", "1");
////        startActivity(intent);
////        Toast.makeText(getActivity().getApplicationContext(), ""+uri, Toast.LENGTH_SHORT).show();
//    }
//    else if (requestCode == REQUEST_VIDEO_TRIMMER_RESULT){
//            try {
//                Uri selectedVideoUri = data.getData();
//
//                if (selectedVideoUri != null) {
//                    selectedVideoFile = data.getData().getPath();
//    //                selectedVideoName = data.getData().getLastPathSegment();
//    //                Bitmap thumb = ThumbnailUtils.createVideoThumbnail(selectedVideoUri.getPath(),
//    //                        MediaStore.Images.Thumbnails.FULL_SCREEN_KIND);
//    //                String path = getRealPathFromURI(selectedVideoUri);
//                    ArrayList dataPath=new ArrayList();
//                    dataPath.add(selectedVideoFile);
//                    Intent intent=new Intent(getActivity(), UploadImageActivity.class);
//                    intent.putStringArrayListExtra(Constants.IMAGEUPLOAD, dataPath);
//                    startActivity(intent);
//     }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//            else {
//            try {
//                if (requestCode == PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
//                    mSelected = Matisse.obtainPathResult(data);
//                    extensionType = mSelected.get(0).substring(mSelected.get(0).lastIndexOf("."));
//                    mSelectedUri = Matisse.obtainResult(data);
//
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
//                        if (mSelected != null && mSelected.size() > 0) {
//                            ArrayList dataPath = new ArrayList();
//                            dataPath.clear();
//                            dataPath.addAll(mSelected);
//                            Intent intent = new Intent(getActivity(), UploadImageActivity.class);
//                            intent.putStringArrayListExtra(Constants.IMAGEUPLOAD, dataPath);
//                            startActivity(intent);
//
//
//
//                  /*  String someFilepath = mSelected.get(0);
//                    String extension = someFilepath.substring(someFilepath.lastIndexOf("."));
//                    Log.e("Extension=",extension);
//
//                    if (extension.equalsIgnoreCase(".mp4")||extension.equalsIgnoreCase(".mpeg")||extension.equalsIgnoreCase(".mpg")||extension.equalsIgnoreCase(".mpg")||extension.equalsIgnoreCase(".mov")||extension.equalsIgnoreCase(".3gp")||extension.equalsIgnoreCase(".mkv")){
//                        Snackbar.make(getView(),getResources().getString(R.string.work_on_progress),Snackbar.LENGTH_LONG).show();
//                    }else{
//                        ArrayList dataPath=new ArrayList();
//                        dataPath.clear();
//                        dataPath.addAll(mSelected);
//                        Intent intent=new Intent(getActivity(), UploadImageActivity.class);
//                        intent.putStringArrayListExtra(Constants.IMAGEUPLOAD,dataPath);
//                        startActivity(intent);
//                    }*/
//
//                        } else {
//                            Snackbar.make(getView(), getResources().getString(R.string.please_select_images), Snackbar.LENGTH_LONG).show();
//                        }
//                    } else {
//                        if (mSelectedUri != null && mSelectedUri.size() > 0) {
//                            mSelected.clear();
//                            for (int i = 0; i < mSelectedUri.size(); i++) {
//                                mSelected.add(getPathFromInputStreamUri(getActivity(), mSelectedUri.get(i)));
//                            }
//                        /*int compressionRatio = 2; //1 == originalImage, 2 = 50% compression, 4=25% compress
//                        File file = new File (mSelected.get(0));
//                        try {
//                            Bitmap bitmap = BitmapFactory.decodeFile (file.getPath ());
//                            bitmap.compress (Bitmap.CompressFormat.JPEG, compressionRatio, new FileOutputStream (file));
//                        }
//                        catch (Throwable t) {
//                            Log.e("ERROR", "Error compressing file." + t.toString ());
//                            t.printStackTrace ();
//                        }*/
//                            ArrayList dataPath = new ArrayList();
//                            dataPath.clear();
//                            dataPath.addAll(mSelected);
//                            Intent intent = new Intent(getActivity(), UploadImageActivity.class);
//                            intent.putStringArrayListExtra(Constants.IMAGEUPLOAD, dataPath);
//                            startActivity(intent);
//
//                        }
//                    }
//
//
//                    // Display in the logs the selected items.
//                    // Outputs something like:
//                    // D/Matisse: mSelected: [
//                    //    content://media/external/images/media/26263,
//                    //    content://media/external/images/media/26264,
//                    //    content://media/external/images/media/26261
//                    // ]
//
//
//                    //Toast.makeText(getContext(),getResources().getString(R.string.capture_image),Toast.LENGTH_LONG).show();
//                }
//            } catch (Resources.NotFoundException e) {
//                e.printStackTrace();
//            }
//        }



        //Log.e("CODE=",requestCode+"    "+resultCode+"   "+data);
       /* if (requestCode == PERMISSION_REQUEST_CODE_GALLERY && data != null) {
            getImages(data,requestCode,resultCode);

        }*/
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    List<LocalMedia> selectList = PictureSelector.obtainMultipleResult(data);
                    for (LocalMedia media : selectList) {
                        Log.e("afdfdfdf:" , ""+media.isCompressed());
                        Log.e( "afdfdfdf:" , ""+media.getCompressPath());
                        Log.e( "afdfdfdf:" , ""+media.getPath());
                        Log.e( "afdfdfdf:" , ""+media.isCut());
                        Log.e( "afdfdfdf:" , ""+media.getCutPath());
                        Log.e( "afdfdfdf:" , ""+media.isOriginal());
                        Log.e( "afdfdfdf:" , ""+media.getOriginalPath());
                        Log.e( "afdfdfdf Q 特有Path:" , ""+media.getAndroidQToPath());
                        Log.e( "afdfdfdf: " , ""+media.getSize());
                    }
                    break;
            }
        }

    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }



    private void startTrimActivity(@NonNull Uri uri) {
        Intent intent = new Intent(getActivity(), VideoTrimmerActivity.class);
        intent.putExtra(EXTRA_VIDEO_PATH, FileUtils.getPath(getActivity(), uri));
        startActivityForResult(intent, REQUEST_VIDEO_TRIMMER_RESULT);
    }





    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }

   public void flipping(String  imagePath)
   {
       int orientation = -1;

       ExifInterface exif = null;
       try {
           exif = new ExifInterface(imagePath);
       } catch (IOException e) {
           e.printStackTrace();
       }

       int exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
               ExifInterface.ORIENTATION_NORMAL);

       switch (exifOrientation) {
           case ExifInterface.ORIENTATION_ROTATE_270:
               orientation = 270;

               break;
           case ExifInterface.ORIENTATION_ROTATE_180:
               orientation = 180;

               break;
           case ExifInterface.ORIENTATION_ROTATE_90:
               orientation = 90;

               break;

           case ExifInterface.ORIENTATION_NORMAL:
               orientation = 0;

               break;
           default:
               break;

           // encoding();
       }
   }


    //getting path from gallery pics for sending on server
    public  String getPathFromInputStreamUri(Context context, Uri uri) {
        InputStream inputStream = null;
        String filePath = null;

        if (uri.getAuthority() != null) {
            try {
                inputStream = context.getContentResolver().openInputStream(uri);
                File photoFile = createTemporalFileFrom(inputStream);

                filePath = photoFile.getPath();

            } catch (FileNotFoundException e) {
                //  HttpLoggingInterceptor.Logger.printStackTrace(e);
            } catch (IOException e) {
                //   Logger.printStackTrace(e);
            } finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return filePath;
    }

    private  File createTemporalFileFrom(InputStream inputStream) throws IOException {
        File targetFile = null;

        if (inputStream != null) {
            int read;
            byte[] buffer = new byte[8 * 1024];

            targetFile = createTemporalFile();
            OutputStream outputStream = new FileOutputStream(targetFile);

            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            outputStream.flush();

            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return targetFile;
    }
    static int countpic=0;
    private  File createTemporalFile() {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "_" + timeStamp + "newAndroid"+countpic++;
        return new File(getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES), imageFileName+extensionType);
    }



    public static Bitmap getCameraPhotoOrientation(Bitmap b,int widht,int height){
        Matrix matrix = new Matrix();

        matrix.postRotate(90);

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, widht, height, true);

        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
    return rotatedBitmap;
    }
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public File saveBitmapToFile(File file){
        try {
            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }
            Log.e("SizeOf",scale+"");

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }



    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 1, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "image1", null);
        return Uri.parse(path);
    }
    public Uri getImageUri1(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 1, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "image1", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri contentUri) {
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return "";
        }
    }

   /* public String getRealPathFromURI(Uri uri) {
        String path = "";
        if (getActivity().getContentResolver() != null) {
            Cursor cursor =getActivity(). getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        }
        return path;
    }*/

    public void getImages(Intent data,int requestCode,int resultCode ) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};



            imagesEncodedList = new ArrayList<String>();
            if (data.getData() != null) {

                Uri mImageUri = data.getData();

                // Get the cursor
                Cursor cursor = getActivity().getContentResolver().query(mImageUri,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imageEncoded = cursor.getString(columnIndex);
                //Log.e("OneImages=",imageEncoded);
                cursor.close();
                imageList.add(getPath(getContext(),mImageUri));
          /*  ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
            mArrayUri.add(mImageUri);
            galleryAdapter = new GalleryAdapter(getContext(), mArrayUri);
            gvGallery.setAdapter(galleryAdapter);
            gvGallery.setVerticalSpacing(gvGallery.getHorizontalSpacing());
            ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) gvGallery
                    .getLayoutParams();
            mlp.setMargins(0, gvGallery.getHorizontalSpacing(), 0, 0);*/

            } else {
                if (data.getClipData() != null) {
                    ClipData mClipData = data.getClipData();
                    ArrayList<Uri> mArrayUri = new ArrayList<Uri>();
                    for (int i = 0; i < mClipData.getItemCount(); i++) {

                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();
                        imageList.add(getPath(getContext(),uri));
                        Log.e("pathis=",getPath(getContext(),uri)+"    "+imageList.size());

                        mArrayUri.add(uri);

                        // Get the cursor
                        Cursor cursor =getActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
                        // Move to first row
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        imageEncoded = cursor.getString(columnIndex);
                        imagesEncodedList.add(imageEncoded);
                        cursor.close();



                    }
                    //Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
                }
            }

            progressBar.setVisibility(View.VISIBLE);
            //RetrofitHelper.getInstance().setMultipleImages(multipleImageMainCallback, imageList, userId);
            Log.e("userImagePath=", imageList.size()+"");

        } catch (Exception e) {
            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }


    }

    Callback<MultipleImageMain> multipleImageMainCallback = new Callback<MultipleImageMain>() {
        @Override
        public void onResponse(Call<MultipleImageMain> call, Response<MultipleImageMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
             Snackbar.make(imgView_uploadImages,response.body().getMessage(),Snackbar.LENGTH_LONG).show();

            }

        }

        @Override
        public void onFailure(Call<MultipleImageMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };




    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
//    public static boolean isExternalStorageDocument(Uri uri) {
//        return "com.android.externalstorage.documents".equals(uri.getAuthority());
//    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
//    public static boolean isDownloadsDocument(Uri uri) {
//        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
//    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
//    public static boolean isMediaDocument(Uri uri) {
//        return "com.android.providers.media.documents".equals(uri.getAuthority());
//    }

//    public static String getPath(final Context context, final Uri uri) {
//
//        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
//
//// DocumentProvider
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
//// ExternalStorageProvider
//                if (isExternalStorageDocument(uri)) {
//                    final String docId = DocumentsContract.getDocumentId(uri);
//                    final String[] split = docId.split(":");
//                    final String type = split[0];
//
//                    if ("primary".equalsIgnoreCase(type)) {
//                        return Environment.getExternalStorageDirectory() + "/" + split[1];
//                    }
//
//// TODO handle non-primary volumes
//                }
//// DownloadsProvider
//                else if (isDownloadsDocument(uri)) {
//
//                    final String id = DocumentsContract.getDocumentId(uri);
//                    final Uri contentUri = ContentUris.withAppendedId(
//                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//
//                    return getDataColumn(context, contentUri, null, null);
//                }
//// MediaProvider
//                else if (isMediaDocument(uri)) {
//                    final String docId = DocumentsContract.getDocumentId(uri);
//                    final String[] split = docId.split(":");
//                    final String type = split[0];
//
//                    Uri contentUri = null;
//                    if ("image".equals(type)) {
//                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                    } else if ("video".equals(type)) {
//                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//                    } else if ("audio".equals(type)) {
//                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//                    }
//
//                    final String selection = "_id=?";
//                    final String[] selectionArgs = new String[] {
//                            split[1]
//                    };
//
//                    return getDataColumn(context, contentUri, selection, selectionArgs);
//                }
//            }
//// MediaStore (and general)
//            else if ("content".equalsIgnoreCase(uri.getScheme())) {
//                return getDataColumn(context, uri, null, null);
//            }
//// File
//            else if ("file".equalsIgnoreCase(uri.getScheme())) {
//                return uri.getPath();
//            }
//        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
//            return getDataColumn(context, uri, null, null);
//        }
//// File
//        else if ("file".equalsIgnoreCase(uri.getScheme())) {
//            return uri.getPath();
//        }
//
//        return null;
//    }
}