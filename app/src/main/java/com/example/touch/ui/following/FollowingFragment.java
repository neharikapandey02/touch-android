package com.example.touch.ui.following;

import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.adapters.FollowingListAdapter;
import com.example.touch.adapters.LikeComFollowAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.constants.SingleShotLocationProvider;
import com.example.touch.models.FollowingMain;
import com.example.touch.models.FollowingMainData;
import com.example.touch.models.LikeCommentFollowMain;
import com.example.touch.models.LikeCommentFollowMainData;
import com.example.touch.models.ReadNotificationMain;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowingFragment extends Fragment {

    private FollowingViewModel mViewModel;

    @BindView(R.id.recyclerView_Following)
    RecyclerView recyclerView_Following;


    @BindView(R.id.recyclerView_LCF)
    RecyclerView recyclerView_LCF;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private static int PERMISSION_ACCESS_FINE_LOCATION=104;
    private SharedPreferencesData sharedPreferencesData;
    private float latitude;
    private float longitude;
    private String userId;
    BottomNavigationView navView;

    public static FollowingFragment newInstance() {
        return new FollowingFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.following_fragment, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(FollowingViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewFinds();
        Log.e("callPage","callpage");
    }

    private void viewFinds() {
        navView=getView().getRootView().findViewById(R.id.nav_view);
        recyclerView_LCF.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView_Following.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView_LCF.setNestedScrollingEnabled(false);
        recyclerView_Following.setNestedScrollingEnabled(false);

        sharedPreferencesData=new SharedPreferencesData(getContext());
        userId=sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            SingleShotLocationProvider.requestSingleUpdate(getContext(),
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            Log.e("Location", "my location is " + location.latitude+"     "+location.longitude);
                            latitude=location.latitude;
                            longitude=location.longitude;

                        }
                    });

            if (NetworkUtil.checkNetworkStatus(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getFollowingList(followingListCallback,latitude+"",longitude+"",userId);

            }else{
                Snackbar.make(recyclerView_Following,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
            }


        } else {
            requestGallery();
        }
        if (NetworkUtil.checkNetworkStatus(getContext())) {
           // progressBar.setVisibility(View.VISIBLE);
            //0 for no unread data 1 for some unread data
            RetrofitHelper.getInstance().getStatusTrue(statusTrueCallabck,userId);

        }else{
            Snackbar.make(recyclerView_Following,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
        }

    }


    public void requestGallery() {
        Log.e("requestGallery","requestGallery");
        requestPermissions(new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_FINE_LOCATION);


    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_ACCESS_FINE_LOCATION) {
            SingleShotLocationProvider.requestSingleUpdate(getContext(),
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            Log.e("Location", "mylocationis1" + location.latitude+"     "+location.longitude);
                            latitude=location.latitude;
                            longitude=location.longitude;
                        }
                    });

            if (NetworkUtil.checkNetworkStatus(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getFollowingList(followingListCallback,latitude+"",longitude+"",userId);

            }else{
                Snackbar.make(recyclerView_Following,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
            }
        }  else {

            Log.e("cancelPermission","cancelPermission");
            latitude=0;
            longitude=0;
            if (NetworkUtil.checkNetworkStatus(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getFollowingList(followingListCallback,latitude+"",longitude+"",userId);

            }else{
                Snackbar.make(recyclerView_Following,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG).show();
            }
            Toast.makeText(getContext(), getResources().getString(R.string.nearby), Toast.LENGTH_SHORT).show();
        }
    }

    List<FollowingMainData> followingMainData1;
    Callback<FollowingMain> followingListCallback=new Callback<FollowingMain>() {
        @Override
        public void onResponse(Call<FollowingMain> call, Response<FollowingMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                followingMainData1 = new ArrayList<>();
                followingMainData1.clear();
                if (response.body().getResult() != null) {

                    followingMainData1.addAll(response.body().getResult());
                }
                if (userId != null && !userId.equalsIgnoreCase("")) {
                    if (NetworkUtil.checkNetworkStatus(getContext())) {
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().getLikeFollowComment(lcfCallback, userId);

                    } else {
                        Snackbar.make(navView, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    if (response.body().getResult() != null) {
                        followingMainData1.addAll(response.body().getResult());
                        FollowingListAdapter followingListAdapter = new FollowingListAdapter(getContext(), followingMainData1, progressBar, true);
                        recyclerView_Following.setAdapter(followingListAdapter);
                    } else {
                        Toast.makeText(getContext(), getResources().getString(R.string.no_nearby), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        @Override
        public void onFailure(Call<FollowingMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        navView = getView().getRootView().findViewById(R.id.nav_view);
        navView.removeBadge(R.id.navigation_likes);
        //Menu menu = navView.getMenu();
        //menu.findItem(R.id.navigation_likes).setIcon(getResources().getDrawable(R.drawable.heart_iocn));

    }

    Callback<LikeCommentFollowMain> lcfCallback=new Callback<LikeCommentFollowMain>(){
        @Override
        public void onResponse(Call<LikeCommentFollowMain> call, Response<LikeCommentFollowMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){
                List<LikeCommentFollowMainData> followingMainData=new ArrayList<>();
                followingMainData.clear();
                if (response.body().getResult()!=null){
                    followingMainData.addAll(response.body().getResult());
                    LikeComFollowAdapter followingListAdapter = new LikeComFollowAdapter(getContext(),followingMainData,progressBar,true);
                    recyclerView_LCF.setAdapter(followingListAdapter);
                    FollowingListAdapter followingListAdapter1 = new FollowingListAdapter(getContext(),followingMainData1,progressBar,true);
                    recyclerView_Following.setAdapter(followingListAdapter1);
                        //followingMainData1.addAll(response.body().getResult());

                    }else {
                    FollowingListAdapter followingListAdapter1 = new FollowingListAdapter(getContext(),followingMainData1,progressBar,true);
                    recyclerView_Following.setAdapter(followingListAdapter1);
                        //Toast.makeText(getContext(), getResources().getString(R.string.no_nearby), Toast.LENGTH_SHORT).show();
                    }
                }else {
                    //Toast.makeText(getContext(), getResources().getString(R.string.no_nearby), Toast.LENGTH_SHORT).show();
                }


        };

        @Override
        public void onFailure(Call<LikeCommentFollowMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };



    Callback<ReadNotificationMain> statusTrueCallabck=new Callback<ReadNotificationMain>(){

        @Override
        public void onResponse(Call<ReadNotificationMain> call, Response<ReadNotificationMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()){


            }
        }

        @Override
        public void onFailure(Call<ReadNotificationMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    TextView textCartItemCount;
    int mCartItemCount = 10;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
       getActivity().getMenuInflater().inflate(R.menu.bottom_nav_menu, menu);

        final MenuItem menuItem = menu.findItem(R.id.navigation_likes);

        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });


    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                textCartItemCount.setVisibility(View.VISIBLE);
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
