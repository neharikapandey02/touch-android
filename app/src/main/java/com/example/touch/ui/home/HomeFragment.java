package com.example.touch.ui.home;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.BlockUserListActivity;
import com.example.touch.activities.ChangePassword;
import com.example.touch.activities.ChatListActivity;
import com.example.touch.activities.LoginActivity;
import com.example.touch.activities.PrivacyPolicyActivity;
import com.example.touch.activities.StripePaymentActivity;
import com.example.touch.adapters.HomeNameAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.GPSTracker;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.constants.SingleShotLocationProvider;
import com.example.touch.forvideofiles.DirectLinkVideoItem;
import com.example.touch.models.DeleteMain;
import com.example.touch.models.FollowersListMain;
import com.example.touch.models.FollowersListMainData;
import com.example.touch.models.HomeMain;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.ProfileVisibleResponse;
import com.example.touch.models.ReadChatCheckMain;
import com.example.touch.models.StatusMain;
import com.example.touch.models.UpdatePhoneSuccess;
import com.example.touch.video_pk.SimpleAdapter1;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.RecyclerViewItemPositionGetter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import im.ene.toro.widget.Container;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    @BindView(R.id.recyclerView_HomeName)
    RecyclerView recyclerView_HomeName;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.settingImg)
    ImageView settingImg;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.recyclerView_ProfileDetails)
    RecyclerView recyclerView_ProfileDetails;

    @BindView(R.id.container)
    Container container;
    private LinearLayoutManager layoutManager;

    @BindView(R.id.loginLL)
    LinearLayout loginLL;
    int profileStatus = 0;

    @BindView(R.id.imgView_ChatList)
    ru.nikartm.support.ImageBadgeView imgView_ChatList;

    private static int PERMISSION_ACCESS_FINE_LOCATION=103;
    BottomNavigationView navView;
    private SharedPreferencesData sharedPreferencesData;
    private String userId;
    private ArrayList<DirectLinkVideoItem> mList = new ArrayList<>();

    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    int page = 1;
    private List<HomeMainData> followersListMainData = new ArrayList<>();
    private SimpleAdapter1 simpleAdapter;



    ListItemsVisibilityCalculator mVideoVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);

    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;

    /**
     * ItemsPositionGetter is used by {@link ListItemsVisibilityCalculator} for getting information about
     * items position in the RecyclerView and LayoutManager
     */
    private ItemsPositionGetter mItemsPositionGetter;

    /**
     * Here we use {@link SingleVideoPlayerManager}, which means that only one video playback is possible.
     */
    private final VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {

        }
    });

    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());
        container.setLayoutManager(layoutManager);
        container.setNestedScrollingEnabled(false);

        viewFInds();


    }

    private void viewFInds() {

    }
    Activity activity = getActivity();
    MediaController mc;
    @Override
    public void onResume() {
        super.onResume();

        mc= new MediaController(getActivity());
        recyclerView_HomeName.setHasFixedSize(true);
        recyclerView_HomeName.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        sharedPreferencesData = new SharedPreferencesData(getContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        if (userId == null || userId.equalsIgnoreCase("")) {
            settingImg.setVisibility(View.GONE);
            loginLL.setVisibility(View.VISIBLE);
        }else {
            loginLL.setVisibility(View.GONE);
        }
        recyclerView_ProfileDetails.setHasFixedSize(true);
        recyclerView_ProfileDetails.setItemViewCacheSize(20);
        recyclerView_ProfileDetails.setDrawingCacheEnabled(true);
        recyclerView_ProfileDetails.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        mLayoutManager=new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView_ProfileDetails.setLayoutManager(mLayoutManager);

        if (NetworkUtil.checkNetworkStatus(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getFollowersUser(followersListCallback, userId);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }


        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtil.checkNetworkStatus(getContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    page = 1;
                    RetrofitHelper.getInstance().getProfileList(profileListCallback, userId, "5", page);

                } else {
                    Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        if (NetworkUtil.checkNetworkStatus(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getProfileList(profileListCallback, userId, "2", page);

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }


        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            GPSTracker gpsTracker=new GPSTracker(getContext());
            String searchLat=gpsTracker.getLatitude()+"";
            String searchLang=gpsTracker.getLongitude()+"";
            Log.e("Location", "myLocation" + searchLat+"     "+searchLang);


            if (NetworkUtil.checkNetworkStatus(getContext())) {
                RetrofitHelper.getInstance().updateUserLatLng(updateLatLngCAllback, userId, searchLat, searchLang);

            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }

        } else {
            requestGallery();
        }



//        callEvery3SecondForLatestChat();




        //if (activity != null) {
            navView = getView().getRootView().findViewById(R.id.nav_view);

            if (NetworkUtil.checkNetworkStatus(getContext())) {
                //progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getStatusOfChat(statusChatCallback, userId);

            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
            String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);


            if (userId != null && !userId.equalsIgnoreCase("")) {
                if (NetworkUtil.checkNetworkStatus(getContext())) {
                    //progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().getstatus(getLikeCommentStatus, userId);

                } else {
                    Snackbar.make(navView, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }


        container.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            Log.e("dfdfd", "Last Item Wow !");
                            if (NetworkUtil.checkNetworkStatus(getContext())) {
                                progressBar.setVisibility(View.VISIBLE);
                                RetrofitHelper.getInstance().getProfileList(profileListCallback1, userId, "5", page);

                            } else {
                                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                            }
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });


        loginLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });

        settingImg.setOnClickListener(view -> {

            Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.profile_menu_layout);
            LinearLayout blockUserTv = dialog.findViewById(R.id.blockUserTv);
            LinearLayout logoutTv = dialog.findViewById(R.id.logoutTv);
            LinearLayout boosterPlanLL = dialog.findViewById(R.id.boosterPlanLL);
            LinearLayout changePasswordLL = dialog.findViewById(R.id.changePasswordLL);
            LinearLayout notificationLL = dialog.findViewById(R.id.notificationLL);
            LinearLayout profileLL = dialog.findViewById(R.id.profileLL);
            LinearLayout privacyPolicyTv = dialog.findViewById(R.id.privacyPolicyTv);
            LinearLayout deleteAccountLL = dialog.findViewById(R.id.deleteAccountLL);
            Switch notificationSwitch = dialog.findViewById(R.id.notificationSwitch);
            Switch profileSwitch = dialog.findViewById(R.id.profileSwitch);

            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_TYPE).equalsIgnoreCase("1")){
                boosterPlanLL.setVisibility(View.GONE);
                notificationLL.setVisibility(View.GONE);
                profileLL.setVisibility(View.GONE);

            }


            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_STATUS).equalsIgnoreCase("1")) {
                profileStatus = 1;
                profileSwitch.setChecked(true);
            } else {
                profileSwitch.setChecked(false);
                profileStatus = 0;
            }

                if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.NOTIFICATION).equalsIgnoreCase("on")) {
                    notificationSwitch.setChecked(true);
                } else {
                    notificationSwitch.setChecked(false);
                }
            notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.NOTIFICATION, "on");
                    }else {
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.NOTIFICATION, "off");
                    }
                }
            });


            profileSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        if (NetworkUtil.checkNetworkStatus(getContext())) {
                            profileStatus = 1;
                            RetrofitHelper.getInstance().profileVisible(profileVisible, userId, "1");
                        }
                    }else {
                        if (NetworkUtil.checkNetworkStatus(getContext())) {
                            profileStatus = 0;
                            RetrofitHelper.getInstance().profileVisible(profileVisible, userId, "0");
                        }
                    }
                }
            });

            deleteAccountLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getString(R.string.app_name));
                    alert.setMessage("Do you want to delete your account?");
                    alert.setCancelable(false);
                    alert.setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> {
                        if (NetworkUtil.checkNetworkStatus(getContext())) {
                            RetrofitHelper.getInstance().deleteAccount(deleteAccountCallback, userId);
                        }

                    });
                    alert.setNegativeButton(getString(R.string.no), (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    });
                    alert.show();
                }
            });

            privacyPolicyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), PrivacyPolicyActivity.class));
                }
            });

            changePasswordLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ChangePassword.class);
                    startActivity(intent);
                }
            });

            boosterPlanLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), StripePaymentActivity.class);
                    startActivity(intent);
                }
            });

            logoutTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getString(R.string.app_name));
                    alert.setMessage(getString(R.string.do_you_want_logout));
                    alert.setCancelable(false);
                    alert.setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> {
                        sharedPreferencesData.clearSharedPreferenceData(Constants.USER_LOGIN_SHARED);
                        getActivity().finishAffinity();
                    });
                    alert.setNegativeButton(getString(R.string.no), (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    });
                    alert.show();
                }
            });
            blockUserTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), BlockUserListActivity.class));
                    dialog.dismiss();
                }
            });
            dialog.show();

        });

    }

    Callback<DeleteMain> updateLatLngCAllback = new Callback<DeleteMain>() {
        @Override
        public void onResponse(Call<DeleteMain> call, Response<DeleteMain> response) {

        }

        @Override
        public void onFailure(Call<DeleteMain> call, Throwable t) {

        }
    };

    public void requestGallery() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_FINE_LOCATION);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_ACCESS_FINE_LOCATION) {
            SingleShotLocationProvider.requestSingleUpdate(getContext(),
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            Log.e("Location", "my location is " + location.latitude+"     "+location.longitude);
                            String lat = location.latitude+"";
                            String lng =location.longitude+"";

                        }
                    });
        }
    }
    private Handler m_Handler;
    private Runnable mRunnable;
    private void callEvery3SecondForLatestChat() {
        m_Handler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                // Log.e("handlerCheck=","handler");
                m_Handler.postDelayed(mRunnable, 3000);// move this inside the run method
                if (userId != null && !userId.equalsIgnoreCase("")) {
                    if (NetworkUtil.checkNetworkStatus(getContext())) {
                        //progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().getstatus(getLikeCommentStatus, userId);
                        RetrofitHelper.getInstance().getStatusOfChat(statusChatCallback, userId);
                    } else {
                        //Snackbar.make(navView, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        };
        mRunnable.run(); // missing
    }

    Callback<ProfileVisibleResponse> profileVisible = new Callback<ProfileVisibleResponse>() {
        @Override
        public void onResponse(Call<ProfileVisibleResponse> call, Response<ProfileVisibleResponse> response) {

            if (response.body().getSuccess() == 1){
                if (profileStatus == 1) {
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_STATUS, "1");
                }else {
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_STATUS, "0");

                }
            }
        }

        @Override
        public void onFailure(Call<ProfileVisibleResponse> call, Throwable t) {

        }
    };

    Callback<UpdatePhoneSuccess> deleteAccountCallback = new Callback<UpdatePhoneSuccess>() {
        @Override
        public void onResponse(Call<UpdatePhoneSuccess> call, Response<UpdatePhoneSuccess> response) {
            try {
                if (response.body().getSuccess() ==1){
                    sharedPreferencesData.clearSharedPreferenceData(Constants.USER_LOGIN_SHARED);
                    getActivity().finishAffinity();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<UpdatePhoneSuccess> call, Throwable t) {

        }
    };

    Callback<StatusMain> getLikeCommentStatus=new Callback<StatusMain>(){

        @Override
        public void onResponse(Call<StatusMain> call, Response<StatusMain> response) {
            if (response.isSuccessful()){
                if (response.body().getStatus().toString().equalsIgnoreCase("0")){
                    navView.removeBadge(R.id.navigation_likes);
                }else{
                    navView.getOrCreateBadge(R.id.navigation_likes).setNumber(response.body().getCount());
                    //Menu menu = navView.getMenu();
                   // menu.findItem(R.id.navigation_likes).setIcon(getResources().getDrawable(R.drawable.hart_icon_notification));
                }
            }


        }

        @Override
        public void onFailure(Call<StatusMain> call, Throwable t) {
            //progressBar.setVisibility(View.GONE);
        }
    };
    Callback<FollowersListMain> followersListCallback = new Callback<FollowersListMain>() {

        @Override
        public void onResponse(Call<FollowersListMain> call, Response<FollowersListMain> response) {
            progressBar.setVisibility(View.GONE);
            List<FollowersListMainData> followersListMainData = new ArrayList<>();
            followersListMainData.clear();

            if (response.body()!=null)
            if (response.body().getResult() != null) {
                followersListMainData.addAll(response.body().getResult());
                HomeNameAdapter homeNameAdapter = new HomeNameAdapter(getContext(), followersListMainData);
                recyclerView_HomeName.setAdapter(homeNameAdapter);
            }

        }

        @Override
        public void onFailure(Call<FollowersListMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<HomeMain> profileListCallback = new Callback<HomeMain>() {

        @Override
        public void onResponse(Call<HomeMain> call, Response<HomeMain> response) {
            progressBar.setVisibility(View.GONE);
            if (swipeRefresh.isRefreshing()){
                swipeRefresh.setRefreshing(false);
            }

//            followersListMainData.clear();
            if (response.isSuccessful()) {
                if (response.body().getResult() != null) {
                    followersListMainData.clear();
                    followersListMainData.addAll(response.body().getResult());

//                    for (int i=0;i<response.body().getResult().size();i++){
//
//                        if (!response.body().getResult().get(i).getType().equalsIgnoreCase("image")) {
//                            followersListMainData.add(response.body().getResult().get(i));
//                        }
//                    }

//                    HomeProfileAdapter homeProfileAdapter = new HomeProfileAdapter(getContext(), followersListMainData,mVideoPlayerManager, getActivity(),mList);
//                    HomeProfileAdapter homeProfileAdapter = new HomeProfileAdapter(getContext(), followersListMainData,mc);
//                    recyclerView_ProfileDetails.setAdapter(homeProfileAdapter);

                    simpleAdapter = new SimpleAdapter1(getContext(), followersListMainData);
                    container.setAdapter(simpleAdapter);
                    page=page+1;
                    recyclerView_ProfileDetails.addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                            mScrollState = scrollState;
                            if(scrollState == RecyclerView.SCROLL_STATE_IDLE && !mList.isEmpty()){

                                mVideoVisibilityCalculator.onScrollStateIdle(
                                        mItemsPositionGetter,
                                        mLayoutManager.findFirstVisibleItemPosition(),
                                        mLayoutManager.findLastVisibleItemPosition());
                            }
                        }

                        /*@Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            if(!mList.isEmpty()&&mVideoVisibilityCalculator!=null){
                                mVideoVisibilityCalculator.onScroll(
                                        mItemsPositionGetter,
                                        mLayoutManager.findFirstVisibleItemPosition(),
                                        mLayoutManager.findLastVisibleItemPosition() - mLayoutManager.findFirstVisibleItemPosition() + 1,
                                        mScrollState);
                            }
                        }*/
                    });
                    mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, recyclerView_ProfileDetails);

                }
            }
        }

        @Override
        public void onFailure(Call<HomeMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<HomeMain> profileListCallback1 = new Callback<HomeMain>() {
        @Override
        public void onResponse(Call<HomeMain> call, Response<HomeMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getResult() != null) {
                    followersListMainData.addAll(response.body().getResult());

                    simpleAdapter.notifyDataSetChanged();
                    loading = true;
                    ++page;
                    Log.e("page", "" + page);
                }
            }
        }
        @Override
        public void onFailure(Call<HomeMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };



    @Override
    public void onStop() {
        super.onStop();
//        m_Handler.removeCallbacksAndMessages(null);
        // we have to stop any playback in onStop
        mVideoPlayerManager.resetMediaPlayer();
    }



    Callback<ReadChatCheckMain> statusChatCallback = new Callback<ReadChatCheckMain>() {
        @Override
        public void onResponse(Call<ReadChatCheckMain> call, Response<ReadChatCheckMain> response) {
            //progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getStatus().toString().equalsIgnoreCase("0")) {
                    imgView_ChatList.clearBadge();
                    imgView_ChatList.visibleBadge(false);
                } else {
                    imgView_ChatList.visibleBadge(true);
                    imgView_ChatList.setBadgeValue(response.body().getCount());
                }
            }
        }
        @Override
        public void onFailure(Call<ReadChatCheckMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.imgView_ChatList)
    public void chatListImageClick(View view) {
        sharedPreferencesData = new SharedPreferencesData(getContext());
        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
            Intent intent = new Intent(getContext(), ChatListActivity.class);
            startActivity(intent);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.please_login_to_chat_list), Snackbar.LENGTH_LONG).show();
        }

    }
}