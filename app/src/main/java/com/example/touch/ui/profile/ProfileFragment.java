package com.example.touch.ui.profile;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.emoji.widget.EmojiTextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.BlockUserListActivity;
import com.example.touch.activities.ChangePassword;
import com.example.touch.activities.ChatActivity;
import com.example.touch.activities.ChatListActivity;
import com.example.touch.activities.EditProfileActivity;
import com.example.touch.activities.FollowersActivity;
import com.example.touch.activities.FollowingActivity;
import com.example.touch.activities.LoginActivity;
import com.example.touch.activities.PriceListActivity;
import com.example.touch.activities.PrivacyPolicyActivity;
import com.example.touch.activities.StripePaymentActivity;
import com.example.touch.activities.TipsActivity;
import com.example.touch.adapters.AddAnotherImageListAdapter1;
import com.example.touch.adapters.AddImagesListAdapter;
import com.example.touch.adapters.SearchLatestPostAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.models.AnotherUserProfileResult;
import com.example.touch.models.CountListFollowIngPost;
import com.example.touch.models.DeleteAudioResponse;
import com.example.touch.models.DeleteMain;
import com.example.touch.models.DisplayPhoneSuccess;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.MultipleImageGetMain;
import com.example.touch.models.MultipleImageGetMainData;
import com.example.touch.models.PackageMainData;
import com.example.touch.models.ProfileLikeSuccess;
import com.example.touch.models.ProfileVisibleResponse;
import com.example.touch.models.ReadChatCheckMain;
import com.example.touch.models.UpdatePhoneSuccess;
import com.example.touch.models.UserProfileStatus;
import com.example.touch.utility.GlideCacheEngine;
import com.example.touch.utility.GlideEngine;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;
import com.luck.picture.lib.entity.LocalMedia;
import com.luck.picture.lib.instagram.InsGallery;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    private ProfileViewModel mViewModel;

    Switch profileSwitch;
    @BindView(R.id.btn_price)
    ImageView btn_price;

    @BindView(R.id.btn_changePassword)
    Button btn_changePassword;

    @BindView(R.id.deleteAudioTv)
    TextView deleteAudioTv;

    @BindView(R.id.tv_firstName)
    TextView tv_firstName;


    @BindView(R.id.countryLL)
    LinearLayout countryLL;
@BindView(R.id.linearLayout_post)
    LinearLayout linearLayout_post;
@BindView(R.id.top_editLayout)
    LinearLayout top_editLayout;

    @BindView(R.id.imgv_top)
    ImageView imgv_top;


    @BindView(R.id.circularImageView_UserImage)
    ImageView circularImageView_UserImage;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.tv_totalFollowing)
    TextView tv_totalFollowing;

    List<AnotherUserProfileResult> multipleImageGetMainData;
    AddImagesListAdapter addImagesListAdapter;
    @BindView(R.id.tv_post)
    TextView tv_post;

    @BindView(R.id.tv_totalFollowers)
    TextView tv_totalFollowers;


    @BindView(R.id.tv_categoryType)
    TextView tv_categoryType;
    @BindView(R.id.chatImg)
    ru.nikartm.support.ImageBadgeView chatImg;


    GridLayoutManager gridLayoutManager;
    @BindView(R.id.tv_aboutUs)
    EmojiTextView tv_aboutUs;

    @BindView(R.id.tv_country)
    TextView tv_country;

     @BindView(R.id.onlineTv)
    TextView onlineTv;

    @BindView(R.id.imgv_location)
    ImageView imgv_location;

    private SharedPreferencesData sharedPreferencesData;
    int profileStatus = 0;
    private String userId = "";

    @BindView(R.id.btn_updateProfile)
    ImageView btn_updateProfile;

    @BindView(R.id.btn_loginProfile)
    Button btn_loginProfile;

    @BindView(R.id.recyclerView_UploadedImages)
    RecyclerView recyclerView_UploadedImages;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean loading = true;

    int page = 1;
    @BindView(R.id.linearLayout_Following)
    LinearLayout linearLayout_Following;

    @BindView(R.id.linearLayout_Followers)
    LinearLayout linearLayout_Followers;

    @BindView(R.id.audioImg)
    ImageView audioImg;

    @BindView(R.id.btn_call)
    ImageView btn_call;

    @BindView(R.id.settingImg)
    ImageView settingImg;

    @BindView(R.id.profileLikeImg)
    ImageView profileLikeImg;
    String likeStatus = "";


//    @BindView(R.id.layoutLinear_Information)
//    LinearLayout layoutLinear_Information;

    View view;
    MediaPlayer mediaPlayer;

    BottomSheetBehavior mBottomSheetBehaviour;
    MediaController mc;
    String playStatus = "0";

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        sharedPreferencesData = new SharedPreferencesData(getContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {

        }else {
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }



        view = inflater.inflate(R.layout.profile_fragment, container, false);
        ButterKnife.bind(this, view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);
        if (userId == null || userId.equalsIgnoreCase("")) {
            settingImg.setVisibility(View.GONE);
        }else {
            settingImg.setVisibility(View.VISIBLE);
        }
        // TODO: Use the ViewModel
    }

    @Override
    public void onResume() {
        super.onResume();
        mc= new MediaController(getActivity());
        sharedPreferencesData = new SharedPreferencesData(getContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        //Log.e("OnResupme=",sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED,Constants.ID));
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            String firstname = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.FIRST_NAME);
            tv_firstName.setText(firstname);
            String userImage = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);
            String userCategory = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.CATEGORY_NAME);
            String userAboutUs = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ABOUT_US);
            String userCountry = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.COUNTRY_NAME);
            String userCity = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.CITY_NAME);
            String audioFile = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.AUDIO);
            if (audioFile == null || audioFile.equalsIgnoreCase("")){
                audioImg.setBackgroundColor(getResources().getColor(R.color.grey));
                deleteAudioTv.setVisibility(View.GONE);
            }
            else {
                audioImg.setVisibility(View.VISIBLE);
                deleteAudioTv.setVisibility(View.VISIBLE);
            }

            deleteAudioTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (NetworkUtil.checkNetworkStatus(getActivity().getApplicationContext())) {
                            progressBar.setVisibility(View.VISIBLE);
                            RetrofitHelper.getInstance().deleteAudio(deleteAudioCallback, userId);

                    } else {
                        Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            audioImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (audioFile != null && !audioFile.equalsIgnoreCase("")) {
                        if (playStatus.equalsIgnoreCase("0")) {
                            mediaPlayer = new MediaPlayer();
                            try {
//                            Log.e("audioF", audioFile);
                                mediaPlayer.setDataSource(audioFile);
                                mediaPlayer.prepare();
                                mediaPlayer.start();
                                playStatus = "1";
                                Toast.makeText(getActivity(), "Playing Audio", Toast.LENGTH_LONG).show();

                                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                    @Override
                                    public void onCompletion(MediaPlayer mediaPlayer) {
                                        mediaPlayer.stop();
                                        playStatus = "0";
                                    }
                                });
                            } catch (Exception e) {
                                // make something
                                e.printStackTrace();
                            }
                        } else {
                            mediaPlayer.stop();
                            playStatus = "0";
                            Toast.makeText(getActivity(), "Stop Audio", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Toast.makeText(getActivity(), "Audio not found. Please record audio from edit profile page.", Toast.LENGTH_LONG).show();
                    }

                }
            });

            profileLikeImg.setOnClickListener(view -> {
                if (NetworkUtil.checkNetworkStatus(getActivity().getApplicationContext())) {
                    if (likeStatus.equalsIgnoreCase("0")) {
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().doLikeOtherProfile(likeCallback, userId, "1", userId);
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                        RetrofitHelper.getInstance().doLikeOtherProfile(likeCallback, userId, "0", userId);
                    }

                } else {
                    Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            });

//            settingImg.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Dialog dialog = new Dialog(getActivity());
//                    dialog.setContentView(R.layout.profile_menu_layout);
//                    TextView blockUserTv = dialog.findViewById(R.id.blockUserTv);
//                    blockUserTv.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            startActivity(new Intent(getActivity(), BlockUserListActivity.class));
//                            dialog.dismiss();
//                        }
//                    });
//                    dialog.show();
//                }
//            });
            //tv_categoryType.setText("#" + userCategory);
            //tv_aboutUs.setText("#" + userAboutUs);

            if (userCity != null && !userCity.equalsIgnoreCase("")) {
                tv_country.setText(userCity);
            }


            if (userCategory != null && !userCategory.equalsIgnoreCase("")) {
                tv_categoryType.setText("#" + userCategory);
            }

            if (userAboutUs != null && !userAboutUs.equalsIgnoreCase("")) {

                tv_aboutUs.setTextSize(13);
                //tv_aboutUs.setEmojiconSize(45);
                tv_aboutUs.setText(userAboutUs);
            }


            if (userImage != null && (!userImage.equalsIgnoreCase(""))) {
                Picasso.with(getContext())
                        .load(userImage)
                        .placeholder(R.drawable.profile_back)
                        .error(R.drawable.profile_back)
                        .into(imgv_top);
                Picasso.with(getContext())
                        .load(userImage)
                        .placeholder(R.drawable.profile_placeholder)
                        .error(R.drawable.profile_placeholder)
                        .into(circularImageView_UserImage);
            }
            if (!userId.equalsIgnoreCase("") && sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_TYPE).equalsIgnoreCase("2")) {
                btn_loginProfile.setVisibility(View.GONE);
//                layoutLinear_Information.setVisibility(View.GONE);
            } else {
                btn_loginProfile.setVisibility(View.GONE);
                btn_loginProfile.setText("LOG OUT");
            }

            if (NetworkUtil.checkNetworkStatus(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getFollowerIngCount(followersListCallback, userId);

            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }
            if (NetworkUtil.checkNetworkStatus(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getMultipleImage(getMultipleImageCallback, userId, "20", String.valueOf(page));

            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }

        } else {
            btn_loginProfile.setVisibility(View.VISIBLE);

//            layoutLinear_Information.setVisibility(View.GONE);
            imgv_top.setImageDrawable(getResources().getDrawable(R.drawable.profile_back));
            tv_firstName.setText("");
            tv_totalFollowing.setText("0");
            tv_totalFollowers.setText("0");
            tv_post.setText("0");
            tv_aboutUs.setText("");
            imgv_location.setVisibility(View.GONE);
            tv_categoryType.setText("");
            tv_country.setText("");
            circularImageView_UserImage.setImageDrawable(getResources().getDrawable(R.drawable.profile_placeholder));

//            List<MultipleImageGetMainData> multipleImageGetMainData = new ArrayList<>();
//            AddImagesListAdapter addImagesListAdapter = new AddImagesListAdapter(getContext(), multipleImageGetMainData, view, ProfileFragment.this,mc);
//            recyclerView_UploadedImages.setAdapter(addImagesListAdapter);

        }

        settingImg.setOnClickListener(view -> {

            Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.profile_menu_layout);
            LinearLayout blockUserTv = dialog.findViewById(R.id.blockUserTv);
            LinearLayout logoutTv = dialog.findViewById(R.id.logoutTv);
            LinearLayout boosterPlanLL = dialog.findViewById(R.id.boosterPlanLL);
            LinearLayout changePasswordLL = dialog.findViewById(R.id.changePasswordLL);
            LinearLayout notificationLL = dialog.findViewById(R.id.notificationLL);
            LinearLayout profileLL = dialog.findViewById(R.id.profileLL);
            LinearLayout tipsTv = dialog.findViewById(R.id.tipsTv);
            LinearLayout privacyPolicyTv = dialog.findViewById(R.id.privacyPolicyTv);
            LinearLayout deleteAccountLL = dialog.findViewById(R.id.deleteAccountLL);
            Switch notificationSwitch = dialog.findViewById(R.id.notificationSwitch);
            profileSwitch = dialog.findViewById(R.id.profileSwitch);

            tipsTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), TipsActivity.class));
                }
            });

            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_TYPE).equalsIgnoreCase("1")){
                boosterPlanLL.setVisibility(View.GONE);
                notificationLL.setVisibility(View.GONE);
                profileLL.setVisibility(View.GONE);
            }


            if (NetworkUtil.checkNetworkStatus(getContext())) {
                RetrofitHelper.getInstance().getUserProfileStatus(profileStatusCallback, userId);
            }





            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.NOTIFICATION).equalsIgnoreCase("on")) {
                notificationSwitch.setChecked(true);
            } else {
                notificationSwitch.setChecked(false);
            }
            notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.NOTIFICATION, "on");
                    }else {
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.NOTIFICATION, "off");
                    }
                }
            });


            profileSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        if (profileStatus ==0){

                            if (NetworkUtil.checkNetworkStatus(getContext())) {
                                profileStatus = 1;
                                RetrofitHelper.getInstance().profileVisible(profileVisible, userId, "1");
                            }
                        }
                    }else {
                        if (profileStatus ==1){
                        if (NetworkUtil.checkNetworkStatus(getContext())) {
                            profileStatus = 0;
                            RetrofitHelper.getInstance().profileVisible(profileVisible, userId, "0");
                        }
                        }
                    }
                }
            });

            deleteAccountLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getString(R.string.app_name));
                    alert.setMessage("Do you want to delete your account?");
                    alert.setCancelable(false);
                    alert.setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> {
                        if (NetworkUtil.checkNetworkStatus(getContext())) {
                            RetrofitHelper.getInstance().deleteAccount(deleteAccountCallback, userId);
                        }

                    });
                    alert.setNegativeButton(getString(R.string.no), (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    });
                    alert.show();
                }
            });

            privacyPolicyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), PrivacyPolicyActivity.class));
                }
            });

            changePasswordLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ChangePassword.class);
                    startActivity(intent);
                }
            });

            boosterPlanLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), StripePaymentActivity.class);
                    startActivity(intent);
                }
            });

            logoutTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                    alert.setTitle(getString(R.string.app_name));
                    alert.setMessage(getString(R.string.do_you_want_logout));
                    alert.setCancelable(false);
                    alert.setPositiveButton(getString(R.string.yes), (dialogInterface, i) -> {
                        if (NetworkUtil.checkNetworkStatus(getContext())) {
                            RetrofitHelper.getInstance().doLogout(logoutCallback, userId);
                        } else {
                            Snackbar.make(imgv_top, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                        }

                    });
                    alert.setNegativeButton(getString(R.string.no), (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    });
                    alert.show();
                }
            });
            blockUserTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), BlockUserListActivity.class));
                    dialog.dismiss();
                }
            });
            dialog.show();

        });

        if (NetworkUtil.checkNetworkStatus(getContext())) {
            //progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getStatusOfChat(statusChatCallback, userId);

        }
    }

    Callback<UserProfileStatus> profileStatusCallback = new Callback<UserProfileStatus>() {
        @Override
        public void onResponse(Call<UserProfileStatus> call, Response<UserProfileStatus> response) {

            try {
                if (response.body().getSuccess() ==1){
                    if (response.body().getProfile_status().equalsIgnoreCase("1")){
                        profileStatus = 1;
                        profileSwitch.setChecked(true);
                    }
                    else {
                        profileStatus = 0;
                        profileSwitch.setChecked(false);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        @Override
        public void onFailure(Call<UserProfileStatus> call, Throwable t) {

        }
    };
    Callback<ReadChatCheckMain> statusChatCallback = new Callback<ReadChatCheckMain>() {
        @Override
        public void onResponse(Call<ReadChatCheckMain> call, Response<ReadChatCheckMain> response) {
            //progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getStatus().toString().equalsIgnoreCase("0")) {
                    chatImg.clearBadge();
                    chatImg.visibleBadge(false);
                } else {
                    chatImg.visibleBadge(true);
                    chatImg.setBadgeValue(response.body().getCount());
                }
            }
        }
        @Override
        public void onFailure(Call<ReadChatCheckMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    Callback<UpdatePhoneSuccess> deleteAccountCallback = new Callback<UpdatePhoneSuccess>() {
        @Override
        public void onResponse(Call<UpdatePhoneSuccess> call, Response<UpdatePhoneSuccess> response) {
            try {
                if (response.body().getSuccess() ==1){
                    sharedPreferencesData.clearSharedPreferenceData(Constants.USER_LOGIN_SHARED);
                    getActivity().finishAffinity();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<UpdatePhoneSuccess> call, Throwable t) {

        }
    };


    Callback<ProfileVisibleResponse> profileVisible = new Callback<ProfileVisibleResponse>() {
        @Override
        public void onResponse(Call<ProfileVisibleResponse> call, Response<ProfileVisibleResponse> response) {

            if (response.body().getSuccess() == 1){
                profileStatus = response.body().getStatus();
                if (profileStatus == 1) {
                    profileSwitch.setChecked(true);
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_STATUS, "1");
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                            alert.setTitle("Alert");
                            alert.setMessage("Profile on successfully.");
                            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            alert.show();
                }else if (profileStatus == 0){
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_STATUS, "0");
                    profileSwitch.setChecked(false);
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    alert.setTitle("Alert");
                    alert.setMessage("Profile off successfully nobody can see your profile.");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();

                }
                else if (profileStatus == 2){
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_STATUS, "0");
                    profileSwitch.setChecked(false);
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    alert.setTitle("Alert");
                    alert.setMessage("Your subscription will be expried need to renew then your profile go online.");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
                else if (profileStatus == 3){
                    sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_STATUS, "0");
                    profileSwitch.setChecked(false);
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    alert.setTitle("Alert");
                    alert.setMessage("You can't post content actually. Please upgrade your profile to be online and post photos & videos.");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
            }
        }

        @Override
        public void onFailure(Call<ProfileVisibleResponse> call, Throwable t) {

        }
    };

    Callback<DeleteAudioResponse> deleteAudioCallback = new Callback<DeleteAudioResponse>() {
        @Override
        public void onResponse(Call<DeleteAudioResponse> call, Response<DeleteAudioResponse> response) {

            if (response.body().getSuccess() == 1){
                progressBar.setVisibility(View.GONE);
                audioImg.setBackgroundColor(getResources().getColor(R.color.grey));
                deleteAudioTv.setVisibility(View.GONE);
                sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.AUDIO, "");
            }
        }

        @Override
        public void onFailure(Call<DeleteAudioResponse> call, Throwable t) {

        }
    };


    Callback<UpdatePhoneSuccess> updatePhoneCallback = new Callback<UpdatePhoneSuccess>() {
        @Override
        public void onResponse(Call<UpdatePhoneSuccess> call, Response<UpdatePhoneSuccess> response) {
            try {
                if (response.body() != null){
                    if (response.body().getSuccess() == 1) {
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PHONE_NUMBER, response.body().getPhone_number());
                        Toast.makeText(getActivity(), "Updated successfully.", Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<UpdatePhoneSuccess> call, Throwable t) {

        }
    };


    Callback<DisplayPhoneSuccess> displayPhoneCallback = new Callback<DisplayPhoneSuccess>() {
        @Override
        public void onResponse(Call<DisplayPhoneSuccess> call, Response<DisplayPhoneSuccess> response) {
            try {
                if (response.body() != null){
                    if (response.body().getSuccess() == 1) {
                        Toast.makeText(getActivity(), "Updated successfully.", Toast.LENGTH_SHORT).show();
                        sharedPreferencesData.setSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PHONE_NUMBER_STATUS, response.body().getDisplay_no());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<DisplayPhoneSuccess> call, Throwable t) {

        }
    };

    Callback<ProfileLikeSuccess> likeCallback = new Callback<ProfileLikeSuccess>() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onResponse(@NotNull Call<ProfileLikeSuccess> call, Response<ProfileLikeSuccess> response) {
            progressBar.setVisibility(View.GONE);
            if (response.body() != null) {
                if (response.body().getMessage().contains("Liked")) {
                    likeStatus = "1";
                    profileLikeImg.setImageResource(R.drawable.like_red_user);
                    String likeCount = tv_totalFollowers.getText().toString();
                    int like = Integer.parseInt(likeCount);
                    like = like + 1;
                    tv_totalFollowers.setText("" + like);

                } else {
                    likeStatus = "0";
                    profileLikeImg.setImageResource(R.drawable.like_black_user);
                    String likeCount = tv_totalFollowers.getText().toString();
                    int like = Integer.parseInt(likeCount);
                    like = like - 1;
                    tv_totalFollowers.setText("" + like);
                }
            }
        }

        @Override
        public void onFailure(@NotNull Call<ProfileLikeSuccess> call, @NotNull Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    public void callDeleteMethod(String key) {
        Log.e("Deleted=", "deleteCalled");
        // Snackbar.make(btn_price,"Called this",Snackbar.LENGTH_SHORT).show();
        if (NetworkUtil.checkNetworkStatus(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().deletedImageList(deleteCallback, key);

        } else {
            Snackbar.make(imgv_top, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }
    }

    Callback<DeleteMain> logoutCallback = new Callback<DeleteMain>() {
        @Override
        public void onResponse(Call<DeleteMain> call, Response<DeleteMain> response) {
            try {
                if (response.body().getSuccess() == 1){
                    sharedPreferencesData.clearSharedPreferenceData(Constants.USER_LOGIN_SHARED);
                    getActivity().finishAffinity();
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<DeleteMain> call, Throwable t) {

        }
    };

    Callback<DeleteMain> deleteCallback = new Callback<DeleteMain>() {

        @Override
        public void onResponse(Call<DeleteMain> call, Response<DeleteMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                Snackbar.make(imgv_top, response.body().getMessage(), Snackbar.LENGTH_LONG).show();

                if (NetworkUtil.checkNetworkStatus(getContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    RetrofitHelper.getInstance().getMultipleImage(getMultipleImageCallback, userId, "5", String.valueOf(page));
                    view.findViewById(R.id.linearlayout_deleteCancel).setVisibility(View.GONE);

                } else {
                    Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void onFailure(Call<DeleteMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedPreferencesData = new SharedPreferencesData(getContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        gridLayoutManager= new GridLayoutManager(getContext(), 3);
        recyclerView_UploadedImages.setLayoutManager(gridLayoutManager);
        multipleImageGetMainData = new ArrayList<>();

        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {

            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_TYPE).equalsIgnoreCase("1")){
                countryLL.setVisibility(View.GONE);
                linearLayout_post.setVisibility(View.GONE);
//                top_editLayout.setVisibility(View.GONE);
                btn_loginProfile.setVisibility(View.GONE);
                btn_price.setVisibility(View.GONE);
                profileLikeImg.setVisibility(View.GONE);
                audioImg.setVisibility(View.GONE);
                btn_call.setVisibility(View.GONE);
                tv_aboutUs.setVisibility(View.GONE);
            }

            if (sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.USER_TYPE).equalsIgnoreCase("0")) {
                btn_price.setVisibility(View.GONE);
                btn_updateProfile.setVisibility(View.GONE);
                audioImg.setVisibility(View.GONE);
                linearLayout_Followers.setVisibility(View.GONE);
//                layoutLinear_Information.setVisibility(View.GONE);
                imgv_location.setVisibility(View.GONE);
                //btn_updateProfile.setVisibility(View.GONE);

            }

            /*if (NetworkUtil.checkNetworkStatus(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getMultipleImage(getMultipleImageCallback, userId);

            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }*/


            String firstname = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.FIRST_NAME) + " " + sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.LAST_NAME);
            tv_firstName.setText(firstname);

            String userImage = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);

            if (userImage != null && (!userImage.equalsIgnoreCase(""))) {
                Picasso.with(getContext())
                        .load(userImage)
                        .placeholder(R.drawable.profile_image)
                        .error(R.drawable.profile_image)
                        .into(imgv_top);
                Picasso.with(getContext())
                        .load(userImage)
                        .placeholder(R.drawable.profile_image)
                        .error(R.drawable.profile_image)
                        .into(circularImageView_UserImage);
            }


          /*  if (NetworkUtil.checkNetworkStatus(getContext())) {
                progressBar.setVisibility(View.VISIBLE);
                RetrofitHelper.getInstance().getFollowerIngCount(followersListCallback, userId);

            } else {
                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
            }*/
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.login_first_profile), Snackbar.LENGTH_LONG).show();
            imgv_location.setVisibility(View.GONE);

        }

//        recyclerView_UploadedImages.addOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                if (dy > 0) //check for scroll down
//                {
//                    visibleItemCount = gridLayoutManager.getChildCount();
//                    totalItemCount = gridLayoutManager.getItemCount();
//                    pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
//
//                    if (loading) {
//                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
//                            loading = false;
//                            if (NetworkUtil.checkNetworkStatus(getContext())) {
//                                progressBar.setVisibility(View.VISIBLE);
//                                RetrofitHelper.getInstance().getMultipleImage(getMultipleImageCallback1, userId, "5", String.valueOf(page));
//                            } else {
//                                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
//                            }
//                            //Do pagination.. i.e. fetch new data
//                        }
//                    }
//                }
//            }
//        });
    }


    Callback<MultipleImageGetMain> getMultipleImageCallback1 = new Callback<MultipleImageGetMain>() {
        @Override
        public void onResponse(Call<MultipleImageGetMain> call, Response<MultipleImageGetMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                if (response.body().getResult() != null) {
                    multipleImageGetMainData.addAll(response.body().getResult());
                    addImagesListAdapter.notifyDataSetChanged();
                    loading = true;
                    ++page;
                }
            }
        }

        @Override
        public void onFailure(Call<MultipleImageGetMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<MultipleImageGetMain> getMultipleImageCallback = new Callback<MultipleImageGetMain>() {
        @Override
        public void onResponse(Call<MultipleImageGetMain> call, Response<MultipleImageGetMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.isSuccessful()) {
                multipleImageGetMainData.clear();
                if (response.body().getResult() != null) {
                    multipleImageGetMainData.addAll(response.body().getResult());
                    addImagesListAdapter = new AddImagesListAdapter(getContext(), multipleImageGetMainData, view, ProfileFragment.this,mc);
                    recyclerView_UploadedImages.setAdapter(addImagesListAdapter);
                } else {
                    addImagesListAdapter = new AddImagesListAdapter(getContext(), multipleImageGetMainData, view, ProfileFragment.this,mc);
                    recyclerView_UploadedImages.setAdapter(addImagesListAdapter);
                    page=page+1;
                }
//                if (response.body().getResult() != null) {
//                    multipleImageGetMainData.addAll(response.body().getResult());
//                    AddAnotherImageListAdapter1 addImagesListAdapter = new AddAnotherImageListAdapter1(getActivity(), multipleImageGetMainData, mc);
//                    recyclerView_UploadedImages.setAdapter(addImagesListAdapter);
//                }

                if (NetworkUtil.checkNetworkStatus(getContext())) {
                    RetrofitHelper.getInstance().getPackageUpdateInfo(getPackageInfoCallback, userId);
                }
            }
        }

        @Override
        public void onFailure(Call<MultipleImageGetMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    Callback<PackageMainData> getPackageInfoCallback = new Callback<PackageMainData>() {
        @Override
        public void onResponse(Call<PackageMainData> call, Response<PackageMainData> response) {
            progressBar.setVisibility(View.GONE);
            if (response.body() != null) {

                if (response.body().getCheckStatus() == 1) {
                    if (response.body().getExpiredOn() ==0){
                        onlineTv.setText("Offline");
                    }else {
                        onlineTv.setText("Online");
                    }


                    }
                }
            }


        @Override
        public void onFailure(Call<PackageMainData> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };


    Callback<CountListFollowIngPost> followersListCallback = new Callback<CountListFollowIngPost>() {

        @Override
        public void onResponse(Call<CountListFollowIngPost> call, Response<CountListFollowIngPost> response) {
            progressBar.setVisibility(View.GONE);
            if (response.body() != null) {
                if (response.body().getFollowingCount() != null) {
                    tv_totalFollowing.setText(response.body().getFollowersCount().toString());
                }
                if (response.body().getFollowersCount() != null) {
                    tv_totalFollowers.setText(response.body().getLikeCount().toString());
                }
                if (response.body().getPostCount() != null) {
                    tv_post.setText(response.body().getPostCount().toString());
                }

            }

        }

        @Override
        public void onFailure(Call<CountListFollowIngPost> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };

    @OnClick(R.id.btn_updateProfile)
    public void updateButtonClick(View view) {
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            Intent intent = new Intent(getContext(), EditProfileActivity.class);
            startActivity(intent);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.login_first_edit), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.chatImg)
    public void chatButtonClick(View view) {

        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {

            startActivity(new Intent(getContext(), ChatListActivity.class));
//                Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.same_user), Toast.LENGTH_LONG).show();

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.please_login_to_chat), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btn_call)
    public void callButtonClick(View view) {
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.edit_number_dialog_layout);
            Switch switcher = dialog.findViewById(R.id.switcher);
            CountryCodePicker ccp = dialog.findViewById(R.id.ccp);
            EditText et_phoneNumber = dialog.findViewById(R.id.et_phoneNumber);
            Button updateButton = dialog.findViewById(R.id.updateButton);

            String p = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PHONE_NUMBER);
            try {
                String[] splited = p.split("\\s+");
                if (splited.length>0)
                et_phoneNumber.setText(splited[1]);


                String a = splited[0];
                a = a.substring(1);
                ccp.setCountryForPhoneCode(Integer.parseInt(a));
                Log.e("kdjfk", a);
            } catch (Exception e) {
                e.printStackTrace();
            }


            Log.e("kdjfk", ccp.getSelectedCountryCode());
//            ccp.getSelectedCountryCode();
            String status = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PHONE_NUMBER_STATUS);
            if (status != null){
                if (status.equalsIgnoreCase("0")){
                    switcher.setChecked(false);
                }else {
                    switcher.setChecked(true);
                }
            }
            switcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (NetworkUtil.checkNetworkStatus(getActivity().getApplicationContext())) {
                        if (isChecked) {
                            RetrofitHelper.getInstance().displayPhone(displayPhoneCallback, userId, "1");
                        }else {
                            RetrofitHelper.getInstance().displayPhone(displayPhoneCallback, userId, "0");
                        }

                    } else {
                        Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }

                }
            });

            updateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!et_phoneNumber.getText().toString().equalsIgnoreCase("")){
                        String phone = ccp.getSelectedCountryCodeWithPlus()+" "+ et_phoneNumber.getText().toString();
                        RetrofitHelper.getInstance().updatePhone(updatePhoneCallback, userId, phone);
                        dialog.dismiss();
                    }else {
                        Toast.makeText(getActivity(), "Please enter phone number", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            dialog.show();

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.login_first_edit), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.btn_loginProfile)
    public void loginButtonClick(View view) {
        sharedPreferencesData = new SharedPreferencesData(getContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
            //Toast.makeText(getContext(),"Logout",Toast.LENGTH_LONG).show();
            btn_loginProfile.setText("Login");
//            layoutLinear_Information.setVisibility(View.GONE);
            sharedPreferencesData.clearSharedPreferenceData(Constants.USER_LOGIN_SHARED);
            Snackbar.make(tv_firstName, getResources().getString(R.string.logout_successfully), Snackbar.LENGTH_LONG).show();
            onResume();
        } else {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_price)
    public void priceButtonClick(View view) {

        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {

            Intent intent = new Intent(getContext(), PriceListActivity.class);
            startActivity(intent);


        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.login_first_price), Snackbar.LENGTH_LONG).show();
        }
    }


    @OnClick(R.id.btn_changePassword)
    public void changePasswordClick(View view) {
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            Intent intent = new Intent(getContext(), ChangePassword.class);
            startActivity(intent);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.login_first_change), Snackbar.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.linearLayout_Following)
    public void followingClick(View view) {
        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
            Intent intent = new Intent(getContext(), FollowingActivity.class);
            startActivity(intent);
        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.please_login_to_see_following), Snackbar.LENGTH_LONG).show();
        }

    }


//    @OnClick(R.id.linearLayout_Followers)
//    public void followersClick(View view) {
//        if (sharedPreferencesData != null && !sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID).equalsIgnoreCase("")) {
//            Intent intent = new Intent(getContext(), FollowersActivity.class);
//            startActivity(intent);
//        } else {
//            Snackbar.make(progressBar, getResources().getString(R.string.please_login_to_see_followers), Snackbar.LENGTH_LONG).show();
//        }
//
//    }

//    @OnClick(R.id.layoutLinear_Information)
//    public void informationClick(View view) {
//        Snackbar.make(btn_price,getResources().getString(R.string.information),Snackbar.LENGTH_LONG).show();
//
//    }
}
