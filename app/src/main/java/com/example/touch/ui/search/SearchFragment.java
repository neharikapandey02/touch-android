package com.example.touch.ui.search;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.NearbySearchActivity;
import com.example.touch.activities.SearchActivity;
import com.example.touch.adapters.SearchLatestPostAdapter;
import com.example.touch.adapters.SearchListImageAdapter;
import com.example.touch.adapters.SearchTabAdapter;
import com.example.touch.constants.Constants;
import com.example.touch.constants.GPSTracker;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.constants.SingleShotLocationProvider;
import com.example.touch.models.CategoryMain;
import com.example.touch.models.CategoryMainData;
import com.example.touch.models.HomeMain;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.LatestPostData;
import com.example.touch.models.LatestPostResponse;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {

    private SearchViewModel searchViewModel;
    private static int PERMISSION_ACCESS_FINE_LOCATION=103;

    @BindView(R.id.recyclerView_Tab)
    RecyclerView recyclerView_Tab;

    @BindView(R.id.recyclerView_ListImages)
    RecyclerView recyclerView_ListImages;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;


    @BindView(R.id.progressBar)
    ProgressBar progressBar;


    @BindView(R.id.layout_LinearSearch)
    RelativeLayout layout_LinearSearch;

    @BindView(R.id.tv_search)
    TextView tv_search;

    @BindView(R.id.imgView_nearBy)
    ImageView imgView_nearBy;


    @BindView(R.id.linearLayout_nearBy)
    RelativeLayout linearLayout_nearBy;

    private String searchLat;
    private String searchLang;

    private SharedPreferencesData sharedPreferencesData;

    private String userId = "";


    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    LinearLayoutManager mLayoutManager;
    int page = 1;
    ArrayList<HomeMainData> arrayList;
    SearchLatestPostAdapter adapter;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        searchViewModel =
                ViewModelProviders.of(this).get(SearchViewModel.class);
        View root = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, root);
        /*final TextView textView = root.findViewById(R.id.text_dashboard);
        searchViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewFinds();

    }

    private void viewFinds() {
        arrayList = new ArrayList<>();
        //Adapter for tab show on Top
        recyclerView_Tab.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));


        //Adapter for list image show after tab
        GridLayoutManager gridLayoutManager= new GridLayoutManager(getContext(), 3);
        recyclerView_ListImages.setLayoutManager(gridLayoutManager);
        sharedPreferencesData = new SharedPreferencesData(getContext());
        userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);


        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtil.checkNetworkStatus(getContext())) {
                    progressBar.setVisibility(View.VISIBLE);
                    page = 1;
                    RetrofitHelper.getInstance().getAllSearch(latestPostCallback, userId, "10", page);

                } else {
                    Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                }
            }
        });


        if (NetworkUtil.checkNetworkStatus(getContext())) {
            progressBar.setVisibility(View.VISIBLE);
            RetrofitHelper.getInstance().getAllSearch(latestPostCallback, userId, "10", page);

        } else {
            Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
        }

        recyclerView_ListImages.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = gridLayoutManager.getChildCount();
                    totalItemCount = gridLayoutManager.getItemCount();
                    pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (NetworkUtil.checkNetworkStatus(getContext())) {
                                progressBar.setVisibility(View.VISIBLE);
                                RetrofitHelper.getInstance().getAllSearch(latestPostCallback1, userId, "10", page);

                            } else {
                                Snackbar.make(progressBar, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                            }
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

    }

    Callback<HomeMain> latestPostCallback = new Callback<HomeMain>() {
        @Override
        public void onResponse(Call<HomeMain> call, Response<HomeMain> response) {
            progressBar.setVisibility(View.GONE);
            if (swipeRefresh.isRefreshing()){
                swipeRefresh.setRefreshing(false);
            }
            if (response.body().getResult()!= null){
                arrayList.clear();
                arrayList.addAll(response.body().getResult());
                adapter = new SearchLatestPostAdapter(getActivity(), arrayList);
                recyclerView_ListImages.setAdapter(adapter);
                page=page+1;
            }

        }

        @Override
        public void onFailure(Call<HomeMain> call, Throwable t) {
            if (swipeRefresh.isRefreshing()){
                swipeRefresh.setRefreshing(false);
            }
        progressBar.setVisibility(View.GONE);
        }
    };


    Callback<HomeMain> latestPostCallback1 = new Callback<HomeMain>() {
        @Override
        public void onResponse(Call<HomeMain> call, Response<HomeMain> response) {
            progressBar.setVisibility(View.GONE);
            if (response.body().getResult()!= null){

                arrayList.addAll(response.body().getResult());

                adapter.notifyDataSetChanged();
                loading = true;
                ++page;

            }

        }

        @Override
        public void onFailure(Call<HomeMain> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
        }
    };




  @OnClick(R.id.tv_search)
    public void searchClick(View view){

      if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
              == PackageManager.PERMISSION_GRANTED) {
          GPSTracker gpsTracker=new GPSTracker(getContext());
          searchLat=gpsTracker.getLatitude()+"";
          searchLang=gpsTracker.getLongitude()+"";
          Log.e("Location", "myLocation" + searchLat+"     "+searchLang);

          /*SingleShotLocationProvider.requestSingleUpdate(getContext(),
                  new SingleShotLocationProvider.LocationCallback() {
                      @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                          Log.e("Location", "my location is " + location.latitude+"     "+location.longitude);
                          searchLat=location.latitude+"";
                          searchLang=location.longitude+"";

                      }
                  });*/
          Intent intent=new Intent(getContext(), SearchActivity.class);
          intent.putExtra(Constants.SEARCH_LAT,searchLat);
          intent.putExtra(Constants.SEARCH_LANG,searchLang);
          startActivity(intent);

      } else {
          requestGallery();
      }
  }

  @OnClick(R.id.linearLayout_nearBy)
    public void nearbyImageClick(View view){
      if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
              == PackageManager.PERMISSION_GRANTED) {
          GPSTracker gpsTracker=new GPSTracker(getContext());
          searchLat=gpsTracker.getLatitude()+"";
          searchLang=gpsTracker.getLongitude()+"";
          Log.e("Location", "myLocation" + searchLat+"     "+searchLang);

          /*SingleShotLocationProvider.requestSingleUpdate(getContext(),
                  new SingleShotLocationProvider.LocationCallback() {
                      @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                          Log.e("Location", "my location is " + location.latitude+"     "+location.longitude);
                          searchLat=location.latitude+"";
                          searchLang=location.longitude+"";

                      }
                  });*/
          Intent intent=new Intent(getContext(), NearbySearchActivity.class);
          intent.putExtra(Constants.SEARCH_LAT,searchLat);
          intent.putExtra(Constants.SEARCH_LANG,searchLang);
          startActivity(intent);

      } else {
          requestGallery();
      }

  }

    public void requestGallery() {
        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_ACCESS_FINE_LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED && requestCode == PERMISSION_ACCESS_FINE_LOCATION) {
            SingleShotLocationProvider.requestSingleUpdate(getContext(),
                    new SingleShotLocationProvider.LocationCallback() {
                        @Override public void onNewLocationAvailable(SingleShotLocationProvider.GPSCoordinates location) {
                            Log.e("Location", "my location is " + location.latitude+"     "+location.longitude);
                            searchLat=location.latitude+"";
                            searchLang=location.longitude+"";
                            Intent intent=new Intent(getContext(), SearchActivity.class);
                            intent.putExtra(Constants.SEARCH_LAT,searchLat);
                            intent.putExtra(Constants.SEARCH_LANG,searchLang);
                            startActivity(intent);
                        }
                    });
        }  else {
            searchLat="0";
            searchLang="0";
            Toast.makeText(getContext(), getResources().getString(R.string.nearby_user), Toast.LENGTH_SHORT).show();
        }
    }

}