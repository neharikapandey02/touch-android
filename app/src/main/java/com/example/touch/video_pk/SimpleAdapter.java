package com.example.touch.video_pk;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.example.touch.models.HomeMainData;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class SimpleAdapter extends RecyclerView.Adapter<SimplePlayerViewHolder> {

    private List<HomeMainData> arrayList;
    public SimpleAdapter(Context context, List<HomeMainData> arrayList){
        this.arrayList = arrayList;
    }


    @NotNull
    @Override public SimplePlayerViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == 1){
        view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_profile_adapter1, parent, false);
            Log.e("viewtype", "" + viewType);
            return new SimplePlayerViewHolder(view);
        }
        else {
//        else {
//            view = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.item_home_profile_adapter, parent, false);
//            Log.e("viewtype", ""+viewType);
//
//        }
            return null;
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (arrayList.get(position).getType().equalsIgnoreCase("image")){
            return 2;
        }else {
            return 1;
        }
    }
    
    @Override public void onBindViewHolder(@NotNull SimplePlayerViewHolder holder, int position) {
//        holder.bind(Uri.parse("my_awesome_video.mp4") /* FIXME use real data */);
        if (!arrayList.get(position).getType().equalsIgnoreCase("image")) {

            if (arrayList.get(position).getBig() != null && !arrayList.get(position).getBig().equalsIgnoreCase("")) {

                try {
                    Log.e("bibb", arrayList.get(position).getBig());
                    holder.bind(Uri.parse(arrayList.get(position).getBig()));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override public int getItemCount() {
        return arrayList.size();
    }

    public static class ImageTypeViewHolder extends RecyclerView.ViewHolder {


        TextView txtType;

        public ImageTypeViewHolder(View itemView) {
            super(itemView);

        }

    }

}
