package com.example.touch.video_pk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.emoji.widget.EmojiTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.ablanco.zoomy.Zoomy;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.touch.R;
import com.example.touch.RetrofitApiCalling.RetrofitHelper;
import com.example.touch.activities.AnotherUserProfile;
import com.example.touch.activities.ChatActivity;
import com.example.touch.activities.ChatListActivity;
import com.example.touch.activities.CommentsActivity;
import com.example.touch.activities.ImagesListByTagAndName;
import com.example.touch.activities.LikesTotalList;
import com.example.touch.activities.MainActivity;
import com.example.touch.activities.VideoFullScreenActivity;
import com.example.touch.constants.Constants;
import com.example.touch.constants.MySpannable;
import com.example.touch.constants.NetworkUtil;
import com.example.touch.constants.SharedPreferencesData;
import com.example.touch.interfaces.StatusMuteUnmute;
import com.example.touch.models.AnotherUserProfileIdByKeySuccess;
import com.example.touch.models.GetTagIdSuccess;
import com.example.touch.models.HomeMainData;
import com.example.touch.models.LikesMain;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SimpleAdapter1 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<HomeMainData> arrayList;
    private Context context;
    private int positionImage;
    private SharedPreferencesData sharedPreferencesData;
    private String userImagePath;
    private final int LIKE_DISPLAY_LENGTH = 700;
    private ImageTypeViewHolder holder;
    private SimplePlayerViewHolder holder1;
    int i = 0;
    AudioManager amanager;
    private String hastagKey = "";

    public SimpleAdapter1(Context context, List<HomeMainData> arrayList){
        this.arrayList = arrayList;
        this.context = context;
        sharedPreferencesData = new SharedPreferencesData(context);
        if (sharedPreferencesData != null) {
            userImagePath = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.PROFILE_IMAGE);
        }
    }


    @NotNull
    @Override public RecyclerView.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == 1){
            try {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_profile_adapter1, parent, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("viewtype", ""+viewType);
        Log.e("viewtype", ""+viewType);
            return new SimplePlayerViewHolder(view);
        }
        else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_home_profile_adapter2, parent, false);
            Log.e("viewtype", ""+viewType);
            return new ImageTypeViewHolder(view);
        }
    }



    @Override
    public int getItemViewType(int position) {
        if (arrayList.get(position).getType().equalsIgnoreCase("image")){
            return 2;
        }else {
            return 1;
        }
    }
    
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NotNull RecyclerView.ViewHolder holder, int position) {
//        holder.bind(Uri.parse("my_awesome_video.mp4") /* FIXME use real data */);
        if (!arrayList.get(position).getType().equalsIgnoreCase("image")) {

            if (arrayList.get(position).getBig() != null && !arrayList.get(position).getBig().equalsIgnoreCase("")) {

                try {
//                    Log.e("bibb", arrayList.get(position).getBig());
                    ((SimplePlayerViewHolder) holder).bind(Uri.parse(arrayList.get(position).getBig()));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            ((SimplePlayerViewHolder) holder).fullScreenImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, VideoFullScreenActivity.class);
                    intent.putExtra("video", arrayList.get(position).getBig());
                        context.startActivity(intent);
                }
            });
//            Zoomy.Builder builder = new Zoomy.Builder((Activity) context).target(((SimplePlayerViewHolder) holder).relativeLL);
//            builder.register();

            ((SimplePlayerViewHolder) holder).progressBarImages.setVisibility(View.VISIBLE);

            Picasso.with(context)
                    .load(arrayList.get(position).getProfile_image())
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(((SimplePlayerViewHolder) holder).circularImageView_Profile);

            ((SimplePlayerViewHolder) holder).circularImageView_Profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    Log.e("userId=", userId + "  " + arrayList.get(position).getUserId());
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        if (!userId.equalsIgnoreCase(arrayList.get(position).getUserId())) {
                            Intent intent = new Intent(context, AnotherUserProfile.class);
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(Constants.ANOTHER_USER_ID, arrayList.get(position).getPost_user_id());
                            context.startActivity(intent);
                        } else {
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.putExtra("fragment", "profile");
                            context.startActivity(intent);
                        }

                    }else{
                        Snackbar.make(((SimplePlayerViewHolder) holder).imgView_Chat,context.getResources().getString(R.string.please_login_first),Snackbar.LENGTH_LONG).show();

                    }
                    }
            });

            StringBuffer stringBuffer = new StringBuffer();
            if (arrayList.get(position).getHashtag_one() != null && !arrayList.get(position).getHashtag_one().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_one()+" ");
            }
            if (arrayList.get(position).getHashtag_two() != null && !arrayList.get(position).getHashtag_two().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_two()+" ");
            }
            if (arrayList.get(position).getHashtag_three() != null && !arrayList.get(position).getHashtag_three().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_three()+" ");
            }
            if (arrayList.get(position).getHashtag_four() != null && !arrayList.get(position).getHashtag_four().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_four()+" ");
            }
            if (arrayList.get(position).getHashtag_five() != null && !arrayList.get(position).getHashtag_five().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_five()+" ");
            }

            if (stringBuffer != null){
                Pattern pattern = Pattern.compile("#\\w+");
                String keywords = stringBuffer.toString();
                Matcher matcher = pattern.matcher(keywords);
                ArrayList<String> atTheRateList = new ArrayList<>();
                while (matcher.find()){
//                    System.out.println(matcher.group());
                    atTheRateList.add(matcher.group());
                    keywords = keywords.replaceAll(matcher.group(), "");
                }
                if (atTheRateList.size()>0){
                    singleTextView1(((SimplePlayerViewHolder) holder).hastagTv, keywords, atTheRateList);
                }
            }
//            ((SimplePlayerViewHolder) holder).hastagTv.setText(stringBuffer.toString());



            RelativeLayout.LayoutParams layoutParams=(RelativeLayout.LayoutParams)((SimplePlayerViewHolder) holder).tv_userName.getLayoutParams();;
            if (arrayList.get(position).getKeyword()!=null){
                String keywords = arrayList.get(position).getKeyword();
                Pattern pattern = Pattern.compile("@\\w+");
                Matcher matcher = pattern.matcher(keywords);
                ArrayList<String> atTheRateList = new ArrayList<>();
                while (matcher.find()){
//                    System.out.println(matcher.group());
                    atTheRateList.add(matcher.group());
                    keywords = keywords.replaceAll(matcher.group(), "");
                }
                if (atTheRateList.size()>0){
                    singleTextView(((SimplePlayerViewHolder) holder).tv_hastags, keywords, atTheRateList);
                }else {
                    ((SimplePlayerViewHolder) holder).tv_hastags.setText(arrayList.get(position).getKeyword());

                }
//                if (keywords!=null&&keywords.length()>20){
//                    makeTextViewResizable(((SimplePlayerViewHolder) holder).tv_hastags, 3, "See More", false);
//                }




//                ((SimplePlayerViewHolder) holder).tv_hastags.setText(arrayList.get(position).getKeyword());
                //holder.tv_hastags.setEmojiconSize(40);
                ((SimplePlayerViewHolder) holder).tv_hastags.setTextSize(13);
                ((SimplePlayerViewHolder) holder).tv_hastags.setVisibility(View.VISIBLE);
            }else{
                ((SimplePlayerViewHolder) holder).tv_hastags.setVisibility(View.GONE);
            }
            if (arrayList.get(position).getLocation()!=null&&!arrayList.get(position).getLocation().equalsIgnoreCase("")){
                Log.e("callLocation","Inside NotNull"+position+"");
//                layoutParams.setMargins(5,10,0,0);
                ((SimplePlayerViewHolder) holder).tv_userName.setLayoutParams(layoutParams);
                ((SimplePlayerViewHolder) holder).tv_Address.setVisibility(View.VISIBLE);
                ((SimplePlayerViewHolder) holder).tv_Address.setText(arrayList.get(position).getLocation()+" ("+arrayList.get(position).getCountry()+")");


            }else{
                Log.e("callLocation","Inside Null"+position+"");
                ((SimplePlayerViewHolder) holder).tv_Address.setVisibility(View.GONE);
//                layoutParams.setMargins(5,30,0,0);
                ((SimplePlayerViewHolder) holder).tv_userName.setLayoutParams(layoutParams);
            }

            if (arrayList.get(position).getCountLike() != null && !arrayList.get(position).getCountLike().equals(0)){
                ((SimplePlayerViewHolder) holder).likesCountTv.setText(""+arrayList.get(position).getCountLike());
                ((SimplePlayerViewHolder) holder).likesCountTv.setVisibility(View.VISIBLE);
            }else {
                ((SimplePlayerViewHolder) holder).likesCountTv.setVisibility(View.GONE);
            }

            if (arrayList.get(position).getCount_comment() != 0){
                ((SimplePlayerViewHolder) holder).commentsCountTv.setText(""+arrayList.get(position).getCount_comment());
                ((SimplePlayerViewHolder) holder).commentsCountTv.setVisibility(View.VISIBLE);
            }else {
                ((SimplePlayerViewHolder) holder).commentsCountTv.setVisibility(View.GONE);
            }



            ((SimplePlayerViewHolder) holder).tv_userName.setText(arrayList.get(position).getFirstName());
            if (arrayList.get(position).getKeyword() != null && !arrayList.get(position).getKeyword().equalsIgnoreCase("")) {
                ((SimplePlayerViewHolder) holder).nameTv.setText(arrayList.get(position).getFirstName() + " : ");
            }
            ((SimplePlayerViewHolder) holder).postTime.setText(arrayList.get(position).getPostTime());
            positionImage = position;

            if (arrayList.get(position).getIsLike() == 1) {
                ((SimplePlayerViewHolder) holder).imgView_likeRed.setVisibility(View.VISIBLE);
                ((SimplePlayerViewHolder) holder).imgView_likeBlack.setVisibility(View.GONE);
            } else {
                ((SimplePlayerViewHolder) holder).imgView_likeRed.setVisibility(View.GONE);
                ((SimplePlayerViewHolder) holder).imgView_likeBlack.setVisibility(View.VISIBLE);
            }

            if (arrayList.get(position).getCount_comment() == 0) {
                ((SimplePlayerViewHolder) holder).tv_totalComments.setVisibility(View.GONE);
            } else {
                ((SimplePlayerViewHolder) holder).tv_totalComments.setVisibility(View.VISIBLE);
                ((SimplePlayerViewHolder) holder).tv_totalComments.setText("View All " + arrayList.get(position).getCount_comment() + " Comments");
            }

            ((SimplePlayerViewHolder) holder).tv_totalLikes.setText(arrayList.get(position).getCountLike() + " " + "Likes");
            ((SimplePlayerViewHolder) holder).tv_totalLikes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        if (arrayList.get(position).getCountLike()!=0){
                            Intent intent = new Intent(context, LikesTotalList.class);
                            intent.putExtra(Constants.POSTID, arrayList.get(position).getId());
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }else{
                            Snackbar.make(((SimplePlayerViewHolder) holder).imgView_Chat,context.getResources().getString(R.string.no_like_yet),Snackbar.LENGTH_LONG).show();
                        }

                    }else{
                        Snackbar.make(((SimplePlayerViewHolder) holder).imgView_Chat,context.getResources().getString(R.string.please_login_to_see_likes),Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            ((SimplePlayerViewHolder) holder).imgView_Chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    Log.e("userId=", userId + "  " + arrayList.get(position).getUserId());
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        if (!userId.equalsIgnoreCase(arrayList.get(position).getUserId())) {
                            Intent intent = new Intent(context, ChatActivity.class);
                            intent.putExtra(Constants.RECEIVER_ID, arrayList.get(position).getPost_user_id());
                            intent.putExtra(Constants.PROFILE_IMAGE, arrayList.get(position).getProfile_image());
                            intent.putExtra(Constants.FIRST_NAME, arrayList.get(position).getFirstName());
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        } else {
                            context.startActivity(new Intent(context, ChatListActivity.class));

//                            Toast.makeText(context, context.getResources().getString(R.string.same_user), Toast.LENGTH_LONG).show();
                        }

                    }else{
                        Snackbar.make(((SimplePlayerViewHolder) holder).imgView_Chat,context.getResources().getString(R.string.please_login_to_chat),Snackbar.LENGTH_LONG).show();

                    }
                }
            });


            ((SimplePlayerViewHolder) holder).imgView_profile.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    i++;
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {

                        @Override
                        public void run() {
                            i = 0;
                        }
                    };

                    if (i == 1) {
                        //Single click
                        handler.postDelayed(r, 250);
                        Log.e("handler=", "handler");
                    } else if (i == 2) {
                        //Double click
                        i = 0;
                        Log.e("doubleClick=", "Double Click");
                        //status 1 for like 2 for unlike mind it
                        if (NetworkUtil.checkNetworkStatus(context)) {
                            String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                            String status = arrayList.get(position).getIsLike().toString();
                            if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                                if (status.equalsIgnoreCase("1")) {
                                    ((SimplePlayerViewHolder) holder).like_unlike.setVisibility(View.VISIBLE);
                                    handlerForLikeUnlikeShow(((SimplePlayerViewHolder) holder));
                                    positionImage = position;
                                    RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "2");
                                } else {
                                    ((SimplePlayerViewHolder) holder).like_unlike.setVisibility(View.VISIBLE);
                                    handlerForLikeUnlikeShow(((SimplePlayerViewHolder) holder));
                                    RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "1");
                                    positionImage = position;
                                }

                            } else {
                                Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                            }


                        } else {
                            Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                        }

                    }


                }
            });

            this.holder1 = ((SimplePlayerViewHolder) holder);
            ((SimplePlayerViewHolder) holder).imgView_likeBlack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        String status = arrayList.get(position).getIsLike().toString();
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            if (status.equalsIgnoreCase("1")) {
                                positionImage = position;
                                ((SimplePlayerViewHolder) holder).imgView_likeRed.setVisibility(View.GONE);
                                ((SimplePlayerViewHolder) holder).imgView_likeBlack.setVisibility(View.VISIBLE);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "2");
                            } else {
                                positionImage = position;
                                ((SimplePlayerViewHolder) holder).imgView_likeRed.setVisibility(View.VISIBLE);
                                ((SimplePlayerViewHolder) holder).imgView_likeBlack.setVisibility(View.GONE);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "1");

                            }

                        } else {
                            Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }

                }
            });
            ((SimplePlayerViewHolder) holder).imgView_likeRed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        String status = arrayList.get(position).getIsLike().toString();
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            if (status.equalsIgnoreCase("1")) {
                                positionImage = position;
                                ((SimplePlayerViewHolder) holder).imgView_likeRed.setVisibility(View.GONE);
                                ((SimplePlayerViewHolder) holder).imgView_likeBlack.setVisibility(View.VISIBLE);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "2");
                            } else {
                                positionImage = position;
                                ((SimplePlayerViewHolder) holder).imgView_likeRed.setVisibility(View.VISIBLE);
                                ((SimplePlayerViewHolder) holder).imgView_likeBlack.setVisibility(View.GONE);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "1");

                            }

                        } else {
                            Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            ((SimplePlayerViewHolder) holder).imgView_Comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            Intent intent = new Intent(context, CommentsActivity.class);
                            intent.putExtra(Constants.POSTID, arrayList.get(position).getId());
                            intent.putExtra("post_user_id", arrayList.get(position).getUserId());
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);

                        } else {
                            Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_comment), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }

                }
            });

            ((SimplePlayerViewHolder) holder).tv_totalComments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            Intent intent = new Intent(context, CommentsActivity.class);
                            intent.putExtra(Constants.POSTID, arrayList.get(position).getId());
                            intent.putExtra("post_user_id", arrayList.get(position).getUserId());
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);

                        } else {
                            Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_comment), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(((SimplePlayerViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }
                }
            });

            //here manage all audio mute unmute method and click listners

            amanager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
            amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
            StatusMuteUnmute.muteunmuteStatus = true;
            if (arrayList.get(position).getType().equalsIgnoreCase("video")){
                if (StatusMuteUnmute.muteunmuteStatus){
                    ((SimplePlayerViewHolder) holder).imgview_unmute.setVisibility(View.GONE);
                    ((SimplePlayerViewHolder) holder).imgview_mute.setVisibility(View.VISIBLE);
                    Log.e("Mute=","Inside MuteVisisble");

                }else{
                    ((SimplePlayerViewHolder) holder).imgview_unmute.setVisibility(View.VISIBLE);
                    ((SimplePlayerViewHolder) holder).imgview_mute.setVisibility(View.GONE);
                    Log.e("Mute=","Inside UnmuteVisisble");
                }

            }else{
                ((SimplePlayerViewHolder) holder).imgview_mute.setVisibility(View.GONE);
                ((SimplePlayerViewHolder) holder).imgview_unmute.setVisibility(View.GONE);
            }

            ((SimplePlayerViewHolder) holder).imgview_mute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
                    StatusMuteUnmute.muteunmuteStatus=false;
                    ((SimplePlayerViewHolder) holder).imgview_unmute.setVisibility(View.VISIBLE);
                    ((SimplePlayerViewHolder) holder).imgview_mute.setVisibility(View.GONE);
                    if (position==0){
                        notifyItemRangeChanged(position+1,arrayList.size());
                    }else{
                        notifyItemRangeChanged(0,position);
                        notifyItemRangeChanged(position+1,arrayList.size());
                    }
                }
            });


            ((SimplePlayerViewHolder) holder).imgview_unmute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
                    StatusMuteUnmute.muteunmuteStatus=true;
                    ((SimplePlayerViewHolder) holder).imgview_unmute.setVisibility(View.GONE);
                    ((SimplePlayerViewHolder) holder).imgview_mute.setVisibility(View.VISIBLE);
                    if (position==0){
                        notifyItemRangeChanged(position+1,arrayList.size());
                    }else {
                        notifyItemRangeChanged(0,position);
                        notifyItemRangeChanged(position+1,arrayList.size());
                    }
                }
            });


        }else {
            Zoomy.Builder builder = new Zoomy.Builder((Activity) context).target(((ImageTypeViewHolder) holder).imgView_profile);
            builder.register();
//            try {
//                if (arrayList.get(position).getBig_height()>500){
//                    ((ImageTypeViewHolder) holder).imgView_profile.setScaleType(ImageView.ScaleType.FIT_XY);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            ((ImageTypeViewHolder) holder).progressBarImages.setVisibility(View.VISIBLE);
            Glide.with(context).load(arrayList.get(position).getBig())
                    .placeholder(R.drawable.loading_placeholder)
                    .error(R.drawable.loading_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)


                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            ((ImageTypeViewHolder) holder).progressBarImages.setVisibility(View.GONE);

                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            ((ImageTypeViewHolder) holder).progressBarImages.setVisibility(View.GONE);

                            return false;
                        }
                    })
                    .into(((ImageTypeViewHolder) holder).imgView_profile);

            Picasso.with(context)
                    .load(arrayList.get(position).getProfile_image())
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder)
                    .into(((ImageTypeViewHolder) holder).circularImageView_Profile);

            ((ImageTypeViewHolder) holder).circularImageView_Profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    Log.e("userId=", userId + "  " + arrayList.get(position).getUserId());
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        if (!userId.equalsIgnoreCase(arrayList.get(position).getUserId())) {
                            Intent intent = new Intent(context, AnotherUserProfile.class);
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(Constants.ANOTHER_USER_ID, arrayList.get(position).getPost_user_id());
                            context.startActivity(intent);
                        } else {
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.putExtra("fragment", "profile");
                            context.startActivity(intent);
                        }

                    }else{
                        Snackbar.make(((SimplePlayerViewHolder) holder).imgView_Chat,context.getResources().getString(R.string.please_login_first),Snackbar.LENGTH_LONG).show();
                    }
                }
            });

            RelativeLayout.LayoutParams layoutParams=(RelativeLayout.LayoutParams)((ImageTypeViewHolder) holder).tv_userName.getLayoutParams();;
            if (arrayList.get(position).getKeyword()!=null){
                String keywords = arrayList.get(position).getKeyword();
                Pattern pattern = Pattern.compile("@\\w+");
                Matcher matcher = pattern.matcher(keywords);
                ArrayList<String> atTheRateList = new ArrayList<>();
                while (matcher.find()){
//                    System.out.println(matcher.group());
                    atTheRateList.add(matcher.group());
                    keywords = keywords.replaceAll(matcher.group(), "");
                }

                if (atTheRateList.size()>0){
                    singleTextView(((ImageTypeViewHolder) holder).tv_hastags, keywords, atTheRateList);
                }else {
                    ((ImageTypeViewHolder) holder).tv_hastags.setText(arrayList.get(position).getKeyword());

                }
//                if (keywords != null && keywords.length() > 20) {
//                    makeTextViewResizable(((ImageTypeViewHolder) holder).tv_hastags, 3, "See More", false);
//                }
//                Toast.makeText(context, ""+atTheRateList.toString(), Toast.LENGTH_SHORT).show();


//                ((ImageTypeViewHolder) holder).tv_hastags.setText(arrayList.get(position).getKeyword());
                //holder.tv_hastags.setEmojiconSize(40);
                ((ImageTypeViewHolder) holder).tv_hastags.setTextSize(13);
                ((ImageTypeViewHolder) holder).tv_hastags.setVisibility(View.VISIBLE);
            }else{
                ((ImageTypeViewHolder) holder).tv_hastags.setVisibility(View.GONE);
            }

            StringBuffer stringBuffer = new StringBuffer();
            if (arrayList.get(position).getHashtag_one() != null && !arrayList.get(position).getHashtag_one().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_one()+" ");
            }
            if (arrayList.get(position).getHashtag_two() != null && !arrayList.get(position).getHashtag_two().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_two()+" ");
            }
            if (arrayList.get(position).getHashtag_three() != null && !arrayList.get(position).getHashtag_three().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_three()+" ");
            }
            if (arrayList.get(position).getHashtag_four() != null && !arrayList.get(position).getHashtag_four().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_four()+" ");
            }
            if (arrayList.get(position).getHashtag_five() != null && !arrayList.get(position).getHashtag_five().equalsIgnoreCase("")){
                stringBuffer.append("#"+arrayList.get(position).getHashtag_five()+" ");
            }
            if (stringBuffer != null){
                Pattern pattern = Pattern.compile("#\\w+");
                String keywords = stringBuffer.toString();
                Matcher matcher = pattern.matcher(keywords);
                ArrayList<String> atTheRateList = new ArrayList<>();
                while (matcher.find()){
//                    System.out.println(matcher.group());
                    atTheRateList.add(matcher.group());
                    keywords = keywords.replaceAll(matcher.group(), "");
                }
                if (atTheRateList.size()>0){
                    singleTextView1(((ImageTypeViewHolder) holder).hastagTv, keywords, atTheRateList);
                }
            }
//            ((ImageTypeViewHolder) holder).hastagTv.setText(stringBuffer.toString());

            if (arrayList.get(position).getCountLike() != null && !arrayList.get(position).getCountLike().equals(0)){
                ((ImageTypeViewHolder) holder).likesCountTv.setText(""+arrayList.get(position).getCountLike());
                ((ImageTypeViewHolder) holder).likesCountTv.setVisibility(View.VISIBLE);
            }else {
                ((ImageTypeViewHolder) holder).likesCountTv.setVisibility(View.GONE);
            }

            if (arrayList.get(position).getCount_comment() != 0){
                ((ImageTypeViewHolder) holder).commentsCountTv.setText(""+arrayList.get(position).getCount_comment());
                ((ImageTypeViewHolder) holder).commentsCountTv.setVisibility(View.VISIBLE);
            }else {
                ((ImageTypeViewHolder) holder).commentsCountTv.setVisibility(View.GONE);
            }

            if (arrayList.get(position).getLocation()!=null&&!arrayList.get(position).getLocation().equalsIgnoreCase("")){
                Log.e("callLocation","Inside NotNull"+position+"");
//                layoutParams.setMargins(5,10,0,0);
                ((ImageTypeViewHolder) holder).tv_userName.setLayoutParams(layoutParams);
                ((ImageTypeViewHolder) holder).tv_Address.setVisibility(View.VISIBLE);
                if (arrayList.get(position).getCountry()!=null&&!arrayList.get(position).getCountry().equalsIgnoreCase("")) {
                    ((ImageTypeViewHolder) holder).tv_Address.setText(arrayList.get(position).getLocation() + " (" + arrayList.get(position).getCountry() + ")");
                }else {
                    ((ImageTypeViewHolder) holder).tv_Address.setText(arrayList.get(position).getLocation());
                }
            }else{
                Log.e("callLocation","Inside Null"+position+"");
                ((ImageTypeViewHolder) holder).tv_Address.setVisibility(View.GONE);
//                layoutParams.setMargins(5,30,0,0);
                ((ImageTypeViewHolder) holder).tv_userName.setLayoutParams(layoutParams);
            }
            ((ImageTypeViewHolder) holder).imgView_profile.setVisibility(View.VISIBLE);
            ((ImageTypeViewHolder) holder).tv_userName.setText(arrayList.get(position).getFirstName());
            if (arrayList.get(position).getKeyword() != null && !arrayList.get(position).getKeyword().equalsIgnoreCase("")) {
                ((ImageTypeViewHolder) holder).nameTv.setText(arrayList.get(position).getFirstName() + " : ");
            }
            ((ImageTypeViewHolder) holder).postTime.setText(arrayList.get(position).getPostTime());
            positionImage = position;

            if (arrayList.get(position).getIsLike() == 1) {
                ((ImageTypeViewHolder) holder).imgView_likeRed.setVisibility(View.VISIBLE);
                ((ImageTypeViewHolder) holder).imgView_likeBlack.setVisibility(View.GONE);
            } else {
                ((ImageTypeViewHolder) holder).imgView_likeRed.setVisibility(View.GONE);
                ((ImageTypeViewHolder) holder).imgView_likeBlack.setVisibility(View.VISIBLE);
            }

            if (arrayList.get(position).getCount_comment() == 0) {
                ((ImageTypeViewHolder) holder).tv_totalComments.setVisibility(View.GONE);
            } else {
                ((ImageTypeViewHolder) holder).tv_totalComments.setVisibility(View.VISIBLE);
                ((ImageTypeViewHolder) holder).tv_totalComments.setText("View All " + arrayList.get(position).getCount_comment() + " Comments");
            }

            ((ImageTypeViewHolder) holder).tv_totalLikes.setText(arrayList.get(position).getCountLike() + " " + "Likes");
            ((ImageTypeViewHolder) holder).tv_totalLikes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        if (arrayList.get(position).getCountLike()!=0){
                            Intent intent = new Intent(context, LikesTotalList.class);
                            intent.putExtra(Constants.POSTID, arrayList.get(position).getId());
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        }else{
                            Snackbar.make(((ImageTypeViewHolder) holder).imgView_Chat,context.getResources().getString(R.string.no_like_yet),Snackbar.LENGTH_LONG).show();
                        }

                    }else{
                        Snackbar.make(((ImageTypeViewHolder) holder).imgView_Chat,context.getResources().getString(R.string.please_login_to_see_likes),Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            ((ImageTypeViewHolder) holder).imgView_Chat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                    Log.e("userId=", userId + "  " + arrayList.get(position).getUserId());
                    if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                        if (!userId.equalsIgnoreCase(arrayList.get(position).getUserId())) {
                            Intent intent = new Intent(context, ChatActivity.class);
                            intent.putExtra(Constants.RECEIVER_ID, arrayList.get(position).getPost_user_id());
                            intent.putExtra(Constants.PROFILE_IMAGE, arrayList.get(position).getProfile_image());
                            intent.putExtra(Constants.FIRST_NAME, arrayList.get(position).getFirstName());
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                        } else {
                            context.startActivity(new Intent(context, ChatListActivity.class));

//                            Toast.makeText(context, context.getResources().getString(R.string.same_user), Toast.LENGTH_LONG).show();
                        }

                    }else{
                        Snackbar.make(((ImageTypeViewHolder) holder).imgView_Chat,context.getResources().getString(R.string.please_login_to_chat),Snackbar.LENGTH_LONG).show();

                    }
                }
            });

            ((ImageTypeViewHolder) holder).imgView_profile.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    i++;
                    Handler handler = new Handler();
                    Runnable r = new Runnable() {

                        @Override
                        public void run() {
                            i = 0;
                        }
                    };

                    if (i == 1) {
                        //Single click
                        handler.postDelayed(r, 250);
                        Log.e("handler=", "handler");
                    } else if (i == 2) {
                        //Double click
                        i = 0;
                        Log.e("doubleClick=", "Double Click");
                        //status 1 for like 2 for unlike mind it
                        if (NetworkUtil.checkNetworkStatus(context)) {
                            String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                            String status = arrayList.get(position).getIsLike().toString();
                            if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                                if (status.equalsIgnoreCase("1")) {
                                    ((ImageTypeViewHolder) holder).like_unlike.setVisibility(View.VISIBLE);
                                    handlerForLikeUnlikeShow(((ImageTypeViewHolder) holder));
                                    positionImage = position;
                                    RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "2");
                                } else {
                                    ((ImageTypeViewHolder) holder).like_unlike.setVisibility(View.VISIBLE);
                                    handlerForLikeUnlikeShow(((ImageTypeViewHolder) holder));
                                    RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "1");
                                    positionImage = position;
                                }

                            } else {
                                Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                            }


                        } else {
                            Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                        }

                    }


                }
            });

            this.holder = ((ImageTypeViewHolder) holder);
            ((ImageTypeViewHolder) holder).imgView_likeBlack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        String status = arrayList.get(position).getIsLike().toString();
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            if (status.equalsIgnoreCase("1")) {
                                positionImage = position;
                                ((ImageTypeViewHolder) holder).imgView_likeRed.setVisibility(View.GONE);
                                ((ImageTypeViewHolder) holder).imgView_likeBlack.setVisibility(View.VISIBLE);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "2");
                            } else {
                                positionImage = position;
                                ((ImageTypeViewHolder) holder).imgView_likeRed.setVisibility(View.VISIBLE);
                                ((ImageTypeViewHolder) holder).imgView_likeBlack.setVisibility(View.GONE);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "1");

                            }

                        } else {
                            Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }

                }
            });
            ((ImageTypeViewHolder) holder).imgView_likeRed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        String status = arrayList.get(position).getIsLike().toString();
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            if (status.equalsIgnoreCase("1")) {
                                positionImage = position;
                                ((ImageTypeViewHolder) holder).imgView_likeRed.setVisibility(View.GONE);
                                ((ImageTypeViewHolder) holder).imgView_likeBlack.setVisibility(View.VISIBLE);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "2");
                            } else {
                                positionImage = position;
                                ((ImageTypeViewHolder) holder).imgView_likeRed.setVisibility(View.VISIBLE);
                                ((ImageTypeViewHolder) holder).imgView_likeBlack.setVisibility(View.GONE);
                                RetrofitHelper.getInstance().getLikeProfileImage(profileLikeCallback, userId, arrayList.get(position).getId(), "1");

                            }

                        } else {
                            Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_like), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }
                }
            });
            ((ImageTypeViewHolder) holder).imgView_Comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            Intent intent = new Intent(context, CommentsActivity.class);
                            intent.putExtra(Constants.POSTID, arrayList.get(position).getId());
                            intent.putExtra("post_user_id", arrayList.get(position).getUserId());
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);

                        } else {
                            Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_comment), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }

                }
            });

            ((ImageTypeViewHolder) holder).tv_totalComments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        String userId = sharedPreferencesData.getSharedPreferenceData(Constants.USER_LOGIN_SHARED, Constants.ID);
                        if (sharedPreferencesData != null && !userId.equalsIgnoreCase("")) {
                            Intent intent = new Intent(context, CommentsActivity.class);
                            intent.putExtra(Constants.POSTID, arrayList.get(position).getId());
                            intent.putExtra("post_user_id", arrayList.get(position).getUserId());
                            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);

                        } else {
                            Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.please_login_to_comment), Snackbar.LENGTH_LONG).show();
                        }


                    } else {
                        Snackbar.make(((ImageTypeViewHolder) holder).imgView_likeBlack, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
                    }
                }
            });

            //here manage all audio mute unmute method and click listners

//            amanager=(AudioManager)context.getSystemService(Context.AUDIO_SERVICE);

//            if (arrayList.get(position).getType().equalsIgnoreCase("video")){
//                if (StatusMuteUnmute.muteunmuteStatus){
//                    ((ImageTypeViewHolder) holder).imgview_unmute.setVisibility(View.GONE);
//                    ((ImageTypeViewHolder) holder).imgview_mute.setVisibility(View.VISIBLE);
//                    Log.e("Mute=","Inside MuteVisisble");
//                }else{
//                    ((ImageTypeViewHolder) holder).imgview_unmute.setVisibility(View.VISIBLE);
//                    ((ImageTypeViewHolder) holder).imgview_mute.setVisibility(View.GONE);
//                    Log.e("Mute=","Inside UnmuteVisisble");
//                }

//            }else{
//                ((ImageTypeViewHolder) holder).imgview_mute.setVisibility(View.GONE);
//                ((ImageTypeViewHolder) holder).imgview_unmute.setVisibility(View.GONE);
//            }

//            ((ImageTypeViewHolder) holder).imgview_mute.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, false);
//                    StatusMuteUnmute.muteunmuteStatus=false;
//                    ((ImageTypeViewHolder) holder).imgview_unmute.setVisibility(View.VISIBLE);
//                    ((ImageTypeViewHolder) holder).imgview_mute.setVisibility(View.GONE);
//                    if (position==0){
//                        notifyItemRangeChanged(position+1,arrayList.size());
//                    }else{
//                        notifyItemRangeChanged(0,position);
//                        notifyItemRangeChanged(position+1,arrayList.size());
//                    }
//                }
//            });


//            ((ImageTypeViewHolder) holder).imgview_unmute.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    amanager.setStreamMute(AudioManager.STREAM_MUSIC, true);
//                    StatusMuteUnmute.muteunmuteStatus=true;
//                    ((ImageTypeViewHolder) holder).imgview_unmute.setVisibility(View.GONE);
//                    ((ImageTypeViewHolder) holder).imgview_mute.setVisibility(View.VISIBLE);
//                    if (position==0){
//                        notifyItemRangeChanged(position+1,arrayList.size());
//                    }else {
//                        notifyItemRangeChanged(0,position);
//                        notifyItemRangeChanged(position+1,arrayList.size());
//                    }
//                }
//            });


        }
    }

    public void handlerForLikeUnlikeShow(ImageTypeViewHolder holder) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                holder.like_unlike.setVisibility(View.GONE);


            }
        }, LIKE_DISPLAY_LENGTH);
    }

    public void handlerForLikeUnlikeShow(SimplePlayerViewHolder holder) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                holder.like_unlike.setVisibility(View.GONE);
            }
        }, LIKE_DISPLAY_LENGTH);
    }


    Callback<LikesMain> profileLikeCallback = new Callback<LikesMain>() {

        @Override
        public void onResponse(Call<LikesMain> call, Response<LikesMain> response) {

            if (response.isSuccessful()) {
                if (response.body().getStatus().equalsIgnoreCase("1")) {
//                    holder.imgView_likeRed.setVisibility(View.VISIBLE);
//                    holder.imgView_likeBlack.setVisibility(View.GONE);
                    HomeMainData homeMainData = new HomeMainData();
//                    homeMainData.setId(arrayList.get(positionImage).getId());
//                    homeMainData.setUserId(arrayList.get(positionImage).getUserId());
//                    homeMainData.setFirstName(arrayList.get(positionImage).getFirstName());
//                    homeMainData.setLastName(arrayList.get(positionImage).getLastName());
//                    homeMainData.setPostDate(arrayList.get(positionImage).getPostDate());
//                    homeMainData.setCountLike(arrayList.get(positionImage).getCountLike() + 1);
//                    homeMainData.setThumb(arrayList.get(positionImage).getThumb());
//                    homeMainData.setBig(arrayList.get(positionImage).getBig());
//                    homeMainData.setCount_comment(arrayList.get(positionImage).getCount_comment());
//                    homeMainData.setProfile_image(arrayList.get(positionImage).getProfile_image());
//                    homeMainData.setLocation(arrayList.get(positionImage).getLocation());
//                    homeMainData.setKeyword(arrayList.get(positionImage).getKeyword());
//                    homeMainData.setType(arrayList.get(positionImage).getType());
                    homeMainData = arrayList.get(positionImage);
                    homeMainData.setIsLike(1);
                    int likeCount = arrayList.get(positionImage).getCountLike()+1;
                    homeMainData.setCountLike(likeCount);


                    arrayList.set(positionImage, homeMainData);
//                    holder.tv_totalLikes.setText(arrayList.get(positionImage).getCountLike() + " " + "Likes");

                    notifyDataSetChanged();
//                    holder.like_unlike.setVisibility(View.GONE);

                } else {

//                    holder.imgView_likeRed.setVisibility(View.GONE);
//                    holder.imgView_likeBlack.setVisibility(View.VISIBLE);
                    HomeMainData homeMainData = new HomeMainData();
//                    homeMainData.setId(arrayList.get(positionImage).getId());
//                    homeMainData.setUserId(arrayList.get(positionImage).getUserId());
//                    homeMainData.setFirstName(arrayList.get(positionImage).getFirstName());
//                    homeMainData.setLastName(arrayList.get(positionImage).getLastName());
//                    homeMainData.setPostDate(arrayList.get(positionImage).getPostDate());
//                    homeMainData.setCountLike(arrayList.get(positionImage).getCountLike() - 1);
//                    homeMainData.setThumb(arrayList.get(positionImage).getThumb());
//                    homeMainData.setBig(arrayList.get(positionImage).getBig());
//                    homeMainData.setCount_comment(arrayList.get(positionImage).getCount_comment());
//                    homeMainData.setCount_comment(arrayList.get(positionImage).getCount_comment());
//                    homeMainData.setProfile_image(arrayList.get(positionImage).getProfile_image());
//                    homeMainData.setLocation(arrayList.get(positionImage).getLocation());
//                    homeMainData.setKeyword(arrayList.get(positionImage).getKeyword());
//                    homeMainData.setType(arrayList.get(positionImage).getType());
                    homeMainData = arrayList.get(positionImage);
                    homeMainData.setIsLike(2);
                    int likeCount = arrayList.get(positionImage).getCountLike()-1;
                    homeMainData.setCountLike(likeCount);


                    arrayList.set(positionImage, homeMainData);
//                    holder.tv_totalLikes.setText(arrayList.get(positionImage).getCountLike() + " " + "Likes");
                    notifyDataSetChanged();
//                    holder.like_unlike.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public void onFailure(Call<LikesMain> call, Throwable t) {
            // holder.like_unlike.setVisibility(View.GONE);
        }
    };


    @Override public int getItemCount() {
        return arrayList.size();
    }

    public static class ImageTypeViewHolder extends RecyclerView.ViewHolder {


//        ImageView imgview_unmute;
//        ImageView imgview_mute;
        ImageView imgView_Chat;
        ImageView imgView_Comment;
        ImageView like_unlike;
        ImageView imgView_profile;
        ImageView imgView_likeRed;
        ImageView imgView_likeBlack;
        TextView tv_userName;
        TextView tv_totalComments;
        TextView tv_totalLikes;
        TextView tv_Address;
        TextView postTime;
        TextView commentsCountTv;
        TextView likesCountTv;
        TextView nameTv;
        EmojiTextView hastagTv;
        EmojiTextView tv_hastags;
        ImageView circularImageView_Profile;
        ProgressBar progressBarHome;
        ProgressBar progressBarImages;
        SurfaceView surfaceView;

        public ImageTypeViewHolder(View itemView) {
            super(itemView);
            progressBarImages =  itemView.findViewById(R.id.progressBarImages);
            progressBarHome =  itemView.findViewById(R.id.progressBarHome);
            tv_hastags = itemView.findViewById(R.id.tv_hastags);
            tv_Address = itemView.findViewById(R.id.tv_Address);
            imgView_Chat = itemView.findViewById(R.id.imgView_Chat);
            tv_totalComments = itemView.findViewById(R.id.tv_totalComments);
            imgView_Comment = itemView.findViewById(R.id.imgView_Comment);
            tv_totalLikes = itemView.findViewById(R.id.tv_totalLikes);
            like_unlike = itemView.findViewById(R.id.like_unlike);
            postTime = itemView.findViewById(R.id.postTime);
            likesCountTv = itemView.findViewById(R.id.likesCountTv);
            commentsCountTv = itemView.findViewById(R.id.commentsCountTv);
            imgView_likeRed = itemView.findViewById(R.id.imgView_likeRed);
            imgView_likeBlack = itemView.findViewById(R.id.imgView_likeBlack);
            imgView_profile = itemView.findViewById(R.id.imgView_profile);
            tv_userName = itemView.findViewById(R.id.tv_userName);
            nameTv = itemView.findViewById(R.id.nameTv);
            hastagTv = itemView.findViewById(R.id.hastagTv);
            circularImageView_Profile = itemView.findViewById(R.id.circularImageView_Profile);
//            imgview_mute = itemView.findViewById(R.id.imgview_mute);
//            imgview_unmute = itemView.findViewById(R.id.imgview_unmute);
        }

    }


    private void singleTextView1(EmojiTextView textView, final String keywordString, final ArrayList<String> atTheList) {

        SpannableStringBuilder spanText = new SpannableStringBuilder();
        spanText.append(keywordString);
        for (int i=0;i<atTheList.size();i++){
            spanText.append(atTheList.get(i)+" ");

            int finalI = i;
            spanText.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        hastagKey = atTheList.get(finalI).replace("#", "");
                        RetrofitHelper.getInstance().getHastagIdByKey(hastagCallback, atTheList.get(finalI).replace("#", ""));
                    }
//                    Toast.makeText(context, ""+atTheList.get(finalI).replace("#", ""), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void updateDrawState(TextPaint textPaint) {
//                    textPaint.setColor(textPaint.linkColor);    // you can use custom color
                    textPaint.setUnderlineText(false);    // this remove the underline
                }
            },spanText.length() - atTheList.get(i).length(), spanText.length(), 0);

            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setText(spanText, TextView.BufferType.SPANNABLE);

        }

    }

    private void singleTextView(EmojiTextView textView, final String keywordString, final ArrayList<String> atTheList) {

        SpannableStringBuilder spanText = new SpannableStringBuilder();
        spanText.append(keywordString);
        for (int i=0;i<atTheList.size();i++){
            spanText.append(atTheList.get(i)+" ");

            int finalI = i;
            spanText.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View widget) {
                    if (NetworkUtil.checkNetworkStatus(context)) {
                        RetrofitHelper.getInstance().getIdByKey(callback, atTheList.get(finalI).replace("@", ""));
                    }
//                    Toast.makeText(context, ""+atTheList.get(finalI).replace("@", ""), Toast.LENGTH_SHORT).show();
                }
                @Override
                public void updateDrawState(TextPaint textPaint) {
                    textPaint.setColor(textPaint.linkColor);    // you can use custom color
                    textPaint.setUnderlineText(false);    // this remove the underline
                }
            },spanText.length() - atTheList.get(i).length(), spanText.length(), 0);

            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setText(spanText, TextView.BufferType.SPANNABLE);
        }
    }


    Callback<GetTagIdSuccess> hastagCallback = new Callback<GetTagIdSuccess>() {
        @Override
        public void onResponse(Call<GetTagIdSuccess> call, Response<GetTagIdSuccess> response) {
            try {
                if (response.body().getSuccess() == 1){
                    Intent intent=new Intent(context, ImagesListByTagAndName.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.TAG_ID, response.body().getId());
                    intent.putExtra(Constants.TAG_NAME, hastagKey);
                    context.startActivity(intent);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<GetTagIdSuccess> call, Throwable t) {

        }
    };


    Callback<AnotherUserProfileIdByKeySuccess> callback = new Callback<AnotherUserProfileIdByKeySuccess>() {
        @Override
        public void onResponse(Call<AnotherUserProfileIdByKeySuccess> call, Response<AnotherUserProfileIdByKeySuccess> response) {
            try {
                if (response.body().getSuccess().equalsIgnoreCase("1")){
                    Intent intent = new Intent(context, AnotherUserProfile.class);
                    intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra(Constants.ANOTHER_USER_ID, response.body().getUser_data());
                    context.startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(Call<AnotherUserProfileIdByKeySuccess> call, Throwable t) {

        }
    };

}
