package com.example.touch.video_pk;

import android.net.Uri;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.emoji.widget.EmojiTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.touch.R;
import com.google.android.exoplayer2.ui.PlayerView;

import org.jetbrains.annotations.NotNull;

import im.ene.toro.ToroPlayer;
import im.ene.toro.ToroUtil;
import im.ene.toro.exoplayer.ExoPlayerViewHelper;
import im.ene.toro.media.PlaybackInfo;
import im.ene.toro.widget.Container;

public class SimplePlayerViewHolder extends RecyclerView.ViewHolder implements ToroPlayer {

    PlayerView playerView;

    private ExoPlayerViewHelper helper;
    private Uri mediaUri;
    ImageView imgview_unmute;
    ImageView imgview_mute;
    ImageView imgView_Chat;
    TextView chatTv;
    ImageView imgView_Comment;
    ImageView like_unlike;
    ImageView imgView_profile;
    ImageView imgView_likeRed;
    ImageView imgView_likeBlack;
    ImageView fullScreenImg;
    TextView tv_userName;
    TextView tv_totalComments;
    TextView tv_totalLikes;
    TextView tv_Address;
    TextView postTime;
    EmojiTextView tv_hastags;
    ProgressBar progressBarHome;
    ProgressBar progressBarImages;
    SurfaceView surfaceView;
    ImageView circularImageView_Profile;
    TextView commentsCountTv;
    TextView likesCountTv;
    TextView nameTv;
    EmojiTextView hastagTv;
//    RelativeLayout relativeLL;



    SimplePlayerViewHolder(@NonNull View itemView) {
        super(itemView);
        playerView =  (PlayerView) itemView.findViewById(R.id.player1);
        progressBarImages =  itemView.findViewById(R.id.progressBarImages);
        progressBarHome =  itemView.findViewById(R.id.progressBarHome);
        tv_hastags = itemView.findViewById(R.id.tv_hastags);
        tv_Address = itemView.findViewById(R.id.tv_Address);
        imgView_Chat = itemView.findViewById(R.id.imgView_Chat);
        chatTv = itemView.findViewById(R.id.chatTv);
        tv_totalComments = itemView.findViewById(R.id.tv_totalComments);
        imgView_Comment = itemView.findViewById(R.id.imgView_Comment);
        tv_totalLikes = itemView.findViewById(R.id.tv_totalLikes);
        postTime = itemView.findViewById(R.id.postTime);
        like_unlike = itemView.findViewById(R.id.like_unlike);
        imgView_likeRed = itemView.findViewById(R.id.imgView_likeRed);
        imgView_likeBlack = itemView.findViewById(R.id.imgView_likeBlack);
        fullScreenImg = itemView.findViewById(R.id.fullScreenImg);
        imgView_profile = itemView.findViewById(R.id.imgView_profile);
        tv_userName = itemView.findViewById(R.id.tv_userName);
        imgview_mute = itemView.findViewById(R.id.imgview_mute);
        imgview_unmute = itemView.findViewById(R.id.imgview_unmute);
        circularImageView_Profile = itemView.findViewById(R.id.circularImageView_Profile);
        likesCountTv = itemView.findViewById(R.id.likesCountTv);
        commentsCountTv = itemView.findViewById(R.id.commentsCountTv);
        nameTv = itemView.findViewById(R.id.nameTv);
        hastagTv = itemView.findViewById(R.id.hastagTv);
//        relativeLL = itemView.findViewById(R.id.relativeLL);

    }



    @NotNull
    @Override public View getPlayerView() {
        return this.playerView;
    }

    @NotNull
    @Override public PlaybackInfo getCurrentPlaybackInfo() {
        return helper != null ? helper.getLatestPlaybackInfo() : new PlaybackInfo();
    }

    @Override
    public void initialize(@NotNull Container container, @NotNull PlaybackInfo playbackInfo) {
        if (helper == null) {
            helper = new ExoPlayerViewHelper(this,  mediaUri);
        }
        helper.initialize(container, playbackInfo);
    }

    @Override public void play() {

        try {
            if (helper != null) helper.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override public void pause() {
        if (helper != null) helper.pause();
    }

    @Override public boolean isPlaying() {
        return helper != null && helper.isPlaying();
    }

    @Override public void release() {
        if (helper != null) {
            helper.release();
            helper = null;
        }
    }

    @Override public boolean wantsToPlay() {
        return ToroUtil.visibleAreaOffset(this, itemView.getParent()) >= 0.85;
    }

    @Override public int getPlayerOrder() {
        return getAdapterPosition();

    }



    public void bind(Uri media) {
        this.mediaUri = media;
    }
}
